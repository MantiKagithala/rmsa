/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithOutLogin.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.aponline.rmsa.database.DataBasePlugin;
import com.aponline.rmsa.commons.CommonConstants;
/**
 *
 * @author 1250892
 */
public class RtaActDao {

    public ArrayList getRtaDetails() {

        String sql = null;
        Map rtaMap = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList rtaDetails = new ArrayList();
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            sql = "select * from RMSA_RtiAct where Status='Active' order by CreatedDate desc";
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    rtaMap = new HashMap();
                    rtaMap.put("rowId", rs.getString(1));
                    rtaMap.put("distId", rs.getString(2));
                    rtaMap.put("distName", rs.getString(3));
                    rtaMap.put("typeOfAct", rs.getString(4));
                    rtaMap.put("ProvisionsOfAct", rs.getString(5));
                    rtaMap.put("UploadAct", rs.getString(6));
                    rtaMap.put("CreatedDate", rs.getString(7));
                    rtaMap.put("UpdatedDate", rs.getString(8));
                    rtaMap.put("Status", rs.getString(9));
                    rtaMap.put("DepartmentId", rs.getString(10));
                    rtaDetails.add(rtaMap);
                    rtaMap = null;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return rtaDetails;
    }

}