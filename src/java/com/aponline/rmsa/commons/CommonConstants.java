/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.commons;

/**
 *
 * @author 1250892
 */
public class CommonConstants {
    
    
    public static final String StateOrAdminRole = "51";
    public static final String ApwidcRole = "52";
    public static final String DistrictRole = "3";
    public static final String SchoolRole = "22";

    public static final String DBCONNECTION = "PENSIONAP";
    public static final String DOCUMENTSPATH = "MDMDOCUMENTS//";
    public static final String DISTRICTPATH = "DISTRICT//";
    public static final String PROCEEDINGSPATH = "PROCEEDINGS//";
    public static final String ACQUITTANCEPDF = "Acquittance.pdf";
    public static final String ACQUITTANCE = "ACQUITTANCE//";
    public static final String TEMPORARYACQUITTANCE = "TEMPORARYACQUITTANCE//";
    public static final String ACQUITTANCEURBANPDF = "Urban";
    public static final String ACQUITTANCERURALPDF = "Rural";
    public static final String ACQUITTANCESPHOTOS = "ACQUITTANCESPHOTOS//";
    public static final String BHARAHFONTFILENAME = "brhtel.ttf";
    public static final String BHARAHRNFONTFILENAME = "brhtelrn.ttf";
    public static final String PROCEEDINGSPRINTPDF = "Proceedingprt.pdf";
    public static final String ABSTRACT = "Abstract.pdf";
    public static final String OFFICENOTEPRINT = "Officenoteprt.pdf";
    public static final String SMARTABSTRACT = "SmartAbstract.pdf";
    public static final String PROCEEDINGSPDF = "Proceeding.pdf";
    public static final String OFFICENOTE = "Officenote.pdf";
 
    
  
    public static final String PDFWEBPATH = "PdfWebPath\\";
    public static final String FOLDERNAMEPATH = "\\Department\\SChoolEducation";
    public static final String KEYCONTACTSWEBPATH = "KeyContacts\\";
    
   
    public static final String first = "1";
    public static final String sixth = "6";
    public static final String second = "2";
    public static final String third = "3";
    public static final String fourth = "4";
    public static final String fifth = "5";
    public static final String seventh = "7";
   
   
    public static final String RTIACT = "RTI\\";
    public static final String UPLOADGOS = "Gos\\";
    public static final String PRESSRELEASES = "PRESSRELEASES\\";
    public static final String NEWSCLIPPINGS = "NEWSCLIPPINGS\\";
    public static final String NEWSEVENTS = "NEWSEVENTS\\";
    public static final String PHOTOGALLERY = "PHOTOGALLERY\\";
    public static final String DISTRICTSDB_lIVE = "10.100.100.29";
    public static final String DISTRICTSDB_lIVE_PWD = "sa@123";
    public static final String DISTRICTSDB_LOCAL = "10.100.100.29";
    public static final String DISTRICTSDB_LOCAL_PWD = "sa@123";
    public static final String KEYCONTACTS = "KEYCONTACTS\\";
    public static final String UPLOADS = "UPLOADS//";
    public static final String MEETINGS = "METINGS\\";
    public static final String AWARDS = "AWARDS\\";
    public static final int Admin = 1;
    public static final String CIRCULARS_MEMOS_PROCEEDINGS = "CMP\\";
    public static final String DOWNLOAD_DOCUMENTS = "DOWNLOADDOCUMENTS\\";
    public static final String WORKSHOPS = "WORKSHOPS\\";
    public static final String BLOG = "BLOG\\";

  
    public static final String ATTENDANCE_QUERY1="[schoolattendance]";
    public static final String MDM_MANAGEMENTS="'10','11','16','17','18','19','31','33','34','35','36','37','48','50','52','53','54','62','97','98','22'";
    
    public static String SGF_Path = "Department\\SChoolEducation\\SGF\\";
     public static String RMSA_Path = "Department\\SChoolEducation\\RMSA\\";
    
}
