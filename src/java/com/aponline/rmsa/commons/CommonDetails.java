/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.commons;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Inet4Address;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 747577
 */
public class CommonDetails {

    public static String getDrivePath() {
        String drivepath = null;
        String eDrive = "E://";
        String dDrive = "D://";
        try {
            File fileDrive = new File(dDrive);
            if (fileDrive.exists()) {
                drivepath = dDrive;
            } else {
                drivepath = eDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    public static String getsystemIp() {
        String systemIp = null;
        try {
            systemIp = Inet4Address.getLocalHost().getHostAddress();
        } catch (Exception e) {
        }
        return systemIp;
    }

    public static String getCurrentDate() {
        String currentDate = "";
        try {
            Format formatter1 = new SimpleDateFormat("d-MMM-y");
            currentDate = formatter1.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentDate;
    }

    public static String getNextMonth() {
        String nextMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUl", "AUG", "SEP", "OCT", "NOV", "DEC"};
            String[] b = date1.split("-");
            int NextMonthVal = Integer.parseInt(b[1]) + 1;
            nextMonth = a[NextMonthVal - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nextMonth;
    }

    public static String getCurrentMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getPriviousMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUl", "AUG", "SEP", "OCT", "NOV", "DEC"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 2];
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }
    public static String getCurrentYear() {
        Integer currentYear = 0;
        
        try {
            currentYear = Calendar.getInstance().get(Calendar.YEAR);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentYear.toString();
    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = CommonDetails.getDrivePath() + filePath + "\\" + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            out.close();
        }
        return fileDownloadStatus;
    }

    public static boolean viewFiles(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String filePath = null;
        String fileName = null;
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (request.getParameter("filePath") != null && request.getParameter("fileName") != null) {
                filePath = request.getParameter("filePath");
                fileName = request.getParameter("fileName");
                filePath = CommonDetails.getDrivePath() + filePath + "\\" + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }
        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            out.close();
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
        }
    }

      public static String getFinancialYear(int month, int year) {
        Calendar cal = Calendar.getInstance();
        String yearsList;
        Map yrList = null;
        int finalYear = 0;
        if (month == 1 || month == 2 || month == 3) {
            finalYear = year;
        } else {
            finalYear = year + 1;
        }


        yearsList = String.valueOf(finalYear - 1) + "-" + String.valueOf(finalYear).substring(0, 4);


        return yearsList;
    }
      
       public static String getCurrentMonthInIntegerFormART() {
        String currentMonth = "";
        try {
            Integer currentMonthvalue = Calendar.getInstance().get(Calendar.MONTH);
            if (currentMonthvalue.toString().length() == 1) {
                currentMonth = "0" + currentMonthvalue;
            } else {
                currentMonth = currentMonthvalue.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;

    }
       
       
    

    

    public static String getCurrentDateForEfms() {
        String currentDate = "";
        try {
            Format formatter1 = new SimpleDateFormat("dd/MM/YYYY");
            currentDate = formatter1.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentDate;
    }


    public static String getCurrentMonthInIntegerFormForEFMS() {
        String currentMonth = "";
        try {
            Integer currentMonthvalue = Calendar.getInstance().get(Calendar.MONTH);
            if (currentMonthvalue.toString().length() == 1) {
                currentMonth = "0" + currentMonthvalue;
            } else {
                currentMonth = currentMonthvalue.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getCurrentDateForEfmsFiles() {
        String currentDate = "";
        try {
            Format formatter1 = new SimpleDateFormat("ddMMYYYY");
            currentDate = formatter1.format(new Date());
        } catch (Exception e) {
        }
        return currentDate;
    }

    public static String getCurrentFullMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getFullMonth(int monthId) {
        String Month = "";
        try {

            String[] a = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            int currentMonthVal = monthId;
            Month = a[currentMonthVal - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Month;
    }

   

    public String calculateMonthFirstDate(int month) {

        String currentMonth = "";
        String currentyear = "";
        String required_date = "";
        Format formatter = new SimpleDateFormat("d-MM-y");
        String date1 = formatter.format(new Date());
        String[] a = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
        String[] b = date1.split("-");
        int currentMonthVal = Integer.parseInt(b[1]);
        if (currentMonthVal < 7) {
            currentyear = String.valueOf(Integer.parseInt(b[2]) - 1);
        } else {
            currentyear = b[2];
        }
        String previousmonth = a[currentMonthVal - month];

        required_date = previousmonth + " " + currentyear;


        return required_date;
    }

    

    public static String getPriviousFullMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 2];

        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getPrivious2FullMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 3];

        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getPrivious3FullMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 4];

        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getPrivious4FullMonth() {
        String currentMonth = "";
        try {
            Format formatter = new SimpleDateFormat("d-MM-y");
            String date1 = formatter.format(new Date());
            String[] a = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            String[] b = date1.split("-");
            int currentMonthVal = Integer.parseInt(b[1]);
            currentMonth = a[currentMonthVal - 5];

        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;
    }

    public static String getCurrentMonthInIntegerForm() {
        String currentMonth = "";
        try {
            Integer currentMonthvalue = Calendar.getInstance().get(Calendar.MONTH) + 1;
            if (currentMonthvalue.toString().length() == 1) {
                currentMonth = "0" + currentMonthvalue;
            } else {
                currentMonth = currentMonthvalue.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth;

    }

   

    public static Integer getCurrentDay() {
        Integer currentDay = 0;
        try {
            currentDay = Calendar.getInstance().get(Calendar.DATE);
        } catch (Exception e) {
        }
        return currentDay;
    }

 

   

 

    public static ArrayList getYears() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        ArrayList yearsList = new ArrayList();
        Map yrList = null;
        for (int i = 2015; i <= year; i++) {
            yrList = new HashMap();
            yrList.put("year", i);
            yearsList.add(yrList);
        }
        return yearsList;
    }
/////////////////////// kalyani -->////////////////////////
    public static ArrayList getMonthsWithID() {
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH);
        ArrayList Month = new ArrayList();
        ArrayList totalMonth = new ArrayList();
        Map totalMnthList = null;
        totalMnthList = new HashMap();
        totalMnthList.put("id", "01");
        totalMnthList.put("month", "January");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "02");
        totalMnthList.put("month", "February");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "03");
        totalMnthList.put("month", "March");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "04");
        totalMnthList.put("month", "April");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "05");
        totalMnthList.put("month", "May");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "06");
        totalMnthList.put("month", "June");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "07");
        totalMnthList.put("month", "July");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "08");
        totalMnthList.put("month", "August");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "09");
        totalMnthList.put("month", "September");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "10");
        totalMnthList.put("month", "October");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "11");
        totalMnthList.put("month", "November");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "12");
        totalMnthList.put("month", "December");
        totalMonth.add(totalMnthList);

        Map mthList = null;
        if (currentYear == currentYear) {
            for (int i = 3; i <= currentMonth; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        } else {
            for (int i = 0; i <= 11; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        }
        return Month;
    }
    
    /////////////////////// kalyani -->////////////////////////
    
    
    
     public static ArrayList getMonthsWithID(int year) {
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH);
        ArrayList Month = new ArrayList();
        ArrayList totalMonth = new ArrayList();
        Map totalMnthList = null;
        totalMnthList = new HashMap();
        totalMnthList.put("id", "01");
        totalMnthList.put("month", "January");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "02");
        totalMnthList.put("month", "February");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "03");
        totalMnthList.put("month", "March");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "04");
        totalMnthList.put("month", "April");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "05");
        totalMnthList.put("month", "May");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "06");
        totalMnthList.put("month", "June");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "07");
        totalMnthList.put("month", "July");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "08");
        totalMnthList.put("month", "August");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "09");
        totalMnthList.put("month", "September");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "10");
        totalMnthList.put("month", "October");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "11");
        totalMnthList.put("month", "November");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "12");
        totalMnthList.put("month", "December");
        totalMonth.add(totalMnthList);

        Map mthList = null;
        if (year == currentYear) {
            for (int i = 0; i <= currentMonth; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        } else {
            for (int i = 0; i <= 11; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        }
        return Month;
    }
    
    
    
    
    
    
    

    public static String getNextMonthsIForSelected(int year) {
        String month = "";
        int y = year + 1;
        if (y < 10) {
            month = "0" + Integer.toString(y);
        } else {
            month = Integer.toString(y);
        }
        return month;

    }

    public static ArrayList getNextMonthsWithID(int year) {
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH);
        ArrayList Month = new ArrayList();
        ArrayList totalMonth = new ArrayList();
        Map totalMnthList = null;
        totalMnthList = new HashMap();
        totalMnthList.put("id", "01");
        totalMnthList.put("month", "January");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "02");
        totalMnthList.put("month", "February");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "03");
        totalMnthList.put("month", "March");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "04");
        totalMnthList.put("month", "April");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "05");
        totalMnthList.put("month", "May");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "06");
        totalMnthList.put("month", "June");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "07");
        totalMnthList.put("month", "July");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "08");
        totalMnthList.put("month", "August");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "09");
        totalMnthList.put("month", "September");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "10");
        totalMnthList.put("month", "October");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "11");
        totalMnthList.put("month", "November");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "12");
        totalMnthList.put("month", "December");
        totalMonth.add(totalMnthList);

        Map mthList = null;
        if (year == currentYear) {
            for (int i = 0; i <= currentMonth + 1; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        } else {
            for (int i = 0; i <= 11; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        }
        return Month;
    }

    public static ArrayList getMonths(int year) {
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH);
        ArrayList Month = new ArrayList();
        ArrayList totalMonth = new ArrayList();
        Map totalMnthList = null;
        totalMnthList = new HashMap();
        totalMnthList.put("id", "JAN");
        totalMnthList.put("month", "January");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "FEB");
        totalMnthList.put("month", "February");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "MAR");
        totalMnthList.put("month", "March");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "APR");
        totalMnthList.put("month", "April");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "MAY");
        totalMnthList.put("month", "May");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "JUN");
        totalMnthList.put("month", "June");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "JUL");
        totalMnthList.put("month", "July");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "AUG");
        totalMnthList.put("month", "August");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "SEP");
        totalMnthList.put("month", "September");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "OCT");
        totalMnthList.put("month", "October");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "NOV");
        totalMnthList.put("month", "November");
        totalMonth.add(totalMnthList);
        totalMnthList = new HashMap();
        totalMnthList.put("id", "DEC");
        totalMnthList.put("month", "December");
        totalMonth.add(totalMnthList);

        Map mthList = null;
        if (year == currentYear) {
            for (int i = 0; i <= currentMonth; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        } else {
            for (int i = 0; i <= 11; i++) {
                HashMap m = new HashMap();
                mthList = new HashMap();

                m = (HashMap) totalMonth.get(i);
                mthList.put("id", m.get("id").toString());
                mthList.put("month", m.get("month").toString());

                Month.add(mthList);
            }
        }
        return Month;
    }

  

    public static boolean getDashBoardstatusForProceedings() {
        boolean status = true;
        try {
            Calendar c = Calendar.getInstance();
            if (getCurrentDay() >= 28 && 28 <= c.getMaximum(Calendar.DAY_OF_MONTH) - 1) {
                status = true;
            } else {
                status = false;
            }
        } catch (Exception e) {
        }
        return status;
    }

    public static String getRequiredPreviousMonthFirstDate(int n) {
        Date date = null;
        SimpleDateFormat dt1 = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MMM/YYYY ");
            dt1 = new SimpleDateFormat("dd MMM YYYY");
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.add(Calendar.MONTH, -n);

            date = cal.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dt1.format(date);
    }

   
}
