/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.aponline.rmsa.login.servicefactory;

import com.aponline.rmsa.login.serviceimpl.LoginServiceImpl;


/**
 *
 * @author 747577
 */
public class LoginServiceFactory {

      public static LoginServiceImpl loginServiceImpl;

    public static LoginServiceImpl getLoginServiceImpl() {

        if (loginServiceImpl == null) {
            loginServiceImpl = new LoginServiceImpl();
        }
        return loginServiceImpl;
    }

}
