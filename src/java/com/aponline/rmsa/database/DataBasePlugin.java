package com.aponline.rmsa.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.sql.DataSource;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

public class DataBasePlugin implements PlugIn {

    static String pension = "PENSIONAP";
    static DataSource datasource;
    static ConnectionPool cp = null;

    public DataBasePlugin() {
        //    	initializing datasource
    }

    public void destroy() {
    }
    /* (non-Javadoc)
     * @see org.apache.struts.action.PlugIn#init(org.apache.struts.action.ActionServlet, org.apache.struts.config.ModuleConfig)
     */

    public void init(ActionServlet actionServlet, ModuleConfig modConfig) throws ServletException {
        ServletContext context = actionServlet.getServletContext();
        datasource = (DataSource) context.getAttribute("pension");

    }

    /**
     * Generalized function to get Connection
     */
    public static Connection getConnection(String user) {

        Connection con = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbcx.JtdsDataSource");
            if (user.equals(pension)) {
               ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/01hw477807/SIMS_120917", "sa", "sa@12345");
//              ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.100.102.82/SIMS", "sa", "sa@12345");
//              ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.100.102.85/SIMS", "sa", "sa@12345");

               con = ConnectionPool.getInstance().checkout();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }

    /**
     * Generalized function to get Connection
     */
    public static Connection getConnectionDistrict(String user, String dbName, String pwd) {

        Connection con = null;

        try {

            if (user != null && user.toString().length() > 0) {
                Class.forName("net.sourceforge.jtds.jdbcx.JtdsDataSource");
                con = DriverManager.getConnection("jdbc:jtds:sqlserver:/" + dbName + "/" + user, "sa", pwd);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }

    public synchronized static void closeAllConnections(Connection con, Statement st, PreparedStatement ps, ResultSet rs) {

        try {
           // cp.checkin(con);
            if (con != null) {
                con.close();
            }
            if (st != null) {
                st.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generalized function to close all connections
     */
    public synchronized static void close(Connection con) {

        try {
            //cp.checkin(con);
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generalized function to close all connections
     */
    public synchronized static void closeStatement(Statement st) {

        try {
            if (st != null) {
                st.close();
               // st = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void closeResulset(ResultSet rs) {

        try {
            if (rs != null) {
                rs.close();
               // rs = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void closeCallableStatement(CallableStatement cs) {

        try {
            if (cs != null) {
                cs.close();
               // cs = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * General Function to execute any select Query
     *
     * @return Single Dimension ArrayList
     */
    public synchronized static ArrayList<String> selectQueryList(String sql, String user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<String> matrix = new ArrayList<String>();
        try {
            con = getConnection(user);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            matrix = getRsSingleArrayList(rs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return matrix;
    }

    public static ArrayList<String> getRsSingleArrayList(ResultSet rs) {
        ArrayList<String> matrix = new ArrayList<String>();
        try {
            if (rs != null) {
                int colCount = getColCount(rs);
                while (rs.next()) {
                    for (int i = 1; i <= colCount; i++) {
                        matrix.add(rs.getString(i).trim());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matrix;
    }

    /**
     * function which returns No. of Columns Count in a ResultSet
     */
    public static int getColCount(ResultSet rs) {
        int rowCount = 0;
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            rowCount = rsmd.getColumnCount();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowCount;
    }

    public static int executeUpdate(String sql, String user) {               //Declare a method executeUpdate
        int insert = 0;
        Connection con = null;
        Statement st = null;

        try {
            //	con = dataSource.getConnection();
            con = getConnection(user);                          //connect the database
            st = con.createStatement();
            insert = st.executeUpdate(sql);
            //    DataBasePlugin.lastUpdatedDate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(con);
            closeStatement(st);

        }
        return insert;
    }

    /**
     * General Function to execute any select Query
     *
     * @return List for Maps
     */
    public static ArrayList<Map<String, Object>> selectQueryMap(String sql, String user) throws SQLException {
        Connection con = null;
        ResultSet rs=null;
        PreparedStatement ps =null;
        ArrayList<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        try {
            con = getConnection(user);
              ps = con.prepareStatement(sql);
              rs = ps.executeQuery();
            result = processResultSet(rs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try{
            if (rs != null) {
                    rs.close();
                }
            if (ps != null) {
                    ps.close();
                }
            if (con != null) {
                    con.close();
                }
            }
            catch (Exception e) {
            e.printStackTrace();
        } 
        }
        return result;
    }

    /* Funtions to convert ResultSet into ArrayList end*/
    /* Funtions to convert ResultSet into List of Maps Start*/
    /**
     * General Functions to convert ResultSet into List of Maps
     *
     * @return List
     */
    private static ArrayList<Map<String, Object>> processResultSet(ResultSet rs) throws SQLException {
        ArrayList<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        ResultSetMetaData rm = rs.getMetaData();
        int cols = rm.getColumnCount();
        String columnName = null;
        String columnValue = null;
        if (rs.next()) {
            result = new ArrayList<Map<String, Object>>();
            do {
                Map<String, Object> row = new HashMap<String, Object>(cols);
                for (int i = 1; i <= cols; i++) {
                    columnName = rm.getColumnName(i);

                    if (rs.getString(i) != null && rs.getString(i).equals("") && rs.getString(i).toString().length() == 0) {
                        columnValue = "-";
                    } else {
                        columnValue = rs.getString(i);
                    }
                    row.put(columnName, columnValue);
                }
                result.add(row);

            } while (rs.next());
        }
        return result;
    }

    /**
     * This method is for formating the selected Date into String
     *
     * @param selectedDate
     * @return String
     */
    public static String doDateFormat(String selectedDate) {
        String formatedDate = null;
        try {

            Date date = new SimpleDateFormat("dd/mm/yyyy").parse(selectedDate);
            formatedDate = new SimpleDateFormat("mm/dd/yyyy").format(date);

        } catch (ParseException p) {
            p.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formatedDate;
    }

    /**
     * Method for checking the String weather the String is empty or not
     *
     * @param literal
     * @return
     */
    public static String emptyCheck(String literal) {
        String replaceString = null;

        if (literal.equals("") || literal.length() == 0) {
            replaceString = "-";
        } else {
            replaceString = literal;
        }

        return replaceString;
    }

    /**
     * This method is for last updated date of website
     *
     * @return
     * @throws TSMDCException
     */
    public static int lastUpdatedDate() throws Exception {
        int updatedDate = 0;
        Connection con = null;
        Statement st = null;
        String sql = null;
        try {
            con = getConnection("tsmdc");
            st = con.createStatement();

            sql = "update LastUpdatedDate set LastUpdatedDate=getDate()";
            updatedDate = st.executeUpdate(sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return updatedDate;
    }

    /**
     * This method is for last updated date of website
     *
     * @return
     * @throws Exception
     */
    public static String getLastUpdatedDate() throws Exception {
        String updatedDate = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = null;
        try {
            con = getConnection("tsmdc");
            st = con.createStatement();

            sql = "select convert(varchar,LastUpdatedDate,103) from LastUpdatedDate";
            rs = st.executeQuery(sql);
            if (rs != null && rs.next()) {
                updatedDate = rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                close(con);
                if (st != null) {
                    st.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return updatedDate;
    }

    /**
     * This method is for last updated date of website
     *
     * @return
     * @throws Exception
     */
    public static String getString(String sql, String user) throws Exception {
        String count = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            con = getConnection(user);
            st = con.createStatement();

            rs = st.executeQuery(sql);
            if (rs != null && rs.next()) {
                count = rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                close(con);
                if (st != null) {
                    st.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    /**
     * This method gives the Current Year
     *
     * @return String
     * @throws Exception
     */
    public static String getCurrentYear() throws Exception {
        String year = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            con = getConnection("PENSIONAP");
            st = con.createStatement();

            rs = st.executeQuery("select year(current_timestamp)");
            if (rs != null && rs.next()) {
                year = rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                close(con);
                if (st != null) {
                    st.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return year;
    }
}
