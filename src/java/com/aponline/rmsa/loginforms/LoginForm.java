/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.loginforms;

import org.apache.struts.action.ActionForm;

/**
 *
 * @author 484898
 */
public class LoginForm extends ActionForm {

    private String roleId;
    private String userName;
    private String password;
    private String transporter;
    private String color;
    private String mode;
    private String loginStatus;
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;
    private String loginRoles;
    private String captchaCode;
    private String newNormPassword;
    private String distCode;
    private int file_type;
    private String mandalCode;
    private String userNameNew;
    private String[] checkbox;
    
    private String currentPassword;
    private String updatePassword;
    private String currentPasswordEncreption;

    
    public String[] getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(String[] checkbox) {
        this.checkbox = checkbox;
    }

    public String getUserNameNew() {
        return userNameNew;
    }

    public void setUserNameNew(String userNameNew) {
        this.userNameNew = userNameNew;
    }

    public String getMandalCode() {
        return mandalCode;
    }

    public void setMandalCode(String mandalCode) {
        this.mandalCode = mandalCode;
    }

    public int getFile_type() {
        return file_type;
    }

    public void setFile_type(int file_type) {
        this.file_type = file_type;
    }
    
     
     

    public String getDistCode() {
        return distCode;
    }

    public void setDistCode(String distCode) {
        this.distCode = distCode;
    }
    

    public String getNewNormPassword() {
        return newNormPassword;
    }

    public void setNewNormPassword(String newNormPassword) {
        this.newNormPassword = newNormPassword;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getLoginRoles() {
        return loginRoles;
    }

    public void setLoginRoles(String loginRoles) {
        this.loginRoles = loginRoles;
    }

    public String getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(String updatePassword) {
        this.updatePassword = updatePassword;
    }

    public String getCurrentPasswordEncreption() {
        return currentPasswordEncreption;
    }

    public void setCurrentPasswordEncreption(String currentPasswordEncreption) {
        this.currentPasswordEncreption = currentPasswordEncreption;
    }
 
}
