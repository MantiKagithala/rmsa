/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.logindao;

//import apol.commons.CommonConstants;
//import apol.commons.CommonDetails;
//import apol.database.DataBasePlugin;
import com.aponline.rmsa.loginforms.LoginForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.commons.CommonDetails;
import com.aponline.rmsa.database.DataBasePlugin;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This Class is having all login related queries
 *
 * @author 1250892
 */
public class LoginDAO {

    /**
     * This method is validate weather the login name and passwords are same or
     * not.
     *
     * @param loginForm
     * @return int
     * @throws Exception
     */
    public int checkLoginDetails(LoginForm loginForm) throws Exception {
        int loginDetails = 0;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = null;
        String password = null;
        String dbPassword = null;
        String userEncPwd = null;
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();
            if (loginForm.getPassword() != null && loginForm.getPassword().length() > 0) {
//             userEncPwd = passwordEncrypt(loginForm.getPassword());
                password = loginForm.getPassword().substring(5, loginForm.getPassword().length() - 5);

            }
            dbPassword = getPassword(loginForm.getUserName());
            userEncPwd = passwordEncrypt(password);
            if (password != null && dbPassword != null && userEncPwd != null) {
                if (dbPassword != null && userEncPwd != null) {
                    if (userEncPwd.equals(dbPassword)) {
                        sql = "select count(*) from dbo.LoginDetails_RMSA where username='" + loginForm.getUserName() + "' "
                                + "and encryptedpassword = '" + userEncPwd + "' and Status ='Active'";
                        rs = st.executeQuery(sql);
                        if (rs != null && rs.next()) {
                            loginDetails = rs.getInt(1);
                        }
                        if (loginDetails == 0) {
//                        loginFailureDetails(loginForm);
                        }
                    } else {
                        loginDetails = 0;
//                    loginFailureDetails(loginForm);
                    }
                }
            } else {
                loginDetails = 0;
//                loginFailureDetails(loginForm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return loginDetails;
    }

    public Vector getUserServices(LoginForm loginForm, HttpServletRequest request) throws Exception {
        Vector services = new Vector();
        Connection con = null;
        ResultSet rs = null;
        Statement st = null;
        String serviceToUserSQL = null;
        HttpSession session = null;
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();
            session = request.getSession(true);
            serviceToUserSQL = "select serviceid,parentid,targeturl,servicename,priority from services_RMSA "
                    + "where serviceid in (select service_id from roles_services_RMSA where roleid = '" + session.getAttribute("RoleId") + "' and status='Active') "
                    + " and status='Active' and (AccessFlag is null or AccessFlag in ('CC')) order by parentid,priority";
            rs = st.executeQuery(serviceToUserSQL);
            if (rs != null) {
                while (rs.next()) {
                    String servicedesc[] = new String[5];
                    servicedesc[0] = rs.getString(1);
                    servicedesc[1] = rs.getString(2);
                    servicedesc[2] = rs.getString(3);
                    servicedesc[3] = rs.getString(4);
                    services.addElement(servicedesc);
                }

            }

        } catch (NullPointerException n) {
            throw new Exception("Problem for getting the Services");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return services;
    }

    public ArrayList getUserInfo(LoginForm loginForm, HttpServletRequest request) throws Exception  {
        ArrayList loginDetails = new ArrayList();
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        HttpSession session = null;
        String dbPassword = null;
        try {
            session = request.getSession(true);
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();

            dbPassword = getPassword(loginForm.getUserName());

            rs = st.executeQuery("select RoleId,username,EmployeeName,empid,departmentid,District_id,ModelSchoolDisrtictCode from loginDetails_RMSA where username='" + loginForm.getUserName() + "' "
                    + "and encryptedPassword='" + dbPassword + "' and status='Active'");
            if (rs != null) {
                while (rs.next()) {
                    loginDetails = new ArrayList();
                    loginDetails.add(rs.getString(1));
                    session.setAttribute("RoleId", rs.getString(1));
                    session.setAttribute("userName", rs.getString(2));
                    session.setAttribute("EmployeeName", rs.getString(3));
                    session.setAttribute("empid", rs.getString(4));
                    session.setAttribute("departmentid", rs.getString(5));
                    session.setAttribute("districtId", rs.getString(6));
                    session.setAttribute("ModelSchoolDisrtictCode", rs.getString(7));                  
                    session.setAttribute("color", loginForm.getColor());
                }
            }

        } catch (NullPointerException n) {
            throw new Exception("iven inputs captured as NULLs");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return loginDetails;
    }

    public synchronized int checkUserAttempts(LoginForm loginForm) throws Exception {

        Connection con = null;
        ResultSet rs = null;
        Statement st = null;
        int attemptCount = 0;
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();

            rs = st.executeQuery("select count(*) from LogDetails where "
                    + "dateadd(dd,0, datediff(dd,0,logindate))=dateadd(dd,0, datediff(dd,0,getDate())) and  "
                    + "datediff(mi,logindate,getDate())<=3 and username='" + loginForm.getUserName() + "' and status='Active'");
            if (rs != null && rs.next()) {
                attemptCount = rs.getInt(1);
            }


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return attemptCount;
    }

    /**
     * This method is track the website login failure details
     *
     * @param loginForm
     * @return
     * @throws Exception
     */
    public synchronized int loginFailureDetails(LoginForm loginForm) throws Exception {

        Connection con = null;

        String query = null;
        String password = null;
        int count = 0;
        ResultSet rs = null;
        Statement st = null;
        int attemptCount = 0;
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();
            rs = st.executeQuery("select coalesce(max(count),0)+1  from LogDetails where UserName='" + loginForm.getUserName() + "' "
                    + "and EncryptPassword='" + loginForm.getPassword() + "' and "
                    + "dateadd(dd,0, datediff(dd,0,logindate))=dateadd(dd,0, datediff(dd,0,getDate())-3)");
            if (rs != null && rs.next()) {
                attemptCount = rs.getInt(1);
            }

            query = "INSERT INTO LogDetails(UserName,Password,EncryptPassword,loginDate,Count)"
                    + " VALUES ('" + loginForm.getUserName() + "','" + password + "','" + loginForm.getPassword() + "',getDate()"
                    + ",'" + attemptCount + "')";

            count = st.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    /**
     * This method is user for blocking the user account if the user attempts
     * more then 3 times.
     *
     * @param loginForm
     * @return blockUserStatus
     * @throws Exception
     */
    public synchronized int blockUserAccount(LoginForm loginForm) throws Exception {

        Connection con = null;
        ResultSet rs = null;
        Statement st = null;
        int blockUserStatus = 0;
        try {

            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();

            blockUserStatus = st.executeUpdate("update loginDetails set status='InActive' where username='" + loginForm.getUserName() + "' "
                    + "and status='Active'");

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return blockUserStatus;
    }

    /**
     * This method is for track the website login information
     *
     * @param loginForm
     * @param request
     * @throws Exception
     */
    public synchronized void insertAccessInformation(LoginForm loginForm, HttpServletRequest request) throws Exception {

        if (loginForm.getUserName() == null) {
            loginForm.setUserName("Guest");
            loginForm.setLoginStatus("Guest");
        }
        DataBasePlugin.executeUpdate("insert into WebsiteAccessReport(Accessname,Accessdatetime,Accessipaddess,"
                + "AccessLoginStatus) values('" + loginForm.getUserName() + "',getDate(),'" + request.getRemoteAddr() + "','"
                + loginForm.getLoginStatus() + "');", CommonConstants.DBCONNECTION);
    }

    /**
     * This mehtod is for update the lost login date of user
     *
     * @param loginForm
     * @throws Exception
     */
    public void lastLoginDate(String userName) throws Exception {

        DataBasePlugin.executeUpdate("update logindetails_RMSA set LastLoginDate=getDate() where userName='" + userName + "'", CommonConstants.DBCONNECTION);
    }

    /**
     * This method is for changing the password
     *
     * @param loginForm
     * @return
     * @throws Exception
     */
    public String changePassword(LoginForm loginForm) throws Exception {
        String changePassword = null;
        String pwd = null;
        int status = 0;
        String OldPwd = null;
        String OldUserEncPwd = null;

        String enteredNewPwd = null;
        String encryptedNewPwd = null;

        String enteredEncPwd = null;
        String encryptedEncPwd = null;

        try {

            if (loginForm.getOldPassword() != null) {

                // For Validating the Old password
                OldPwd = loginForm.getOldPassword().substring(5, loginForm.getOldPassword().length() - 5); // Getting Encrypted Old Password
                OldUserEncPwd = passwordEncrypt(OldPwd); // encrypt user entered Old Password
                pwd = getPasswordForChange(loginForm.getUserName(), OldUserEncPwd);
                // End of Old Password

                if (pwd != null && pwd.equals(OldUserEncPwd)) {
                    if (loginForm.getNewPassword() != null && loginForm.getConfirmPassword() != null) {

                        // Converting the new password to encrypted password
                        enteredNewPwd = loginForm.getNewPassword().substring(5, loginForm.getNewPassword().length() - 5);
                        encryptedNewPwd = passwordEncrypt(enteredNewPwd);

                        // Converting the password to encrypted password
                        enteredEncPwd = loginForm.getConfirmPassword().substring(5, loginForm.getConfirmPassword().length() - 5);
                        encryptedEncPwd = passwordEncrypt(enteredEncPwd);

                        if (encryptedNewPwd.equals(encryptedEncPwd)) {
                            status = DataBasePlugin.executeUpdate("update logindetails set encryptedPassword='" + encryptedEncPwd + "' where userName='" + loginForm.getUserName() + "' and status='Active'", CommonConstants.DBCONNECTION);
                            if (status != 0) {
                                changePassword = "<font color=\"green\">Password Changed Successfully</font>";
                            } else {
                                changePassword = "<font color=\"red\">Error in Change Password</font>";
                            }
                        } else {
                            changePassword = "<font color=\"red\">New Password and Confirm Password are Not Match</font>";
                        }
                    } else {
                        changePassword = "<font color=\"red\">Error in Change Password</font>";
                    }
                } else {
                    changePassword = "<font color=\"red\">Incorrect Old Password!! Please Enter Correct Password</font>";
                }
            } else {
                changePassword = "<font color=\"red\">Error in Change Password</font>";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return changePassword;
    }

    /**
     * This methos is for getting the password for change password
     *
     * @param logPassword
     * @return
     * @throws Exception
     */
    public String getPasswordForChange(String userName, String password) throws Exception {
        String pwd = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = null;
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            st = con.createStatement();
            sql = "select encryptedPassword from logindetails_RMSA where userName='" + userName + "' and encryptedPassword='" + password + "' and status='Active'";
            rs = st.executeQuery(sql);
            if (rs != null && rs.next()) {
                pwd = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pwd;
    }

    /**
     * This method is for saving the encryptedDetails,and update password in
     * logindetails table
     *
     * @param userName
     * @param password
     * @return String
     * @throws Exception
     */
    public void saveEncryptedPassword(String password) throws Exception {

        DataBasePlugin.executeUpdate("INSERT INTO EncryptedLoginDetails (EncryptedPassword) VALUES('" + password + "')", CommonConstants.DBCONNECTION);
    }

    /**
     * This method is for checking the existing passwords
     *
     * @param userName
     * @param password
     * @return
     * @throws Exception
     */
    public int encryptPasswordCheck(String password) throws Exception {
        String count = null;
        count = DataBasePlugin.getString("select COUNT(*) from EncryptedLoginDetails where EncryptedPassword='" + password + "' and status='Active'", CommonConstants.DBCONNECTION);
        return Integer.parseInt(count);
    }

    /**
     * This method is for saving the encryptedDetails,and update password in
     * logindetails table
     *
     * @param userName
     * @param password
     * @return String
     * @throws Exception
     */
    public void saveEncryptedPasswordAuth(String userName, String password) throws Exception {

        DataBasePlugin.executeUpdate("INSERT INTO AuthDetails(userName,Token) VALUES('" + userName + "','" + password + "')", CommonConstants.DBCONNECTION);
    }

    /**
     * This method is for checking the existing passwords
     *
     * @param userName
     * @param password
     * @return
     * @throws Exception
     */
    public int encryptPasswordCheckAuth(String userName, String password) throws Exception {
        String count = null;
        count = DataBasePlugin.getString("select COUNT(*) from AuthDetails where userName='" + userName + "' and Password='" + password + "' and status='Active'", CommonConstants.DBCONNECTION);
        return Integer.parseInt(count);
    }

    /**
     * This method is for getting the encrypted password
     *
     * @param userName
     * @return
     * @throws Exception
     */
    public String getPassword(String userName) throws Exception {
        String password = null;
        password = DataBasePlugin.getString("select encryptedpassword from LoginDetails_RMSA where username='" + userName + "' and status='Active'", CommonConstants.DBCONNECTION);
        return password;
    }

    public String passwordEncrypt(String password) throws Exception, NoSuchAlgorithmException {
        String sr = null;
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] dataBytes = password.getBytes();
        md.update(dataBytes);
        byte[] mdbytes = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int j = 0; j < mdbytes.length; j++) {
            sb.append(Integer.toString((mdbytes[j] & 0xff) + 0x100, 16).substring(1));
        }
        sr = sb.toString();

        return sr;
    }

    public String getDepartmentId(String roleId, String userName) throws Exception {
        String departmentId = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = null;
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select departmentId from LoginDetails"
                    + " where roleid = '" + roleId + "' and USERNAME='" + userName + "' and Status ='Active'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    departmentId = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return departmentId;


    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    public ArrayList getDirectorDetails(LoginForm LoginForm) throws Exception, SQLException {
        ArrayList dirDetails = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt1 = null;
        ResultSet rs1 = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs2 = null;
        boolean checkfto = false;
        boolean checkftop = false;

        String query = "";
        String query1 = "";
        String query2 = "";
        Map map = null;

        String month = "";
        if (CommonDetails.getDashBoardstatusForProceedings()) {
            month = CommonDetails.getCurrentMonth();
        } else {
            month = CommonDetails.getPriviousMonth();
        }
        String year = CommonDetails.getCurrentYear();
        // String previousMonth = CommonDetails.getPriviousMonth();
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select distinct distcode,distname from dbo.districtmst";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            String districtId = null;
            while (rs.next()) {
                map = new HashMap();
                //map.put("previousMonth", previousMonth);
                districtId = rs.getString(1);
                map.put("district_id", rs.getString(1));
                map.put("district_name", rs.getString(2));
                map.put("month", month);
                map.put("year", year);
                //dirDetails.add(map);
                //getting proceeding details based on district id
                //endif
                //dirDetails.add(map);
                if (districtId != null) {
                    //query2 ="select distinct distcode,proceedingtype,uploadpath,filename from dbo.proceedingsdownloaddetails where distcode='"+map.get("distcode")+"' and month='"+map.get("month")+"' and year='"+map.get("year")+"'";
                    query2 = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('1','2','4','6') and distcode='" + districtId + "' and month='" + month + "' and year='" + year + "'";
                    pstmt2 = con.prepareStatement(query2);
                    rs2 = pstmt2.executeQuery();
                    while (rs2.next()) {
                        if (rs2.getString(1).equalsIgnoreCase("4")) {
                            map.put("disstatus", "<a style=\"color:blue\" href=\"login.do?mode=disFilesList&distcode=" + districtId + "&month=" + month + "&year=" + year + "&type=disb\" target=\"_blank\">" + "Completed" + "</a>");
                        } else {
                            map.put("disstatus", "Not&nbsp;Generated");
                        }


                    }//endwhile
                    query2 = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('3','5') and distcode='" + districtId + "' and month='" + map.get("month") + "' and year='" + map.get("year") + "'";
                    pstmt2 = con.prepareStatement(query2);
                    rs2 = pstmt2.executeQuery();
                    while (rs2.next()) {
                        if (rs2.getString(1).equalsIgnoreCase("2")) {
                            map.put("printstatus", "<a style=\"color:blue\" href=\"login.do?mode=disFilesList&distcode=" + districtId + "&month=" + month + "&year=" + year + "&type=prnt\" target=\"_blank\">" + "Completed" + "</a>");
                        } else {
                            map.put("printstatus", "Not&nbsp;Generated");
                        }

                    }//endwhile

                    query2 = "select COUNT(1)  from dbo.proceedingsdownloaddetails where proceedingtype in ('7') and distcode='" + districtId + "' and month='" + month + "' and year='" + year + "'";
                    pstmt2 = con.prepareStatement(query2);
                    rs2 = pstmt2.executeQuery();
                    while (rs2.next()) {
                        if (Integer.parseInt(rs2.getString(1)) > 1) {
                            map.put("acqstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=Acquittance&distcode=" + districtId + "&month=" + month + "&year=" + year + "\" target=\"_blank\">" + "Available" + "</a>");
                        } else {
                            map.put("acqstatus", "Unavailable");
                        }
                    }//endwhile


                    String qry = "select COUNT(*) from efmsftrinformation where distcode='" + districtId + "' and  monthyear='" + CommonDetails.getCurrentYear() + CommonDetails.getCurrentMonthInIntegerForm() + "' and status=1 and paymenttype=1";
                    pstmt2 = con.prepareStatement(qry);
                    rs2 = pstmt2.executeQuery();
                    if (rs2 != null && rs2.next()) {
                        if (rs2.getString(1) != null && rs2.getString(1).equals("0")) {
                            checkfto = false;
                        } else {
                            checkfto = true;
                        }

                    }
                    String qry1 = "select COUNT(*) from efmsftrinformation where distcode='" + districtId + "' and  monthyear='" + CommonDetails.getCurrentYear() + CommonDetails.getCurrentMonthInIntegerForm() + "' and status=1 and paymenttype=2";
                    pstmt2 = con.prepareStatement(qry1);
                    rs2 = pstmt2.executeQuery();
                    if (rs2 != null && rs2.next()) {
                        if (rs2.getString(1) != null && rs2.getString(1).equals("0")) {
                            checkftop = false;
                        } else {
                            checkftop = true;
                        }

                    }
                    if (checkfto) {
                        map.put("ftodisbstatus", "<a style=\"color:blue\" href=\"login.do?mode=FTODetails&distcode=" + districtId + "&month=" + month + "&year=" + year + "\" target=\"_blank\">Pending&nbsp;for&nbsp;Director&nbsp;Approval</a>");

                    } else {
                        map.put("ftodisbstatus", "Sent To Bank");
                    }
                    if (checkftop) {

                        map.put("ftoprintstatus", "<a style=\"color:blue\" href=\"login.do?mode=FTOPrintingChargeDetails&distcode=" + districtId + "&month=" + month + "&year=" + year + "\" target=\"_blank\">Pending&nbsp;for&nbsp;Director&nbsp;Approval</a>");

                    } else {
                        map.put("ftoprintstatus", "Sent To Bank");
                    }


                    if (districtId != null) {
                        query1 = "select distinct distcode,monthyear from dbo.procedinginfo where distcode='" + districtId + "' ";
                        pstmt1 = con.prepareStatement(query1);
                        rs1 = pstmt1.executeQuery();
                        while (rs1.next()) {
                            map.put("distcode", rs1.getString(1));
                            map.put("monthyear", rs1.getString(2));
                            if (rs1.getString(2) != null) {
                                map.put("month", month);
                                map.put("year", CommonDetails.getCurrentYear());
                            }
                            query2 = "select file_type,PD_approvedate from dbo.procedinginfo where distcode='" + map.get("distcode") + "' and file_type=1";
                            pstmt2 = con.prepareStatement(query2);
                            rs2 = pstmt2.executeQuery();
                            while (rs2.next()) {
                                if (!rs2.getString(2).equalsIgnoreCase("")) {
                                    //<a href="login.do?mode=disFilesList&distcode=${list.distcode}&month=${list.month}&year=${list.year}&type=coldis&file_type=1" target="_blank">${list.colaprdisstatus}======</a> 
                                    map.put("colaprdisstatus", "<a style=\"color:blue\" href=\"login.do?mode=disFilesList&distcode=" + rs1.getString(1) + "&month=" + month + "&year=" + year + "&type=coldis&file_type=1\" target=\"_blank\">" + "Completed" + "</a>");
                                }
                            }//endwhile
                            query2 = "select file_type,PD_approvedate from dbo.procedinginfo where distcode='" + map.get("distcode") + "' and file_type=2";
                            pstmt2 = con.prepareStatement(query2);
                            rs2 = pstmt2.executeQuery();
                            while (rs2.next()) {
                                if (!rs2.getString(2).equalsIgnoreCase("")) {
                                    map.put("colaprprntstatus", " <a style=\"color:blue\" href=\"login.do?mode=disFilesList&distcode=" + rs1.getString(1) + "&month=" + month + "&year=" + year + "&type=coldis&file_type=2\" target=\"_blank\">" + "Completed" + "</a>");
                                }
                            }//endwhile

                        }
                    }


                    //endif
                }

                //endwhile
                if (map.get("ftodisbstatus") == null) {
                    map.put("ftodisbstatus", "Sent to Bank");
                }
                if (map.get("ftoprintstatus") == null) {
                    map.put("ftoprintstatus", "Sent To Bank");
                }
                if (map.get("printstatus") == null) {
                    map.put("printstatus", "Not&nbsp;Generated");
                }
                if (map.get("disstatus") == null) {
                    map.put("disstatus", "Not&nbsp;Generated");
                }
                if (map.get("colaprdisstatus") == null) {
                    map.put("colaprdisstatus", "Pending&nbsp;For&nbsp;Approval");
                }
                if (map.get("colaprprntstatus") == null) {
                    map.put("colaprprntstatus", "Pending&nbsp;For&nbsp;Approval");
                }
                if (map.get("acqstatus") == null) {
                    map.put("acqstatus", "Unavailable");
                }
                dirDetails.add(map);

                //endif



            }//endtry

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);
                DataBasePlugin.closeAllConnections(con, null, pstmt1, rs1);
                DataBasePlugin.closeAllConnections(con, null, pstmt2, rs2);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return dirDetails;

    }

    public ArrayList getPDdetails(LoginForm LoginForm) throws Exception, SQLException {

        ArrayList pdDetails = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        String query = "";
        String pdapprvdt1 = null;
        String pdapprvdt2 = null;
        int disstatuscount = -1;
        int printstatuscount = -1;
        int acqcount = -1;
        Map map = null;
        String month = "";
        if (CommonDetails.getDashBoardstatusForProceedings()) {
            month = CommonDetails.getCurrentMonth();
        } else {
            month = CommonDetails.getPriviousMonth();
        }
        String year = CommonDetails.getCurrentYear();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 0);
        //    String previousMonth = new SimpleDateFormat("MMMM").format(cal.getTime());
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('1','2','4','6') and distcode='" + LoginForm.getDistCode() + "' and month='" + month + "' and year='" + year + "'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            map = new HashMap();
            map.put("month", month);
            while (rs.next()) {
                disstatuscount = rs.getInt(1);
            }
            if (disstatuscount == 4) {
                map.put("disstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=NotefileDisbursement\" target=\"_blank\">Completed</a>");
            } else {
                map.put("disstatus", "Not&nbsp;Generated");
            }
            query = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('3','5') and distcode='" + LoginForm.getDistCode() + "' and month='" + month + "' and year='" + year + "'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                printstatuscount = rs.getInt(1);
            }
            if (printstatuscount == 2) {
                map.put("printstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=NotefilePrintingCharges\" target=\"_blank\">Completed</a>");
            } else {
                map.put("printstatus", "Not&nbsp;Generated");
            }
            query = "select PD_approvedate from dbo.procedinginfo where distcode='" + LoginForm.getDistCode() + "' and file_type=1";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                pdapprvdt1 = rs.getString(1);

            }
            if (pdapprvdt1 == null) {
                map.put("coldisbstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=CollectorsApprovalDisbursement\" target=\"_blank\">Pending&nbsp;For&nbsp;Approval</a>");
            } else {
                map.put("coldisbstatus", "Completed");
            }
            query = "select PD_approvedate from dbo.procedinginfo where distcode='" + LoginForm.getDistCode() + "' and file_type=2";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                pdapprvdt2 = rs.getString(1);

            }
            if (pdapprvdt2 == null) {
                map.put("colprintstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=CollectorsApprovalPrintingCharges\" target=\"_blank\">Pending&nbsp;For&nbsp;Approval</a>");
            } else {
                map.put("colprintstatus", "Completed");
            }
            query = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('7') and distcode='" + LoginForm.getDistCode() + "' and month='" + month + "' and year='" + year + "'";

            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                acqcount = rs.getInt(1);
            }
            if (acqcount > 0) {
                map.put("acqstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=Acquittance\" target=\"_blank\">Available</a>");
            } else {
                map.put("acqstatus", "UnAvailable");
            }
            cstmt = con.prepareCall("{CALL Sp_FTO_selection_director_new('" + LoginForm.getDistCode() + "')}");
            //cstmt.set
            rs = cstmt.executeQuery();
            if (rs == null) {
                map.put("ftodisbstatus", "<a style=\"color:blue\" href=\"notefileProceedingsDownload.do?mode=proceedingFile&type=FTODetailsDisbursement\" target=\"_blank\">Sent to Bank</a>");
            } else {
                map.put("ftodisbstatus", "Pending For Approval");
            }


            pdDetails.add(map);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);
                DataBasePlugin.closeCallableStatement(cstmt);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return pdDetails;

    }

    public ArrayList getMpdodetails(LoginForm loginForm) throws Exception, SQLException {

        ArrayList list = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        String query = "";
        int acqcount = -1;
        Map map = new HashMap();
        String month = "";
        if (CommonDetails.getDashBoardstatusForProceedings()) {
            month = CommonDetails.getCurrentMonth();
        } else {
            month = CommonDetails.getPriviousMonth();
        }
        String year = CommonDetails.getCurrentYear();
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('7') and distcode='" + loginForm.getDistCode() + "'"
                    + " and mandalcode='" + loginForm.getMandalCode() + "' and month='" + month + "' and year='" + year + "'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                acqcount = rs.getInt(1);
            }
            if (acqcount > 0) {
                map.put("acqstatus", "<a style=\"color:blue\" href =\"notefileProceedingsDownload.do?mode=downloadAcquittance&distId=" + loginForm.getDistCode() + "&mandalCode=" + loginForm.getMandalCode() + "\">\n"
                        + "                                     <img src=\"NBP/images/pdficon.png\" width=\"70px\" height=\"60px\"/></a>");
            } else {
                map.put("acqstatus", "UnAvailable");
            }
            list.add(map);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);
                DataBasePlugin.closeCallableStatement(cstmt);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return list;

    }

    public ArrayList getMpdodetailsSup(LoginForm loginForm) throws Exception, SQLException {

        ArrayList list = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        String query = "";
        int acqcount = -1;
        Map map = new HashMap();
        String month = "";
        if (CommonDetails.getDashBoardstatusForProceedings()) {
            month = CommonDetails.getCurrentMonth();
        } else {
            month = CommonDetails.getPriviousMonth();
        }
        String year = CommonDetails.getCurrentYear();
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('11') and distcode='" + loginForm.getDistCode() + "'"
                    + " and mandalcode='" + loginForm.getMandalCode() + "' and month='" + month + "' and year='" + year + "'";

            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                acqcount = rs.getInt(1);
            }
            if (acqcount > 0) {
                map.put("acqstatus", "<a style=\"color:blue\" href =\"notefileProceedingsDownload.do?mode=downloadAcquittance&distId=" + loginForm.getDistCode() + "&mandalCode=" + loginForm.getMandalCode() + "&sulppAcq=11\">\n"
                        + "                                     <img src=\"NBP/images/pdficon.png\" width=\"70px\" height=\"60px\"/></a>");
            } else {
                map.put("acqstatus", "UnAvailable");
            }
            list.add(map);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);
                DataBasePlugin.closeCallableStatement(cstmt);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return list;

    }

    public ArrayList getCollectorApprovalDetails(LoginForm LoginForm) throws Exception, SQLException {
        ArrayList list = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "";
        Map map = null;
        try {

            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select convert(varchar,pd_approvedate,103) from procedinginfo where distcode='" + LoginForm.getDistCode() + "' and file_type=" + LoginForm.getFile_type();
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                map = new HashMap();
                //String strDate = sdf.format(rs.getString(1).substring(0,10));
                map.put("coldisapprdate", rs.getString(1));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return list;
        }
    }

    public String getPasswordReset(LoginForm LoginForm) throws Exception, SQLException {
        String flag = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = null;
        String password = null;
        String dbPassword = null;
        String userEncPwd = null;
        try {
            password = LoginForm.getPassword().substring(5, LoginForm.getPassword().length() - 5);
            dbPassword = getPassword(LoginForm.getUserName());
            userEncPwd = passwordEncrypt(password);
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select LoginFlag from dbo.LoginDetails_RMSA where username='" + LoginForm.getUserName() + "'\n"
                    + "and EncryptedPassword='" + userEncPwd + "' and status='Active'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getString(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return flag;
        }
    }

    /**
     * This method is for changing the password
     *
     * @param loginForm
     * @return
     * @throws Exception
     */
    public String changePasswordForReset(LoginForm loginForm) throws Exception {
        String changePassword = null;
        String pwd = null;
        int status = 0;
        String OldPwd = null;
        String OldUserEncPwd = null;
        String enteredEncPwd = null;
        String encryptedEncPwd = null;
        Statement st = null;
        Connection con = null;
        String query = null;

        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);

            if (loginForm.getOldPassword() != null) {

                OldPwd = loginForm.getOldPassword().substring(5, loginForm.getOldPassword().length() - 5); // Getting Encrypted Old Password

                OldUserEncPwd = passwordEncrypt(OldPwd); // encrypt user entered Old Password
                pwd = getPasswordForChange(loginForm.getUserNameNew(), OldUserEncPwd);
                if (pwd != null && pwd.equals(OldUserEncPwd)) {
                    if (loginForm.getNewPassword() != null && loginForm.getConfirmPassword() != null) {

                        enteredEncPwd = loginForm.getConfirmPassword().substring(5, loginForm.getConfirmPassword().length() - 5);
                        encryptedEncPwd = passwordEncrypt(enteredEncPwd);


                        st = con.createStatement();
                        query = " update logindetails set  encryptedPassword='" + encryptedEncPwd + "' , LoginStatus='EN', password='" + loginForm.getNewPassword() + "', LastModifiedDate=getDate() "
                                + " where userName='" + loginForm.getUserNameNew() + "' and status='Active'";
                        status = st.executeUpdate(query);

                        if (status != 0) {
                            changePassword = "<font color=\"green\">Password Changed Successfully, Please login with your current Password</font>";
                        } else {
                            changePassword = "<font color=\"red\">Error in Change Password</font>";
                        }
                    } else {
                        changePassword = "<font color=\"red\">Error in Change Password</font>";
                    }
                } else {
                    changePassword = "<font color=\"red\">Incorrect Old Password!! Please Enter Correct Password</font>";
                }
            } else {
                changePassword = "<font color=\"red\">Error in Change Password</font>";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, st, null, null);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return changePassword;
    }

    public ArrayList getDashBoardCollection() throws Exception, SQLException {



        ArrayList list = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        CallableStatement cstmt = null;
        long totalLive = 0;
        long totalDisbursed = 0;
        long oapAh = 0;
        Map oapmap = new HashMap();
        Map widowmap = new HashMap();
        Map disabledmap = new HashMap();
        Map weaversmap = new HashMap();
        Map toddymap = new HashMap();
        Map artmap = new HashMap();
        try {
            Calendar cal = Calendar.getInstance();
            // cal.setTime(date);
            Integer month = cal.get(Calendar.MONTH) + 1;
            Integer year = Calendar.getInstance().get(Calendar.YEAR);
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            pstmt = con.prepareStatement("select * from tblcmdashboard");

            rs = pstmt.executeQuery();


            if (rs != null) {
                while (rs.next()) {
                    if ("O.A.P".equals(rs.getString(2))) {

                        oapmap.put("details", rs.getString(2));

                        oapmap.put("live", "" + rs.getString(3));
                        oapmap.put("releases", "" + rs.getString(3));
                        oapmap.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));
                        oapmap.put("totalLive", totalLive);
                        oapmap.put("totalReleased", totalLive);
                        oapmap.put("totalDisbursed", totalDisbursed);
                    } else if ("Weavers".equals(rs.getString(2))) {

                        weaversmap.put("details", rs.getString(2));

                        weaversmap.put("live", "" + rs.getString(3));
                        weaversmap.put("releases", "" + rs.getString(3));
                        weaversmap.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));
                        weaversmap.put("totalLive", totalLive);
                        weaversmap.put("totalReleased", totalLive);
                        weaversmap.put("totalDisbursed", totalDisbursed);
                    } else if ("Disabled".equals(rs.getString(2))) {

                        disabledmap.put("details", rs.getString(2));

                        disabledmap.put("live", "" + rs.getString(3));
                        disabledmap.put("releases", "" + rs.getString(3));
                        disabledmap.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));
                        disabledmap.put("totalLive", totalLive);
                        disabledmap.put("totalReleased", totalLive);
                        disabledmap.put("totalDisbursed", totalDisbursed);
                    } else if ("Widow".equals(rs.getString(2))) {

                        widowmap.put("details", rs.getString(2));

                        widowmap.put("live", "" + rs.getString(3));
                        widowmap.put("releases", "" + rs.getString(3));
                        widowmap.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));
                        widowmap.put("totalLive", totalLive);
                        widowmap.put("totalReleased", totalLive);
                        widowmap.put("totalDisbursed", totalDisbursed);
                    } else if ("Toddy Tappers".equals(rs.getString(2))) {

                        toddymap.put("details", rs.getString(2));

                        toddymap.put("live", "" + rs.getString(3));
                        toddymap.put("releases", "" + rs.getString(3));
                        toddymap.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));
                        toddymap.put("totalLive", totalLive);
                        toddymap.put("totalReleased", totalLive);
                        toddymap.put("totalDisbursed", totalDisbursed);
                    } else if ("Art Pensions".equals(rs.getString(2))) {

                        artmap.put("details", rs.getString(2));

                        artmap.put("live", "" + rs.getString(3));
                        artmap.put("releases", "" + rs.getString(3));
                        artmap.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));
                        artmap.put("totalLive", totalLive);
                        artmap.put("totalReleased", totalLive);
                        artmap.put("totalDisbursed", totalDisbursed);
                    }

                }
                list.add(oapmap);
                list.add(widowmap);
                list.add(disabledmap);
                list.add(weaversmap);
                list.add(toddymap);
                list.add(artmap);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return list;
        }
    }

    public ArrayList getOap() throws Exception, SQLException {
        ArrayList list = new ArrayList();
        Connection con = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;
        long totalLive = 0;
        long totalDisbursed = 0;
        Map map = null;
        try {
            Calendar cal = Calendar.getInstance();
            // cal.setTime(date);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            cstmt = con.prepareCall("{call CMDashboard(?,?)}");
            cstmt.setInt(1, month);
            cstmt.setInt(2, year);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();

                    if (rs.getString(1).equals("1") || rs.getString(1).equals("6")) {
                        map.put("details", rs.getString(2));
                        map.put("live", "" + rs.getString(3));
                        map.put("releases", "" + rs.getString(3));
                        map.put("disbursedthismonth", "" + rs.getString(6));
                        totalLive = totalLive + Long.parseLong(rs.getString(3));
                        totalDisbursed = totalDisbursed + Long.parseLong(rs.getString(6));

                        map.put("totalLive", totalLive);
                        map.put("totalReleased", totalLive);
                        map.put("totalDisbursed", totalDisbursed);
                        list.add(map);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, null, rs);
                DataBasePlugin.closeCallableStatement(cstmt);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return list;
        }



    }

    public ArrayList getUserNamePasswordDeailsDetails(LoginForm loginForm) throws Exception, SQLException {
        ArrayList list = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "";
        Map map = null;
        try {

            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            if (loginForm.getDistCode() != null && loginForm.getDistCode().length() == 2) {
                query = "select distname,mndname,username,password,a.status from LoginDetails a,districtmst b,mandalmst c where \n"
                        + "a.DistrictId=b.distcode and c.distcode=a.DistrictId and c.mndcode=a.MandalId\n"
                        + " and a.DistrictId='" + loginForm.getDistCode() + "'";
            } else {
                query = "select distname,mndname,username,password,a.status from LoginDetails a,districtmst b,mandalmst c where \n"
                        + "a.DistrictId=b.distcode and c.distcode=a.DistrictId and c.mndcode=a.MandalId\n"
                        + " ";
            }

            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                map = new HashMap();
                map.put("dname", rs.getString(1));
                map.put("mname", rs.getString(2));
                map.put("un", rs.getString(3));
                map.put("pwd", rs.getString(4));
                map.put("status", rs.getString(5));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return list;
        }
    }

    public ArrayList getIndividualProceedingDetails(LoginForm loginForm) throws Exception, SQLException {

        ArrayList list = new ArrayList();
        Connection con = null;
        PreparedStatement pstmt = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        String query = "";
        int acqcount = -1;
        Map map = new HashMap();
        String month = "";
        if (CommonDetails.getDashBoardstatusForProceedings()) {
            month = CommonDetails.getCurrentMonth();
        } else {
            month = CommonDetails.getPriviousMonth();
        }
        String year = CommonDetails.getCurrentYear();
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select COUNT(1) from dbo.proceedingsdownloaddetails where proceedingtype in ('18') and distcode='" + loginForm.getDistCode() + "'"
                    + " and mandalcode='" + loginForm.getMandalCode() + "' and month='" + month + "' and year='" + year + "'";

            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                acqcount = rs.getInt(1);
            }
            if (acqcount > 0) {
                map.put("acqstatus", "<a style=\"color:blue\" href =\"notefileProceedingsDownload.do?mode=downloadAcquittance&distId=" + loginForm.getDistCode() + "&mandalCode=" + loginForm.getMandalCode() + "&sulppAcq=18\">\n"
                        + "                                     <img src=\"NBP/images/pdficon.png\" width=\"70px\" height=\"60px\"/></a>");
            } else {
                map.put("acqstatus", "UnAvailable");
            }
            list.add(map);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DataBasePlugin.closeAllConnections(con, null, pstmt, rs);
                DataBasePlugin.closeCallableStatement(cstmt);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return list;

    }

    public int updatePassword(LoginForm myform) throws Exception {
        int myReturn = 0;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement st = null;
        PreparedStatement pstmt = null;
        try {
            String password = myform.getNewNormPassword().substring(5, myform.getNewNormPassword().length() - 5);
            String userEncPwd = passwordEncrypt(password);
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "update Logindetails set Password='" + myform.getConfirmPassword() + "',EncryptedPassword='" + userEncPwd + "' where UserName='" + myform.getUserName() + "'";
            pstmt = con.prepareStatement(query);
            myReturn = pstmt.executeUpdate();
            userEncPwd = null;
        } catch (Exception e) {
            myReturn = 100;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                myReturn = 200;
                e.printStackTrace();
            }
        }
        return myReturn;
    }
}
