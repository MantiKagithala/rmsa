package com.aponline.rmsa.WithLogin.Form;



import java.util.ArrayList;
import java.util.HashMap;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author 1250892
 */
public class RMSAForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private String expenditureIncurred;
    private String balanceAvailable;
    private String ucsSubmitted;
    private String[] ChequeNumber;
    private String[] ChequeDate;
    private String[] ChequeAmount;
    private String url;
    private String userName;
    private String delete;
    private ArrayList distList = new ArrayList();
    private String districtId;
    private String mandalId;
    private ArrayList mandalList = new ArrayList();
    private String villageId;
    private ArrayList villageList = new ArrayList();
    private String schoolId;
    private ArrayList schoolList = new ArrayList();
    private String artCraftSanction;
    private String artCraftStageOfProgress;
     private String artCraftStageOfProgress1;
    private String computerRoomSanction;
    private String computerRoomStageOfProgress;
    private String computerRoomStageOfProgress1;
    private String scienceLabSanction;
    private String scienceLabStageOfProgress;
    private String scienceLabStageOfProgress1;
    private String libraryRoomSanction;
    private String libraryRoomStageOfProgress;
    private String libraryRoomStageOfProgress1;
    private String additionalClassRoomSanction;
    private String additionalClassRoomStageOfProgress;
    private String additionalClassRoomStageOfProgress1;
    private String toiletBlockSanction;
    private String toiletBlockStageOfProgress;
    private String toiletBlockStageOfProgress1;
    private String drinkingWaterSanction;
    private String drinkingWaterStageOfProgress;
    private String drinkingWaterStageOfProgress1;
    private String labEquipmentSanction;
    private String labEquipmentStageOfProgress;
    private String furnitureSanction;
    private String furnitureStageOfProgress;
    private String administrativeSanction;
    private String furnitureAndLabEquipment;
    private String tenderCostValue;
    private String vat;
    private String departemntCharges;
    private String totalEligibleCost;
    private String totalFurniture;
    private String releases;
    private String balanceToBeReleased;
    private String estimatedCost;
    private FormFile ucsFileUpload;
    private String[] UCSAmount;
    private String ucsFilesCount;
    private String noofRec;
    private ArrayList formFile;
    private HashMap kpiinsert = new HashMap();
    private String schoolName;
    private String villageName;
    private String mandalName;
    private String districtName;
    private String phaseNo;
    private FormFile excelFile;
    private String excelFileType;
    private String releaseMasterDate;
    
    private String mgntId;
    private ArrayList mgntList = new ArrayList();
    
    private String reportType;
    
    
    //reshma
    private String year;
    private String totalSanctioned;
    private ArrayList phaseList = new ArrayList();
    private String labEquipment;
    private String[] expenditureIncurredDate;
    private String[] releasesRow;
    private Double release;
    
    
    //omkar 20180226
    private String checkedBoxesdp;
    private String sanctionedForCivilCost ;
    private String sanctionedForFurniture;
    private String sanctionofLabEquipment;
    private String totalRelease;
    private int checkedIndex;
    private String totalSanctioned1 ;
    private String phaseNumber;
    
    //omkar 2018112
    private ArrayList yearList = new ArrayList();

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
    
    public String getMgntId() {
        return mgntId;
    }

    public void setMgntId(String mgntId) {
        this.mgntId = mgntId;
    }

    public ArrayList getMgntList() {
        return mgntList;
    }

    public void setMgntList(ArrayList mgntList) {
        this.mgntList = mgntList;
    }
    
    
    public String getComputerRoomStageOfProgress1() {
        return computerRoomStageOfProgress1;
    }

    public void setComputerRoomStageOfProgress1(String computerRoomStageOfProgress1) {
        this.computerRoomStageOfProgress1 = computerRoomStageOfProgress1;
    }

    public String getScienceLabStageOfProgress1() {
        return scienceLabStageOfProgress1;
    }

    public void setScienceLabStageOfProgress1(String scienceLabStageOfProgress1) {
        this.scienceLabStageOfProgress1 = scienceLabStageOfProgress1;
    }

    public String getLibraryRoomStageOfProgress1() {
        return libraryRoomStageOfProgress1;
    }

    public void setLibraryRoomStageOfProgress1(String libraryRoomStageOfProgress1) {
        this.libraryRoomStageOfProgress1 = libraryRoomStageOfProgress1;
    }

    public String getAdditionalClassRoomStageOfProgress1() {
        return additionalClassRoomStageOfProgress1;
    }

    public void setAdditionalClassRoomStageOfProgress1(String additionalClassRoomStageOfProgress1) {
        this.additionalClassRoomStageOfProgress1 = additionalClassRoomStageOfProgress1;
    }

    public String getToiletBlockStageOfProgress1() {
        return toiletBlockStageOfProgress1;
    }

    public void setToiletBlockStageOfProgress1(String toiletBlockStageOfProgress1) {
        this.toiletBlockStageOfProgress1 = toiletBlockStageOfProgress1;
    }

    public String getDrinkingWaterStageOfProgress1() {
        return drinkingWaterStageOfProgress1;
    }

    public void setDrinkingWaterStageOfProgress1(String drinkingWaterStageOfProgress1) {
        this.drinkingWaterStageOfProgress1 = drinkingWaterStageOfProgress1;
    }
    
    public String getArtCraftStageOfProgress1() {
        return artCraftStageOfProgress1;
    }

    public void setArtCraftStageOfProgress1(String artCraftStageOfProgress1) {
        this.artCraftStageOfProgress1 = artCraftStageOfProgress1;
    }
    
    public String getLabEquipment() {
        return labEquipment;
    }

    public void setLabEquipment(String labEquipment) {
        this.labEquipment = labEquipment;
    }
    
    public ArrayList getPhaseList() {
        return phaseList;
    }

    public void setPhaseList(ArrayList phaseList) {
        this.phaseList = phaseList;
    }

    public String getPhaseNo() {
        return phaseNo;
    }

    public void setPhaseNo(String phaseNo) {
        this.phaseNo = phaseNo;
    }

    public ArrayList getDistList() {
        return distList;
    }

    public void setDistList(ArrayList distList) {
        this.distList = distList;
    }
    
    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getMandalName() {
        return mandalName;
    }

    public void setMandalName(String mandalName) {
        this.mandalName = mandalName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    
    public String getNoofRec() {
        return noofRec;
    }

    public void setNoofRec(String noofRec) {
        this.noofRec = noofRec;
    }

    public ArrayList getFormFile() {
        return formFile;
    }

    public void setFormFile(ArrayList formFile) {
        this.formFile = formFile;
    }

    public Object getKpiinsert(String key) {
        return kpiinsert.get(key);
    }

    public HashMap getKpiinsert() {
        return kpiinsert;
    }

    public void setKpiinsert(HashMap kpiinsert) {
        this.kpiinsert = kpiinsert;
    }

    public String getUcsFilesCount() {
        return ucsFilesCount;
    }

    public void setUcsFilesCount(String ucsFilesCount) {
        this.ucsFilesCount = ucsFilesCount;
    }

    public FormFile getUcsFileUpload() {
        return ucsFileUpload;
    }

    public void setUcsFileUpload(FormFile ucsFileUpload) {
        this.ucsFileUpload = ucsFileUpload;
    }

    public String[] getUCSAmount() {
        return UCSAmount;
    }

    public void setUCSAmount(String[] UCSAmount) {
        this.UCSAmount = UCSAmount;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getTotalFurniture() {
        return totalFurniture;
    }

    public void setTotalFurniture(String totalFurniture) {
        this.totalFurniture = totalFurniture;
    }

    public ArrayList getMandalList() {
        return mandalList;
    }

    public void setMandalList(ArrayList mandalList) {
        this.mandalList = mandalList;
    }

    public ArrayList getVillageList() {
        return villageList;
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public void setVillageList(ArrayList villageList) {
        this.villageList = villageList;
    }

    public ArrayList getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(ArrayList schoolList) {
        this.schoolList = schoolList;
    }
    
    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getMandalId() {
        return mandalId;
    }

    public void setMandalId(String mandalId) {
        this.mandalId = mandalId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String[] getChequeNumber() {
        return ChequeNumber;
    }

    public void setChequeNumber(String[] ChequeNumber) {
        this.ChequeNumber = ChequeNumber;
    }

    public String[] getChequeDate() {
        return ChequeDate;
    }

    public void setChequeDate(String[] ChequeDate) {
        this.ChequeDate = ChequeDate;
    }

    public String[] getChequeAmount() {
        return ChequeAmount;
    }

    public void setChequeAmount(String[] ChequeAmount) {
        this.ChequeAmount = ChequeAmount;
    }

    public String getArtCraftSanction() {
        return artCraftSanction;
    }

    public void setArtCraftSanction(String artCraftSanction) {
        this.artCraftSanction = artCraftSanction;
    }

    public String getArtCraftStageOfProgress() {
        return artCraftStageOfProgress;
    }

    public void setArtCraftStageOfProgress(String artCraftStageOfProgress) {
        this.artCraftStageOfProgress = artCraftStageOfProgress;
    }

    public String getComputerRoomSanction() {
        return computerRoomSanction;
    }

    public void setComputerRoomSanction(String computerRoomSanction) {
        this.computerRoomSanction = computerRoomSanction;
    }

    public String getComputerRoomStageOfProgress() {
        return computerRoomStageOfProgress;
    }

    public void setComputerRoomStageOfProgress(String computerRoomStageOfProgress) {
        this.computerRoomStageOfProgress = computerRoomStageOfProgress;
    }

    public String getScienceLabSanction() {
        return scienceLabSanction;
    }

    public void setScienceLabSanction(String scienceLabSanction) {
        this.scienceLabSanction = scienceLabSanction;
    }

    public String getScienceLabStageOfProgress() {
        return scienceLabStageOfProgress;
    }

    public void setScienceLabStageOfProgress(String scienceLabStageOfProgress) {
        this.scienceLabStageOfProgress = scienceLabStageOfProgress;
    }

    public String getLibraryRoomSanction() {
        return libraryRoomSanction;
    }

    public void setLibraryRoomSanction(String libraryRoomSanction) {
        this.libraryRoomSanction = libraryRoomSanction;
    }

    public String getLibraryRoomStageOfProgress() {
        return libraryRoomStageOfProgress;
    }

    public void setLibraryRoomStageOfProgress(String libraryRoomStageOfProgress) {
        this.libraryRoomStageOfProgress = libraryRoomStageOfProgress;
    }

    public String getAdditionalClassRoomSanction() {
        return additionalClassRoomSanction;
    }

    public void setAdditionalClassRoomSanction(String additionalClassRoomSanction) {
        this.additionalClassRoomSanction = additionalClassRoomSanction;
    }

    public String getAdditionalClassRoomStageOfProgress() {
        return additionalClassRoomStageOfProgress;
    }

    public void setAdditionalClassRoomStageOfProgress(String additionalClassRoomStageOfProgress) {
        this.additionalClassRoomStageOfProgress = additionalClassRoomStageOfProgress;
    }

    public String getToiletBlockSanction() {
        return toiletBlockSanction;
    }

    public void setToiletBlockSanction(String toiletBlockSanction) {
        this.toiletBlockSanction = toiletBlockSanction;
    }

    public String getToiletBlockStageOfProgress() {
        return toiletBlockStageOfProgress;
    }

    public void setToiletBlockStageOfProgress(String toiletBlockStageOfProgress) {
        this.toiletBlockStageOfProgress = toiletBlockStageOfProgress;
    }

    public String getDrinkingWaterSanction() {
        return drinkingWaterSanction;
    }

    public void setDrinkingWaterSanction(String drinkingWaterSanction) {
        this.drinkingWaterSanction = drinkingWaterSanction;
    }

    public String getDrinkingWaterStageOfProgress() {
        return drinkingWaterStageOfProgress;
    }

    public void setDrinkingWaterStageOfProgress(String drinkingWaterStageOfProgress) {
        this.drinkingWaterStageOfProgress = drinkingWaterStageOfProgress;
    }

    public String getLabEquipmentSanction() {
        return labEquipmentSanction;
    }

    public void setLabEquipmentSanction(String labEquipmentSanction) {
        this.labEquipmentSanction = labEquipmentSanction;
    }

    public String getLabEquipmentStageOfProgress() {
        return labEquipmentStageOfProgress;
    }

    public void setLabEquipmentStageOfProgress(String labEquipmentStageOfProgress) {
        this.labEquipmentStageOfProgress = labEquipmentStageOfProgress;
    }

    public String getFurnitureSanction() {
        return furnitureSanction;
    }

    public void setFurnitureSanction(String furnitureSanction) {
        this.furnitureSanction = furnitureSanction;
    }

    public String getFurnitureStageOfProgress() {
        return furnitureStageOfProgress;
    }

    public void setFurnitureStageOfProgress(String furnitureStageOfProgress) {
        this.furnitureStageOfProgress = furnitureStageOfProgress;
    }

    public String getAdministrativeSanction() {
        return administrativeSanction;
    }

    public void setAdministrativeSanction(String administrativeSanction) {
        this.administrativeSanction = administrativeSanction;
    }

    public String getFurnitureAndLabEquipment() {
        return furnitureAndLabEquipment;
    }

    public void setFurnitureAndLabEquipment(String furnitureAndLabEquipment) {
        this.furnitureAndLabEquipment = furnitureAndLabEquipment;
    }

    public String getTenderCostValue() {
        return tenderCostValue;
    }

    public void setTenderCostValue(String tenderCostValue) {
        this.tenderCostValue = tenderCostValue;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getDepartemntCharges() {
        return departemntCharges;
    }

    public void setDepartemntCharges(String departemntCharges) {
        this.departemntCharges = departemntCharges;
    }

    public String getTotalEligibleCost() {
        return totalEligibleCost;
    }

    public void setTotalEligibleCost(String totalEligibleCost) {
        this.totalEligibleCost = totalEligibleCost;
    }

    public String getReleases() {
        return releases;
    }

    public void setReleases(String releases) {
        this.releases = releases;
    }

    public String getBalanceToBeReleased() {
        return balanceToBeReleased;
    }

    public void setBalanceToBeReleased(String balanceToBeReleased) {
        this.balanceToBeReleased = balanceToBeReleased;
    }

    public String getExpenditureIncurred() {
        return expenditureIncurred;
    }

    public void setExpenditureIncurred(String expenditureIncurred) {
        this.expenditureIncurred = expenditureIncurred;
    }

    public String getBalanceAvailable() {
        return balanceAvailable;
    }

    public void setBalanceAvailable(String balanceAvailable) {
        this.balanceAvailable = balanceAvailable;
    }

    public String getUcsSubmitted() {
        return ucsSubmitted;
    }

    public void setUcsSubmitted(String ucsSubmitted) {
        this.ucsSubmitted = ucsSubmitted;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
    
    //reshma
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTotalSanctioned() {
        return totalSanctioned;
    }

    public void setTotalSanctioned(String totalSanctioned) {
        this.totalSanctioned = totalSanctioned;
    }

    public String[] getExpenditureIncurredDate() {
        return expenditureIncurredDate;
    }

    public void setExpenditureIncurredDate(String[] expenditureIncurredDate) {
        this.expenditureIncurredDate = expenditureIncurredDate;
    }

    public String[] getReleasesRow() {
        return releasesRow;
    }

    public void setReleasesRow(String[] releasesRow) {
        this.releasesRow = releasesRow;
    }

    public FormFile getExcelFile() {
        return excelFile;
    }

    public void setExcelFile(FormFile excelFile) {
        this.excelFile = excelFile;
    }

    public String getExcelFileType() {
        return excelFileType;
    }

    public void setExcelFileType(String excelFileType) {
        this.excelFileType = excelFileType;
    }

    public String getReleaseMasterDate() {
        return releaseMasterDate;
    }

    public void setReleaseMasterDate(String releaseMasterDate) {
        this.releaseMasterDate = releaseMasterDate;
    }

    public Double getRelease() {
        return release;
    }

    public void setRelease(Double release) {
        this.release = release;
    }

    public String getCheckedBoxesdp() {
        return checkedBoxesdp;
    }

    public void setCheckedBoxesdp(String checkedBoxesdp) {
        this.checkedBoxesdp = checkedBoxesdp;
    }

    public String getSanctionedForFurniture() {
        return sanctionedForFurniture;
    }

    public void setSanctionedForFurniture(String SanctionedForFurniture) {
        this.sanctionedForFurniture = SanctionedForFurniture;
    }

    public String getSanctionofLabEquipment() {
        return sanctionofLabEquipment;
    }

    public void setSanctionofLabEquipment(String SanctionofLabEquipment) {
        this.sanctionofLabEquipment = SanctionofLabEquipment;
    }

    public String getTotalRelease() {
        return totalRelease;
    }

    public void setTotalRelease(String TotalRelease) {
        this.totalRelease = TotalRelease;
    }

    public int getCheckedIndex() {
        return checkedIndex;
    }

    public void setCheckedIndex(int checkedIndex) {
        this.checkedIndex = checkedIndex;
    }

    public String getSanctionedForCivilCost() {
        return sanctionedForCivilCost;
    }

    public void setSanctionedForCivilCost(String sanctionedForCivilCost) {
        this.sanctionedForCivilCost = sanctionedForCivilCost;
    }

    public String getTotalSanctioned1() {
        return totalSanctioned1;
    }

    public void setTotalSanctioned1(String totalSanctioned1) {
        this.totalSanctioned1 = totalSanctioned1;
    }

    public ArrayList getYearList() {
        return yearList;
    }

    public void setYearList(ArrayList yearList) {
        this.yearList = yearList;
    }

    public String getPhaseNumber() {
        return phaseNumber;
    }

    public void setPhaseNumber(String phaseNumber) {
        this.phaseNumber = phaseNumber;
    }
    
    
    
    

 }
