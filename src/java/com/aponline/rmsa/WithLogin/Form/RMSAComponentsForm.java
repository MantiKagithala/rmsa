package com.aponline.rmsa.WithLogin.Form;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author 1075924
 */
public class RMSAComponentsForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private ArrayList schemelist = new ArrayList();
    private String scheme;
    private String nonrecuring;
    private String noOfRecords;
    private String statusFlag;
    private String noOfRecords1;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
    

    public String getNoOfRecords() {
        return noOfRecords;
    }

    public void setNoOfRecords(String noOfRecords) {
        this.noOfRecords = noOfRecords;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public ArrayList getSchemelist() {
        return schemelist;
    }

    public void setSchemelist(ArrayList schemelist) {
        this.schemelist = schemelist;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getNonrecuring() {
        return nonrecuring;
    }

    public void setNonrecuring(String nonrecuring) {
        this.nonrecuring = nonrecuring;
    }

    public String getNoOfRecords1() {
        return noOfRecords1;
    }

    public void setNoOfRecords1(String noOfRecords1) {
        this.noOfRecords1 = noOfRecords1;
    }
    private HashMap rmsainsert = new HashMap();

    public HashMap getRmsainsert() {
        return rmsainsert;
    }

    public void setRmsainsert(HashMap prafoma) {
        this.rmsainsert = rmsainsert;
    }

    public Object getRmsainsert(String key) {
        return rmsainsert.get(key);
    }

}
