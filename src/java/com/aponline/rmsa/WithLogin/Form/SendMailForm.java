/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;
import java.util.HashMap;

/**
 *
 * @author 1250892
 */
public class SendMailForm extends ActionForm {

    private String mode = null;
    private String sms = null;
    private String level = null;
    private String subject = null;
    private String body = null;
    private String count = null;
    private String systemIp = null;
    private String sendStatus = null;
    private String name = null;
    private String designation = null;
    private String mobileNumber = null;
    private String loginId = null;
    private String[] tahsildar = null;
    private String[] email = null;
    private String[] checkUrl = null;
    private String[] phoneNumber = null;
    private String[] dros = null;
    private String[] distoff = null;
    private String[] deptoff = null;
    private String department = null;
    private ArrayList tahasildarList = new ArrayList();
    private ArrayList districtofficersList = new ArrayList();
    private ArrayList depeofficersList = new ArrayList();
    private ArrayList departmentList = new ArrayList();
    private FormFile uploadFile1;
    private FormFile uploadFile2;
    private FormFile uploadFile3;
    private String officerType = null;
    private String officerPost = null;
    private String mandalDesignation = null;
    private String villageDesignation = null;
    private String radioButtonProperty = null;
    private HashMap<String, FormFile> hMap = new HashMap<String, FormFile>();
    private String HODOff;
    private String DEOOff;
    private String meoOff;
    private String rjdOff;
    private String allOff;
    private String hmOff;
    private String otherOff;
    private String districtId;
    private ArrayList distList = new ArrayList();
    private String officerTypeHoDs;
    private String officerTypeMEOs;
    private String officerTypeDEOs;
    private String officerTypeRJDs;
    private String officerTypeHMS;
    private String officerTypeALL;
    private String officerTypeOther;
    private String officerMobNum;
    private String officerName;
    private Integer[] smsCheck;
    private String message;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getSystemIp() {
        return systemIp;
    }

    public void setSystemIp(String systemIp) {
        this.systemIp = systemIp;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String[] getTahsildar() {
        return tahsildar;
    }

    public void setTahsildar(String[] tahsildar) {
        this.tahsildar = tahsildar;
    }

    public String[] getEmail() {
        return email;
    }

    public void setEmail(String[] email) {
        this.email = email;
    }

    public String[] getCheckUrl() {
        return checkUrl;
    }

    public void setCheckUrl(String[] checkUrl) {
        this.checkUrl = checkUrl;
    }

    public String[] getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String[] phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String[] getDros() {
        return dros;
    }

    public void setDros(String[] dros) {
        this.dros = dros;
    }

    public String[] getDistoff() {
        return distoff;
    }

    public void setDistoff(String[] distoff) {
        this.distoff = distoff;
    }

    public String[] getDeptoff() {
        return deptoff;
    }

    public void setDeptoff(String[] deptoff) {
        this.deptoff = deptoff;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ArrayList getTahasildarList() {
        return tahasildarList;
    }

    public void setTahasildarList(ArrayList tahasildarList) {
        this.tahasildarList = tahasildarList;
    }

    public ArrayList getDistrictofficersList() {
        return districtofficersList;
    }

    public void setDistrictofficersList(ArrayList districtofficersList) {
        this.districtofficersList = districtofficersList;
    }

    public ArrayList getDepeofficersList() {
        return depeofficersList;
    }

    public void setDepeofficersList(ArrayList depeofficersList) {
        this.depeofficersList = depeofficersList;
    }

    public ArrayList getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(ArrayList departmentList) {
        this.departmentList = departmentList;
    }

    public FormFile getUploadFile1() {
        return uploadFile1;
    }

    public void setUploadFile1(FormFile uploadFile1) {
        this.uploadFile1 = uploadFile1;
    }

    public FormFile getUploadFile2() {
        return uploadFile2;
    }

    public void setUploadFile2(FormFile uploadFile2) {
        this.uploadFile2 = uploadFile2;
    }

    public FormFile getUploadFile3() {
        return uploadFile3;
    }

    public void setUploadFile3(FormFile uploadFile3) {
        this.uploadFile3 = uploadFile3;
    }

    public String getOfficerType() {
        return officerType;
    }

    public void setOfficerType(String officerType) {
        this.officerType = officerType;
    }

    public String getOfficerPost() {
        return officerPost;
    }

    public void setOfficerPost(String officerPost) {
        this.officerPost = officerPost;
    }

    public String getMandalDesignation() {
        return mandalDesignation;
    }

    public void setMandalDesignation(String mandalDesignation) {
        this.mandalDesignation = mandalDesignation;
    }

    public String getVillageDesignation() {
        return villageDesignation;
    }

    public void setVillageDesignation(String villageDesignation) {
        this.villageDesignation = villageDesignation;
    }

    public String getRadioButtonProperty() {
        return radioButtonProperty;
    }

    public void setRadioButtonProperty(String radioButtonProperty) {
        this.radioButtonProperty = radioButtonProperty;
    }

    public HashMap<String, FormFile> gethMap() {
        return hMap;
    }

    public void sethMap(HashMap<String, FormFile> hMap) {
        this.hMap = hMap;
    }

    public String getHODOff() {
        return HODOff;
    }

    public void setHODOff(String HODOff) {
        this.HODOff = HODOff;
    }

    public String getDEOOff() {
        return DEOOff;
    }

    public void setDEOOff(String DEOOff) {
        this.DEOOff = DEOOff;
    }

    public String getMeoOff() {
        return meoOff;
    }

    public void setMeoOff(String meoOff) {
        this.meoOff = meoOff;
    }

    public String getRjdOff() {
        return rjdOff;
    }

    public void setRjdOff(String rjdOff) {
        this.rjdOff = rjdOff;
    }

    public String getAllOff() {
        return allOff;
    }

    public void setAllOff(String allOff) {
        this.allOff = allOff;
    }

    public String getHmOff() {
        return hmOff;
    }

    public void setHmOff(String hmOff) {
        this.hmOff = hmOff;
    }

    public String getOtherOff() {
        return otherOff;
    }

    public void setOtherOff(String otherOff) {
        this.otherOff = otherOff;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public ArrayList getDistList() {
        return distList;
    }

    public void setDistList(ArrayList distList) {
        this.distList = distList;
    }

    public String getOfficerTypeHoDs() {
        return officerTypeHoDs;
    }

    public void setOfficerTypeHoDs(String officerTypeHoDs) {
        this.officerTypeHoDs = officerTypeHoDs;
    }

    public String getOfficerTypeMEOs() {
        return officerTypeMEOs;
    }

    public void setOfficerTypeMEOs(String officerTypeMEOs) {
        this.officerTypeMEOs = officerTypeMEOs;
    }

    public String getOfficerTypeDEOs() {
        return officerTypeDEOs;
    }

    public void setOfficerTypeDEOs(String officerTypeDEOs) {
        this.officerTypeDEOs = officerTypeDEOs;
    }

    public String getOfficerTypeRJDs() {
        return officerTypeRJDs;
    }

    public void setOfficerTypeRJDs(String officerTypeRJDs) {
        this.officerTypeRJDs = officerTypeRJDs;
    }

    public String getOfficerTypeHMS() {
        return officerTypeHMS;
    }

    public void setOfficerTypeHMS(String officerTypeHMS) {
        this.officerTypeHMS = officerTypeHMS;
    }

    public String getOfficerTypeALL() {
        return officerTypeALL;
    }

    public void setOfficerTypeALL(String officerTypeALL) {
        this.officerTypeALL = officerTypeALL;
    }

    public String getOfficerTypeOther() {
        return officerTypeOther;
    }

    public void setOfficerTypeOther(String officerTypeOther) {
        this.officerTypeOther = officerTypeOther;
    }

    public String getOfficerMobNum() {
        return officerMobNum;
    }

    public void setOfficerMobNum(String officerMobNum) {
        this.officerMobNum = officerMobNum;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public Integer[] getSmsCheck() {
        return smsCheck;
    }

    public void setSmsCheck(Integer[] smsCheck) {
        this.smsCheck = smsCheck;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
