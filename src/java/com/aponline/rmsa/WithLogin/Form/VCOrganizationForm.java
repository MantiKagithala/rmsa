/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Form;

import java.util.ArrayList;

/**
 *
 * @author 1250881
 */
public class VCOrganizationForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private String[] vcCheckBox = null;
    private String vcTrainerIdValue;
    private String statusFlag;
    private String vcOrganizationId;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String[] getVcCheckBox() {
        return vcCheckBox;
    }

    public void setVcCheckBox(String[] vcCheckBox) {
        this.vcCheckBox = vcCheckBox;
    }

    public String getVcTrainerIdValue() {
        return vcTrainerIdValue;
    }

    public void setVcTrainerIdValue(String vcTrainerIdValue) {
        this.vcTrainerIdValue = vcTrainerIdValue;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public String getVcOrganizationId() {
        return vcOrganizationId;
    }

    public void setVcOrganizationId(String vcOrganizationId) {
        this.vcOrganizationId = vcOrganizationId;
    }
}
