/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Form;

import java.util.ArrayList;

/**
 *
 * @author 1250881
 */
public class VCHMConfirmationForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private String schoolId;
    private String vcTrainerId;
    private String vcTrainerName;
    private String sectorId;
    private String sectorName;
    private String tradeid;
    private String tradeName;
    private String firstQA;
    private String secondQA;
    private String thirdQA;
    private String fourthQA;
    private String fivthQA;
    private String sixthQA;
    private String seventhQA;
    private String eightQA;
    private String ninthQA;
    private String tenthQA;
    private String remarks;
    private String aadhar;
    private String mobileNo;
    private String emailId;
    private String dOB;
    private String acyear;
    private ArrayList vcList = new ArrayList();
    private String vcHMremarks;
    private String roleName;
    private String vcSrAstRema;
    private String vcSuptRemarks;
    private String vcAsstDirRemarks;
    private String vcDirRemarks;
    private String statusDirector;
    private String status;
     private String vcHMRemarksText;
      private String vTrainerId;
       private String[] vcCheckBox = null;
       
        private String vcHMRemarksValue;
         private String vcHMRemarksFeedbackValue;
         
         private String show;
         private String comments;
          private String vcRemarksValue;
      

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getVcTrainerId() {
        return vcTrainerId;
    }

    public void setVcTrainerId(String vcTrainerId) {
        this.vcTrainerId = vcTrainerId;
    }

    public String getVcTrainerName() {
        return vcTrainerName;
    }

    public void setVcTrainerName(String vcTrainerName) {
        this.vcTrainerName = vcTrainerName;
    }

    public String getSectorId() {
        return sectorId;
    }

    public void setSectorId(String sectorId) {
        this.sectorId = sectorId;
    }

    public String getSectorName() {
        return sectorName;
    }

    public void setSectorName(String sectorName) {
        this.sectorName = sectorName;
    }

    public String getTradeid() {
        return tradeid;
    }

    public void setTradeid(String tradeid) {
        this.tradeid = tradeid;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getFirstQA() {
        return firstQA;
    }

    public void setFirstQA(String firstQA) {
        this.firstQA = firstQA;
    }

    public String getSecondQA() {
        return secondQA;
    }

    public void setSecondQA(String secondQA) {
        this.secondQA = secondQA;
    }

    public String getThirdQA() {
        return thirdQA;
    }

    public void setThirdQA(String thirdQA) {
        this.thirdQA = thirdQA;
    }

    public String getFourthQA() {
        return fourthQA;
    }

    public void setFourthQA(String fourthQA) {
        this.fourthQA = fourthQA;
    }

    public String getFivthQA() {
        return fivthQA;
    }

    public void setFivthQA(String fivthQA) {
        this.fivthQA = fivthQA;
    }

    public String getSixthQA() {
        return sixthQA;
    }

    public void setSixthQA(String sixthQA) {
        this.sixthQA = sixthQA;
    }

    public String getSeventhQA() {
        return seventhQA;
    }

    public void setSeventhQA(String seventhQA) {
        this.seventhQA = seventhQA;
    }

    public String getEightQA() {
        return eightQA;
    }

    public void setEightQA(String eightQA) {
        this.eightQA = eightQA;
    }

    public String getNinthQA() {
        return ninthQA;
    }

    public void setNinthQA(String ninthQA) {
        this.ninthQA = ninthQA;
    }

    public String getTenthQA() {
        return tenthQA;
    }

    public void setTenthQA(String tenthQA) {
        this.tenthQA = tenthQA;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getdOB() {
        return dOB;
    }

    public void setdOB(String dOB) {
        this.dOB = dOB;
    }

    public String getAcyear() {
        return acyear;
    }

    public void setAcyear(String acyear) {
        this.acyear = acyear;
    }

    public ArrayList getVcList() {
        return vcList;
    }

    public void setVcList(ArrayList vcList) {
        this.vcList = vcList;
    }

    public String getVcHMremarks() {
        return vcHMremarks;
    }

    public void setVcHMremarks(String vcHMremarks) {
        this.vcHMremarks = vcHMremarks;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getVcSrAstRema() {
        return vcSrAstRema;
    }

    public void setVcSrAstRema(String vcSrAstRema) {
        this.vcSrAstRema = vcSrAstRema;
    }

    public String getVcSuptRemarks() {
        return vcSuptRemarks;
    }

    public void setVcSuptRemarks(String vcSuptRemarks) {
        this.vcSuptRemarks = vcSuptRemarks;
    }

    public String getVcAsstDirRemarks() {
        return vcAsstDirRemarks;
    }

    public void setVcAsstDirRemarks(String vcAsstDirRemarks) {
        this.vcAsstDirRemarks = vcAsstDirRemarks;
    }

    public String getVcDirRemarks() {
        return vcDirRemarks;
    }

    public void setVcDirRemarks(String vcDirRemarks) {
        this.vcDirRemarks = vcDirRemarks;
    }

    public String getStatusDirector() {
        return statusDirector;
    }

    public void setStatusDirector(String statusDirector) {
        this.statusDirector = statusDirector;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVcHMRemarksText() {
        return vcHMRemarksText;
    }

    public void setVcHMRemarksText(String vcHMRemarksText) {
        this.vcHMRemarksText = vcHMRemarksText;
    }

    public String getvTrainerId() {
        return vTrainerId;
    }

    public void setvTrainerId(String vTrainerId) {
        this.vTrainerId = vTrainerId;
    }

    public String[] getVcCheckBox() {
        return vcCheckBox;
    }

    public void setVcCheckBox(String[] vcCheckBox) {
        this.vcCheckBox = vcCheckBox;
    }

    public String getVcHMRemarksValue() {
        return vcHMRemarksValue;
    }

    public void setVcHMRemarksValue(String vcHMRemarksValue) {
        this.vcHMRemarksValue = vcHMRemarksValue;
    }

    public String getVcHMRemarksFeedbackValue() {
        return vcHMRemarksFeedbackValue;
    }

    public void setVcHMRemarksFeedbackValue(String vcHMRemarksFeedbackValue) {
        this.vcHMRemarksFeedbackValue = vcHMRemarksFeedbackValue;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getVcRemarksValue() {
        return vcRemarksValue;
    }

    public void setVcRemarksValue(String vcRemarksValue) {
        this.vcRemarksValue = vcRemarksValue;
    }

   
}
