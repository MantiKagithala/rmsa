/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Form;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author 1259084
 */
public class RMSAComponentForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private String sno;
    private String year;
    private String userName;
    private String schoolCode;
    private String schoolName;
    private String districtName;
    private String districtId;
    private String mandalName;
    private String mandalId;
    private String villageName;
    private String villageId;
    private String phaseNumber;
    
    private ArrayList yearList = new ArrayList();
    private ArrayList distList = new ArrayList();
    private ArrayList mandalList = new ArrayList();
    private ArrayList villageList = new ArrayList();
    private ArrayList schoolList = new ArrayList();
    private ArrayList phaseList = new ArrayList();
    
    private String bankName;
    private String bankAccountNumber;
    private String ifscCode;
    
    private String civilWorksReleased ;
    private String majorRepairsReleased;
    private String minorRepairsReleased;
    private String annualAndOtherReleased;
    private String toiletsReleased;
    private String selfDefenseGrantsReleased;
    private String totalCivilReleased;
    
    private String civilWorksSpent ;
    private String majorRepairsSpent;
    private String minorRepairsSpent;
    private String annualAndOtherSpent;
    private String toiletsSpent;
    private String selfDefenseGrantsSpent;
    private String totalCivilSpent;
    
    private String civilWorksAvailable ;
    private String majorRepairsAvailable;
    private String minorRepairsAvailable;
    private String annualAndOtherAvailable;
    private String toiletsAvailable;
    private String selfDefenseGrantsAvailable;
    private String totalCivilAvailable;
    
    private String waterElectricityTelephoneChargesReleased ;
    private String purchaseofBooksAndPeriodicalsAndNewsPapersReleased;
    private String minorRepairsAnnualReleased;
    private String sanitationAndICTReleased;
    private String needBasedWorksReleased;
    private String provisionalLaboratoryReleased;
    private String totalAnnualReleased;
    
    private String waterElectricityTelephoneChargesSpent;
    private String purchaseofBooksAndPeriodicalsAndNewsPapersSpent;
    private String minorRepairsAnnualSpent;
    private String sanitationAndICTSpent;
    private String needBasedWorksSpent;
    private String provisionalLaboratorySpent;
    private String totalAnnualSpent;
    
    private String waterElectricityTelephoneChargesAvailable;
    private String purchaseofBooksAndPeriodicalsAndNewsPapersAvailable;
    private String minorRepairsAnnualAvailable;
    private String sanitationAndICTAvailable;
    private String needBasedWorksAvailable;
    private String provisionalLaboratoryAvailable;
    private String totalAnnualAvailable;
    
    private String furnitureReleased ;
    private String labEquipmentReleased;
    private String totalfurnitureAndLabReleased;
    
    private String furnitureSpent;
    private String labEquipmentSpent;
    private String totalfurnitureAndLabSpent;
    
    private String furnitureAvailable;
    private String labEquipmentAvailable;
    private String totalfurnitureAndLabAvailable;
    
    private String recurringExcursionRelease;
    private String recurringExcursionSpent;
    private String recurringExcursionAvailable;
    
    private String recurringSelfDefenseRelease;
    private String recurringSelfDefenseSpent;
    private String recurringSelfDefenseAvailable;
    
    private String recurringOtherRelease;
    private String recurringOtherSpent;
    private String recurringOtherAvailable;
    
    private String recurringTotalRelease;
    private String recurringTotalSpent;
    private String recurringTotalAvailable;
    
    private String interestEarned;
    private String interestExpenditure;
    private String interestBalance;
    
    

    public String getProvisionalLaboratorySpent() {
        return provisionalLaboratorySpent;
    }

    public void setProvisionalLaboratorySpent(String provisionalLaboratorySpent) {
        this.provisionalLaboratorySpent = provisionalLaboratorySpent;
    }

    public String getProvisionalLaboratoryAvailable() {
        return provisionalLaboratoryAvailable;
    }

    public void setProvisionalLaboratoryAvailable(String provisionalLaboratoryAvailable) {
        this.provisionalLaboratoryAvailable = provisionalLaboratoryAvailable;
    }

    public String getProvisionalLaboratoryReleased() {
        return provisionalLaboratoryReleased;
    }

    public void setProvisionalLaboratoryReleased(String provisionalLaboratoryReleased) {
        this.provisionalLaboratoryReleased = provisionalLaboratoryReleased;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }
    
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSchoolCode() {
        return schoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getMandalName() {
        return mandalName;
    }

    public void setMandalName(String mandalName) {
        this.mandalName = mandalName;
    }

    public String getMandalId() {
        return mandalId;
    }

    public void setMandalId(String mandalId) {
        this.mandalId = mandalId;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public String getPhaseNumber() {
        return phaseNumber;
    }

    public void setPhaseNumber(String phaseNumber) {
        this.phaseNumber = phaseNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getCivilWorksReleased() {
        return civilWorksReleased;
    }

    public void setCivilWorksReleased(String civilWorksReleased) {
        this.civilWorksReleased = civilWorksReleased;
    }

    public String getMajorRepairsReleased() {
        return majorRepairsReleased;
    }

    public void setMajorRepairsReleased(String majorRepairsReleased) {
        this.majorRepairsReleased = majorRepairsReleased;
    }

    public String getMinorRepairsReleased() {
        return minorRepairsReleased;
    }

    public void setMinorRepairsReleased(String minorRepairsReleased) {
        this.minorRepairsReleased = minorRepairsReleased;
    }

    public String getAnnualAndOtherReleased() {
        return annualAndOtherReleased;
    }

    public void setAnnualAndOtherReleased(String annualAndOtherReleased) {
        this.annualAndOtherReleased = annualAndOtherReleased;
    }

    public String getToiletsReleased() {
        return toiletsReleased;
    }

    public void setToiletsReleased(String toiletsReleased) {
        this.toiletsReleased = toiletsReleased;
    }

    public String getSelfDefenseGrantsReleased() {
        return selfDefenseGrantsReleased;
    }

    public void setSelfDefenseGrantsReleased(String selfDefenseGrantsReleased) {
        this.selfDefenseGrantsReleased = selfDefenseGrantsReleased;
    }

    public String getTotalCivilReleased() {
        return totalCivilReleased;
    }

    public void setTotalCivilReleased(String totalCivilReleased) {
        this.totalCivilReleased = totalCivilReleased;
    }

    public String getCivilWorksSpent() {
        return civilWorksSpent;
    }

    public void setCivilWorksSpent(String civilWorksSpent) {
        this.civilWorksSpent = civilWorksSpent;
    }

    public String getMajorRepairsSpent() {
        return majorRepairsSpent;
    }

    public void setMajorRepairsSpent(String majorRepairsSpent) {
        this.majorRepairsSpent = majorRepairsSpent;
    }

    public String getMinorRepairsSpent() {
        return minorRepairsSpent;
    }

    public void setMinorRepairsSpent(String minorRepairsSpent) {
        this.minorRepairsSpent = minorRepairsSpent;
    }

    public String getAnnualAndOtherSpent() {
        return annualAndOtherSpent;
    }

    public void setAnnualAndOtherSpent(String annualAndOtherSpent) {
        this.annualAndOtherSpent = annualAndOtherSpent;
    }

    public String getToiletsSpent() {
        return toiletsSpent;
    }

    public void setToiletsSpent(String toiletsSpent) {
        this.toiletsSpent = toiletsSpent;
    }

    public String getSelfDefenseGrantsSpent() {
        return selfDefenseGrantsSpent;
    }

    public void setSelfDefenseGrantsSpent(String selfDefenseGrantsSpent) {
        this.selfDefenseGrantsSpent = selfDefenseGrantsSpent;
    }

    public String getTotalCivilSpent() {
        return totalCivilSpent;
    }

    public void setTotalCivilSpent(String totalCivilSpent) {
        this.totalCivilSpent = totalCivilSpent;
    }

    public String getCivilWorksAvailable() {
        return civilWorksAvailable;
    }

    public void setCivilWorksAvailable(String civilWorksAvailable) {
        this.civilWorksAvailable = civilWorksAvailable;
    }

    public String getMajorRepairsAvailable() {
        return majorRepairsAvailable;
    }

    public void setMajorRepairsAvailable(String majorRepairsAvailable) {
        this.majorRepairsAvailable = majorRepairsAvailable;
    }

    public String getMinorRepairsAvailable() {
        return minorRepairsAvailable;
    }

    public void setMinorRepairsAvailable(String minorRepairsAvailable) {
        this.minorRepairsAvailable = minorRepairsAvailable;
    }

    public String getAnnualAndOtherAvailable() {
        return annualAndOtherAvailable;
    }

    public void setAnnualAndOtherAvailable(String annualAndOtherAvailable) {
        this.annualAndOtherAvailable = annualAndOtherAvailable;
    }

    public String getToiletsAvailable() {
        return toiletsAvailable;
    }

    public void setToiletsAvailable(String toiletsAvailable) {
        this.toiletsAvailable = toiletsAvailable;
    }

    public String getSelfDefenseGrantsAvailable() {
        return selfDefenseGrantsAvailable;
    }

    public void setSelfDefenseGrantsAvailable(String selfDefenseGrantsAvailable) {
        this.selfDefenseGrantsAvailable = selfDefenseGrantsAvailable;
    }

    public String getTotalCivilAvailable() {
        return totalCivilAvailable;
    }

    public void setTotalCivilAvailable(String totalCivilAvailable) {
        this.totalCivilAvailable = totalCivilAvailable;
    }

    public String getWaterElectricityTelephoneChargesReleased() {
        return waterElectricityTelephoneChargesReleased;
    }

    public void setWaterElectricityTelephoneChargesReleased(String waterElectricityTelephoneChargesReleased) {
        this.waterElectricityTelephoneChargesReleased = waterElectricityTelephoneChargesReleased;
    }

    public String getPurchaseofBooksAndPeriodicalsAndNewsPapersReleased() {
        return purchaseofBooksAndPeriodicalsAndNewsPapersReleased;
    }

    public void setPurchaseofBooksAndPeriodicalsAndNewsPapersReleased(String purchaseofBooksAndPeriodicalsAndNewsPapersReleased) {
        this.purchaseofBooksAndPeriodicalsAndNewsPapersReleased = purchaseofBooksAndPeriodicalsAndNewsPapersReleased;
    }

    public String getMinorRepairsAnnualReleased() {
        return minorRepairsAnnualReleased;
    }

    public void setMinorRepairsAnnualReleased(String minorRepairsAnnualReleased) {
        this.minorRepairsAnnualReleased = minorRepairsAnnualReleased;
    }

    public String getSanitationAndICTReleased() {
        return sanitationAndICTReleased;
    }

    public void setSanitationAndICTReleased(String sanitationAndICTReleased) {
        this.sanitationAndICTReleased = sanitationAndICTReleased;
    }

    public String getNeedBasedWorksReleased() {
        return needBasedWorksReleased;
    }

    public void setNeedBasedWorksReleased(String needBasedWorksReleased) {
        this.needBasedWorksReleased = needBasedWorksReleased;
    }

    public String getTotalAnnualReleased() {
        return totalAnnualReleased;
    }

    public void setTotalAnnualReleased(String totalAnnualReleased) {
        this.totalAnnualReleased = totalAnnualReleased;
    }

    public String getWaterElectricityTelephoneChargesSpent() {
        return waterElectricityTelephoneChargesSpent;
    }

    public void setWaterElectricityTelephoneChargesSpent(String waterElectricityTelephoneChargesSpent) {
        this.waterElectricityTelephoneChargesSpent = waterElectricityTelephoneChargesSpent;
    }

    public String getPurchaseofBooksAndPeriodicalsAndNewsPapersSpent() {
        return purchaseofBooksAndPeriodicalsAndNewsPapersSpent;
    }

    public void setPurchaseofBooksAndPeriodicalsAndNewsPapersSpent(String purchaseofBooksAndPeriodicalsAndNewsPapersSpent) {
        this.purchaseofBooksAndPeriodicalsAndNewsPapersSpent = purchaseofBooksAndPeriodicalsAndNewsPapersSpent;
    }

    public String getMinorRepairsAnnualSpent() {
        return minorRepairsAnnualSpent;
    }

    public void setMinorRepairsAnnualSpent(String minorRepairsAnnualSpent) {
        this.minorRepairsAnnualSpent = minorRepairsAnnualSpent;
    }

    public String getSanitationAndICTSpent() {
        return sanitationAndICTSpent;
    }

    public void setSanitationAndICTSpent(String sanitationAndICTSpent) {
        this.sanitationAndICTSpent = sanitationAndICTSpent;
    }

    public String getNeedBasedWorksSpent() {
        return needBasedWorksSpent;
    }

    public void setNeedBasedWorksSpent(String needBasedWorksSpent) {
        this.needBasedWorksSpent = needBasedWorksSpent;
    }

    public String getTotalAnnualSpent() {
        return totalAnnualSpent;
    }

    public void setTotalAnnualSpent(String totalAnnualSpent) {
        this.totalAnnualSpent = totalAnnualSpent;
    }

    public String getWaterElectricityTelephoneChargesAvailable() {
        return waterElectricityTelephoneChargesAvailable;
    }

    public void setWaterElectricityTelephoneChargesAvailable(String waterElectricityTelephoneChargesAvailable) {
        this.waterElectricityTelephoneChargesAvailable = waterElectricityTelephoneChargesAvailable;
    }

    public String getPurchaseofBooksAndPeriodicalsAndNewsPapersAvailable() {
        return purchaseofBooksAndPeriodicalsAndNewsPapersAvailable;
    }

    public void setPurchaseofBooksAndPeriodicalsAndNewsPapersAvailable(String purchaseofBooksAndPeriodicalsAndNewsPapersAvailable) {
        this.purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = purchaseofBooksAndPeriodicalsAndNewsPapersAvailable;
    }

    public String getMinorRepairsAnnualAvailable() {
        return minorRepairsAnnualAvailable;
    }

    public void setMinorRepairsAnnualAvailable(String minorRepairsAnnualAvailable) {
        this.minorRepairsAnnualAvailable = minorRepairsAnnualAvailable;
    }

    public String getSanitationAndICTAvailable() {
        return sanitationAndICTAvailable;
    }

    public void setSanitationAndICTAvailable(String sanitationAndICTAvailable) {
        this.sanitationAndICTAvailable = sanitationAndICTAvailable;
    }

    public String getNeedBasedWorksAvailable() {
        return needBasedWorksAvailable;
    }

    public void setNeedBasedWorksAvailable(String needBasedWorksAvailable) {
        this.needBasedWorksAvailable = needBasedWorksAvailable;
    }

    public String getTotalAnnualAvailable() {
        return totalAnnualAvailable;
    }

    public void setTotalAnnualAvailable(String totalAnnualAvailable) {
        this.totalAnnualAvailable = totalAnnualAvailable;
    }

    public String getFurnitureReleased() {
        return furnitureReleased;
    }

    public void setFurnitureReleased(String furnitureReleased) {
        this.furnitureReleased = furnitureReleased;
    }

    public String getLabEquipmentReleased() {
        return labEquipmentReleased;
    }

    public void setLabEquipmentReleased(String labEquipmentReleased) {
        this.labEquipmentReleased = labEquipmentReleased;
    }

    public String getTotalfurnitureAndLabReleased() {
        return totalfurnitureAndLabReleased;
    }

    public void setTotalfurnitureAndLabReleased(String totalfurnitureAndLabReleased) {
        this.totalfurnitureAndLabReleased = totalfurnitureAndLabReleased;
    }

    public String getFurnitureSpent() {
        return furnitureSpent;
    }

    public void setFurnitureSpent(String furnitureSpent) {
        this.furnitureSpent = furnitureSpent;
    }

    public String getLabEquipmentSpent() {
        return labEquipmentSpent;
    }

    public void setLabEquipmentSpent(String labEquipmentSpent) {
        this.labEquipmentSpent = labEquipmentSpent;
    }

    public String getTotalfurnitureAndLabSpent() {
        return totalfurnitureAndLabSpent;
    }

    public void setTotalfurnitureAndLabSpent(String totalfurnitureAndLabSpent) {
        this.totalfurnitureAndLabSpent = totalfurnitureAndLabSpent;
    }

    public String getFurnitureAvailable() {
        return furnitureAvailable;
    }

    public void setFurnitureAvailable(String furnitureAvailable) {
        this.furnitureAvailable = furnitureAvailable;
    }

    public String getLabEquipmentAvailable() {
        return labEquipmentAvailable;
    }

    public void setLabEquipmentAvailable(String labEquipmentAvailable) {
        this.labEquipmentAvailable = labEquipmentAvailable;
    }

    public String getTotalfurnitureAndLabAvailable() {
        return totalfurnitureAndLabAvailable;
    }

    public void setTotalfurnitureAndLabAvailable(String totalfurnitureAndLabAvailable) {
        this.totalfurnitureAndLabAvailable = totalfurnitureAndLabAvailable;
    }

    public ArrayList getYearList() {
        return yearList;
    }

    public void setYearList(ArrayList yearList) {
        this.yearList = yearList;
    }
    
    public ArrayList getDistList() {
        return distList;
    }

    public void setDistList(ArrayList distList) {
        this.distList = distList;
    }

    public ArrayList getMandalList() {
        return mandalList;
    }

    public void setMandalList(ArrayList mandalList) {
        this.mandalList = mandalList;
    }

    public ArrayList getVillageList() {
        return villageList;
    }

    public void setVillageList(ArrayList villageList) {
        this.villageList = villageList;
    }

    public ArrayList getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(ArrayList schoolList) {
        this.schoolList = schoolList;
    }

    public ArrayList getPhaseList() {
        return phaseList;
    }

    public void setPhaseList(ArrayList phaseList) {
        this.phaseList = phaseList;
    }

    public String getRecurringExcursionRelease() {
        return recurringExcursionRelease;
    }

    public void setRecurringExcursionRelease(String recurringExcursionRelease) {
        this.recurringExcursionRelease = recurringExcursionRelease;
    }

    public String getRecurringExcursionSpent() {
        return recurringExcursionSpent;
    }

    public void setRecurringExcursionSpent(String recurringExcursionSpent) {
        this.recurringExcursionSpent = recurringExcursionSpent;
    }

    public String getRecurringExcursionAvailable() {
        return recurringExcursionAvailable;
    }

    public void setRecurringExcursionAvailable(String recurringExcursionAvailable) {
        this.recurringExcursionAvailable = recurringExcursionAvailable;
    }

    public String getRecurringSelfDefenseRelease() {
        return recurringSelfDefenseRelease;
    }

    public void setRecurringSelfDefenseRelease(String recurringSelfDefenseRelease) {
        this.recurringSelfDefenseRelease = recurringSelfDefenseRelease;
    }

    public String getRecurringSelfDefenseSpent() {
        return recurringSelfDefenseSpent;
    }

    public void setRecurringSelfDefenseSpent(String recurringSelfDefenseSpent) {
        this.recurringSelfDefenseSpent = recurringSelfDefenseSpent;
    }

    public String getRecurringSelfDefenseAvailable() {
        return recurringSelfDefenseAvailable;
    }

    public void setRecurringSelfDefenseAvailable(String recurringSelfDefenseAvailable) {
        this.recurringSelfDefenseAvailable = recurringSelfDefenseAvailable;
    }

    public String getRecurringOtherRelease() {
        return recurringOtherRelease;
    }

    public void setRecurringOtherRelease(String recurringOtherRelease) {
        this.recurringOtherRelease = recurringOtherRelease;
    }

    public String getRecurringOtherSpent() {
        return recurringOtherSpent;
    }

    public void setRecurringOtherSpent(String recurringOtherSpent) {
        this.recurringOtherSpent = recurringOtherSpent;
    }

    public String getRecurringOtherAvailable() {
        return recurringOtherAvailable;
    }

    public void setRecurringOtherAvailable(String recurringOtherAvailable) {
        this.recurringOtherAvailable = recurringOtherAvailable;
    }

    public String getRecurringTotalRelease() {
        return recurringTotalRelease;
    }

    public void setRecurringTotalRelease(String recurringTotalRelease) {
        this.recurringTotalRelease = recurringTotalRelease;
    }

    public String getRecurringTotalSpent() {
        return recurringTotalSpent;
    }

    public void setRecurringTotalSpent(String recurringTotalSpent) {
        this.recurringTotalSpent = recurringTotalSpent;
    }

    public String getRecurringTotalAvailable() {
        return recurringTotalAvailable;
    }

    public void setRecurringTotalAvailable(String recurringTotalAvailable) {
        this.recurringTotalAvailable = recurringTotalAvailable;
    }

    public String getInterestEarned() {
        return interestEarned;
    }

    public void setInterestEarned(String interestEarned) {
        this.interestEarned = interestEarned;
    }

    public String getInterestExpenditure() {
        return interestExpenditure;
    }

    public void setInterestExpenditure(String interestExpenditure) {
        this.interestExpenditure = interestExpenditure;
    }

    public String getInterestBalance() {
        return interestBalance;
    }

    public void setInterestBalance(String interestBalance) {
        this.interestBalance = interestBalance;
    }
    
    
    
    
 
}