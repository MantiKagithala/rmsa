/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.ChangePasswordForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;
import com.aponline.rmsa.loginforms.LoginForm;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author 1250892
 */
public class ChangePasswordDao {

    public String passwordEncrypt(String password) throws Exception, NoSuchAlgorithmException {
        String sr = null;
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] dataBytes = password.getBytes();
        md.update(dataBytes);
        byte[] mdbytes = md.digest();
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int j = 0; j < mdbytes.length; j++) {
            sb.append(Integer.toString((mdbytes[j] & 0xff) + 0x100, 16).substring(1));
        }
        sr = sb.toString();
        return sr;
    }

    public int updatePassword(ChangePasswordForm myform) {
        int myreturn = 0;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[RMSA_Change_Password](?,?,?,?,?)}");
            cstmt.setString(1, "RMSA");
            cstmt.setString(2, myform.getUserName());
            cstmt.setString(3, myform.getCurrentPasswordEncreption());
            cstmt.setString(4, myform.getNewPassword());
            cstmt.setString(5, myform.getUpdatePassword());
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    myreturn = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myreturn;
    }
    
        public int passwordReset(LoginForm myform) {
        int myreturn = 0;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[RMSA_First_Time_Change_Password](?,?,?,?,?)}");
            cstmt.setString(1, "RMSA");
            cstmt.setString(2, myform.getUserName());
            cstmt.setString(3, myform.getCurrentPasswordEncreption());
            cstmt.setString(4, myform.getNewPassword());
            cstmt.setString(5, myform.getUpdatePassword());
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    myreturn = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myreturn;
    }
}
