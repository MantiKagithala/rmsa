/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;

/**
 *
 * @author 1250881
 */
public class RMSASchoolComponentReportDAO {

    public ArrayList getSchoolList(String id) {
        ArrayList schoollist = new ArrayList();
          CommonConstants conistants = new CommonConstants();
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CallableStatement cstmt = null;
        long civilNonRecTotal = 0;
        long majorNonRecTotal = 0;
        long minorNonRecTotal = 0;
        long toiletNonRecTotal = 0;
        long totNonRecTotal = 0;

        long expcivilNonRecTotal = 0;
        long expmajorNonRecTotal = 0;
        long expminorNonRecTotal = 0;
        long exptoiletNonRecTotal = 0;
        long exptotNonRecTotal = 0;

        long totcivilNonRecTotal = 0;
        long totmajorNonRecTotal = 0;
        long totminorNonRecTotal = 0;
        long tottoiletNonRecTotal = 0;
        long tottotNonRecTotal = 0;

        long waterAnnualGrantsTotal = 0;
        long booksAnnualGrantsTotal = 0;
        long minorAnnualGrantsTotal = 0;
        long sanitationAnnualGrantsTotal = 0;
        long needAnnualGrantsTotal = 0;
        long provisiAnnualGrantsTotal = 0;
        long totAnnualGrantsTotal = 0;

        long expwaterAnnualGrantsTotal = 0;
        long expbooksAnnualGrantsTotal = 0;
        long expminorAnnualGrantsTotal = 0;
        long expsanitationAnnualGrantsTotal = 0;
        long expneedAnnualGrantsTotal = 0;
        long expprovisiAnnualGrantsTotal = 0;
        long exptotAnnualGrantsTotal = 0;

        long twaterAnnualGrantsTotal = 0;
        long tbooksAnnualGrantsTotal = 0;
        long tminorAnnualGrantsTotal = 0;
        long tsanitationAnnualGrantsTotal = 0;
        long tneedAnnualGrantsTotal = 0;
        long tprovisiAnnualGrantsTotal = 0;
        long ttotAnnualGrantsTotal = 0;

        long excurTriprecurringTotal = 0;
        long selfdefencerecurringTotal = 0;
        long otherrecurringTotal = 0;
        long totrecurringTotal = 0;

        long excexcurTriprecurringTotal = 0;
        long excselfdefencerecurringTotal = 0;
        long excotherrecurringTotal = 0;
        long exctotrecurringTotal = 0;

        long texcurTriprecurringTotal = 0;
        long tselfdefencerecurringTotal = 0;
        long totherrecurringTotal = 0;
        long ttotrecurringTotal = 0;

        long relfurFurniturLabEquipmentTotal = 0;
        long rellabFurniturLabEquipmentTotal = 0;
        long reltotFurniturLabEquipmentTotal = 0;

        long exfurFurniturLabEquipmentTotal = 0;
        long exlabFurniturLabEquipmentTotal = 0;
        long exltotFurniturLabEquipmentTotal = 0;

        long tfurFurniturLabEquipmentTotal = 0;
        long tlabFurniturLabEquipmentTotal = 0;
        long tltotFurniturLabEquipmentTotal = 0;
        
         long total_CivilWorks_OB = 0;
        long total_MajorRepairs_OB = 0;
        long total_MinorRepairs_OB = 0;
        long total_AnnualOtherRecurringGrants_OB = 0;
        long total_Toilets_OB = 0;
        long total_SelfDefenseTrainingGrants_OB = 0;
        long total_Interest_OB = 0;
        long total_total_total_OB = 0;

        long total_CivilWorks_EOB = 0;
        long total_MajorRepairs_EOB = 0;
        long total_MinorRepairs_EOB = 0;
        long total_AnnualOtherRecurringGrants_EOB = 0;
        long total_Toilets_EOB = 0;
        long total_SelfDefenseTrainingGrants_EOB = 0;
        long total_Interest_EOB = 0;
        long total_total_total_EOB = 0;

        long total_CivilWorks_UBOB = 0;
        long total_MajorRepairs_UBOB = 0;
        long total_MinorRepairs_UBOB = 0;
        long total_AnnualOtherRecurringGrants_UBOB = 0;
        long total_Toilets_UBOB = 0;
        long total_SelfDefenseTrainingGrants_UBOB = 0;
        long total_Interest_UBOB = 0;
        long total_total_total_UBOB = 0;

        long total_Earned_Interest = 0;
        long total_Expenditure_Interest = 0;
        long total_Balance_Interest = 0;
        long total_RemmitanceAmount_ExpenditureIncurred = 0;


        long total_total_OB = 0;
        long total_totalRealse_Realses = 0;
        long total_Earned_Interest_total = 0;
        long totalRealse_total_total = 0;

        long Grand_Total_Expenditure_Incurred_on_Opening_balances = 0;
        long Grand_Total_Expenditure_Incurred_on_Releases_total = 0;
        long total_Expenditure_Interest_total = 0;
        long total_RemmitanceAmount_ExpenditureIncurred_total = 0;
        long Grand_Total_Total_Expenditure_total = 0;

        long close_balnce_ub_ob_total = 0;
        long close_balnce_ub_rb_total = 0;
        long close_balnce_ub_ineterest_total = 0;
        long close_balnce_ub_total_total = 0;



        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[RMSAcomponent_school_Report](?)}");
            cstmt.setString(1, id);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schoolcode", rs.getString(1));
                    map.put("schoolname", rs.getString(2));
                    map.put("civilReleased", rs.getString(3));
                    map.put("majorReleased", rs.getString(4));
                    map.put("minorReleased", rs.getString(5));
                    map.put("toiletsReleased", rs.getString(6));
                    map.put("civilExp", rs.getString(7));
                    map.put("majorExp", rs.getString(8));
                    map.put("minorExp", rs.getString(9));
                    map.put("toiletsExp", rs.getString(10));
                    map.put("waterReleased", rs.getString(11));
                    map.put("booksReleased", rs.getString(12));
                    map.put("minorReleasedAnnual", rs.getString(13));
                    map.put("sanitationReleased", rs.getString(14));
                    map.put("needReleased", rs.getString(15));
                    map.put("provisionalReleased", rs.getString(16));
                    map.put("waterExp", rs.getString(17));
                    map.put("booksExp", rs.getString(18));
                    map.put("minorExpAnnual", rs.getString(19));
                    map.put("sanitationExp", rs.getString(20));
                    map.put("needExp", rs.getString(21));
                    map.put("provisionalExp", rs.getString(22));
                    map.put("excurtionReleased", rs.getString(23));
                    map.put("selfdefReleased", rs.getString(24));
                    map.put("otherRecuGrantReleased", rs.getString(25));
                    map.put("excurtionExp", rs.getString(26));
                    map.put("selfdefExp", rs.getString(27));
                    map.put("otherRecuGrantExp", rs.getString(28));
                    map.put("furnitureReleased", rs.getString(29));
                    map.put("labEquipReleased", rs.getString(30));
                    map.put("furnitureExp", rs.getString(31));
                    map.put("labEquipExp", rs.getString(32));
                    //bank
                    map.put("bankname", rs.getString(33));
                    map.put("accountno", rs.getString(34));
                    map.put("ifsccode", rs.getString(35));
                    map.put("year", rs.getString(36));
                    int acivirel = rs.getInt(3);
                    int aciviexp = rs.getInt(7);
                    int civibaaltot = acivirel - aciviexp;
                    map.put("civilBal", civibaaltot);
                    int majrel = rs.getInt(4);
                    int majexp = rs.getInt(8);
                    int majtot = majrel - majexp;
                    map.put("majorBal", majtot);
                    int minrel = rs.getInt(5);
                    int minexp = rs.getInt(9);
                    int mintot = minrel - minexp;
                    map.put("minorBal", mintot);
                    int toirel = rs.getInt(6);
                    int toiexp = rs.getInt(10);
                    int toitot = toirel - toiexp;
                    map.put("toiletsBal", toitot);
                    //totals
                    int tocivicnorecrel = rs.getInt(3);
                    int tomajnonrecrel = rs.getInt(4);
                    int tominnonrecrel = rs.getInt(5);
                    int tototinonrecrel = rs.getInt(6);
                    int tonorectot = tocivicnorecrel + tomajnonrecrel + tominnonrecrel + tototinonrecrel;
                    map.put("totalNonRecurReleased", tonorectot);
                    int tocivicnorecExp = rs.getInt(7);
                    int tomajnonrecExp = rs.getInt(8);
                    int tominnonrecExp = rs.getInt(9);
                    int tototinonrecExp = rs.getInt(10);
                    int tonorectotExp = tocivicnorecExp + tomajnonrecExp + tominnonrecExp + tototinonrecExp;
                    map.put("totalNonRecurExp", tonorectotExp);
                    int totciv = civibaaltot;
                    int totmaj = majtot;
                    int totmin = mintot;
                    int tottoil = toitot;
                    int totmajexpbal = totciv + totmaj + totmin + tottoil;
                    map.put("totalNonRecurBal", totmajexpbal);
                    //endtotal
                    int warel = rs.getInt(11);
                    int waexp = rs.getInt(17);
                    int watot = warel - waexp;
                    map.put("waterBal", watot);
                    int borel = rs.getInt(12);
                    int boxp = rs.getInt(18);
                    int botot = borel - boxp;
                    map.put("booksBal", botot);
                    int mianrel = rs.getInt(13);
                    int mianxp = rs.getInt(19);
                    int miantot = mianrel - mianxp;
                    map.put("minorBalAnnual", miantot);
                    int sarel = rs.getInt(14);
                    int saexp = rs.getInt(20);
                    int satot = sarel - saexp;
                    map.put("sanitationBal", satot);
                    int needrel = rs.getInt(15);
                    int needexp = rs.getInt(21);
                    int needtot = needrel - needexp;
                    map.put("needBal", needtot);
                    int pvrel = rs.getInt(15);
                    int pvexp = rs.getInt(21);
                    int pvtot = pvrel - pvexp;
                    map.put("provisionalBal", pvtot);
                    //totals
                    int towatcannualgrantrel = rs.getInt(11);
                    int tobookannualgrantrel = rs.getInt(12);
                    int tominannualgrantrel = rs.getInt(13);
                    int tosaniannualgrantrel = rs.getInt(14);
                    int toneedannualgrantrel = rs.getInt(15);
                    int toprovannualgrantrel = rs.getInt(16);
                    int toannualgranttot = towatcannualgrantrel + tobookannualgrantrel + tominannualgrantrel + tosaniannualgrantrel + toneedannualgrantrel + toprovannualgrantrel;
                    map.put("totalGrantReleased", toannualgranttot);
                    int towatcannualgrantExp = rs.getInt(17);
                    int tobookannualgrantExp = rs.getInt(18);
                    int tominannualgrantExp = rs.getInt(19);
                    int tosaniannualgrantExp = rs.getInt(20);
                    int toneedannualgrantExp = rs.getInt(21);
                    int toprovannualgrantExp = rs.getInt(22);
                    int toannualgrantExptot = towatcannualgrantExp + tobookannualgrantExp + tominannualgrantExp + tosaniannualgrantExp + toneedannualgrantExp + toprovannualgrantExp;
                    map.put("totalGrantExp", toannualgrantExptot);
                    int towatcannualgrantt = watot;
                    int tobookannualgrantt = botot;
                    int tominannualgrantt = miantot;
                    int tosaniannualgrantt = satot;
                    int toneedannualgrantt = needtot;
                    int toprovannualgrantt = pvtot;
                    int toannualgranttott = towatcannualgrantt + tobookannualgrantt + tominannualgrantt + tosaniannualgrantt + toneedannualgrantt + toprovannualgrantt;
                    map.put("totalGrantBal", toannualgranttott);
                    //endtotal
                    int exrel = rs.getInt(23);
                    int exexp = rs.getInt(26);
                    int extot = exrel - exexp;
                    map.put("excurtionBal", extot);
                    int serel = rs.getInt(24);
                    int seexp = rs.getInt(27);
                    int setot = serel - seexp;
                    map.put("selfdefBal", setot);
                    int otrel = rs.getInt(25);
                    int otexp = rs.getInt(28);
                    int ottot = otrel - otexp;
                    map.put("otherRecuGrantBal", ottot);
                    //totals          
                    int excurrecurrel = rs.getInt(23);
                    int selfrecurrel = rs.getInt(24);
                    int otherrecurrel = rs.getInt(25);
                    int recutot = excurrecurrel + selfrecurrel + otherrecurrel;
                    map.put("totalRecurReleased", recutot);
                    int excurrecurExp = rs.getInt(26);
                    int selfrecurExp = rs.getInt(27);
                    int otherrecurExp = rs.getInt(28);
                    int recutotExp = excurrecurExp + selfrecurExp + otherrecurExp;
                    map.put("totalRecurExp", recutotExp);
                    int excurrecurt = extot;
                    int selfrecurt = setot;
                    int otherrecurt = ottot;
                    int recutott = excurrecurt + selfrecurt + otherrecurt;
                    map.put("totalRecurBal", recutott);
                    //endtotal
                    int frel = rs.getInt(29);
                    int fexp = rs.getInt(31);
                    int ftot = frel - fexp;
                    map.put("furnitureBal", ftot);
                    int labrel = rs.getInt(30);
                    int labexp = rs.getInt(32);
                    int labtot = labrel - labexp;
                    map.put("labEquipBal", labtot);
                    //totals    
                    int furnfurlabrel = rs.getInt(29);
                    int labfurlabrel = rs.getInt(30);
                    int furlabrelreltot = furnfurlabrel + labfurlabrel;
                    map.put("totallabEquipReleased", furlabrelreltot);
                    int furnfurlabExp = rs.getInt(31);
                    int labfurlabExp = rs.getInt(32);
                    int furlabrelExptot = furnfurlabExp + labfurlabExp;
                    map.put("totallabEquipExp", furlabrelExptot);
                    int furnfurlabt = ftot;
                    int labfurlabt = labtot;
                    int furlabrelttot = furnfurlabt + labfurlabt;
                    map.put("totallabEquipBal", furlabrelttot);
                    //endtotal

                    /////////Final totals in tfoot
                    civilNonRecTotal = civilNonRecTotal + rs.getLong(3);
                    majorNonRecTotal = majorNonRecTotal + rs.getLong(4);
                    minorNonRecTotal = minorNonRecTotal + rs.getLong(5);
                    toiletNonRecTotal = toiletNonRecTotal + rs.getLong(6);
                    totNonRecTotal = totNonRecTotal + tonorectot;
                    expcivilNonRecTotal = expcivilNonRecTotal + rs.getLong(7);
                    expmajorNonRecTotal = expmajorNonRecTotal + rs.getLong(8);
                    expminorNonRecTotal = expminorNonRecTotal + rs.getLong(9);
                    exptoiletNonRecTotal = exptoiletNonRecTotal + rs.getLong(10);
                    exptotNonRecTotal = exptotNonRecTotal + tonorectotExp;
                    totcivilNonRecTotal = totcivilNonRecTotal + civibaaltot;
                    totmajorNonRecTotal = totmajorNonRecTotal + majtot;
                    totminorNonRecTotal = totminorNonRecTotal + mintot;
                    tottoiletNonRecTotal = tottoiletNonRecTotal + toitot;
                    tottotNonRecTotal = tottotNonRecTotal + totmajexpbal;
                    waterAnnualGrantsTotal = waterAnnualGrantsTotal + rs.getLong(11);
                    booksAnnualGrantsTotal = booksAnnualGrantsTotal + rs.getLong(12);
                    minorAnnualGrantsTotal = minorAnnualGrantsTotal + rs.getLong(13);
                    sanitationAnnualGrantsTotal = sanitationAnnualGrantsTotal + rs.getLong(14);
                    needAnnualGrantsTotal = needAnnualGrantsTotal + rs.getLong(15);
                    provisiAnnualGrantsTotal = provisiAnnualGrantsTotal + rs.getLong(16);
                    totAnnualGrantsTotal = totAnnualGrantsTotal + toannualgranttot;
                    expwaterAnnualGrantsTotal = expwaterAnnualGrantsTotal + rs.getLong(17);
                    expbooksAnnualGrantsTotal = expbooksAnnualGrantsTotal + rs.getLong(18);
                    expminorAnnualGrantsTotal = expminorAnnualGrantsTotal + rs.getLong(19);
                    expsanitationAnnualGrantsTotal = expsanitationAnnualGrantsTotal + rs.getLong(20);
                    expneedAnnualGrantsTotal = expneedAnnualGrantsTotal + rs.getLong(21);
                    expprovisiAnnualGrantsTotal = expprovisiAnnualGrantsTotal + rs.getLong(22);
                    exptotAnnualGrantsTotal = exptotAnnualGrantsTotal + toannualgrantExptot;
                    twaterAnnualGrantsTotal = twaterAnnualGrantsTotal + watot;
                    tbooksAnnualGrantsTotal = tbooksAnnualGrantsTotal + botot;
                    tminorAnnualGrantsTotal = tminorAnnualGrantsTotal + miantot;
                    tsanitationAnnualGrantsTotal = tsanitationAnnualGrantsTotal + satot;
                    tneedAnnualGrantsTotal = tneedAnnualGrantsTotal + needexp;
                    tprovisiAnnualGrantsTotal = tprovisiAnnualGrantsTotal + pvtot;
                    ttotAnnualGrantsTotal = ttotAnnualGrantsTotal + toannualgranttott;
                    excurTriprecurringTotal = excurTriprecurringTotal + rs.getLong(23);
                    selfdefencerecurringTotal = selfdefencerecurringTotal + rs.getLong(24);
                    otherrecurringTotal = otherrecurringTotal + rs.getLong(25);
                    totrecurringTotal = totrecurringTotal + recutot;
                    excexcurTriprecurringTotal = excexcurTriprecurringTotal + rs.getLong(26);
                    excselfdefencerecurringTotal = excselfdefencerecurringTotal + rs.getLong(27);
                    excotherrecurringTotal = excotherrecurringTotal + rs.getLong(28);
                    exctotrecurringTotal = exctotrecurringTotal + recutotExp;
                    texcurTriprecurringTotal = texcurTriprecurringTotal + extot;
                    tselfdefencerecurringTotal = tselfdefencerecurringTotal + setot;
                    totherrecurringTotal = totherrecurringTotal + ottot;
                    ttotrecurringTotal = ttotrecurringTotal + recutott;
                    relfurFurniturLabEquipmentTotal = relfurFurniturLabEquipmentTotal + rs.getLong(29);
                    rellabFurniturLabEquipmentTotal = rellabFurniturLabEquipmentTotal + rs.getLong(30);
                    reltotFurniturLabEquipmentTotal = reltotFurniturLabEquipmentTotal + furlabrelreltot;
                    exfurFurniturLabEquipmentTotal = exfurFurniturLabEquipmentTotal + rs.getLong(31);
                    exlabFurniturLabEquipmentTotal = exlabFurniturLabEquipmentTotal + rs.getLong(32);
                    exltotFurniturLabEquipmentTotal = exltotFurniturLabEquipmentTotal + furlabrelExptot;
                    tfurFurniturLabEquipmentTotal = tfurFurniturLabEquipmentTotal + furlabrelreltot;
                    tlabFurniturLabEquipmentTotal = tlabFurniturLabEquipmentTotal + furlabrelExptot;
                    tltotFurniturLabEquipmentTotal = tltotFurniturLabEquipmentTotal + furlabrelttot;

                    //annual grants

                    map.put("civilNonRecTotal", civilNonRecTotal);
                    map.put("majorNonRecTotal", majorNonRecTotal);
                    map.put("minorNonRecTotal", minorNonRecTotal);
                    map.put("toiletNonRecTotal", toiletNonRecTotal);
                    map.put("totNonRecTotal", totNonRecTotal);

                    map.put("expcivilNonRecTotal", expcivilNonRecTotal);
                    map.put("expmajorNonRecTotal", expmajorNonRecTotal);
                    map.put("expminorNonRecTotal", expminorNonRecTotal);
                    map.put("exptoiletNonRecTotal", exptoiletNonRecTotal);
                    map.put("exptotNonRecTotal", exptotNonRecTotal);

                    map.put("totcivilNonRecTotal", totcivilNonRecTotal);
                    map.put("totmajorNonRecTotal", totmajorNonRecTotal);
                    map.put("totminorNonRecTotal", totminorNonRecTotal);
                    map.put("tottoiletNonRecTotal", tottoiletNonRecTotal);
                    map.put("tottotNonRecTotal", tottotNonRecTotal);

                    map.put("waterAnnualGrantsTotal", waterAnnualGrantsTotal);
                    map.put("booksAnnualGrantsTotal", booksAnnualGrantsTotal);
                    map.put("minorAnnualGrantsTotal", minorAnnualGrantsTotal);
                    map.put("sanitationAnnualGrantsTotal", sanitationAnnualGrantsTotal);
                    map.put("needAnnualGrantsTotal", needAnnualGrantsTotal);
                    map.put("provisiAnnualGrantsTotal", provisiAnnualGrantsTotal);
                    map.put("totAnnualGrantsTotal", totAnnualGrantsTotal);

                    map.put("expwaterAnnualGrantsTotal", expwaterAnnualGrantsTotal);
                    map.put("expbooksAnnualGrantsTotal", expbooksAnnualGrantsTotal);
                    map.put("expminorAnnualGrantsTotal", expminorAnnualGrantsTotal);
                    map.put("expsanitationAnnualGrantsTotal", expsanitationAnnualGrantsTotal);
                    map.put("expneedAnnualGrantsTotal", expneedAnnualGrantsTotal);
                    map.put("expprovisiAnnualGrantsTotal", expprovisiAnnualGrantsTotal);
                    map.put("exptotAnnualGrantsTotal", exptotAnnualGrantsTotal);

                    map.put("twaterAnnualGrantsTotal", twaterAnnualGrantsTotal);
                    map.put("tbooksAnnualGrantsTotal", tbooksAnnualGrantsTotal);
                    map.put("tminorAnnualGrantsTotal", tminorAnnualGrantsTotal);
                    map.put("tsanitationAnnualGrantsTotal", tsanitationAnnualGrantsTotal);
                    map.put("tneedAnnualGrantsTotal", tneedAnnualGrantsTotal);
                    map.put("tprovisiAnnualGrantsTotal", tprovisiAnnualGrantsTotal);
                    map.put("ttotAnnualGrantsTotal", ttotAnnualGrantsTotal);

                    map.put("excurTriprecurringTotal", excurTriprecurringTotal);
                    map.put("selfdefencerecurringTotal", selfdefencerecurringTotal);
                    map.put("otherrecurringTotal", otherrecurringTotal);
                    map.put("totrecurringTotal", totrecurringTotal);

                    map.put("excexcurTriprecurringTotal", excexcurTriprecurringTotal);
                    map.put("excselfdefencerecurringTotal", excselfdefencerecurringTotal);
                    map.put("excotherrecurringTotal", excotherrecurringTotal);
                    map.put("exctotrecurringTotal", exctotrecurringTotal);

                    map.put("texcurTriprecurringTotal", texcurTriprecurringTotal);
                    map.put("tselfdefencerecurringTotal", tselfdefencerecurringTotal);
                    map.put("totherrecurringTotal", totherrecurringTotal);
                    map.put("ttotrecurringTotal", ttotrecurringTotal);

                    map.put("relfurFurniturLabEquipmentTotal", relfurFurniturLabEquipmentTotal);
                    map.put("rellabFurniturLabEquipmentTotal", rellabFurniturLabEquipmentTotal);
                    map.put("reltotFurniturLabEquipmentTotal", reltotFurniturLabEquipmentTotal);

                    map.put("exfurFurniturLabEquipmentTotal", exfurFurniturLabEquipmentTotal);
                    map.put("exlabFurniturLabEquipmentTotal", exlabFurniturLabEquipmentTotal);
                    map.put("exltotFurniturLabEquipmentTotal", exltotFurniturLabEquipmentTotal);

                    map.put("tfurFurniturLabEquipmentTotal", tfurFurniturLabEquipmentTotal);
                    map.put("tlabFurniturLabEquipmentTotal", tlabFurniturLabEquipmentTotal);
                    map.put("tltotFurniturLabEquipmentTotal", tltotFurniturLabEquipmentTotal);

                    map.put("id", id);
                    
                    
                    map.put("CivilWorks_OB", rs.getString(37));
                    map.put("MajorRepairs_OB", rs.getString(38));
                    map.put("MinorRepairs_OB", rs.getString(39));
                    map.put("AnnualOtherRecurringGrants_OB", rs.getString(40));
                    map.put("Toilets_OB", rs.getString(41));
                    map.put("SelfDefenseTrainingGrants_OB", rs.getString(42));
                    map.put("Interest_OB", rs.getString(43));
                    int total_OB = rs.getInt(37) + rs.getInt(38) + rs.getInt(39) + rs.getInt(40) + rs.getInt(41) + rs.getInt(42) + rs.getInt(43) ;
                    map.put("total_OB", total_OB);
                    
                    total_CivilWorks_OB = total_CivilWorks_OB + rs.getInt(37);
                    map.put("total_CivilWorks_OB", total_CivilWorks_OB);
                    total_MajorRepairs_OB = total_MajorRepairs_OB + rs.getInt(38);
                    map.put("total_MajorRepairs_OB", total_MajorRepairs_OB);
                    total_MinorRepairs_OB = total_MinorRepairs_OB + rs.getInt(39);
                    map.put("total_MinorRepairs_OB", total_MinorRepairs_OB);
                    total_AnnualOtherRecurringGrants_OB = total_AnnualOtherRecurringGrants_OB + rs.getInt(40);
                    map.put("total_AnnualOtherRecurringGrants_OB", total_AnnualOtherRecurringGrants_OB);
                    total_Toilets_OB = total_Toilets_OB + rs.getInt(41);
                    map.put("total_Toilets_OB", total_Toilets_OB);
                    total_SelfDefenseTrainingGrants_OB = total_SelfDefenseTrainingGrants_OB + rs.getInt(42);
                    map.put("total_SelfDefenseTrainingGrants_OB", total_SelfDefenseTrainingGrants_OB);
                    total_Interest_OB = total_Interest_OB + rs.getInt(43);
                    map.put("total_Interest_OB", total_Interest_OB);
                    total_total_total_OB = total_total_total_OB + total_OB;
                    map.put("total_total_total_OB", total_total_total_OB);
                    
                    map.put("CivilWorks_EOB", rs.getString(44));
                    map.put("MajorRepairs_EOB", rs.getString(45));
                    map.put("MinorRepairs_EOB", rs.getString(46));
                    map.put("AnnualOtherRecurringGrants_EOB", rs.getString(47));
                    map.put("Toilets_EOB", rs.getString(48));
                    map.put("SelfDefenseETrainingGrants_EOB", rs.getString(49));
                    map.put("Interest_EOB", rs.getString(50));
                    int total_EOB = rs.getInt(44) + rs.getInt(45) + rs.getInt(46) + rs.getInt(47) + rs.getInt(48) + rs.getInt(49) + rs.getInt(50);
                    map.put("total_EOB", total_EOB);

                    total_CivilWorks_EOB = total_CivilWorks_EOB + rs.getInt(44);
                    map.put("total_CivilWorks_EOB", total_CivilWorks_EOB);
                    total_MajorRepairs_EOB = total_MajorRepairs_EOB + rs.getInt(45);
                    map.put("total_MajorRepairs_EOB", total_MajorRepairs_EOB);
                    total_MinorRepairs_EOB = total_MinorRepairs_EOB + rs.getInt(46);
                    map.put("total_MinorRepairs_EOB", total_MinorRepairs_EOB);
                    total_AnnualOtherRecurringGrants_EOB = total_AnnualOtherRecurringGrants_EOB + rs.getInt(47);
                    map.put("total_AnnualOtherRecurringGrants_EOB", total_AnnualOtherRecurringGrants_EOB);
                    total_Toilets_EOB = total_Toilets_EOB + rs.getInt(48);
                    map.put("total_Toilets_EOB", total_Toilets_EOB);
                    total_SelfDefenseTrainingGrants_EOB = total_SelfDefenseTrainingGrants_EOB + rs.getInt(49);
                    map.put("total_SelfDefenseTrainingGrants_EOB", total_SelfDefenseTrainingGrants_EOB);
                    total_Interest_EOB = total_Interest_EOB + rs.getInt(50);
                    map.put("total_Interest_EOB", total_Interest_EOB);
                    total_total_total_EOB = total_total_total_EOB + total_EOB;
                    map.put("total_total_total_EOB", total_total_total_EOB);
                    
                    map.put("CivilWorks_UBOB", rs.getString(51));
                    map.put("MajorRepairs_UBOB", rs.getString(52));
                    map.put("MinorRepairs_UBOB", rs.getString(53));
                    map.put("AnnualOtherRecurringGrants_UBOB", rs.getString(54));
                    map.put("Toilets_UBOB", rs.getString(55));
                    map.put("SelfDefenseETrainingGrants_UBOB", rs.getString(56));
                    map.put("Interest_UBOB", rs.getString(57));
                    map.put("Released", rs.getString(58));
                    int total_UEOB = rs.getInt(51) + rs.getInt(52) + rs.getInt(53) + rs.getInt(54) + rs.getInt(55) + rs.getInt(56) + rs.getInt(57);
                    map.put("total_UEOB", total_UEOB);

                    total_CivilWorks_UBOB = total_CivilWorks_UBOB + rs.getInt(51);
                    map.put("total_CivilWorks_UBOB", total_CivilWorks_UBOB);
                    total_MajorRepairs_UBOB = total_MajorRepairs_UBOB + rs.getInt(52);
                    map.put("total_MajorRepairs_UBOB", total_MajorRepairs_UBOB);
                    total_MinorRepairs_UBOB = total_MinorRepairs_UBOB + rs.getInt(53);
                    map.put("total_MinorRepairs_UBOB", total_MinorRepairs_UBOB);
                    total_AnnualOtherRecurringGrants_UBOB = total_AnnualOtherRecurringGrants_UBOB + rs.getInt(54);
                    map.put("total_AnnualOtherRecurringGrants_UBOB", total_AnnualOtherRecurringGrants_UBOB);
                    total_Toilets_UBOB = total_Toilets_UBOB + rs.getInt(55);
                    map.put("total_Toilets_UBOB", total_Toilets_UBOB);
                    total_SelfDefenseTrainingGrants_UBOB = total_SelfDefenseTrainingGrants_UBOB + rs.getInt(56);
                    map.put("total_SelfDefenseTrainingGrants_UBOB", total_SelfDefenseTrainingGrants_UBOB);
                    total_Interest_UBOB = total_Interest_UBOB + rs.getInt(57);
                    map.put("total_Interest_UBOB", total_Interest_UBOB);
                    total_total_total_UBOB = total_total_total_UBOB + total_UEOB;
                    map.put("total_total_total_UBOB", total_total_total_UBOB);

                    map.put("Earned_Interest", rs.getString(59));
                    map.put("Expenditure_Interest", rs.getString(60));
                    map.put("Balance_Interest", rs.getString(61));
                    map.put("RemmitanceAmount_ExpenditureIncurred", rs.getString(62));

                    total_Earned_Interest = total_Earned_Interest + rs.getInt(59);
                    map.put("total_Earned_Interest", total_Earned_Interest);
                    total_Expenditure_Interest = total_Expenditure_Interest + rs.getInt(60);
                    map.put("total_Expenditure_Interest", total_Expenditure_Interest);
                    total_Balance_Interest = total_Balance_Interest + rs.getInt(61);
                    map.put("total_Balance_Interest", total_Balance_Interest);
                    total_RemmitanceAmount_ExpenditureIncurred = total_RemmitanceAmount_ExpenditureIncurred + rs.getInt(62);
                    map.put("total_RemmitanceAmount_ExpenditureIncurred", total_RemmitanceAmount_ExpenditureIncurred);
                    
                    int totalRealse_Realses = rs.getInt(58) + rs.getInt(29) + rs.getInt(30) + rs.getInt(3) + rs.getInt(6) + rs.getInt(24) + rs.getInt(23);
                    map.put("totalRealse_Realses", totalRealse_Realses);
                    int totalRealse_total = totalRealse_Realses + total_EOB + rs.getInt(59);
                    map.put("totalRealse_total", totalRealse_total);
                    
                    total_total_OB = total_total_OB + total_OB;
                    map.put("total_total_OB", total_total_OB);
                    total_totalRealse_Realses = total_totalRealse_Realses + totalRealse_Realses;
                    map.put("total_totalRealse_Realses", total_totalRealse_Realses);
                    total_Earned_Interest_total = total_Earned_Interest_total + total_Earned_Interest;
                    map.put("total_Earned_Interest_total", total_Earned_Interest_total);
                    totalRealse_total_total = totalRealse_total_total + totalRealse_total;
                    map.put("totalRealse_total_total", totalRealse_total_total);
                    
                    int Grand_Total_Expenditure_Incurred_on_Releases = rs.getInt(17) + rs.getInt(18) + rs.getInt(19) + rs.getInt(20) + rs.getInt(21) + rs.getInt(22) + rs.getInt(31) + rs.getInt(32) + rs.getInt(7) + rs.getInt(10) + rs.getInt(26) + rs.getInt(27);
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases", Grand_Total_Expenditure_Incurred_on_Releases);
                    int Grand_Total_Total_Expenditure = total_EOB + Grand_Total_Expenditure_Incurred_on_Releases + rs.getInt(60) + rs.getInt(62) ;
                    map.put("Grand_Total_Total_Expenditure", Grand_Total_Total_Expenditure);
                    int close_balnce_ub_ob = total_OB - total_UEOB;
                    map.put("close_balnce_ub_ob", close_balnce_ub_ob);
                    int close_balnce_ub_rb = totalRealse_Realses - Grand_Total_Expenditure_Incurred_on_Releases;
                    map.put("close_balnce_ub_rb", close_balnce_ub_rb);
                    int close_balnce_ub_ineterest = rs.getInt(59) - rs.getInt(60);
                    map.put("close_balnce_ub_ineterest", close_balnce_ub_ineterest);
                    int close_balnce_ub_total = close_balnce_ub_ob + close_balnce_ub_rb + close_balnce_ub_ineterest - -rs.getInt(62);
                    map.put("close_balnce_ub_total", close_balnce_ub_total);
                    
                    Grand_Total_Expenditure_Incurred_on_Opening_balances = Grand_Total_Expenditure_Incurred_on_Opening_balances + total_EOB;
                    map.put("Grand_Total_Expenditure_Incurred_on_Opening_balances", Grand_Total_Expenditure_Incurred_on_Opening_balances);
                    Grand_Total_Expenditure_Incurred_on_Releases_total = Grand_Total_Expenditure_Incurred_on_Releases_total + Grand_Total_Expenditure_Incurred_on_Releases;
                    map.put("Grand_Total_Expenditure_Incurred_on_Releases_total", Grand_Total_Expenditure_Incurred_on_Releases_total);
                    total_Expenditure_Interest_total = total_Expenditure_Interest_total + total_Expenditure_Interest;
                    map.put("total_Expenditure_Interest_total", total_Expenditure_Interest_total);
                    total_RemmitanceAmount_ExpenditureIncurred_total = total_RemmitanceAmount_ExpenditureIncurred_total + total_RemmitanceAmount_ExpenditureIncurred;
                    map.put("total_RemmitanceAmount_ExpenditureIncurred_total", total_RemmitanceAmount_ExpenditureIncurred_total);
                    Grand_Total_Total_Expenditure_total = Grand_Total_Total_Expenditure_total + Grand_Total_Total_Expenditure;
                    map.put("Grand_Total_Total_Expenditure_total", Grand_Total_Total_Expenditure_total);

                    close_balnce_ub_ob_total = close_balnce_ub_ob_total + close_balnce_ub_ob;
                    map.put("close_balnce_ub_ob_total", close_balnce_ub_ob_total);
                    close_balnce_ub_rb_total = close_balnce_ub_rb_total + close_balnce_ub_rb;
                    map.put("close_balnce_ub_rb_total", close_balnce_ub_rb_total);
                    close_balnce_ub_ineterest_total = close_balnce_ub_ineterest_total + close_balnce_ub_ineterest;
                    map.put("close_balnce_ub_ineterest_total", close_balnce_ub_ineterest_total);
                    close_balnce_ub_total_total = close_balnce_ub_total_total + close_balnce_ub_total;
                    map.put("close_balnce_ub_total_total", close_balnce_ub_total_total);


                    schoollist.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return schoollist;
    }
}
