/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.VCOrganizationForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 1250881
 */
public class VCOrganizationDAO {

    public ArrayList getVCHMConfirmationList(VCOrganizationForm myform) {

        String query = null;
        ArrayList vlist = new ArrayList();
        PreparedStatement st = null, st1 = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = new HashMap();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "  SELECT  b.schcd,b.vocationalTrainerId,b.VocationalTrainerName,b.SectorId,b.SectorName,"
                    + "b.Tradeid,b.TradeName,b.Remarks,b.HM_Remarks,b.DOB,b.Aadhar,b.MobileNo,b.emailId,"
                    + "a.OrganizationType,a.OrganizationId,b.vc_status FROM  VC_APPTable  b left join VC_Organization a"
                    + " on(a.vcTrainerId=b.vocationalTrainerId)  left join VC_Organization_Master c  "
                    + " on(a.organizationId=c.organizationId) where a.organizationId=? and b.HM_Remarks is not null";
            st = con.prepareStatement(query);
            st.setString(1, myform.getVcOrganizationId());
            rs = st.executeQuery();
            if (rs != null) {
//                ResultSetMetaData meta = rs.getMetaData();
//                System.out.println("meta===" + meta.getColumnCount());
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schcd", rs.getString(1));
                    map.put("vctrainerId", rs.getString(2));
                    map.put("vctrainerName", rs.getString(3));
                    map.put("sectorId", rs.getString(4));
                    map.put("sectorName", rs.getString(5));
                    map.put("tradeId", rs.getString(6));
                    map.put("tradeName", rs.getString(7));
                    map.put("appRemarks", rs.getString(8));
                    map.put("hmRemarks", rs.getString(9));
                    map.put("dob", rs.getString(10));
                    map.put("aadhar", rs.getString(11));
                    map.put("mobileno", rs.getString(12));
                    map.put("emailId", rs.getString(13));
                    map.put("orgType", rs.getString(14));
                    map.put("orgId", rs.getString(15));
                    if (rs.getString(16).equalsIgnoreCase("N")) {
                        map.put("vcCheckBox", "disable");
                        map.put("read", "return true");
                    } else {
                        map.put("vcCheckBox", "checked");
                        map.put("read", "return false");
                    }
                    map.put("statusFlag", rs.getString(16));
                    vlist.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return vlist;
    }

    public static String dateTime() {
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        return dateFormat.format(date);
    }

    public String updateRecords(VCOrganizationForm myform) {
        Connection con = null;
        PreparedStatement pstmt = null;
        String hmStatus = null;
        int hmRemark;
        CommonConstants conistants = new CommonConstants();
        try {
            String datetime = dateTime();
//            System.out.println("===dao==stt=" + myform.getVcTrainerIdValue() + "=====" + datetime + "===" + myform.getStatusFlag());
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            String query = "UPDATE VC_APPTable SET vc_status = ?, VC_Status_updatedDate = ? WHERE VocationalTrainerId = ? ";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, myform.getStatusFlag());
            pstmt.setString(2, datetime);
            pstmt.setString(3, myform.getVcTrainerIdValue());
            hmRemark = pstmt.executeUpdate();
            if (hmRemark > 0) {
                hmStatus = myform.getVcTrainerIdValue() + " Approved Successfully";
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    DataBasePlugin.close(con);
                }
                if (con != null) {
                    pstmt.close();
                }
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        return hmStatus;
    }
}
