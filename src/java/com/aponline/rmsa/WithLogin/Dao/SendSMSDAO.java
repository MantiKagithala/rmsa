/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.SendSMSDTO;
import com.aponline.rmsa.WithLogin.Form.SendSMSForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author 1250892
 */
public class SendSMSDAO {

    private static SendSMSDAO sendSMSDAO = null;

    public static SendSMSDAO getInsatnce() {

        if (sendSMSDAO == null) {
            sendSMSDAO = new SendSMSDAO();
        }
        return sendSMSDAO;
    }
    CommonConstants conistants = new CommonConstants();

    public ArrayList getDepartmentContacts(SendSMSForm formBean) throws Exception {

        String query = null;
        ArrayList contactDetails = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map contactMap = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select SNO,SchoolName,Schcd,Designation ,PhoneNo,Email from RMSA_Department_Contacts where Status='Active' and Distcd=? order by Schcd";
            st = con.prepareStatement(query);
            st.setString(1, formBean.getDistrictId());
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    contactMap = new HashMap();
                    contactMap.put("id", rs.getString(1));
                    contactMap.put("name", rs.getString(2));
                    contactMap.put("designation", rs.getString(3));
                    contactMap.put("department", rs.getString(4));
                    contactMap.put("phone", rs.getString(5));
                    contactMap.put("officenumber", rs.getString(5));
                    contactMap.put("email", rs.getString(6));
                    contactDetails.add(contactMap);
                    contactMap = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return contactDetails;

    }

    public SendSMSDTO getTahasildarMobileDetails(String id) throws Exception {
        Connection con = null;
        Statement st = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = null;
        SendSMSDTO sendSMSForm = new SendSMSDTO();

        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select a.PhoneNo,a.SchoolName,a.Designation,a.Designation from RMSA_Department_Contacts a \n"
                    + "                     where a.SNO=? and a.Status='Active'";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    sendSMSForm.setMobileNumber(rs.getString(1));
                    sendSMSForm.setName(rs.getString(2));
                    sendSMSForm.setDesignation(rs.getString(3));
                    sendSMSForm.setDepartment(rs.getString(4));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sendSMSForm;
    }

    public String insertSmsLogDetails(SendSMSDTO sendSMSForm) throws Exception {
        Connection con = null;
        Statement st = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String mobiles = "";
        String query = null;
        PreparedStatement ps = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "insert into RMSA_SmsLog(MessageDescription,Messageto,LoginId,Systemip,senddate,sendstatus,Recevername,Receverdesignation,Receverdepartment) values('" + sendSMSForm.getSms() + "',"
                    + "'" + sendSMSForm.getMobileNumber() + "','" + sendSMSForm.getLoginId() + "','" + sendSMSForm.getSystemIp() + "',getDate(),"
                    + "'" + sendSMSForm.getSendStatus() + "','" + sendSMSForm.getName() + "','" + sendSMSForm.getDesignation() + "','" + sendSMSForm.getDepartment() + "') ";

            ps = con.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mobiles;
    }

    public ArrayList getDepartmentContactsForMail() throws Exception {
        String query = null;
        ArrayList contactDetails = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map contactMap = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select SNO,SchoolName,Schcd,Designation ,PhoneNo,Email from RMSA_Department_Contacts where Status='Active' order by Schcd";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    contactMap = new HashMap();
                    contactMap.put("id", rs.getString(1));
                    contactMap.put("name", rs.getString(2));
                    contactMap.put("designation", rs.getString(3));
                    contactMap.put("department", rs.getString(4));
                    contactMap.put("phone", rs.getString(5));
                    contactMap.put("officenumber", rs.getString(5));
                    contactMap.put("email", rs.getString(6));
                    contactDetails.add(contactMap);
                    contactMap = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return contactDetails;
    }

    public String getEmailFromDepartmentContacts(String id) throws Exception {
        String query = null;
        String email = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select Email from RMSA_Department_Contacts where SNO=? and Status='Active'";
            st = con.prepareStatement(query);
            st.setString(1, id);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    if (rs.getString(1) == null) {
                        email = "-";
                    } else {
                        email = rs.getString(1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return email;
    }

    public int recordEmail(ArrayList<InternetAddress> ToMailsList,
            ArrayList<InternetAddress> CCMailsList,
            ArrayList<InternetAddress> BCCMailsList,
            String subject,
            String body,
            String loginid,
            String system_ip_address,
            String attach1,
            String attach2,
            String attach3,
            boolean result, String filePath1, String filePath2, String filePath3) {
        int k = 0;
        Connection con = null;
        String query = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Boolean store = false;
        String attachments = "";
        String filePath = "";
        String stat;
        String tomails = "";
        String ccmails = "";
        String bccmails = "";

        if (filePath1 != null) {
            filePath += filePath1 + ",";
        }
        if (filePath2 != null) {
            filePath += filePath2 + ",";
        }
        if (filePath3 != null) {
            filePath += filePath3 + ",";
        }

        if (attach1 != null) {
            attachments += attach1 + ",";
        }
        if (attach2 != null) {
            attachments += attach2 + ",";
        }
        if (attach3 != null) {
            attachments += attach3 + ",";
        }

        if (result) {
            stat = "Success";
        } else {
            stat = "Failed";
        }
        for (InternetAddress ia : ToMailsList) {
            tomails += ia.toString() + ",";
        }
        for (InternetAddress ia : CCMailsList) {
            ccmails += ia.toString() + ",";
        }
        for (InternetAddress ia : BCCMailsList) {
            bccmails += ia.toString() + ",";
        }

        try {
            try {
                con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
                query = "insert into RMSA_Emaillog (toemails,cc,bcc,subject,mailbody,filenames,send_date,result,login_id,system_ip_address,FilePath)"
                        + " values('" + tomails + "','" + ccmails + "','" + bccmails + "','" + subject + "','" + body + "','" + attachments + "'"
                        + ",getDate(),'" + stat + "','" + loginid + "','" + system_ip_address + "','" + filePath + "')";
                ps = con.prepareStatement(query);
                k = ps.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (con != null) {
                        con.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return k;
    }
}
