/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;

import java.util.Map;

/**
 *
 * @author 1250892
 */
public class CommonDAO {

    public String getMandalName(String mandalId) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String districtName = null;
        String sql = null;
        CommonConstants conistants = new CommonConstants();
        try {

            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (mandalId.equalsIgnoreCase("281499")) {
//                sql = "select 'YANAM' blkname";
                districtName = "YANAM";
            } else {

                sql = "select blkname from STEPS_BLOCK where blkcd=?";
                st = con.prepareStatement(sql);
                st.setString(1, mandalId);
                rs = st.executeQuery();

                if (rs != null) {
                    while (rs.next()) {
                        districtName = rs.getString(1);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return districtName;
    }

    public String getDistrictname(String districtId) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String districtName = null;
        String sql = null;
        CommonConstants conistants = new CommonConstants();
        try {

            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            sql = "select distname from STEPS_DISTRICT where DISTCD=? order by distname";
            st = con.prepareStatement(sql);
            st.setString(1, districtId);
            rs = st.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    districtName = rs.getString(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return districtName;
    }

    public String getVillagename(String villageid) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String villagename = null;
        String sql = null;
        CommonConstants conistants = new CommonConstants();
        try {

            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            sql = "select vilname from STEPS_VILLAGE where vilcd=? order by vilname";
            st = con.prepareStatement(sql);
            st.setString(1, villageid);
            rs = st.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    villagename = rs.getString(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return villagename;
    }

    public ArrayList getDistrictDetails() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select distcd,distname from STEPS_DISTRICT order by distname";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("distCode", rs.getString(1));
                    map.put("distname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;

    }

    public ArrayList getCastDetails() throws Exception {
        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select caste_id,caste_desc from STEPS_CASTE order by caste_id";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("castecode", rs.getString(1));
                    map.put("castename", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public ArrayList getMandalDetails(String distCode) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select blkcd,blkname from dbo.STEPS_BLOCK where blkcd like ? order by blkname ";
            st = con.prepareStatement(query);
            st.setString(1, distCode+"%");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("mndCode", rs.getString(1));
                    map.put("mndname", rs.getString(2));
                    mandalList.add(map);
                }
                if (distCode != null && distCode.equalsIgnoreCase("2814")) {
                    map = new HashMap();
                    map.put("mndCode", "281499");
                    map.put("mndname", "YANAM");
                    mandalList.add(map);
                }
                map = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return mandalList;
    }

    public ArrayList getVillageDetails(String mndCode) throws Exception {
        Map map = null;
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (mndCode != null && mndCode.equalsIgnoreCase("281499")) {
                map = new HashMap();
                map.put("vcode", "281499999");
                map.put("vname", "YANAM");
                mandalList.add(map);

            } else {
                query = "select vilcd,vilname from dbo.STEPS_VILLAGE where vilcd like ? order by vilname";
                st = con.prepareStatement(query);
                st.setString(1, mndCode+"%");
                rs = st.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        map = new HashMap();
                        map.put("vcode", rs.getString(1));
                        map.put("vname", rs.getString(2));
                        mandalList.add(map);
                        map = null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }

    public ArrayList getSchoolDetails(String mndCode) throws Exception {
        Map map = null;
        String query = null;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select a.schcd,a.schcd+'-'+b.schname from steps_master a join steps_school b on a.schcd=b.schcd where a.schcd like ? and schstatus='0' and schcat<>'11'";
            st = con.prepareStatement(query);
            st.setString(1, mndCode+"%");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sccode", rs.getString(1));
                    map.put("scname", rs.getString(2));
                    mandalList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;

    }

    public ArrayList getclassDetails(String schcd) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        int lowclass = 0;
        int highclass = 0;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select lowclass,highclass from steps_master where schcd=?";
            st = con.prepareStatement(query);
            st.setString(1, schcd);
            rs = st.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    lowclass = rs.getInt(1);
                    highclass = rs.getInt(2);
                }
            }
//            for (int c = lowclass; c <= highclass; c++) {
//            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }

    public String mandalName(String mandalId) throws Exception {

        String query = null;
        String mandalName = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select blkname from dbo.STEPS_BLOCK where blkcd= ? order by blkname";
            st = con.prepareStatement(query);
            st.setString(1, mandalId);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mandalName = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return mandalName;

    }

    public String getpanchayatDetails(String mandalCode) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        String namesList = "<option value='00'>--select--</option>";
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            pst = con.prepareStatement("SELECT [PANCD],[PANNAME]  FROM [dbo].[STEPS_PANCHAYAT] where [PANCD] like ? order by PANCD asc");
            pst.setString(1, mandalCode + "%");
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    namesList = namesList + "<option value='" + rs.getString(1) + "'>" + rs.getString(2) + "</option>";
                }
            }
        } catch (Exception e) {
            //System.out.println("Exception in AjaxAction.java at getMandalDetails()##" + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return namesList;

    }

    public String getSchoolList(String panchayathCode) {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        String namesList = "<option value='00'>--select--</option>";
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            pst = con.prepareStatement("select a.schcd,a.schcd+'-'+b.schname from steps_master a join steps_school b on a.schcd=b.schcd where a.schcd like ? order by a.schcd asc");
            pst.setString(1, panchayathCode.trim() + "%");
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    namesList = namesList + "<option value='" + rs.getString(2) + "'>" + rs.getString(2) + "</option>";
                }
            }
            if (namesList == null) {
                namesList = "NoData";
            }
        } catch (Exception e) {
            // System.out.println("Exception in AjaxAction.java at getVillageDetails()##" + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return namesList;
    }

    public ArrayList SchoolCodeListNew(String villageCode) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "SELECT [SCHCD],[SCHNAME]  FROM [sims].[dbo].[STEPS_MASTER_NEW]"
                    + "  where villageid=? and schstatus='0' and schcat<>'11' and SCHMGT in ('33','10','11','31') order by SCHNAME ";
            st = con.prepareStatement(query);
            st.setString(1, villageCode);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Map map = new HashMap();
                    map.put("sccode", rs.getString(1));
                    map.put("scname", rs.getString(1) + "-" + rs.getString(2));
                    mandalList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return mandalList;
    }

    public ArrayList SchoolCodeListForDseVacancy(String villageCode) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "SELECT a.SCHCD,b.[SCHNAME],c.SCHMGT_DESC  FROM STEPS_MASTER a join STEPS_School b\n"
                    + "on a.schcd=b.SCHCD join STEPS_SCHMGT c on a.SCHMGT=c.SCHMGT_ID\n"
                    + "where a.VILCD=? and schstatus='0' and schcat<>'11' \n"
                    + "and  a.ac_year='2015-16' ";

            /* if(villageCode!=null && villageCode.substring(0, 4).equals("2823")){
             query= query + " and SCHMGT in ('33','10','11','34','31') order by b.SCHNAME ";
             }
             else{
             query= query + " and SCHMGT in ('33','10','11','34') order by b.SCHNAME ";
             }*/
            query = query + " and SCHMGT in ('33','10','11','34') order by b.SCHNAME ";
            st = con.prepareStatement(query);
            st.setString(1, villageCode);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Map map = new HashMap();
                    map.put("sccode", rs.getString(1));
                    map.put("scname", rs.getString(1) + "-" + rs.getString(2));
                    map.put("mgmt", rs.getString(3));
                    mandalList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return mandalList;
    }

    public String getSchoolMgmtName(String schcd) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        String mgmtName = null;
        PreparedStatement st = null;
        ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            query = "SELECT a.SCHCD,c.SCHMGT_DESC  FROM [STEPS_MASTER] a \n"
//                    + "join STEPS_SCHMGT c on a.SCHMGT=c.SCHMGT_ID\n"
//                    + "where a.schcd='" + schcd + "' and schstatus='0' and schcat<>'11' \n"
//                    + " and SCHMGT in ('33','10','11','31','34')  ";
            query = "SELECT a.SCHCD,c.SCHMGT_DESC  FROM [STEPS_MASTER] a \n"
                    + "join STEPS_SCHMGT c on a.SCHMGT=c.SCHMGT_ID\n"
                    + "where a.schcd=? and schstatus='0' and schcat<>'11' \n"
                    + " and SCHMGT in ('33','10','11','31','34')  ";
            st = con.prepareStatement(query);
            st.setString(1, schcd);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mgmtName = rs.getString(2);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return mgmtName;
    }

    public String getSchoolCategory(String schcd) throws Exception {
        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        String cat = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            query = "SELECT schcat  FROM [STEPS_MASTER] a \n"
//                    + "where a.schcd='" + schcd + "' and schstatus='0' and schcat<>'11' ";
            query = "SELECT schcat  FROM [STEPS_MASTER] a \n"
                    + "where a.schcd=? and schstatus='0' and schcat<>'11' ";
            st = con.prepareStatement(query);
            st.setString(1, schcd);
            rs = st.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    cat = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return cat;
    }

    public String getCaste(String castid) throws Exception {
        String query = null;
        ResultSet rs = null;
        Statement st = null;
        Connection con = null;
        PreparedStatement stmt = null;
         ArrayList mandalList = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        String castname = null;
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            //st = con.createStatement();
            //query = "select caste_desc from STEPS_CASTE where caste_id = '" + castid + "' ";
            //rs = st.executeQuery(query);
            query = "select caste_desc from STEPS_CASTE where caste_id = '" + castid + "' ";
            stmt = con.prepareStatement(query);
            stmt.setString(1, castid);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    castname = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return castname;
    }

    public String getSchoolName(String id) throws Exception {
        String scname = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String query = null;
        PreparedStatement stmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            st = con.createStatement();
//            query = "select a.schcd+'-'+b.schname from steps_master a\n"
//                    + "join steps_school b on(a.schcd=b.schcd)\n"
//                    + " where a.schcd='" + id + "'";
            //rs = st.executeQuery(query);
            query = "select a.schcd+'-'+b.schname from steps_master a\n"
                    + "join steps_school b on(a.schcd=b.schcd)\n"
                    + " where a.schcd=?";
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    scname = rs.getString(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return scname;
    }

    public ArrayList getAcList() {


        int year = Calendar.getInstance().get(Calendar.YEAR);

        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        ArrayList al = new ArrayList();
        Map map = new HashMap();


        for (int i = 2014; i < year; i++) {
            al.add((i) + "-" + String.valueOf(i + 1).substring(2));
            // map.put((i) + "-" + String.valueOf(i + 1).substring(2), (i) + "-" + String.valueOf(i + 1).substring(2));
            // System.out.println("Financial Year : " + (year - 1) + "-" + String.valueOf(year).substring(2));

        }
        if (month > 3) {
            // System.out.println("Financial Year : " + year + "-" + String.valueOf((year + 1)).substring(2));
            al.add((year) + "-" + String.valueOf(year + 1).substring(2));
            //map.put((year) + "-" + String.valueOf(year + 1).substring(2), (year) + "-" + String.valueOf(year + 1).substring(2));
        }

        // al.add(map);
        //System.out.println("Financial month : " + month);    

        // System.out.println("al::" + al);
        return al;
    }

    public ArrayList managementSchoolDetails(String mndCode) throws Exception {

        String query = null;
        ArrayList mandalList = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CommonConstants conistants = new CommonConstants();

        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            query = "select a.schcd,a.schcd+'-'+b.schname from steps_master a "
//                    + "join steps_school b on a.schcd=b.schcd where"
//                    + " a.schcd like '" + mndCode + "%' and schstatus='0' and schcat<>'11' and SCHMGT in('10','11','33') ";
            query = "select a.schcd,a.schcd+'-'+b.schname from steps_master a "
                    + "join steps_school b on a.schcd=b.schcd where"
                    + " a.schcd like ? and schstatus='0' and schcat<>'11' and SCHMGT in('10','11','33') ";
            st = con.prepareStatement(query);
            st.setString(1, mndCode+"%");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sccode", rs.getString(1));
                    map.put("scname", rs.getString(2));
                    mandalList.add(map);
                    map = null;
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return mandalList;

    }

//     public ArrayList getSchoolCategoryWise(String vilcd, String roleID) throws Exception {
//
//        String query = null;
//        ArrayList mandalList = new ArrayList();
//        PreparedStatement st = null;
//        Connection con = null;
//        ResultSet rs = null;
//           CommonConstants conistants = new CommonConstants();
//        ArrayList getSchoolList = new ArrayList();
//        HashMap map = null;
//
//        try {
//            con = DataBasePlugin.getEISTEACHER(conistants.DBCONNECTION);
//
//
//            if (roleID.equals("28")) {
//                query = "SELECT a.SCHCD,schname  FROM [STEPS_MASTER] a \n"
//                        + "join steps_school b on a.schcd = b.schcd\n"                     
//                        + "where left(a.schcd,6) = '" + vilcd + "' and schstatus='0' and schcat<>'11' \n"
//                        + " and SCHMGT in ('33','10','11') and schcat in ('3','5','6','7','8','10') ";
//
//            } else {
//                query = "SELECT a.SCHCD,schname  FROM [STEPS_MASTER] a \n"
//                        + "join steps_school b on a.schcd = b.schcd\n"                     
//                        + "where left(a.schcd,6) = '" + vilcd + "' and schstatus='0' and schcat<>'11' \n"
//                        + " and SCHMGT in ('33','10','11') and schcat not in ('3','5','6','7','8','10') ";
//
//            }
//
//
//
//            st = con.prepareStatement(query);
//            rs = st.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    map = new HashMap();
//                    map.put("SCHCD", rs.getString(1));
//                    map.put("schName", rs.getString(1) + "-" + rs.getString(2));
//                    getSchoolList.add(map);
//                }
//
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (con != null) {
//                    con.close();
//                }
//                if (st != null) {
//                    st.close();
//                }
//                if (rs != null) {
//                    rs.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//
//
//        return getSchoolList;
//
//    }
//      public ArrayList getSchoolMgmtSchools(String vilcd) throws Exception {
//
//        String query = null;
//        ArrayList mandalList = new ArrayList();
//        PreparedStatement st = null;
//        Connection con = null;
//        ResultSet rs = null;
//         CommonConstants conistants = new CommonConstants();
//        ArrayList getSchoolList = new ArrayList();
//        HashMap map = null;
//
//        try {
//            con = DataBasePlugin.getEISTEACHER(commonConistants.KEY);
//            query = "SELECT a.SCHCD,schname  FROM [STEPS_MASTER] a \n"
//                        + "join steps_school b on a.schcd = b.schcd\n"                       
//                        + "where left(a.schcd,6) = '" + vilcd + "' and schstatus='0' and schcat<>'11' \n"
//                        + " and SCHMGT in ('33','10','11') ";
//
//            st = con.prepareStatement(query);
//            rs = st.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    map = new HashMap();
//                    map.put("SCHCD", rs.getString(1));
//                    map.put("schName", rs.getString(1) + "-" + rs.getString(2));
//                    getSchoolList.add(map);
//                }
//
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (con != null) {
//                    con.close();
//                }
//                if (st != null) {
//                    st.close();
//                }
//                if (rs != null) {
//                    rs.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//
//
//        return getSchoolList;
//
//    }
//       public ArrayList getMandalDetailsDyEo(String userName) throws Exception {
//        String query = null;
//        ArrayList mandalList = new ArrayList();
//        PreparedStatement st = null;
//        Connection con = null;
//        Map map = null;
//        ResultSet rs = null;
//           CommonConstants conistants = new CommonConstants();
//        try {
//            con = DataBasePlugin.getEISTEACHER(commonConistants.KEY);
//            query = "SELECT [BLKCD]\n"
//                    + "      ,[BLKNAME]     \n"
//                    + "  FROM [DeputyEOMandalMapping] where [Loginid]='" + userName + "'  order by BLKNAME";
//            st = con.prepareStatement(query);
//            rs = st.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    map = new HashMap();
//                    map.put("mndCode", rs.getString(1));
//                    map.put("mndname", rs.getString(2));
//                    mandalList.add(map);
//                    map = null;
//                }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (con != null) {
//                    con.close();
//                }
//                if (st != null) {
//                    st.close();
//                }
//                if (rs != null) {
//                    rs.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            System.gc();
//        }
//
//
//        return mandalList;
//
//    }
    //Sudheer
//         public boolean getNgoDetails(NgonamesForm reportForm) throws Exception {
//             
//        PreparedStatement st = null;
//        Connection con = null;
//        String query = null;
//        int value = 0;
//        ArrayList list= null;
//            CommonConstants conistants = new CommonConstants();
//     try{
//         
//              con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//             query = "insert into [NGOSWorkShop] values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),getDate())";
//             st = con.prepareStatement(query);
//             st.setString(1, reportForm.getNgoName());
//             st.setString(2, reportForm.getRegnumber());
//             st.setString(3, reportForm.getEstYear());
//             st.setString(4, reportForm.getPresidentName());
//             st.setString(5, reportForm.getMobilenum());
//             st.setString(6, reportForm.getEmailid());
//             st.setString(7, reportForm.getAddress());
//             st.setString(8, reportForm.getWorkexperience());
//             st.setString(9, reportForm.getInterestedArea());
//             st.setString(10, reportForm.getNoOfQualifiedStaff());
//             st.setString(11, reportForm.getAdministratvieMechanism());
//             st.setString(12, reportForm.getCwSNActivities());
//             st.setString(13, reportForm.getSpecialServices());
//             st.setString(14, reportForm.getOrganizationTargets());
//             st.setString(15, reportForm.getIedssActivities());
//             st.setString(16, reportForm.getApRMSASupport());
//             st.executeUpdate();
//               
//        }catch (Exception e) {
//            
//            e.printStackTrace();
//            return false;
//        }
//     
//         return true;
//     
//       }
//        public ArrayList getCheckingNgoDetails(NgonamesForm reportForm) throws Exception {
//             
//        PreparedStatement st = null;
//        Connection con = null;
//        String query = null;
//        int value = 0;
//        ArrayList list= new ArrayList();
//        ResultSet rs = null;
//         CommonConstants conistants = new CommonConstants();
//       
//        try{
//             con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//             query = "select RegistrationNumber from [NGOSWorkShop] where RegistrationNumber = ? ";
//            
//             st = con.prepareStatement(query);
//             st.setString(1, reportForm.getRegnumber());
//             rs = st.executeQuery();
//            // System.out.println("query=========="+query);
//            // System.out.println("reportForm.getRegnumber()========"+reportForm.getRegnumber());
//             if (rs != null) {
//               while (rs.next()) {
//                    reportForm= new NgonamesForm();
//                    reportForm.setRegnumber(rs.getString(1));
//                    list.add(reportForm);
//                    System.out.println("this is the reg numbers: "+list);
//                }
//
//            }
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//     
//         return list;
//     
//       }
    public ArrayList geNgoDetailsReport() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();

        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            query = "select * from NGOSWorkShop";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Map map = new HashMap();
                    map.put("Name", rs.getString(1));
                    map.put("RegistrationNumber", rs.getString(2));
                    map.put("Year", rs.getString(3));
                    map.put("presidentName", rs.getString(4));
                    map.put("mobileNo", rs.getString(5));
                    map.put("Emailid", rs.getString(6));
                    map.put("ADDRESS", rs.getString(7));
                    map.put("WorkExperiance", rs.getString(8));
                    map.put("Intrestedarea", rs.getString(9));
                    map.put("Qualifiedstaff", rs.getString(10));
                    map.put("AdministrativeMechanism", rs.getString(11));
                    map.put("CwsnActivities", rs.getString(12));
                    map.put("SpecialService", rs.getString(13));
                    map.put("targetGoal", rs.getString(14));
                    map.put("IedssActivities", rs.getString(15));
                    map.put("support", rs.getString(16));
//                    map.put("createdDate", rs.getString(17));
//                    map.put("updatedDate", rs.getString(18));
                    // map.put("IedssActivities", rs.getString(20));

                    //map.put("CreatedSince", rs.getString(11));
                    list.add(map);
                    // System.out.println("listlistlistlist======="+list);
                    // map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.gc();
        }
        return list;
    }

    public String getDistrcitIDMR(String claimID) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String district = null;
        String sql = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            sql = "select DistrictId from Employees_Claim_Details where Rowid=?";
            st = con.prepareStatement(sql);
            st.setString(1, claimID);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    district = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return district;
        }
    }

    //omkaram
    public ArrayList getMandalDetails1(String distCode) throws Exception {
        String query = null;
        ArrayList mandalList = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        Map map = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            //query = "select blkcd,blkname from dbo.STEPS_BLOCK where blkcd like '" + distCode + "%' order by blkname ";
//            query = " select distinct left(UDISE_CODE,6) as blkcd ,blkname from RMSAMaster,STEPS_BLOCK where left(UDISE_CODE,4)='" + distCode + "' and left(UDISE_CODE,6)=blkcd";
            query = " select distinct left(UDISE_CODE,6) as blkcd ,blkname from RMSAMaster,STEPS_BLOCK where left(UDISE_CODE,4)=? and left(UDISE_CODE,6)=blkcd";
            st = con.prepareStatement(query);
            st.setString(1, distCode);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("mndCode", rs.getString(1));
                    map.put("mndname", rs.getString(2));
                    mandalList.add(map);
                }
                if (distCode != null && distCode.equalsIgnoreCase("2814")) {
                    map = new HashMap();
                    map.put("mndCode", "281499");
                    map.put("mndname", "YANAM");
                    mandalList.add(map);
                }
                map = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }

    public ArrayList getVillageDetails1(String mndCode) throws Exception {
        String query = null;
        ArrayList mandalList = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (mndCode != null && mndCode.equalsIgnoreCase("281499")) {
                map = new HashMap();
                map.put("vcode", "281499999");
                map.put("vname", "YANAM");
                mandalList.add(map);

            } else {
                //query = "select vilcd,vilname from dbo.STEPS_VILLAGE where vilcd like'" + mndCode + "%' order by vilname";
//                query = "select distinct left(UDISE_CODE,9) as vilcd ,vilname from RMSAMaster,STEPS_VILLAGE where left(UDISE_CODE,6)='"+mndCode+"' and left(UDISE_CODE,9)=vilcd";
                query = "select distinct left(UDISE_CODE,9) as vilcd ,vilname from RMSAMaster,STEPS_VILLAGE where left(UDISE_CODE,6)=? and left(UDISE_CODE,9)=vilcd";
                st = con.prepareStatement(query);
                st.setString(1, mndCode);
                rs = st.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        map = new HashMap();
                        map.put("vcode", rs.getString(1));
                        map.put("vname", rs.getString(2));
                        mandalList.add(map);
                        map = null;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }
}
