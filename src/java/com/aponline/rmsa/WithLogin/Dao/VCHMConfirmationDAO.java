/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.VCHMConfirmationForm;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.database.DataBasePlugin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 1250881
 */
public class VCHMConfirmationDAO {

    public ArrayList getVCHMConfirmationList(VCHMConfirmationForm myform) {

        String query = null;
        ArrayList vlist = new ArrayList();
        PreparedStatement st = null, st1 = null;
        Connection con = null;
        ResultSet rs = null, rs1 = null;
        CommonConstants conistants = new CommonConstants();
        Map map = new HashMap();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            if (myform.getVcTrainerId() == null || myform.getVcTrainerId() == "") {
                query = "select * from VC_APPTable where schcd =? ";
                st = con.prepareStatement(query);
                st.setString(1, myform.getSchoolId());
            } else {
                query = "select * from VC_APPTable where schcd =? and VocationalTrainerId =?";
                st = con.prepareStatement(query);
                st.setString(1, myform.getSchoolId());
                st.setString(2, myform.getVcTrainerId());
            }
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("schcd", rs.getString(1));
                    map.put("vctrainerId", rs.getString(2));
                    map.put("vctrainerName", rs.getString(3));
                    map.put("sectorId", rs.getString(4));
                    map.put("sectorName", rs.getString(5));
                    map.put("tradeId", rs.getString(6));
                    map.put("tradeName", rs.getString(7));
                    map.put("firstQAns", rs.getString(8));
                    map.put("secondQAns", rs.getString(9));
                    map.put("thirdQAns", rs.getString(10));
                    map.put("fourthQAns", rs.getString(11));
                    map.put("fifthQAns", rs.getString(12));
                    map.put("sixthQAns", rs.getString(13));
                    map.put("seventhQAns", rs.getString(14));
                    map.put("eightQAns", rs.getString(15));
                    map.put("ninethQAns", rs.getString(16));
                    map.put("tenthQAns", rs.getString(17));
                    map.put("remarks", rs.getString(18));
                    map.put("aadharno", rs.getString(19));
                    map.put("mobileno", rs.getString(20));
                    map.put("emailId", rs.getString(21));
                    map.put("dob", rs.getString(22));
                    map.put("year", rs.getString(23));
                    if (rs.getString(24) == null || rs.getString(24) == "") {
                        map.put("hmRemark", "0");
                    } else {
                        map.put("hmRemark", rs.getString(24));
                    }
                    if (rs.getString(38) == null || rs.getString(38) == "") {
                        map.put("hmRemarksText", "0");
                    } else {
                        map.put("hmRemarksText", rs.getString(38));
                    }
                   
                    vlist.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return vlist;
    }

    public static String dateTime() {
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        return dateFormat.format(date);
    }

    public String vcHMStatusUpdate(VCHMConfirmationForm myform) {
        Connection con = null;
        PreparedStatement pstmt = null;
        String hmStatus = null;
        int hmRemark;
        CommonConstants conistants = new CommonConstants();
        try {
//            System.out.println("da===sch=="+myform.getSchoolId()+"==vct=="+myform.getvTrainerId()+"==hmfeed=="+myform.getVcHMremarks()+"==rema=="+myform.getVcHMRemarksText());

            String datetime = dateTime();
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            String query = "UPDATE VC_APPTable SET HM_remarks = ?, HM_RemarksText = ?, UpdatedDate = ? WHERE schcd = ? and VocationalTrainerId= ? ";
            pstmt = con.prepareStatement(query);
           
            pstmt.setString(1, myform.getVcHMRemarksFeedbackValue());
             pstmt.setString(2, myform.getVcHMRemarksValue());
            pstmt.setString(3, datetime);
            pstmt.setString(4, myform.getSchoolId());
            pstmt.setString(5, myform.getvTrainerId());
            hmRemark = pstmt.executeUpdate();
            if (hmRemark > 0) {
                hmStatus = myform.getSchoolId() + " Approved Successfully";
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    DataBasePlugin.close(con);
                }
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        return hmStatus;
    }
}
