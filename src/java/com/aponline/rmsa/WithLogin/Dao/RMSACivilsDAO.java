/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Dao;

import com.aponline.rmsa.WithLogin.Form.RMSAComponentForm;
import com.aponline.rmsa.WithLogin.Form.RMSAComponentsForm;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import com.aponline.rmsa.database.DataBasePlugin;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.struts.upload.FormFile;
import com.aponline.rmsa.commons.CommonConstants;
import com.aponline.rmsa.commons.CommonDetails;
import java.math.RoundingMode;
import javax.sql.DataSource;

/**
 *
 * @author 1259084
 */
public class RMSACivilsDAO {

    /// Omkaram addded 20180222
    public List getPhaseList() {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String query = null;
        HashMap map = null;
        ArrayList phaseList = new ArrayList();
        String phaselist = "<option value='0'>Select Phase</option>";
        try {
            con = DataBasePlugin.getConnection(CommonConstants.DBCONNECTION);
            query = "select distinct(PHASE) from RMSAMASTER order by PHASE";
            pst = con.prepareStatement(query);
            rs = pst.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("PhaseNo", rs.getString(1));
                    String phaseName = "Phase-" + rs.getString(1);
                    map.put("phaseName", phaseName);
                    phaseList.add(map);
                    phaselist = phaselist + "<option value='" + rs.getString(1) + "'>" + phaseName + "</option>";
                    map = null;
                    phaseName = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    DataBasePlugin.close(con);
                }
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return phaseList;
    }

    /// Omkaram addded 20180222
    public List getDistrictList(String phaseNumber) {
        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//            query = "select  distinct left(UDISE_CODE,4) distcode,District from RMSAMaster where PHASE='" + phaseNumber + "' order by District";
            query = "select  distinct left(UDISE_CODE,4) distcode,District from RMSAMaster where PHASE=? order by District";
            st = con.prepareStatement(query);
            st.setString(1, phaseNumber);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("distCode", rs.getString(1));
                    map.put("distname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static ArrayList getDistrictList1(String phaseNumber) throws Exception {
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        ArrayList district_info = new ArrayList();
        ArrayList district_id_list = new ArrayList();
        ArrayList district_name_list = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//          query = "select  distinct left(UDISE_CODE,4) distcode,District from RMSAMaster where PHASE='" + phaseNumber + "' order by District";
            query = "select  distinct left(UDISE_CODE,4) distcode,District from RMSAMaster where PHASE=? order by District";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, phaseNumber);
            //stmt = con.createStatement();
            rs = pstmt.executeQuery();
            while (rs.next()) {
                district_id_list.add(rs.getString(1));
                district_name_list.add(rs.getString(2));
            }
        } catch (Exception e) {
        } finally {
            district_info.add(district_name_list);
            district_info.add(district_id_list);
            if (con != null) {
                con.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return district_info;
    }

    public ArrayList getMandalDetails(String distCode, String phaseNumber) throws Exception {
        String query = null;
        ArrayList mandalList = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        Map map = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            //query = "select distinct left(udise_code,6)mandalcd,block from RMSAMaster where PHASE='" + phaseNumber + "' and left(udise_code,4) like '" + distCode + "%' order by block ";
            query = "select distinct left(udise_code,6)mandalcd,block from RMSAMaster where PHASE=? and left(udise_code,4) like ? order by block ";
            st = con.prepareStatement(query);
            st.setString(1, phaseNumber);
            st.setString(2, distCode + "%");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("mndCode", rs.getString(1));
                    map.put("mndname", rs.getString(2));
                    mandalList.add(map);

                }
                if (distCode != null && distCode.equalsIgnoreCase("2814")) {
                    map = new HashMap();
                    map.put("mndCode", "281499");
                    map.put("mndname", "YANAM");
                    mandalList.add(map);
                }
                map = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mandalList;
    }

    public static ArrayList getMandalList1(String distCode, String phaseNumber) throws Exception {
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        Statement stmt = null;
        PreparedStatement st = null;
        ArrayList mandal_info = new ArrayList();
        ArrayList mandal_id_list = new ArrayList();
        ArrayList mandal_name_list = new ArrayList();
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
//          query = "select distinct left(udise_code,6)mandalcd,block from RMSAMaster where PHASE='" + phaseNumber + "' and left(udise_code,4) like '" + distCode + "%' order by block ";
            query = "select distinct left(udise_code,6)mandalcd,block from RMSAMaster where PHASE=? and left(udise_code,4) like ? order by block ";
            st = con.prepareStatement(query);
            st.setString(1, phaseNumber);
            st.setString(2, distCode + "%");
            //stmt = con.createStatement();
            //rs = stmt.executeQuery(query);
            rs = st.executeQuery();
            while (rs.next()) {
                mandal_id_list.add(rs.getString(1));
                mandal_name_list.add(rs.getString(2));
            }
        } catch (Exception e) {
        } finally {
            mandal_info.add(mandal_name_list);
            mandal_info.add(mandal_id_list);
            if (con != null) {
                con.close();
            }
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return mandal_info;
    }

    public ArrayList getRmsaMasterData(String mandalCode, String phaseNumber) throws Exception {
        String query = null;
        ArrayList rmsamasterDataList = new ArrayList();
        PreparedStatement st = null;
        Connection con = null;
        Map map = null;
        ResultSet rs = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            //query = "select SNO,PHASE,UDISE_CODE,SchoolAsPerUdise,v.VILNAME ,Totalcivilsanction,TOTALFURNITURE_IN_LAKHS,labequipment,FINANCIAL_SANC_IN_LAKHS,Releases from RMSAMaster, STEPS_VILLAGE v \n"
            //        + "where left(UDISE_CODE,9)=v.VILCD and PHASE='" + phaseNumber + "' and left(udise_code,6) like '" + mandalCode + "%' order by block ";
            query = "select SNO,PHASE,UDISE_CODE,SchoolAsPerUdise,v.VILNAME ,Totalcivilsanction,TOTALFURNITURE_IN_LAKHS,labequipment,FINANCIAL_SANC_IN_LAKHS,Releases from RMSAMaster, STEPS_VILLAGE v \n"
                    + "where left(UDISE_CODE,9)=v.VILCD and PHASE=? and left(udise_code,6) like ? order by block";
            st = con.prepareStatement(query);
            st.setString(1, phaseNumber);
            st.setString(2, mandalCode + "%");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("SNO", rs.getString(1));
                    map.put("PHASE", rs.getString(2));
                    map.put("UDISE_CODE", rs.getString(3));
                    map.put("SchoolAsPerUdise", rs.getString(4));
                    map.put("VILNAME", rs.getString(5));
                    if (rs.getString(6) == null) {
                        map.put("Totalcivilsanction", "0.00");
                    } else {
                        BigDecimal totalcivilsanction1 = new BigDecimal(rs.getString(6));
                        BigDecimal totalcivilsanction = totalcivilsanction1.setScale(2, RoundingMode.HALF_UP);
                        map.put("Totalcivilsanction", totalcivilsanction);
                    }
                    if (rs.getString(7) == null) {
                        map.put("TOTALFURNITURE_IN_LAKHS", "0.00");
                    } else {
                        BigDecimal tOTALFURNITURE_IN_LAKHS1 = new BigDecimal(rs.getString(7));
                        BigDecimal tOTALFURNITURE_IN_LAKHS = tOTALFURNITURE_IN_LAKHS1.setScale(2, RoundingMode.HALF_UP);
                        map.put("TOTALFURNITURE_IN_LAKHS", tOTALFURNITURE_IN_LAKHS);
                    }
                    if (rs.getString(8) == null) {
                        map.put("labequipment", "0.00");
                    } else {
                        BigDecimal labequipment1 = new BigDecimal(rs.getString(8));
                        BigDecimal labequipment = labequipment1.setScale(2, RoundingMode.HALF_UP);
                        map.put("labequipment", labequipment);
                    }
                    if (rs.getString(9) == null) {
                        map.put("FINANCIAL_SANC_IN_LAKHS", "0.00");
                    } else {
                        BigDecimal fINANCIAL_SANC_IN_LAKHS1 = new BigDecimal(rs.getString(9));
                        BigDecimal fINANCIAL_SANC_IN_LAKHS = fINANCIAL_SANC_IN_LAKHS1.setScale(2, RoundingMode.HALF_UP);
                        map.put("FINANCIAL_SANC_IN_LAKHS", fINANCIAL_SANC_IN_LAKHS);
                    }
                    if (rs.getString(10) == null) {
                        map.put("Releases", "0.00");
                    } else {
                        BigDecimal releases1 = new BigDecimal(rs.getString(10));
                        BigDecimal releases = releases1.setScale(2, RoundingMode.HALF_UP);
                        map.put("Releases", releases);
                    }
                    rmsamasterDataList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rmsamasterDataList;
    }

    public String updateBudgetRelease(String sno, String user, String totalcivilsanction, String totalFurnitureInLakhs, String labequipment, String financialSancInLakhs, String releases) {
        String myreturn = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        CommonConstants conistants = new CommonConstants();
        try {
            con = DataBasePlugin.getConnection(conistants.DBCONNECTION);
            cstmt = con.prepareCall("{call [dbo].[RMSA_Budget_Update](?,?,?,?,?,?,?)}");
            cstmt.setString(1, sno);
            cstmt.setString(2, user);
            cstmt.setString(3, totalcivilsanction);
            cstmt.setString(4, totalFurnitureInLakhs);
            cstmt.setString(5, labequipment);
            cstmt.setString(6, financialSancInLakhs);
            cstmt.setString(7, releases);
            //System.out.println("sno " + sno + " user " + user + " totalcivilsanction " + totalcivilsanction + " totalFurnitureInLakhs " + totalFurnitureInLakhs + " labequipment " + labequipment + " financialSancInLakhs " + financialSancInLakhs + " releases" + releases);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    myreturn = rs.getString(1).toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myreturn;
    }
}
