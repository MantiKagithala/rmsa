/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.SendSMSDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.aponline.rmsa.WithLogin.Form.SendMailForm;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author 567999
 */
public class SendMailAction extends DispatchAction {

    String target = "success";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SendMailForm sendMailForm = (SendMailForm) form;
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("userName") != null) {
                sendMailForm.setOfficerType(null);
                sendMailForm.setSms(null);
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward sendMail(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // SendSMSService sendSMSService = SendSMSServiceFactory.getSendSMSServiceImpl();
        SendMailForm formBean = (SendMailForm) form;
        String target = null;
        File send_dir = null;
        List<String> uploadFile = new ArrayList();
        String att = null;
        ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        String[] attachFiles = new String[3];
        InternetAddress temailId = null;
        InternetAddress demailId = null;
        InternetAddress distemailId = null;
        InternetAddress deptemailId = null;
        String filename1 = null;
        String filename2 = null;
        String filename3 = null;
        // CCMailsList.add(new InternetAddress("vijaykumardevu@gmail.com"));
        HttpSession session = request.getSession();
        String sesionTokenCode = null;
        String requestTokenCode = null;
        SendSMSDAO dao = new SendSMSDAO();
        try {
            int file1 = formBean.getUploadFile1().getFileSize() / 1024;
            int file2 = formBean.getUploadFile2().getFileSize() / 1024;
            int file3 = formBean.getUploadFile3().getFileSize() / 1024;
            int tottalFileSize = file1 + file2 + file3;
            if (file1 > 0 && file1 >= 25600) {
                request.setAttribute("tottalFileSize", "First File Size Less Then 25 MB");
            } else if (file2 > 0 && file2 >= 25600) {
                request.setAttribute("tottalFileSize", "Second File Size Less Then 25 MB");
            } else if (file1 > 0 && file2 > 0 && file1 + file2 >= 25600) {
                request.setAttribute("tottalFileSize", "First And Second File Size Less Then 25 MB");
            } else if (file3 > 0 && file3 >= 25600) {
                request.setAttribute("tottalFileSize", "Third File Size Less Then 25 MB");
            } else if (file2 > 0 && file3 > 0 && file2 + file3 >= 25600) {
                request.setAttribute("tottalFileSize", "Second And Third File Size Less Then 25 MB");
            } else if (tottalFileSize > 0 && tottalFileSize == 25600) {
                request.setAttribute("tottalFileSize", "Files Size Less Then 25 MB");
            }
            if( formBean.getEmail()!=null){
            for (String id : formBean.getEmail()) {
                String emails = dao.getEmailFromDepartmentContacts(id);
                temailId = new InternetAddress(emails, "");
                ToMailsList.add(temailId);
            }
            }

            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            send_dir = new File("D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date);
            if (!send_dir.exists()) {
                send_dir.mkdirs();
            }
            String filePath1=null,filePath2=null,filePath3=null;
            if (!formBean.getUploadFile1().equals("") && formBean.getUploadFile1().getFileSize() > 0) {
                //  CommonDetails.uploadingFile(formBean.getUploadFile1(), "" + send_dir, formBean.getUploadFile1().toString());
                if (!("").equals(file1)) {
                    File newFile = new File("D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date, formBean.getUploadFile1().getFileName());
                    if (!newFile.exists()) {
                        FileOutputStream fos = new FileOutputStream(newFile);
                        fos.write(formBean.getUploadFile1().getFileData());
                        fos.flush();
                        fos.close();
                    }
                }
                uploadFile.add(formBean.getUploadFile1().toString());
                attachFiles[0] = "D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date+"\\"+ formBean.getUploadFile1().toString();
                filePath1 = "D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date+"\\"+ formBean.getUploadFile1().toString();
                filename1 = formBean.getUploadFile1().getFileName();

            }
            if (!formBean.getUploadFile2().equals("") && formBean.getUploadFile2().getFileSize() > 0) {
                //   CommonDetails.uploadingFile(formBean.getUploadFile2(), "" + send_dir, formBean.getUploadFile2().toString());
                if (!("").equals(file2)) {
                    File newFile = new File("D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date, formBean.getUploadFile2().getFileName());
                    if (!newFile.exists()) {
                        FileOutputStream fos = new FileOutputStream(newFile);
                        fos.write(formBean.getUploadFile2().getFileData());
                        fos.flush();
                        fos.close();
                    }
                }
                uploadFile.add(formBean.getUploadFile2().toString());
                attachFiles[1] = "D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date+"\\"+  formBean.getUploadFile2().toString();
                filePath2 = "D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date+"\\"+ formBean.getUploadFile2().toString();
                filename2 = formBean.getUploadFile2().getFileName();
            }
            if (!formBean.getUploadFile3().equals("") && formBean.getUploadFile3().getFileSize() > 0) {
                // CommonDetails.uploadingFile(formBean.getUploadFile3(), "" + send_dir, formBean.getUploadFile3().toString());
                if (!("").equals(file3)) {
                    File newFile = new File("D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date, formBean.getUploadFile3().getFileName());
                    if (!newFile.exists()) {
                        FileOutputStream fos = new FileOutputStream(newFile);
                        fos.write(formBean.getUploadFile3().getFileData());
                        fos.flush();
                        fos.close();
                    }
                }
                uploadFile.add(formBean.getUploadFile3().toString());
                attachFiles[2] = "D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date+"\\"+ formBean.getUploadFile3().toString();
                filePath3 = "D:\\Department\\SChoolEducation\\RMSA\\MailDocuments\\"+date+"\\"+ formBean.getUploadFile3().toString();
                filename3 = formBean.getUploadFile3().getFileName();
            }
            String deptId = null;
            if (session.getAttribute("department") != null) {
                deptId = session.getAttribute("department").toString();
            }
            boolean result = SendFileEmail.sendEmailWithAttachments(ToMailsList, CCMailsList, BCCMailsList, formBean.getSubject(), formBean.getBody(), attachFiles);
            if (result == true) {
                request.setAttribute("msg", "Mail Successfuly Sent");
            } else {
                request.setAttribute("nodata", "Mail Not Sent");
            }
            String username = session.getAttribute("userName").toString();
            String systemid = request.getRemoteAddr();
            dao.recordEmail(ToMailsList, CCMailsList, BCCMailsList, formBean.getSubject(), formBean.getBody(), username, systemid, filename1, filename2, filename3, result, filePath1,filePath2,filePath3);
            formBean.setSubject(null);
            formBean.setBody(null);
            formBean.setOfficerType(null);
            //formBean.setLevel(null);
            target = "mails";
        } catch (Exception e) {
            target = "mails";
            e.printStackTrace();
            unspecified(mapping, form, request, response);
        }
        return mapping.findForward(target);
    }

    public ActionForward getSelectedListmails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SendMailForm formBean = (SendMailForm) form;
        ArrayList list = null;
        ArrayList mpdoList = null;
        ArrayList drolist = null;
        ArrayList deptlist = null;
        try {
            String officerType = formBean.getOfficerType();
            formBean.setRadioButtonProperty(null);
            formBean.setDepartment(null);
            SendSMSDAO dao = new SendSMSDAO();

            if (officerType.equalsIgnoreCase("School")) {
                formBean.setOfficerType("Divisional HOD");
                formBean.setLevel("district");
                drolist = dao.getDepartmentContactsForMail();
                if (drolist.size() > 0) {
                    request.setAttribute("rdoList", drolist);
                    formBean.setOfficerType("school");
                } else {
                    request.setAttribute("DEPTHODDETAILS", "Division Officers Details Not Available");
                }
            }

            if (officerType.equalsIgnoreCase("rdo")) {
                formBean.setOfficerType("Divisional HOD");
                formBean.setLevel("district");
                drolist = dao.getDepartmentContactsForMail();
                if (drolist.size() > 0) {
                    request.setAttribute("rdoList", drolist);
                    formBean.setOfficerType("rdo");
                } else {
                    request.setAttribute("DEPTHODDETAILS", "Division Officers Details Not Available");
                }
            }




        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("mails");
    }
}
