/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentReportDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAComponentReportForm;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1250881
 */
public class RMSAComponentReportAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RMSAComponentReportForm myform = (RMSAComponentReportForm) form;
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("userName") != null) {
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                ArrayList yesrList = rmsaDAO.getComponentYears();
                myform.setYearList(yesrList);
                request.setAttribute("yList", "yList");
                ArrayList mgtList = (ArrayList) rmsaDAO.getSchoolManagementList();
                myform.setMgmtList(mgtList);
                request.setAttribute("mgtList", "mgtList");
                if (session.getAttribute("RoleId").equals("3")) {
                    String distid1 = session.getAttribute("districtId").toString();
                    request.setAttribute("schoolAll", distid1);
                }
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);
    }

    public ActionForward getDistList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = null;
        String id = null;

        ArrayList distlist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        RMSAComponentReportForm myform = (RMSAComponentReportForm) form;

        try {
            HttpSession session = request.getSession();

            if (session.getAttribute("userName") != null) {
                if (session.getAttribute("districtId").toString().equals("00")) {
                    id = "ALL";
                } else {
                    id = session.getAttribute("districtId").toString();
                }
                if (session.getAttribute("RoleId").equals("3")) {
                    String distid1 = session.getAttribute("districtId").toString();
                    request.setAttribute("schoolAll", distid1);
                }

                distlist = dao.getDistList(id, myform.getYear(), myform.getManagement());

                if (distlist.size() > 0) {
                    request.setAttribute("distlist", distlist);
                } else {
                    request.setAttribute("msg", "No Records Available");
                }
                request.setAttribute("year", myform.getYear());
                request.setAttribute("management", myform.getManagement());
                RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                ArrayList yesrList = rmsaDAO.getComponentYears();
                myform.setYearList(yesrList);
                request.setAttribute("yList", "yList");
                ArrayList mgtList = (ArrayList) rmsaDAO.getSchoolManagementList();
                myform.setMgmtList(mgtList);
                request.setAttribute("mgtList", "mgtList");
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getMandalList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String district = null;
        ArrayList mandallist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        RMSAComponentReportForm myform = (RMSAComponentReportForm) form;
        CommonDAO commonDAO = new CommonDAO();
        String distname = null;
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("userName") != null) {
                district = request.getParameter("district").toString();
                distname = commonDAO.getDistrictname(district);
                request.setAttribute("distname", distname);
                if (session.getAttribute("RoleId").equals("3")) {
                    String distid1 = session.getAttribute("districtId").toString();
                    request.setAttribute("schoolAll", distid1);
                }
                mandallist = dao.getMandalList(district, request.getParameter("year").toString(), request.getParameter("management").toString());
                if (mandallist.size() > 0) {
                    request.setAttribute("mandallist", mandallist);
                    if (request.getParameter("year") != null && request.getParameter("management") != null) {
                        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                        ArrayList yesrList = rmsaDAO.getComponentYears();
                        myform.setYearList(yesrList);
                        request.setAttribute("yList", request.getParameter("year").toString());
                        request.setAttribute("year", request.getParameter("year").toString());
                        ArrayList mgtList = (ArrayList) rmsaDAO.getSchoolManagementList();
                        myform.setMgmtList(mgtList);
                        request.setAttribute("mgtList", request.getParameter("management").toString());
                        request.setAttribute("management", request.getParameter("management").toString());

                    }
                } else {
                    request.setAttribute("msg", "No Records Available");
                }
                request.setAttribute("district", district);
            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);
    }

    public ActionForward getSchoolList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String district = null;
        ArrayList schoollist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        RMSAComponentReportForm myform = (RMSAComponentReportForm) form;
        CommonDAO commonDAO = new CommonDAO();
        String mandalname = null;
        String distname = null;
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("userName") != null) {
                String mandal = request.getParameter("mandal");
                district = request.getParameter("mandal").toString();
                mandalname = commonDAO.getMandalName(mandal);
                request.setAttribute("mandalname", mandalname);
                district = mandal.substring(0, 4);
                distname = commonDAO.getDistrictname(district);
                request.setAttribute("distname", distname);
                if (session.getAttribute("RoleId").equals("3")) {
                    String distid1 = session.getAttribute("districtId").toString();
                    request.setAttribute("schoolAll", distid1);
                }
                schoollist = dao.getSchoolList(mandal, request.getParameter("year").toString(), request.getParameter("management").toString());
                if (schoollist.size() > 0) {
                    request.setAttribute("schoollist", schoollist);
                    if (request.getParameter("year") != null && request.getParameter("management") != null) {
                        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
                        ArrayList yesrList = rmsaDAO.getComponentYears();
                        myform.setYearList(yesrList);
                        request.setAttribute("yList", request.getParameter("year").toString());
                        request.setAttribute("year", request.getParameter("year").toString());
                        ArrayList mgtList = (ArrayList) rmsaDAO.getSchoolManagementList();
                        myform.setMgmtList(mgtList);
                        request.setAttribute("mgtList", request.getParameter("management").toString());
                        request.setAttribute("management", request.getParameter("management").toString());

                    }
                } else {
                    request.setAttribute("msg", "No Records Available");
                }
                request.setAttribute("mandal", mandal);
                request.setAttribute("district", district);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);
    }

    public ActionForward getDistrictExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList distlist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        try {
            HttpSession session = request.getSession();
            String id = null;
            if (session.getAttribute("districtId").toString().equals("00")) {
                id = "ALL";
            } else {
                id = session.getAttribute("districtId").toString();
            }
            distlist = dao.getDistList(id, request.getParameter("year").toString(), request.getParameter("management").toString());
            if (distlist.size() > 0) {
                request.setAttribute("distlist", distlist);
            } else {
                request.setAttribute("msg", "No Records Available");
            }
            if (session.getAttribute("RoleId").equals("3")) {
                    String distid1 = session.getAttribute("districtId").toString();
                    request.setAttribute("schoolAll", distid1);
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelsuccess");
    }

    public ActionForward getMandalExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList mandallist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        String district = null;
        try {
            HttpSession session = request.getSession();
            district = request.getParameter("district").toString();
            mandallist = dao.getMandalList(district, request.getParameter("year").toString(), request.getParameter("management").toString());
            if (mandallist.size() > 0) {
                request.setAttribute("mandallist", mandallist);
            } else {
                request.setAttribute("msg", "No Records Available");
            }
            request.setAttribute("district", district);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelsuccess");
    }

    public ActionForward getSchoolExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList schoollist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        String district = null;
        try {
            String mandal = request.getParameter("mandal");
            schoollist = dao.getSchoolList(mandal, request.getParameter("year").toString(), request.getParameter("management").toString());
            if (schoollist.size() > 0) {
                request.setAttribute("schoollist", schoollist);
            } else {
                request.setAttribute("msg", "No Records Available");
            }
            request.setAttribute("mandal", mandal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelsuccess");
    }

    public ActionForward getAllSchoolsExcelData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList schoollist = new ArrayList();
        RMSAComponentReportDAO dao = new RMSAComponentReportDAO();
        String district = null;
        try {
            String mandal = request.getParameter("distid");
            schoollist = dao.getAllSchoolList(mandal, request.getParameter("year").toString(), request.getParameter("management").toString());
            if (schoollist.size() > 0) {
                request.setAttribute("schoollist", schoollist);
            } else {
                request.setAttribute("msg", "No Records Available");
            }
            request.setAttribute("mandal", mandal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelsuccess");
    }
}