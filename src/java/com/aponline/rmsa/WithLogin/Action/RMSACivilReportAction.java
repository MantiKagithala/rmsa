/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.RMSAComponentsDAO;
import com.aponline.rmsa.WithLogin.Form.RMSAComponentForm;
import com.aponline.rmsa.WithLogin.Form.RMSAForm;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1259084
 */
public class RMSACivilReportAction extends DispatchAction {

    public RMSACivilReportAction() {
    }

    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {

                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);

                ArrayList mgntList = (ArrayList) rmsaDAO.getSchoolManagementList();
                myform.setMgntList(mgntList);


                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getRmsaDistWiseReport(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ArrayList distList = null;
        String user = null;
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        CommonDAO commonDAO = new CommonDAO();
        String distname = null;
        String mandalname = null;
        try {
            HttpSession session = request.getSession();
            //session checking
            if (session.getAttribute("userName") != null) {

                request.setAttribute("phaseNo", myform.getPhaseNo());
                request.setAttribute("mgntId", myform.getMgntId());
                request.setAttribute("reportType", request.getParameter("reportType").toString());

                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);

                ArrayList mgntList = (ArrayList) rmsaDAO.getSchoolManagementList();
                myform.setMgntList(mgntList);

                if (session.getAttribute("RoleId").equals("51") || session.getAttribute("RoleId").equals("52")) {
                    if (request.getParameter("distId") == null) {
                        user = "ALL";
                    } else {
                        user = request.getParameter("distId").toString();
                    }
                } else if (session.getAttribute("RoleId").equals("3")) {
                    user = session.getAttribute("districtId").toString();
                } else if (session.getAttribute("RoleId").equals("22") || session.getAttribute("RoleId").equals("41")) {
                    user = session.getAttribute("userName").toString();
                }
                if (user.length() == 3 && myform.getReportType().equalsIgnoreCase("1")) {
                    distList = rmsaDAO.getPhysicalCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("physicalStateList", distList);
                    }
                } else if (user.length() == 4 && myform.getReportType().equalsIgnoreCase("1")) {
                    distList = rmsaDAO.getPhysicalCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("physicalMandalList", distList);
                    }
                    distname = commonDAO.getDistrictname(user);
                    request.setAttribute("distname", distname);
                } else if (user.length() == 6 && myform.getReportType().equalsIgnoreCase("1")) {
                    distList = rmsaDAO.getPhysicalCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("physicalSchoolList", distList);
                    }
                    request.setAttribute("distId", user.substring(0, 4));
                    mandalname = commonDAO.getMandalName(user);
                    request.setAttribute("mandalname", mandalname);
                    distname = commonDAO.getDistrictname(user.substring(0, 4));
                    request.setAttribute("distname", distname);
                } else if (user.length() == 3 && myform.getReportType().equalsIgnoreCase("2")) {
                    distList = rmsaDAO.getFinancialCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("financialStateList", distList);
                    }
                } else if (user.length() == 4 && myform.getReportType().equalsIgnoreCase("2")) {
                    distList = rmsaDAO.getFinancialCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("financialMandalList", distList);
                    }
                    distname = commonDAO.getDistrictname(user);
                    request.setAttribute("distname", distname);
                } else if (user.length() == 6 && myform.getReportType().equalsIgnoreCase("2")) {
                    distList = rmsaDAO.getFinancialCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("financialSchoolList", distList);
                    }
                    request.setAttribute("distId", user.substring(0, 4));
                    mandalname = commonDAO.getMandalName(user);
                    request.setAttribute("mandalname", mandalname);
                    distname = commonDAO.getDistrictname(user.substring(0, 4));
                    request.setAttribute("distname", distname);
                } else if (user.length() == 3 && myform.getReportType().equalsIgnoreCase("3")) {
                    distList = rmsaDAO.getDistWiseCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("masterList", distList);
                    }
                } else if (user.length() == 4 && myform.getReportType().equalsIgnoreCase("3")) {
                    distList = rmsaDAO.getDistWiseCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("mandalList", distList);
                    }
                    distname = commonDAO.getDistrictname(user);
                    request.setAttribute("distname", distname);
                } else if (user.length() == 6 && myform.getReportType().equalsIgnoreCase("3")) {
                    distList = rmsaDAO.getDistWiseCivilsReport(myform.getPhaseNo(), myform.getMgntId(), myform.getReportType(), user);
                    if (distList.size() > 0) {
                        request.setAttribute("schoolList", distList);
                    }
                    request.setAttribute("distId", user.substring(0, 4));
                    mandalname = commonDAO.getMandalName(user);
                    request.setAttribute("mandalname", mandalname);
                    distname = commonDAO.getDistrictname(user.substring(0, 4));
                    request.setAttribute("distname", distname);
                }
                if (distList.size() == 0) {
                    request.setAttribute("msg", "fghfg");
                }
                request.setAttribute("user", user);
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getRmsaDistWiseExcelReport(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ArrayList distList = null;
        String user = null;
        String target = "failure";
        RMSAForm myform = (RMSAForm) form;
        RMSAComponentsDAO rmsaDAO = new RMSAComponentsDAO();
        try {
            HttpSession session = request.getSession();
            //session checking

            if (session.getAttribute("userName") != null) {

                request.setAttribute("phaseNo", myform.getPhaseNo());
                request.setAttribute("mgntId", myform.getMgntId());
                request.setAttribute("reportType", request.getParameter("reportType").toString());

                String phaseNo = request.getParameter("phaseNo").toString();
                String mgntId = request.getParameter("mgntId").toString();
                String reportType = request.getParameter("reportType").toString();
                user = request.getParameter("user").toString();

                ArrayList list1 = (ArrayList) rmsaDAO.getPhaseList();
                myform.setPhaseList(list1);

                ArrayList mgntList = (ArrayList) rmsaDAO.getSchoolManagementList();
                myform.setMgntList(mgntList);

                if (user.length() == 3 && myform.getReportType().equalsIgnoreCase("1")) {
                    distList = rmsaDAO.getPhysicalCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("physicalStateList", distList);
                } else if (user.length() == 4 && myform.getReportType().equalsIgnoreCase("1")) {
                    distList = rmsaDAO.getPhysicalCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("physicalMandalList", distList);
                } else if (user.length() == 6 && myform.getReportType().equalsIgnoreCase("1")) {
                    distList = rmsaDAO.getPhysicalCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("physicalSchoolList", distList);

                } else if (user.length() == 3 && myform.getReportType().equalsIgnoreCase("2")) {
                    distList = rmsaDAO.getFinancialCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("financialStateList", distList);
                } else if (user.length() == 4 && myform.getReportType().equalsIgnoreCase("2")) {
                    distList = rmsaDAO.getFinancialCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("financialMandalList", distList);
                } else if (user.length() == 6 && myform.getReportType().equalsIgnoreCase("2")) {
                    distList = rmsaDAO.getFinancialCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("financialSchoolList", distList);
                } else if (user.length() == 3 && myform.getReportType().equalsIgnoreCase("3")) {
                    distList = rmsaDAO.getDistWiseCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("masterList", distList);
                } else if (user.length() == 4 && myform.getReportType().equalsIgnoreCase("3")) {
                    distList = rmsaDAO.getDistWiseCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("mandalList", distList);
                } else if (user.length() == 6 && myform.getReportType().equalsIgnoreCase("3")) {
                    distList = rmsaDAO.getDistWiseCivilsReport(phaseNo, mgntId, reportType, user);
                    request.setAttribute("schoolList", distList);
                }
                target = "civilsExcelExport";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
}
