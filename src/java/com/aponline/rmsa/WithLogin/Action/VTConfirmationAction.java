/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.VTConfirmationDAO;
import com.aponline.rmsa.WithLogin.Form.VCHMConfirmationForm;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1250881
 */
public class VTConfirmationAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        String schoolId = "";
        VTConfirmationDAO dao = new VTConfirmationDAO();
        HttpSession session = request.getSession();
        try {
            
            if (session.getAttribute("userName") != null && session.getAttribute("userName") != "" && session.getAttribute("RoleId") != null && session.getAttribute("RoleId") != "") {
                if (session.getAttribute("RoleId").equals("60")) {
                    myform.setRoleName("SrAsst");
                    ArrayList vclist = dao.getVCHMConfirmationList(myform);
                    if (vclist.size() > 0) {
                        request.setAttribute("vclist", vclist);
                        request.setAttribute("vcSrAsstlist", vclist);
                    } else {
                        request.setAttribute("msg", "No Records are available");
                    }
                } else if (session.getAttribute("RoleId").equals("61")) {
                    myform.setRoleName("Supdnt");
                    ArrayList vclist = dao.getVCHMConfirmationList(myform);
                    if (vclist.size() > 0) {
                        request.setAttribute("vclist", vclist);
                        request.setAttribute("vcSuplist", vclist);
                    } else {
                        request.setAttribute("msg", "No Records are available");
                    }
                } else if (session.getAttribute("RoleId").equals("62")) {
                    myform.setRoleName("AsstDire");
                    ArrayList vclist = dao.getVCHMConfirmationList(myform);
                    if (vclist.size() > 0) {
                        request.setAttribute("vclist", vclist);
                        request.setAttribute("vcAsstDirectorlist", vclist);
                    } else {
                        request.setAttribute("msg", "No Records are available");
                    }
                } else if (session.getAttribute("RoleId").equals("63")) {
                    myform.setRoleName("Director");
                    ArrayList vclist = dao.getVCHMConfirmationList(myform);
                    if (vclist.size() > 0) {
                        request.setAttribute("vclist", vclist);
                        request.setAttribute("vcDirectorlist", vclist);
                    } else {
                        request.setAttribute("msg", "No Records are available");
                    }
                } else {
                    request.setAttribute("msg", "This Service is not available for your Management");
                }

//                schoolId = (String) session.getAttribute("userName");
//                myform.setSchoolId(schoolId);

            } else {
                response.sendRedirect("officialLogin.do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return mapping.findForward(SUCCESS);
    }
    
    public ActionForward remarksStatusUpdate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VTConfirmationDAO dao = new VTConfirmationDAO();
        HttpSession session = request.getSession();
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        String status = null;
        try {
            
            String schoolId = request.getParameter("schcd");
            String vctraineId = request.getParameter("vctrainerId");
            String vcRemarksId = request.getParameter("vcRemarksId");
            System.out.println("=sc="+myform.getSchoolId()+"=trid=="+myform.getvTrainerId()+"==vcrem="+myform.getVcRemarksValue());
            if (session.getAttribute("RoleId").equals("60")) {
                myform.setRoleName("SrAsst");
                status = dao.remarksStatusUpdate(myform);
            } else if (session.getAttribute("RoleId").equals("61")) {
                myform.setRoleName("Supdnt");
                status = dao.remarksStatusUpdate(myform);
            } else if (session.getAttribute("RoleId").equals("62")) {
                myform.setRoleName("AsstDire");
                status = dao.remarksStatusUpdate(myform);
            } else if (session.getAttribute("RoleId").equals("63")) {
                String statusDirector = request.getParameter("statusDirectorId");
                myform.setStatusDirector(statusDirector);
//                    String remarks = request.getParameter("remarks") != null ? request.getParameter("remarks").trim() : "";
                myform.setRoleName("Director");
                status = dao.remarksStatusUpdate(myform);
            }
            
            if (status != null) {
                request.setAttribute("status", status);
            } else {
                request.setAttribute("msg", "No records are available");
            }
            myform.setVcSrAstRema("");
            myform.setVcSuptRemarks("");
            myform.setVcAsstDirRemarks("");
            myform.setVcDirRemarks("");
            myform.setStatusDirector("");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }
    
    public ActionForward vcHMStatusPDF(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        VTConfirmationDAO dao = new VTConfirmationDAO();
        try {
            String schoolId = request.getParameter("schcd");
            String vctraineId = request.getParameter("vctrainerId");
            myform.setSchoolId(schoolId);
            myform.setVcTrainerId(vctraineId);
            
            ArrayList vclist = dao.getVCHMConfirmationList(myform);
            myform.setVcList(vclist);
            request.setAttribute("vclist", vclist);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("vchmpdf");
    }
    
    public ActionForward vcHMStatusView(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VCHMConfirmationForm myform = (VCHMConfirmationForm) form;
        VTConfirmationDAO dao = new VTConfirmationDAO();
        try {
            String schoolId = request.getParameter("schcd");
            String vctraineId = request.getParameter("vctrainerId");
            myform.setSchoolId(schoolId);
            myform.setVcTrainerId(vctraineId);
            
            ArrayList vclist = dao.getVCHMConfirmationList(myform);
            myform.setVcList(vclist);
            request.setAttribute("vclist", vclist);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("vchmview");
    }
}
