/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.aponline.rmsa.WithLogin.Dao.RMSASchoolComponentReportDAO;

/**
 *
 * @author 1250881
 */
public class RMSASchoolComponentReportAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList schoollist = new ArrayList();
        RMSASchoolComponentReportDAO dao = new RMSASchoolComponentReportDAO();
      
        try {
            HttpSession session = request.getSession();
            String id = session.getAttribute("userName").toString();
           schoollist = dao.getSchoolList(id);
            if (schoollist.size() > 0) {
                request.setAttribute("schoollist", schoollist);
            } else {
                request.setAttribute("msg", "No Records Available");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);
    }

    public ActionForward getSchoolExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ArrayList schoollist = new ArrayList();
        RMSASchoolComponentReportDAO dao = new RMSASchoolComponentReportDAO();
        try {
             HttpSession session = request.getSession();
              String id = session.getAttribute("userName").toString();
            schoollist = dao.getSchoolList(id);
            if (schoollist.size() > 0) {
                request.setAttribute("schoollist", schoollist);
            } else {
                request.setAttribute("msg", "No Records Available");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelsuccess");
    }
}
