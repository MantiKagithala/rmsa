/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import java.util.ArrayList;
import java.util.List;
import java.io.*;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.aponline.rmsa.commons.CommonDetails;
import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.SendSMSDAO;
//import org.apo.sunriseap.Services.DTO.SendSMSDTO;
import com.aponline.rmsa.WithLogin.Form.SendSMSForm;
import com.aponline.rmsa.WithLogin.Form.SendSMSDTO;
//import org.apo.sunriseap.Services.ServiceFactory.SendSMSServiceFactory;
//import org.apo.sunriseap.Services.Services.SendSMSService;

/**
 *
 * @author 567999
 */
public class SendSMSAction extends DispatchAction {

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SendSMSForm sendSMSForm = (SendSMSForm) form;
        try {
            SendSMSForm formBean = (SendSMSForm) form;
            //KeyContactsDetailsService keyContactsDetailsService = KeyContactsDetailsServiceFactory.getContactsDetailsServiceImpl();
            // formBean.setDepartmentList(keyContactsDetailsService.getDepartments());
            sendSMSForm.setOfficerType(null);
            sendSMSForm.setOfficerTypeMEOs(null);
            sendSMSForm.setOfficerTypeDEOs(null);
            sendSMSForm.setOfficerTypeHoDs(null);
            sendSMSForm.setOfficerType(null);
            sendSMSForm.setHODOff(null);
            sendSMSForm.setDEOOff(null);
            sendSMSForm.setMeoOff(null);
            sendSMSForm.setRjdOff(null);
            sendSMSForm.setHmOff(null);
            sendSMSForm.setAllOff(null);
            sendSMSForm.setOtherOff(null);
            sendSMSForm.setSms(null);
            // request.getSession(false).setAttribute("tokenCode",
            //   CommonDetails.generateTokenCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

    public ActionForward getSelectedListsms(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SendSMSForm formBean = (SendSMSForm) form;
        ArrayList list = null;
        ArrayList mpdoList = null;
        ArrayList drolist = null;
        ArrayList deptlist = null;
        ArrayList distArrayList = null;
        try {
            String officerTypeMEOs = null;
            String officerTypeHoDs = null;
            String officerTypeDEOs = null;
            String officerTypeRJDs = null;
            String officerTypeHMS = null;
            String officerTypeALL = null;

            String officerType = null;
            if (formBean.getOfficerTypeHoDs() != null) {
                officerTypeHoDs = formBean.getOfficerTypeHoDs();
                formBean.setAllOff(null);
            }
            if (formBean.getOfficerTypeALL() != null) {
                officerTypeALL = formBean.getOfficerTypeALL();
            }
            if (formBean.getOfficerTypeMEOs() != null) {
                officerTypeMEOs = formBean.getOfficerTypeMEOs();
            }
            if (formBean.getOfficerTypeDEOs() != null) {
                officerTypeDEOs = formBean.getOfficerTypeDEOs();
            }
            if (formBean.getOfficerTypeRJDs() != null) {
                officerTypeRJDs = formBean.getOfficerTypeRJDs();
            }
            if (formBean.getOfficerTypeHMS() != null) {
                officerTypeHMS = formBean.getOfficerTypeHMS();
            }
            if (formBean.getOfficerTypeDEOs().equalsIgnoreCase("")) {
                formBean.setDEOOff(null);
            }
            if (formBean.getOfficerTypeHoDs().equalsIgnoreCase("")) {
                formBean.setHODOff(null);
            }
            if (formBean.getOfficerTypeMEOs().equalsIgnoreCase("")) {
                formBean.setMeoOff(null);
            }
            if (formBean.getOfficerTypeRJDs().equalsIgnoreCase("")) {
                formBean.setRjdOff(null);
            }
            if (formBean.getOfficerTypeHMS().equalsIgnoreCase("")) {
                formBean.setHmOff(null);
            }
            if (formBean.getOfficerTypeOther().equalsIgnoreCase("")) {
                formBean.setOtherOff(null);
            }
            if (formBean.getOfficerTypeALL().equalsIgnoreCase("")) {
                formBean.setAllOff(null);
            }

            if (formBean.getOfficerTypeOther() != null) {
                officerType = formBean.getOfficerTypeOther();
            }
            formBean.setRadioButtonProperty(null);
            formBean.setDepartment(null);
            SendSMSDAO sendSMSService = new SendSMSDAO();


            if (officerTypeMEOs != null && officerTypeMEOs.equalsIgnoreCase("ml")) {
                formBean.setOfficerType("Mandal HOD");
                formBean.setLevel("mandal");
                formBean.setMandalDesignation("MEO");

                list = sendSMSService.getDepartmentContacts(formBean);
                if (list.size() > 0) {
                    request.setAttribute("mpdoList", list);
                    formBean.setOfficerType("ml");
                } else {
                    request.setAttribute("DEPTHODDETAILS", "MEO Level Officers Details Not Available");
                }
                getDistrictList(mapping, form, request, response);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapping.findForward("success");

    }

    public ActionForward getDistrictList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SendSMSForm sendSMSForm = (SendSMSForm) form;
        ArrayList distArrayList = null;
        try {
            CommonDAO commonDAO = new CommonDAO();
            distArrayList = commonDAO.getDistrictDetails();

            if (distArrayList.size() > 0) {
                request.setAttribute("distArrayList", distArrayList);
                sendSMSForm.setDistList(distArrayList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

    public ActionForward sendSMS(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = null;
        SendSMSForm formBean = (SendSMSForm) form;
        HttpSession session = request.getSession();
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        SendSMSDAO mydao = new SendSMSDAO();
        String resultStatus = "";
        String sesionTokenCode = null;
        String requestTokenCode = null;
        try {
            if (session.getAttribute("userName") != null) {
                if (session.getAttribute("tokenCode") != null) {
                    sesionTokenCode = session.getAttribute("tokenCode").toString();
                    requestTokenCode = request.getParameter("tokenCodeChecking");
                }

                formBean.setSystemIp(CommonDetails.getsystemIp());
                formBean.setLoginId(session.getAttribute("userName").toString());
                String deptId = null;
                if (session.getAttribute("department") != null) {
                    deptId = session.getAttribute("department").toString();
                }
                int batchSize = 100;
                int start = 0;
                int end = batchSize;
                int bluk = formBean.getDistoff().length;
                int count = formBean.getDistoff().length / batchSize;
                int reminder = formBean.getDistoff().length % batchSize;
                int counter = 0;
                for (int i = 0; i < count; i++) {
                    for (int counter1 = start; counter < end; counter++) {
                        sendSMSDTO = mydao.getTahasildarMobileDetails(formBean.getDistoff()[counter]);
                        resultStatus = TpSendSMS.sendSMS(sendSMSDTO.getMobileNumber(), formBean.getSms());
                        formBean.setMobileNumber(sendSMSDTO.getMobileNumber());
                        sendSMSDTO.setSystemIp(formBean.getSystemIp());
                        sendSMSDTO.setSms(formBean.getSms());
                        sendSMSDTO.setLoginId(formBean.getLoginId());
                        if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully")) {
                            sendSMSDTO.setSendStatus("Sent");
                        } else {
                            sendSMSDTO.setSendStatus("Not Send");
                        }
                        mydao.insertSmsLogDetails(sendSMSDTO);
                    
                }
                start = start + batchSize;
                end = end + batchSize;
                Thread.sleep(5 * 1000);
            }
            if (reminder != 0) {
                end = end + reminder;
                for (int counter2 = start; counter2 < bluk; counter2++) {
                    sendSMSDTO = mydao.getTahasildarMobileDetails(formBean.getDistoff()[counter2]);
                    resultStatus = TpSendSMS.sendSMS(sendSMSDTO.getMobileNumber(), formBean.getSms());
                    formBean.setMobileNumber(sendSMSDTO.getMobileNumber());
                    sendSMSDTO.setSystemIp(formBean.getSystemIp());
                    sendSMSDTO.setSms(formBean.getSms());
                    sendSMSDTO.setLoginId(formBean.getLoginId());
                    if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully")) {
                        sendSMSDTO.setSendStatus("Sent");
                    } else {
                        sendSMSDTO.setSendStatus("Not Send");
                    }
                    mydao.insertSmsLogDetails(sendSMSDTO);
                }
            }
            if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully")) {
                request.setAttribute("msg", "SMS Send Successfully");
            } else {
                request.setAttribute("msg", "Error while sending sms");
            }
            formBean.setSms(null);
            formBean.setLevel(null);
            target = "success";
            unspecified(mapping, form, request, response);
        } 

    
        else {
                target = "failure";
    }
}
catch (Exception e) {
            e.printStackTrace();
            unspecified(mapping, form, request, response);
        }
        return mapping.findForward(target);
    }
}
