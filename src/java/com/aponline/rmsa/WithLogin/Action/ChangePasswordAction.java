/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aponline.rmsa.WithLogin.Action;

import com.aponline.rmsa.WithLogin.Dao.ChangePasswordDao;
import java.util.ArrayList;
import java.util.List;
import java.io.*;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.aponline.rmsa.commons.CommonDetails;
import com.aponline.rmsa.WithLogin.Dao.CommonDAO;
import com.aponline.rmsa.WithLogin.Dao.SendSMSDAO;
import com.aponline.rmsa.WithLogin.Form.ChangePasswordForm;
//import org.apo.sunriseap.Services.DTO.SendSMSDTO;
import com.aponline.rmsa.WithLogin.Form.SendSMSForm;
import com.aponline.rmsa.WithLogin.Form.SendSMSDTO;
//import org.apo.sunriseap.Services.ServiceFactory.SendSMSServiceFactory;
//import org.apo.sunriseap.Services.Services.SendSMSService;

/**
 *
 * @author 567999
 */
public class ChangePasswordAction extends DispatchAction {

    String target = "success";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ChangePasswordForm sendSMSForm = (ChangePasswordForm) form;
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("userName") != null) {
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward updatePassword(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String password = null;
        String password1 = null;
        HttpSession session = request.getSession();
        ChangePasswordDao mydao = new ChangePasswordDao();
        ChangePasswordForm formBean = (ChangePasswordForm) form;
        try {
            if (session.getAttribute("userName") != null) {
                String userName = session.getAttribute("userName").toString();
                formBean.setUserName(userName);

                //Changing updated pasword as encreption
                if (formBean.getUpdatePassword() != null && formBean.getUpdatePassword().length() > 0) {
                    password = formBean.getUpdatePassword().substring(5, formBean.getUpdatePassword().length() - 5);
                }
                String updateEncrepted = mydao.passwordEncrypt(password);
                formBean.setUpdatePassword(updateEncrepted);

                //Changing current pasword as encreption
                if (formBean.getCurrentPasswordEncreption() != null && formBean.getCurrentPasswordEncreption().length() > 0) {
                    password1 = formBean.getCurrentPasswordEncreption().substring(5, formBean.getCurrentPasswordEncreption().length() - 5);
                }
                String currentEncrepted = mydao.passwordEncrypt(password1);
                formBean.setCurrentPasswordEncreption(currentEncrepted);
                
                //Updating password
                int myreturn = mydao.updatePassword(formBean);
                
                if (myreturn == 0) {
                    request.setAttribute("SuccessMsg", "Current Password didn't match please try again");
                    request.setAttribute("colour", "red");
                } else if (myreturn == 1) {
                    request.setAttribute("SuccessMsg", "Password updated successfully");
                    request.setAttribute("colour", "#0E94C5");
                }else {
                    request.setAttribute("SuccessMsg", "Password updated failed please try again");
                    request.setAttribute("colour", "red");
                }
                target = "success";
            } else {
                target = "sessionExp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
}
