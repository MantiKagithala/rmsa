<%-- 
    Document   : LoginMainTile
    Created on : Mar 19, 2015, 9:15:29 AM
    Author     : 747577
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld"  prefix="tiles"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    response.addHeader("X-FRAME-OPTIONS", "DENY");
    response.addHeader("X-Xss-Protection", "1; mode=block");
    response.addHeader("X-Content-Type-Options", "nosniff");
    response.addHeader("Content-Security-Policy", "frame-ancestors  script-src https://rmsa.ap.gov.in/RMSA/");
    response.addHeader("Referrer-Policy", "no-referrer");
    response.addHeader("Strict-Transport-Security", "frame-ancestors  script-src https://rmsa.ap.gov.in/RMSA/");
    response.addHeader("Feature-Policy", "frame-ancestors  script-src https://rmsa.ap.gov.in/RMSA/");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);

%>
<html>
    <head>
        <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: RMSA ::</title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/css/bootstrap.css" media="screen">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,300,300italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="<%=basePath%>/js/jquery-2.1.4.js"></script> 
        <link rel="stylesheet" href="<%=basePath%>/css/font-awesome.min.css">
        <!-- Stroke Gap Icon -->
        <!-- owl-carousel css -->
        <link rel="stylesheet" href="<%=basePath%>/css/owl.carousel.css">
        <link rel="stylesheet" href="<%=basePath%>/css/owl.theme.css">
        <!-- Custom Css -->
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/css/responsive.css">

        <style type="text/css">
            .blink_me {
                -webkit-animation-name: blinker;
                -webkit-animation-duration: 2s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;

                -moz-animation-name: blinker;
                -moz-animation-duration: 2s;
                -moz-animation-timing-function: linear;
                -moz-animation-iteration-count: infinite;

                animation-name: blinker;
                animation-duration: 2s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;

                font-size: 18px;
                text-align: center;
                font-weight: bold;
                margin-top: 100px;

            }
            @-moz-keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
            @-webkit-keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
            @keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
        </style>
        <script type='text/javascript'>
            var isCtrl = false;
            document.onkeyup = function(e) {
                if (e.which === 17)
                    isCtrl = false;

                if (e.which === 16)
                    isCtrl = false;
            };
            document.onkeydown = function(e) {
                 //F12
                 alert(e.which);
                 alert(e.which === 123);
                if ((e.which === 123)) {
                    alert();
                    return false;
                }
                if ((e.which === 116)) {
                    return false;
                }
                
                if (e.which === 17)
                    isCtrl = true;
                if (e.which === 85 && isCtrl === true) {
                    //run code for CTRL+U -- ie, whatever!
                    //        alert('CTRL + U stuff');
                    return false;
                }
                //I
                if (e.which === 73 && isCtrl === true) {
                    return false;
                }//J
                if (e.which === 74 && isCtrl === true) {
                    return false;
                }
               
            };
        </script>
    </head>
    <body class="home" oncontextmenu="return false;">
        <header>
            <div class="container-fluid top_header"> 
                <!-- end container --> 
            </div>
            <!-- end top_header -->
            <div class="bottom_header top-bar-gradient">
                <div class="container clear_fix">
                    <div class="row hidden-xs">
                        <div class="">
                            <div class="col-xs-6 col-sm-2 logo"> <a href="index.html"> <img src="<%=basePath%>/img/RMSA-Logo.png" style="width:90px" class="pull-left" alt=""> </a> </div>
                            <div class="col-xs-12 col-sm-8 text-center">
                                <h2 class="logo-txt">Rashtriya Madhyamik Shiksha Abhiyan</h2>
                                <h2 class="logo-sm">( Department of School Education , Government of Andhra Pradesh )</h2>
                            </div>
                            <div class="col-xs-6 col-sm-2 logo"> <a href="index.html"> <img src="<%=basePath%>/img/Andhra-Pradesh-AP-Govt-Logo.png" style="width:80px" class="pull-right" alt=""> </a> </div>
                        </div>
                    </div>
                    <div class="row visible-xs">
                        <div class="">
                            <div class="col-xs-6 logo"> <a href="index.html"> <img src="<%=basePath%>/img/RMSA-Logo.png" style="width:90px" class="pull-left" alt=""> </a> </div>
                            <div class="col-xs-6 logo"> <a href="index.html"> <img src="<%=basePath%>/img/Andhra-Pradesh-AP-Govt-Logo.png" style="width:80px" class="pull-right" alt=""> </a> </div>
                            <div class="col-xs-12 text-center">
                                <h2 class="logo-txt">Rashtriya Madhyamik Shiksha Abhiyan</h2>
                                <h2 class="logo-sm">( Department of School Education , Government of Andhra Pradesh )</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end container --> 
            </div>
        </header>
        <!-- end bottom_header --> 
        <div style="background: #000">
            <div class="container">
                <tiles:insert attribute="menu" name="menu"/>
            </div>
        </div>
        <!-- ======= revolution slider section ======= -->

        <img src="img/inner.jpg" alt="..." class="img-responsive" />

        <!-- ======= /revolution slider section ======= -->

        <div class="container" style="min-height: 400px;">
            <tiles:insert attribute="content" name="content"/>
        </div>
        <!-- ******FOOTER****** -->
        <footer>
            <!-- End top_footer -->
            <div class="bottom_footer container-fluid">
                <div class="container">
                    <p class="float_left">Copyright &copy; RMSA. All rights reserved. </p>
                    <p class="float_right">Designed &amp; Developed by <img src="<%=basePath%>/img/APO.png" class="apo"></p>
                </div>
            </div>
            <!-- End bottom_footer -->
        </footer>
        <!--//footer--> 
        <script type="text/javascript" src="<%=basePath%>/js/validate.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/jquery.mCustomScrollbar.concat.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/jquery.bxslider.min.js"></script> 
        <script src="<%=basePath%>/js/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools --> 
        <script src="<%=basePath%>/js/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider --> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.actions.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.carousel.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.kenburn.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.layeranimation.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.migration.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.navigation.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.parallax.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.slideanims.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/revolution.extension.video.min.js"></script> 

        <!-- Bootstrap JS --> 
        <script type="text/javascript" src="<%=basePath%>/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/jquery.appear.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/jquery.countTo.js"></script> 
        <script type="text/javascript" src="<%=basePath%>/js/jquery.fancybox.pack.js"></script> 

        <!-- owl-carousel --> 
        <script type="text/javascript" src="<%=basePath%>/js/owl.carousel.js"></script> 
        <script src="<%=basePath%>/js/owl-custom.js"></script> 
        <!-- Custom & Vendor js --> 
        <script type="text/javascript" src="<%=basePath%>/js/custom.js"></script>
        <script>
            $('#carousel - 900202').carousel({
                interval: 2000
            });
            $('.carousel').carousel({
                interval: 3000
            });
        </script>
    </body>
</html>