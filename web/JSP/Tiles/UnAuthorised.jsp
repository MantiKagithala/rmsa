<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
    Document   : UnAuthorised
    Created on : Jan 05, 2015, 13:39:52 PM
    Author     : 567999
--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SGF</title>
        <script type = "text/javascript" >

            function preventBack(){
                window.history.forward();
            }

            setTimeout("preventBack()", 0);

            window.onunload=function(){null};

        </script>
    </head>
    <body style="padding: 0px;margin: 0px;">

        <table align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><br/><br/><br/><br/><br/>
                    <b><font color="red" size="7">Your Session Has Expired</font></b>
                </td>
            </tr>
            <tr>
                <td align="center"><br/>
                    <a href="<%=basePath%>"> <font color="Blue" size="4"><b>Login Again</b></font></a>
                </td>
            </tr>
        </table>
    </body>
</html>
