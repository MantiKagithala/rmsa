<%-- 
    Document   : RmsaMasterReport
    Created on : June 06, 2017, 11:31:47 AM
    Author     : 1250892
--%>
<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<!DOCTYPE html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
%>
<html>
<head>

        <title>JSP Page</title>


        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/>
    </script>
    <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/>
</script>
<script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/>
</script>


<style type="text/css">
    table.dataTable.nowrap td {
        border-bottom: 1px #083254 solid;
        border-left: 1px #083254 solid;
        vertical-align: middle;
        padding-left: 3px;
        padding-right: 3px;

        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
    }

    table.dataTable.nowrap th {
        background-color: #0E94C5;
        text-align: center;
        border-bottom: 1px #000 solid;
        padding-left: 3px;
        padding-right: 3px;
        border-left: 1px #000 solid;
        vertical-align: left;
        font-size: 11px;
        font-family: verdana;

        font-weight: bold;
        color: #fff;
        padding: 5px;
    }

    table.dataTable.display tbody tr.odd {
        background: #E2E4FF !important;
    }
    table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
        background: #E2E4FF !important;
    }
    .dataTables_wrapper {

        padding: 10px !important;
    }
    .dataTables_length label {
        font-size: 12px !important;
    }
    .dataTables_filter label {
        font-size: 12px !important;
    }
    .dataTables_info {
        font-size: 12px !important;
    }
    .dataTables_paginate {
        font-size: 12px !important;
    }
    form input {
        margin-bottom: 5px;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: initial !important;
    }
    .well {
        overflow: hidden;
    }
    .well-lg {
        padding: 10px !important;
    }
    form input {
        padding: 0px !important;
        border-radius: 0 !important;
        font-size: 12px;
        padding: 3px 5px !important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
        height: auto !important;
    }
    select {
        display: inline-block !important;
    }
    /* .dataTables_wrapper .dataTables_scroll {
        clear: both;
width: 1000px;
    } */

    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid #49A0E3 !important;
    }
    .table {
        border: 0 !important;
        border-radius: 0 !important;
    }
    input[type="button"] {
        width: 60px !important;
    }

</style>

<style type="text/css">
    .districts_main {
        width:1000px;
        margin:0px auto;
        text-align: center;
    }
    .distirct1 {
        width:250px;
        float:left;
        margin:10px 0;
    }
    .distirct2 {
        width:230px;
        float:left;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;

        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;

        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;

        font-size: 16px;
        text-align: right;
        font-weight: bold;

    }
    @-moz-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @-webkit-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
</style>
<style type="text/css">
    table.altrowstable1 th {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        text-align: center !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 td {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 {
        border-right: 1px #000000 solid !important;
        border-top: 1px #000000 solid !important;
    }
    table.altrowstable1 thead th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    table.altrowstable1 tbody th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    input { 
        padding: 2px 5px;
        margin: 5px 0px;

    }
</style>
</head>
<script>
    function goBack() {
        window.history.back();
    }

</script>
<script type="text/javascript">

    function mySearch() {

        var phaseNo = $('#phaseNo').val();
        var mgntId = $('#mgntId').val();
        var reportType = $('#reportType').val();

        if (phaseNo === "0") {
            alert("Select PhaseNo ");
            $("#phaseNo").focus().css({'border': '1px solid red'});
        }
        else if (mgntId === "00") {
            alert("Select School Management ");
            $("#mgntId").focus().css({'border': '1px solid red'});
        }
        else if (reportType === "00") {
            alert("Select Report Type");
            $("#reportType").focus().css({'border': '1px solid red'});
        }
        else {
            document.forms[0].mode.value = "getRmsaDistWiseReport";
            document.forms[0].submit();
        }
    }

    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 300,
            "scrollX": true
        });
        var table = $('#demo_datatables').DataTable();

//        $("#Search").button().click(function() {
//            var phaseNo = $('#phaseNo').val();
//            if (phaseNo === "0") {
//                alert("Select PhaseNo ");
//                $("#phaseNo").focus().css({'border': '1px solid red'});
//            }
//            else {
//                document.forms[0].mode.value = "getRmsaDistWiseReport";
//                document.forms[0].submit();
//            }
//        });

    });</script>

<body>
    <html:form action="/rmsaCivilReport">
        <html:hidden property="mode" name="status" value="displayDistricts"/>
        <br/>
        <!--<h3> GIS Coordinates</h3>--> 
        <!--<h5 style="font-weight: bold;text-align: center;color: #ffffff;background: #fd8760;padding: 7px;margin: 10px;box-shadow: 10px 7px 10px #D4D4D4;text-transform: uppercase; margin-bottom: 30px;"> </h5>-->
        <section class="testimonial_sec clear_fix">
            <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                        <div class="innerbodyrow">
                            <div class="col-xs-12">
                                <h3>RMSA CIVILS REPORT</h3>
                            </div>
                            <logic:present name="msg">
                                <center> <font color="red" align="center">No Records Available</font></center>
                                </logic:present> 
                          
                        </div>
                        <br>
                        <!-- Start of both selection of report type --> 
                        <logic:present name="masterList">
                            <% String phaseNo1 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo1);
                            %>
                               <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> District </th>
                                            <th rowspan="2" style="text-align: center">No of schools</th>
                                            <th colspan="2" style="text-align: center">Art/Craft room</th>
                                            <th colspan="2" style="text-align: center">Computer Room</th>
                                            <th colspan="2" style="text-align: center">Science Lab</th>
                                            <th colspan="2" style="text-align: center">Library Room</th>
                                            <th colspan="2" style="text-align: center">Additional Class Room</th>
                                            <th colspan="2" style="text-align: center">Toilet Block</th>
                                            <th colspan="2" style="text-align: center">Drinking Water</th>

                                            <th rowspan="2" style="text-align: center">Financial Sanction In Lakhs</th>
                                            <th rowspan="2" style="text-align: center">Eligible Cost</th>
                                            <th rowspan="2" style="text-align: center">Releases</th>
                                            <th rowspan="2" style="text-align: center">Expenditure</th>
                                            <th rowspan="2" style="text-align: center">Amount Yet To Be Released</th>
                                            <th rowspan="2" style="text-align: center">UCS Submit</th>
                                            <th rowspan="2" style="text-align: center">Balance At UCS</th>
                                            <th rowspan="2" style="text-align: center">Balance At Apwidc</th>

                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                        </tr>
                                         <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                            <th  style="text-align: center">(11)</th>
                                            <th  style="text-align: center">(12)</th>
                                            <th  style="text-align: center">(13)</th>
                                            <th  style="text-align: center">(14)</th>
                                            <th  style="text-align: center">(15)</th>
                                            <th  style="text-align: center">(16)</th>
                                            <th  style="text-align: center">(17)</th>
                                            <th  style="text-align: center">(18)</th>
                                            <th  style="text-align: center">(19)</th>
                                            <th  style="text-align: center">(20)</th>
                                            <th  style="text-align: center">(21)</th>
                                            <th  style="text-align: center">(22)</th>
                                            <th  style="text-align: center">(23)</th>
                                            <th  style="text-align: center">(24)</th>
                                            <th  style="text-align: center">(25)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        
                                        <logic:iterate id="list" name="masterList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.noOfSchools}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicasanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.physicalsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsance_DrinkingWater}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_DrinkingWater}
                                                </td>

                                                <td style="text-align: center">
                                                    ${list.FINANCIAL_SANC_IN_LAKHS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.EstimatedCost}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Releases}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Expenditure}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountAtToBeReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmit}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtUCS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtApwidc}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b>${list.total_noOfSchools}</b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicasanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_physicalsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsance_DrinkingWater} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_DrinkingWater} </b>
                                            </th>


                                            <th style="text-align: center">
                                                <b> ${list.total_FINANCIAL_SANC_IN_LAKHS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_EstimatedCost} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Releases} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Expenditure} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_amountAtToBeReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_ucsSubmit} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtUCS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtApwidc} </b>
                                            </th>
                                        </tr>
                               </table>
                        </logic:present>
                        <logic:present name="mandalList">
                            <% String phaseNo2 = null;
                                phaseNo2 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo2);
                            %>

                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> Mandal Name </th>
                                            <th rowspan="2" style="text-align: center">No of schools</th>
                                            <th colspan="2" style="text-align: center">Art/Craft room</th>
                                            <th colspan="2" style="text-align: center">Computer Room</th>
                                            <th colspan="2" style="text-align: center">Science Lab</th>
                                            <th colspan="2" style="text-align: center">Library Room</th>
                                            <th colspan="2" style="text-align: center">Additional Class Room</th>
                                            <th colspan="2" style="text-align: center">Toilet Block</th>
                                            <th colspan="2" style="text-align: center">Drinking Water</th>

                                            <th rowspan="2" style="text-align: center">Financial Sanction In Lakhs</th>
                                            <th rowspan="2" style="text-align: center">Eligible Cost</th>
                                            <th rowspan="2" style="text-align: center">Releases</th>
                                            <th rowspan="2" style="text-align: center">Expenditure</th>
                                            <th rowspan="2" style="text-align: center">Amount Yet To Be Released</th>
                                            <th rowspan="2" style="text-align: center">UCS Submit</th>
                                            <th rowspan="2" style="text-align: center">Balance At UCS</th>
                                            <th rowspan="2" style="text-align: center">Balance At Apwidc</th>

                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                        </tr>
                                         <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                            <th  style="text-align: center">(11)</th>
                                            <th  style="text-align: center">(12)</th>
                                            <th  style="text-align: center">(13)</th>
                                            <th  style="text-align: center">(14)</th>
                                            <th  style="text-align: center">(15)</th>
                                            <th  style="text-align: center">(16)</th>
                                            <th  style="text-align: center">(17)</th>
                                            <th  style="text-align: center">(18)</th>
                                            <th  style="text-align: center">(19)</th>
                                            <th  style="text-align: center">(20)</th>
                                            <th  style="text-align: center">(21)</th>
                                            <th  style="text-align: center">(22)</th>
                                            <th  style="text-align: center">(23)</th>
                                            <th  style="text-align: center">(24)</th>
                                            <th  style="text-align: center">(25)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <logic:iterate id="list" name="mandalList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                   ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.noOfSchools}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicasanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.physicalsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsance_DrinkingWater}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_DrinkingWater}
                                                </td>

                                                <td style="text-align: center">
                                                    ${list.FINANCIAL_SANC_IN_LAKHS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.EstimatedCost}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Releases}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Expenditure}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountAtToBeReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmit}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtUCS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtApwidc}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b>${list.total_noOfSchools}</b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicasanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_physicalsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsance_DrinkingWater} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_DrinkingWater} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_FINANCIAL_SANC_IN_LAKHS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_EstimatedCost} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Releases} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Expenditure} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_amountAtToBeReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_ucsSubmit} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtUCS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtApwidc} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <logic:present name="schoolList">
                            <% String phaseNo3 = null;
                                phaseNo3 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo3);
                            %>

                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> School Name </th>
                                            <th colspan="2" style="text-align: center">Art/Craft room</th>
                                            <th colspan="2" style="text-align: center">Computer Room</th>
                                            <th colspan="2" style="text-align: center">Science Lab</th>
                                            <th colspan="2" style="text-align: center">Library Room</th>
                                            <th colspan="2" style="text-align: center">Additional Class Room</th>
                                            <th colspan="2" style="text-align: center">Toilet Block</th>
                                            <th colspan="2" style="text-align: center">Drinking Water</th>

                                            <th rowspan="2" style="text-align: center">Financial Sanction In Lakhs</th>
                                            <th rowspan="2" style="text-align: center">Eligible Cost</th>
                                            <th rowspan="2" style="text-align: center">Releases</th>
                                            <th rowspan="2" style="text-align: center">Expenditure</th>
                                            <th rowspan="2" style="text-align: center">Amount Yet To Be Released</th>
                                            <th rowspan="2" style="text-align: center">UCS Submit</th>
                                            <th rowspan="2" style="text-align: center">Balance At UCS</th>
                                            <th rowspan="2" style="text-align: center">Balance At Apwidc</th>

                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                            <th rowspan="1" style="text-align: left">Physical Sanction</th>
                                            <th rowspan="1" style="text-align: left">Financial Sanction</th>
                                        </tr>
                                         <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                            <th  style="text-align: center">(11)</th>
                                            <th  style="text-align: center">(12)</th>
                                            <th  style="text-align: center">(13)</th>
                                            <th  style="text-align: center">(14)</th>
                                            <th  style="text-align: center">(15)</th>
                                            <th  style="text-align: center">(16)</th>
                                            <th  style="text-align: center">(17)</th>
                                            <th  style="text-align: center">(18)</th>
                                            <th  style="text-align: center">(19)</th>
                                            <th  style="text-align: center">(20)</th>
                                            <th  style="text-align: center">(21)</th>
                                            <th  style="text-align: center">(22)</th>
                                            <th  style="text-align: center">(23)</th>
                                            <th  style="text-align: center">(24)</th>
                                          
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <logic:iterate id="list" name="schoolList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.Physicasanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.physicalsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsance_DrinkingWater}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_DrinkingWater}
                                                </td>

                                                <td style="text-align: center">
                                                    ${list.FINANCIAL_SANC_IN_LAKHS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.EstimatedCost}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Releases}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Expenditure}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountAtToBeReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmit}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtUCS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtApwidc}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicasanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_physicalsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsance_DrinkingWater} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_DrinkingWater} </b>
                                            </th>


                                            <th style="text-align: center">
                                                <b> ${list.total_FINANCIAL_SANC_IN_LAKHS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_EstimatedCost} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Releases} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Expenditure} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_amountAtToBeReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_ucsSubmit} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtUCS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtApwidc} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <!-- End of both selection of report type --> 
                        <!-- Start of Physical selection  of report type --> 
                        <logic:present name="physicalStateList">
                            <% String phaseNo4 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo4);
                            %>
 
                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> District </th>
                                            <th rowspan="2" style="text-align: center">No of schools</th>
                                            <th colspan="7" style="text-align: center">Physical Sanction</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Art/Craft room</th>
                                            <th rowspan="1" style="text-align: left">Computer Room</th>
                                            <th rowspan="1" style="text-align: left">Science Lab</th>
                                            <th rowspan="1" style="text-align: left">Library Room</th>
                                            <th rowspan="1" style="text-align: left">Additional Class Room</th>
                                            <th rowspan="1" style="text-align: left">Toilet Block</th>
                                            <th rowspan="1" style="text-align: left">Drinking Water</th>
                                        </tr>
                                        <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                      
                                        <logic:iterate id="list" name="physicalStateList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.noOfSchools}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicasanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.physicalsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsance_DrinkingWater}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b>${list.total_noOfSchools}</b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicasanc_artcraftroom} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_CompRoom} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_ScienceLab} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Library} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_physicalsanc_ACR} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Toilet} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsance_DrinkingWater} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <logic:present name="physicalMandalList">
                            <% String phaseNo5 = null;
                                phaseNo5 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo5);
                            %>

                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> Mandal Name</th>
                                            <th rowspan="2" style="text-align: center">No of schools</th>
                                            <th colspan="7" style="text-align: center">Physical Sanction</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Art/Craft room</th>
                                            <th rowspan="1" style="text-align: left">Computer Room</th>
                                            <th rowspan="1" style="text-align: left">Science Lab</th>
                                            <th rowspan="1" style="text-align: left">Library Room</th>
                                            <th rowspan="1" style="text-align: left">Additional Class Room</th>
                                            <th rowspan="1" style="text-align: left">Toilet Block</th>
                                            <th rowspan="1" style="text-align: left">Drinking Water</th>
                                        </tr>
                                       <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <logic:iterate id="list" name="physicalMandalList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.noOfSchools}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicasanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.physicalsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsance_DrinkingWater}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b>${list.total_noOfSchools}</b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicasanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_physicalsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsance_DrinkingWater} </b>
                                            </th>
                                        </tr>
                               </table>
                            </div>
                        </logic:present>
                        <logic:present name="physicalSchoolList">
                            <% String phaseNo6 = null;
                                phaseNo6 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo6);
                            %>
                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> School Name </th>
                                            <th colspan="7" style="text-align: center">Physical Sanction</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Art/Craft room</th>
                                            <th rowspan="1" style="text-align: left">Computer Room</th>
                                            <th rowspan="1" style="text-align: left">Science Lab</th>
                                            <th rowspan="1" style="text-align: left">Library Room</th>
                                            <th rowspan="1" style="text-align: left">Additional Class Room</th>
                                            <th rowspan="1" style="text-align: left">Toilet Block</th>
                                            <th rowspan="1" style="text-align: left">Drinking Water</th>
                                        </tr>
                                        <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <logic:iterate id="list" name="physicalSchoolList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.Physicasanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.physicalsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Physicalsance_DrinkingWater}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicasanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_physicalsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Physicalsance_DrinkingWater} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <!-- End of Physical selection of report type --> 

                        <!-- Start of Physical selection  of report type --> 
                        <logic:present name="financialStateList">
                            <% String phaseNo7 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo7);
                            %>
                          

                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center">District</th>
                                            <th rowspan="2" style="text-align: center">No of schools</th>
                                            <th colspan="7" style="text-align: center">Financial Sanction</th>

                                            <th rowspan="2" style="text-align: center">Financial Sanction In Lakhs</th>
                                            <th rowspan="2" style="text-align: center">Eligible Cost</th>
                                            <th rowspan="2" style="text-align: center">Releases</th>
                                            <th rowspan="2" style="text-align: center">Expenditure</th>
                                            <th rowspan="2" style="text-align: center">Amount Yet To Be Released</th>
                                            <th rowspan="2" style="text-align: center">UCS Submit</th>
                                            <th rowspan="2" style="text-align: center">Balance At UCS</th>
                                            <th rowspan="2" style="text-align: center">Balance At Apwidc</th>

                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: left">Art/Craft room</th>
                                            <th rowspan="1" style="text-align: left">Computer Room</th>
                                            <th rowspan="1" style="text-align: left">Science Lab</th>
                                            <th rowspan="1" style="text-align: left">Library Room</th>
                                            <th rowspan="1" style="text-align: left">Additional Class Room</th>
                                            <th rowspan="1" style="text-align: left">Toilet Block</th>
                                            <th rowspan="1" style="text-align: left">Drinking Water</th>
                                        </tr>
                                        <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                            <th  style="text-align: center">(11)</th>
                                            <th  style="text-align: center">(12)</th>
                                            <th  style="text-align: center">(13)</th>
                                            <th  style="text-align: center">(14)</th>
                                            <th  style="text-align: center">(15)</th>
                                            <th  style="text-align: center">(16)</th>
                                            <th  style="text-align: center">(17)</th>
                                            <th  style="text-align: center">(18)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <logic:iterate id="list" name="financialStateList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.noOfSchools}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_DrinkingWater}
                                                </td>

                                                <td style="text-align: center">
                                                    ${list.FINANCIAL_SANC_IN_LAKHS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.EstimatedCost}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Releases}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Expenditure}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountAtToBeReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmit}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtUCS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtApwidc}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b>${list.total_noOfSchools}</b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_artcraftroom} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_CompRoom} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ScienceLab} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Library} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ACR} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Toilet} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_DrinkingWater} </b>
                                            </th>

                                            <th style="text-align: center">
                                                <b> ${list.total_FINANCIAL_SANC_IN_LAKHS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_EstimatedCost} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Releases} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Expenditure} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_amountAtToBeReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_ucsSubmit} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtUCS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtApwidc} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <logic:present name="financialMandalList">
                            <% String phaseNo8 = null;
                                phaseNo8 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo8);
                            %>

                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center"> Sl.No </th>
                                            <th rowspan="2" style="text-align: center"> Mandal Name </th>
                                            <th rowspan="2" style="text-align: center">No of schools</th>
                                            <th colspan="7" style="text-align: center">Financial Sanction</th>
                                            
                                            <th rowspan="2" style="text-align: center">Financial Sanction In Lakhs</th>
                                            <th rowspan="2" style="text-align: center">Eligible Cost</th>
                                            <th rowspan="2" style="text-align: center">Releases</th>
                                            <th rowspan="2" style="text-align: center">Expenditure</th>
                                            <th rowspan="2" style="text-align: center">Amount Yet To Be Released</th>
                                            <th rowspan="2" style="text-align: center">UCS Submit</th>
                                            <th rowspan="2" style="text-align: center">Balance At UCS</th>
                                            <th rowspan="2" style="text-align: center">Balance At Apwidc</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: center">Art/Craft room</th>
                                            <th rowspan="1" style="text-align: center">Computer Room</th>
                                            <th rowspan="1" style="text-align: center">Science Lab</th>
                                            <th rowspan="1" style="text-align: center">Library Room</th>
                                            <th rowspan="1" style="text-align: center">Additional Class Room</th>
                                            <th rowspan="1" style="text-align: center">Toilet Block</th>
                                            <th rowspan="1" style="text-align: center">Drinking Water</th>
                                        </tr>
                                        <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                            <th  style="text-align: center">(11)</th>
                                            <th  style="text-align: center">(12)</th>
                                            <th  style="text-align: center">(13)</th>
                                            <th  style="text-align: center">(14)</th>
                                            <th  style="text-align: center">(15)</th>
                                            <th  style="text-align: center">(16)</th>
                                            <th  style="text-align: center">(17)</th>
                                            <th  style="text-align: center">(18)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                       
                                        <logic:iterate id="list" name="financialMandalList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.noOfSchools}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_DrinkingWater}
                                                </td>

                                                <td style="text-align: center">
                                                    ${list.FINANCIAL_SANC_IN_LAKHS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.EstimatedCost}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Releases}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Expenditure}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountAtToBeReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmit}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtUCS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtApwidc}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b>${list.total_noOfSchools}</b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ScienceLab} </b>
                                            </th>
                                             <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Toilet} </b>
                                            </th>
                                            
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_DrinkingWater} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_FINANCIAL_SANC_IN_LAKHS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_EstimatedCost} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Releases} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Expenditure} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_amountAtToBeReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_ucsSubmit} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtUCS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtApwidc} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <logic:present name="financialSchoolList">
                            <% String phaseNo9 = null;
                                phaseNo9 = request.getAttribute("phaseNo").toString();
                                request.setAttribute("phaseNo", phaseNo9);
                            %>
                            <div style="overflow-x: scroll; width: 1000px; margin: 0 20px;">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th rowspan="2" style="text-align: center">Sl.No</th>
                                            <th rowspan="2" style="text-align: center">School Name</th>
                                            <th colspan="7" style="text-align: center">Financial Sanction</th>
                                            
                                            <th rowspan="2" style="text-align: center">Financial Sanction In Lakhs</th>
                                            <th rowspan="2" style="text-align: center">Eligible Cost</th>
                                            <th rowspan="2" style="text-align: center">Releases</th>
                                            <th rowspan="2" style="text-align: center">Expenditure</th>
                                            <th rowspan="2" style="text-align: center">Amount Yet To Be Released</th>
                                            <th rowspan="2" style="text-align: center">UCS Submit</th>
                                            <th rowspan="2" style="text-align: center">Balance At UCS</th>
                                            <th rowspan="2" style="text-align: center">Balance At Apwidc</th>

                                        </tr>
                                        <tr>
                                            <th rowspan="1" style="text-align: center">Art/Craft room</th>
                                            <th rowspan="1" style="text-align: center">Computer Room</th>
                                            <th rowspan="1" style="text-align: center">Science Lab</th>
                                            <th rowspan="1" style="text-align: center">Library Room</th>
                                            <th rowspan="1" style="text-align: center">Additional Class Room</th>
                                            <th rowspan="1" style="text-align: center">Toilet Block</th>
                                            <th rowspan="1" style="text-align: center">Drinking Water</th>
                                        </tr>
                                         <tr class="darkgrey num_height">
                                            <th  style="text-align: center">(1)</th>
                                            <th  style="text-align: center">(2)</th>
                                            <th  style="text-align: center">(3)</th>
                                            <th  style="text-align: center">(4)</th>
                                            <th  style="text-align: center">(5)</th>
                                            <th  style="text-align: center">(6)</th>
                                            <th  style="text-align: center">(7)</th>
                                            <th  style="text-align: center">(8)</th>
                                            <th  style="text-align: center">(9)</th>
                                            <th  style="text-align: center">(10)</th>
                                            <th  style="text-align: center">(11)</th>
                                            <th  style="text-align: center">(12)</th>
                                            <th  style="text-align: center">(13)</th>
                                            <th  style="text-align: center">(14)</th>
                                            <th  style="text-align: center">(15)</th>
                                            <th  style="text-align: center">(16)</th>
                                            <th  style="text-align: center">(17)</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                       
                                        <logic:iterate id="list" name="financialSchoolList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: left">
                                                    ${list.distName}
                                                </td> 
                                                <td style="text-align: center">
                                                    ${list.financialsanc_artcraftroom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_CompRoom}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ScienceLab}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Library}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_ACR}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_Toilet}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.financialsanc_DrinkingWater}
                                                </td>

                                                <td style="text-align: center">
                                                    ${list.FINANCIAL_SANC_IN_LAKHS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.EstimatedCost}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Releases}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.Expenditure}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountAtToBeReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmit}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtUCS}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.balanceAtApwidc}
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tr>
                                            <th colspan="2" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_artcraftroom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_CompRoom} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ScienceLab} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Library} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_ACR} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_Toilet} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_financialsanc_DrinkingWater} </b>
                                            </th>


                                            <th style="text-align: center">
                                                <b> ${list.total_FINANCIAL_SANC_IN_LAKHS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_EstimatedCost} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Releases} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_Expenditure} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_amountAtToBeReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_ucsSubmit} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtUCS} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${list.total_balanceAtApwidc} </b>
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </logic:present>
                        <!-- End of Financial selection of report type --> 
                    </div>    
                </div>
                <!-- End row --> 
            </div>
            <!-- End Container --> 
        </section>
    </html:form>
</body>
</html>
