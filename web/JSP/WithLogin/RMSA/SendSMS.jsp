<%-- 
    Document   : rmsaComponentReport
    Created on : Jan 23, 2018, 5:20:00 PM
    Author     : 1259084
--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 0;
%>
<html>
    <head>
        <title>:: RMSA ::</title>

        <script type="text/javascript">
            function hodcheckAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].checkall1[0].checked == true)
                    {
                        elems[i].checked = true;
                        document.getElementById("sendButton").style.display = "";
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }
            function hodremoveAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].checkall1[1].checked == true)
                    {
                        elems[i].checked = false;
                        document.getElementById("sendButton").style.display = "none";
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }

            function rdocheckeAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].rdocheckall[0].checked == true)
                    {
                        elems[i].checked = true;
                        document.getElementById("sendButton").style.display = "";
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }
            function rdoremoveAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].rdocheckall[1].checked == true)
                    {
                        elems[i].checked = false;
                        document.getElementById("sendButton").style.display = "none";
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }

            function mlcheckedAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].mlcheck[0].checked == true)
                    {
                        document.getElementById("sendButton").style.display = "";
                        elems[i].checked = true;
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }
            function mlremoveAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].mlcheck[1].checked == true)
                    {
                        document.getElementById("sendButton").style.display = "none";
                        elems[i].checked = false;
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }
            function vlcheckedAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].vlcheck[0].checked == true)
                    {
                        document.getElementById("sendButton").style.display = "";
                        elems[i].checked = true;
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }
            function vlremoveAll() {
                var elems = document.getElementsByName("distoff");
                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].vlcheck[1].checked == true)
                    {
                        document.getElementById("sendButton").style.display = "none";
                        elems[i].checked = false;
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            }
            function textCounter(field, cntfield, maxlimit) {
                if (field.value.length > maxlimit) {  // if too long...trim it!
                    field.value = field.value.substring(0, maxlimit);
                    // otherwise, update 'characters left' counter
                }
                else {
                    cntfield.value = maxlimit - field.value.length;
                }
            }



            function getDetails() {
                var d = document.forms[0];

                var msg = "Are you sure do you want send sms";
                var accept = confirm(msg);
                if (accept)
                {
                    document.forms[0].elements['mode'].value = "sendSMS";
                    //d.mode.value='sendSMS';
                    d.submit();
                    document.getElementById('hide').style.display = 'none';
                    document.getElementById('loding').style.display = '';
                }



            }
            function getTahasildar() {

                document.forms[0].elements['mode'].value = "getTahasildar";
                document.forms[0].submit();
            }
            function getMpdo() {

                document.forms[0].elements['mode'].value = "getMpdo";
                document.forms[0].submit();
            }
            function getOfficersList(selType) {
                if (selType == "ALLOff") {
                    alert("in all condition")
                    if (document.getElementById("allOff").checked == false) {
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.getElementById("All").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {

                        alert("in else")
                        document.forms[0].elements['officerTypeALL'].value = "ALL";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();

                    }
                }
                if (selType == "HODOff") {

                    if (document.getElementById("HODOff").checked == false) {


                        document.forms[0].elements['officerTypeHoDs'].value = "";
                        document.getElementById("HOD1").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {
                        document.forms[0].elements['allOff'].value = "";
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.forms[0].elements['officerTypeHoDs'].value = "HOD1";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    }


                }
                if (selType == "DEOOff") {

                    if (document.getElementById("DEOOff").checked == false) {


                        document.forms[0].elements['officerTypeDEOs'].value = "";
                        document.forms[0].elements['DEOOff'].value = "";
                        document.getElementById("hod").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {

                        document.forms[0].elements['allOff'].value = "";
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.forms[0].elements['officerTypeDEOs'].value = "hod";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    }


                }
                if (selType == "RJDOff") {
                    if (document.getElementById("rjdOff").checked == false) {


                        document.forms[0].elements['officerTypeRJDs'].value = "";
                        //document.forms[0].elements['meoOff'].value = "";
                        document.getElementById("rdo1").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {
                        document.forms[0].elements['allOff'].value = "";
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.forms[0].elements['officerTypeRJDs'].value = "rdo";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    }
                }

                if (selType == "MEOOff") {
                    if (document.getElementById("meoOff").checked == false) {


                        document.forms[0].elements['officerTypeMEOs'].value = "";
                        //document.forms[0].elements['meoOff'].value = "";
                        document.getElementById("ml1").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {
                        document.forms[0].elements['allOff'].value = "";
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.forms[0].elements['officerTypeMEOs'].value = "ml";
                        document.forms[0].elements['mode'].value = "getDistrictList";
                        document.forms[0].submit();
                    }
                }

                if (selType == "HMOff") {
                    if (document.getElementById("hmOff").checked == false) {

                        document.forms[0].elements['officerTypeHMS'].value = "";
                        document.getElementById("vl1").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {
                        document.forms[0].elements['allOff'].value = "";
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.forms[0].elements['officerTypeHMS'].value = "vl";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    }
                }
                if (selType == "OtherOFF") {


                    if (document.getElementById("otherOff").checked == false) {

                        document.forms[0].elements['officerTypeOther'].value = "";
                        document.getElementById("vl1").style.display = "none";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    } else {
                        document.forms[0].elements['allOff'].value = "";
                        document.forms[0].elements['officerTypeALL'].value = "";
                        document.forms[0].elements['officerTypeOther'].value = "State1";
                        document.forms[0].elements['mode'].value = "getSelectedListsms";
                        document.forms[0].submit();
                    }
                }




            }
            function getMandalWiseMeo() {
                document.forms[0].elements['mode'].value = "getSelectedListsms";
                document.forms[0].submit();
            }
            function getDepartmentContactDetailsSms(value) {
                //document.getElementById("hod1").style.display="";
                document.forms[0].elements['mode'].value = "getDepartmentContactDetails";
                document.forms[0].submit();

            <%--      if(value=="hod" ){
                      document.getElementById("hod1").style.display="";
                  }--%>
                          if (value == "hod2") {

                              document.getElementById("hod1").style.display = "";

                          }


                      }

                      function loadHod() {
                          var value = document.forms[0].elements['offtype'].value;

                          if (value == "hod") {

                              document.getElementById("hod1").style.display = "";
                              document.getElementById("rdo1").style.display = "none";
                              document.getElementById("ml1").style.display = "none";
                              document.getElementById("vl1").style.display = "none";

                          } else if (value == "rdo") {

                              document.getElementById("rdo1").style.display = "";
                              document.getElementById("hod1").style.display = "none";

                              document.getElementById("ml1").style.display = "none";
                              document.getElementById("vl1").style.display = "none";

                          } else if (value == "ml") {
                              document.getElementById("hod1").style.display = "none";
                              document.getElementById("rdo1").style.display = "none";
                              document.getElementById("ml1").style.display = "";
                              document.getElementById("vl1").style.display = "none";

                          } else if (value == "vl") {
                              document.getElementById("hod1").style.display = "none";
                              document.getElementById("rdo1").style.display = "none";
                              document.getElementById("ml1").style.display = "none";
                              document.getElementById("vl1").style.display = "";

                          }
                      }



        </script>
        <script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script language="javascript" type="text/javascript">
            $(document).ready(function() {
                $('#search').keyup(function() {
                    searchTable($(this).val());
                });
            });
            $(document).ready(function() {
                $('#search1').keyup(function() {
                    searchTable1($(this).val());
                });
            });
            $(document).ready(function() {
                $('#search2').keyup(function() {
                    searchTable2($(this).val());
                });
            });
            $(document).ready(function() {
                $('#search3').keyup(function() {
                    searchTable3($(this).val());
                });
            });
            $(document).ready(function() {
                $('#search4').keyup(function() {
                    searchTable4($(this).val());
                });
            });
            $(document).ready(function() {
                $('#search5').keyup(function() {
                    searchTable5($(this).val());
                });
            });
            function searchTable(inputVal) {
                var table = $('#data');
                table.find('tr').each(function(index, row) {
                    var allCells = $(row).find('td');
                    if (allCells.length > 0) {
                        var found = false;
                        allCells.each(function(index, td) {
                            var regExp = new RegExp(inputVal, 'i');
                            if (regExp.test($(td).text())) {
                                found = true;
                                return false;
                            }
                        });
                        if (found == true)
                            $(row).show();
                        else
                            $(row).hide();
                    }
                });
            }
            function searchTable1(inputVal) {
                var table = $('#data1');
                table.find('tr').each(function(index, row) {
                    var allCells = $(row).find('td');
                    if (allCells.length > 0) {
                        var found = false;
                        allCells.each(function(index, td) {
                            var regExp = new RegExp(inputVal, 'i');
                            if (regExp.test($(td).text())) {
                                found = true;
                                return false;
                            }
                        });
                        if (found == true)
                            $(row).show();
                        else
                            $(row).hide();
                    }
                });
            }
            function searchTable2(inputVal) {
                var table = $('#data2');
                table.find('tr').each(function(index, row) {
                    var allCells = $(row).find('td');
                    if (allCells.length > 0) {
                        var found = false;
                        allCells.each(function(index, td) {
                            var regExp = new RegExp(inputVal, 'i');
                            if (regExp.test($(td).text())) {
                                found = true;
                                return false;
                            }
                        });
                        if (found == true)
                            $(row).show();
                        else
                            $(row).hide();
                    }
                });
            }
            function searchTable3(inputVal) {
                var table = $('#data3');
                table.find('tr').each(function(index, row) {
                    var allCells = $(row).find('td');
                    if (allCells.length > 0) {
                        var found = false;
                        allCells.each(function(index, td) {
                            var regExp = new RegExp(inputVal, 'i');
                            if (regExp.test($(td).text())) {
                                found = true;
                                return false;
                            }
                        });
                        if (found == true)
                            $(row).show();
                        else
                            $(row).hide();
                    }
                });
            }
            function searchTable4(inputVal) {
                var table = $('#data4');
                table.find('tr').each(function(index, row) {
                    var allCells = $(row).find('td');
                    if (allCells.length > 0) {
                        var found = false;
                        allCells.each(function(index, td) {
                            var regExp = new RegExp(inputVal, 'i');
                            if (regExp.test($(td).text())) {
                                found = true;
                                return false;
                            }
                        });
                        if (found == true)
                            $(row).show();
                        else
                            $(row).hide();
                    }
                });
            }
            function searchTable5(inputVal) {
                var table = $('#data5');
                table.find('tr').each(function(index, row) {
                    var allCells = $(row).find('td');
                    if (allCells.length > 0) {
                        var found = false;
                        allCells.each(function(index, td) {
                            var regExp = new RegExp(inputVal, 'i');
                            if (regExp.test($(td).text())) {
                                found = true;
                                return false;
                            }
                        });
                        if (found == true)
                            $(row).show();
                        else
                            $(row).hide();
                    }
                });
            }

            function validate() {
                var sms = $.trim($("#sms").val());
                if (sms == "") {
                    alert('SMS field cannot be empty');
                    $("#sms").focus();
                    return false;
                }
                if ($('#offtype').val() == '00') {
                    alert('Please select Officer Type');
                    $('#offtype').focus();
                    return false;
                }
                var k = false;
                $('.distoff').each(function() {


                    if (this.checked) {
                        k = true;
                    }


                });
                if (!k) {
                    alert("Atleast one contact must be selected");
                    return false();
                }
                getDetails();
            }
            function validateButton() {
                var k = false;
                $('.distoff').each(function() {
                    if (this.checked) {
                        document.getElementById("sendButton").style.display = "";
                        k = true;
                    }
                    if (k == false) {
                        document.getElementById("sendButton").style.display = "none";
                    }
                });

            }
            function allSMS() {
            <%if (request.getAttribute("hod1List") != null) {%>
                document.getElementById("allOff").disabled = true;
                document.getElementById("sendButton").style.display = "none";
            <%}%>
            <%if (request.getAttribute("deptlist") != null) {%>
                document.getElementById("allOff").disabled = true;
            <%}%>
            <%if (request.getAttribute("mpdoList") != null) {%>
                document.getElementById("allOff").disabled = true;
            <%}%>
            <%if (request.getAttribute("rdoList") != null) {%>
                document.getElementById("allOff").disabled = true;
            <%}%>
            <%if (request.getAttribute("state1List") != null) {%>
                document.getElementById("allOff").disabled = true;
            <%}%>
            <%if (request.getAttribute("vlList") != null) {%>
                document.getElementById("allOff").disabled = true;
            <%}%>
            <%if (request.getAttribute("ALL") != null) {%>

                document.getElementById("data").style.display = "none";
                //document.getElementsByName("checkall1").checked=true;
                document.getElementById("checkall1").checked = true;
                document.getElementById("allOff").disabled = false;
                document.getElementById("DEOOff").disabled = true;
                document.getElementById("meoOff").disabled = true;
                document.getElementById("rjdOff").disabled = true;
                document.getElementById("hmOff").disabled = true;
                document.getElementById("otherOff").disabled = true;
                document.getElementById("HODOff").disabled = true;
                var elems = document.getElementsByName("distoff");
                //            document.getElementById("sendButton").style.display = "block";

                for (var i = 0; i < elems.length; i++)
                {
                    if (document.forms[0].checkall1[0].checked == true)
                    {
                        elems[i].checked = true;
                        document.getElementById("sendButton").style.display = "block";
                    }
                    else {
                        elems[i].checked = false;
                    }
                }
            <%} else {
            %>
                document.getElementById("allOff").checked = false;
            <%}%>
            }
        </script>

        <script type="text/javascript">
            window.onload = function() {
                altRows('altrowstable');

            };
        </script>
        <script type="text/javascript">
            window.onload = function() {
                altRows('altrowstable1');
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            };
        </script>
        <style type="text/css">
            .table td, th {
    padding: 5px !important;
    font-size: 12px !important;
}
.table>tbody>tr>td {
        border: 1px solid #ccc !important;
}
input[type="radio"], input[type="checkbox"] {
    margin: 4px 7px 0 !important;
}
        </style>

    </head>
    <body>
        <html:form action="/sendSMS" method="post" enctype="multipart/form-data">
            <html:hidden property="mode"/>
            <html:hidden property="officerPost"/>
            <%  String msg = null;
                if (request.getAttribute("msg") != null) {
                    msg = request.getAttribute("msg").toString();

            %>
        <center><div id="msg"><span style="color:#0095ff !important"><b><%=msg%><b></span></div></center><%}%>

                            <section class="testimonial_sec clear_fix">
                                <div class="container">
                                    <div class="row">
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">

                                            <div class="innerbodyrow">
                                                <div class="col-xs-12" style="padding: 0px;">
                                                    <h3>Broadcast SMS </h3>
                                                </div>

                                                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table">
                                                    <tr>
                                                        <td>
                                                             <tr>
                                                                    <td width="30%"  align="left" >
                                                                        <strong>SMS<font color="red">*</font>&nbsp;</strong>
                                                                    </td>
                                                                    <td width="70%" colspan="5" >
                                                                        <html:textarea property="sms" rows="5" cols="50" styleId="sms" onkeydown="textCounter(this,document.forms[0].remLen1,150)" onkeyup="textCounter(this,document.forms[0].remLen1,150)" onkeypress="return space(event,this)"/>
                                                                        <input readonly type="text" name="remLen1" size="3" maxlength="3" value="150">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="40%" class="formbg1" style="text-align: left">
                                                                        <strong> Officers Type<font color="red">*</font>&nbsp;</strong>
                                                                    </td>
                                                                    <td width="80%" class="gridbg3" style="text-align: left" >
                                                                       <!-- <html:checkbox property="officerType" value="00" onclick="getOfficersList();"/>All
                                                                        <html:checkbox property="officerTypeHoDs" styleId="officerTypeHoDs"  value="HOD1" onclick="getOfficersList();"/>HoDs
                                                                        <html:checkbox property="officerType" value="rdo" onclick="getOfficersList();"/>RJDs
                                                                        <html:checkbox property="officerTypeDEOs" styleId="officerTypeDEOs" value="hod" onclick="getOfficersList();"/>DEOs
                                                                        <html:checkbox property="officerTypeMEOs" styleId="officerTypeMEOs" value="ml" onclick="getOfficersList();"/>MEOs
                                                                        <html:checkbox property="officerType" value="vl" onclick="getOfficersList();"/>HMs
                                                                        <html:checkbox property="officerType" value="State1" onclick="getOfficersList();"/>Others-->
                                                                        <html:hidden property="officerTypeALL" styleId="officerTypeALL"/>
                                                                        <html:hidden property="officerTypeHoDs" styleId="officerTypeHoDs"/>
                                                                        <html:hidden property="officerTypeDEOs" styleId="officerTypeDEOs"/>
                                                                        <html:hidden property="officerTypeRJDs" styleId="officerTypeRJDs"/>
                                                                        <html:hidden property="officerTypeMEOs" styleId="officerTypeMEOs"/>
                                                                        <html:hidden property="officerTypeHMS" styleId="officerTypeHMS"/>
                                                                        <html:hidden property="officerTypeOther" styleId="officerTypeOther"/>
                                                                        <html:checkbox  property="allOff" styleId="allOff" value="ALLOff" onclick="getOfficersList(this.value);" style="display:none"/>
                                                                        <html:checkbox  property="HODOff" styleId="HODOff" value="HODOff" onclick="getOfficersList(this.value);" style="display:none"/>
                                                                        <html:checkbox  property="DEOOff" styleId="DEOOff" value="DEOOff" onclick="getOfficersList(this.value);" style="display:none"/>
                                                                        <html:checkbox  property="rjdOff" styleId="rjdOff" value="RJDOff" onclick="getOfficersList(this.value);" style="display:none"/>
                                                                        <html:checkbox  property="meoOff" styleId="meoOff" value="MEOOff" onclick="getOfficersList(this.value);"/>Schools
                                                                        <html:checkbox  property="hmOff" styleId="hmOff" value="HMOff" onclick="getOfficersList(this.value);" style="display:none"/>
                                                                        <html:checkbox  property="otherOff" styleId="otherOff" value="OtherOFF" onclick="getOfficersList(this.value);" style="display:none"/>

                                                                    </td>
                                                                </tr>
                                                            
                                                        </td>
                                                        
                                                        <td>
                                                            <table align="center" cellpadding="5" cellspacing="0" border="0" width="100%" id="altrowstable1" class="altrowstable1">
                                                                <logic:present name="distArrayList">
                                                                    <tr>
                                                                        <td width="40%" class="formbg1" style="text-align: left">
                                                                            <strong>Select District<font color="red">*</font>&nbsp;</strong>
                                                                        </td>
                                                                        <td width="80%" class="gridbg3" style="text-align: left">
                                                                            <html:select property="districtId" style="width:200px" onchange="getMandalWiseMeo();">
                                                                                <html:option value="00"> ---All-- </html:option> 
                                                                                <html:optionsCollection property="distList" label="distname" value="distCode"/>
                                                                            </html:select> 
                                                                        </td>
                                                                    </tr>
                                                                </logic:present>
                                                                <%if (request.getAttribute("ALL") != null) {%>
                                                                <tr id="All" style="display: block" >
                                                                    <%} else {%>
                                                                <tr id="All" style="display: none">
                                                                    <%}%>


                                                                    <logic:notEmpty name="ALL" >
                                                                    <tr id="All">

                                                                        <td  width="20%" class="formbg1" style="text-align: left">
                                                                            <strong>HOD's<font color="red">*</font>&nbsp;</strong>
                                                                        </td>
                                                                        <td width="80%" class="gridbg3" style="text-align: left">
                                                                            Select All <input type="radio" name="checkall1" id="checkall1" onclick="hodcheckAll();"  style="border: none">
                                                                            <input type="radio" name="checkall1" onclick="hodremoveAll();"  style="border: none;display: none">
                                                                            <br/> <br/>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="data" >
                                                                                <tr>
                                                                                    <th class="grn-hd-txt" colspan="2" style="text-align: left">HOD Officers</th>
                                                                                    <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search" title="Search" style="width:auto;" />
                                                                                    </th>
                                                                                </tr>
                                                                                <%i = 2;%>
                                                                                <tr class="tr1" id="one" >
                                                                                    <logic:iterate name="ALL" id="deff">
                                                                                        <% if (i > 1) {%>
                                                                                        <td class="gridbg0" colspan="2">
                                                                                            <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.designation}-${deff.department}<!-- (${deff.department})-->
                                                                                        </td>

                                                                                        <%  i--;
                                                                                        } else {%>
                                                                                        <td class="gridbg0" colspan="2">
                                                                                            <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.designation}-${deff.department}<!-- (${deff.department})-->
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="tr1" id="one">
                                                                                        <% i = 2;
                                                                                            }%>
                                                                                    </logic:iterate>
                                                                                </tr>
                                                                            </table>
                                                                        </td>

                                                                    </logic:notEmpty>
                                                                    <%if (request.getAttribute("hod1List") != null) {%>
                                                                <tr id="HOD1" style="display: block" >
                                                                    <%} else {%>
                                                                <tr id="HOD1" style="display: none">
                                                                    <%}%>



                                                                <tr id="HOD1">
                                                                    <logic:notEmpty name="hod1List" >
                                                                        <td  width="20%" class="formbg1" style="text-align: left">
                                                                            <strong>HOD's<font color="red">*</font>&nbsp;</strong>
                                                                        </td>
                                                                        <td width="80%" class="gridbg3" style="text-align: left">
                                                                            Select All <input type="radio" name="checkall1" onclick="hodcheckAll();"  style="border: none">
                                                                            Deselect All <input type="radio" name="checkall1" onclick="hodremoveAll();"  style="border: none">
                                                                            <br/> <br/>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="data" >
                                                                                <tr>
                                                                                    <th class="grn-hd-txt" colspan="2" style="text-align: left">HOD Officers</th>
                                                                                    <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search" title="Search" style="width:auto;" />
                                                                                    </th>
                                                                                </tr>
                                                                                <%i = 2;%>
                                                                                <tr class="tr1" id="one" >
                                                                                    <logic:iterate name="hod1List" id="deff">
                                                                                        <% if (i > 1) {%>
                                                                                        <td class="gridbg0" colspan="2">
                                                                                            <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.designation}-${deff.department}<!-- (${deff.department})-->
                                                                                        </td>

                                                                                        <%  i--;
                                                                                        } else {%>
                                                                                        <td class="gridbg0" colspan="2">
                                                                                            <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.designation}-${deff.department}<!-- (${deff.department})-->
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="tr1" id="one">
                                                                                        <% i = 2;
                                                                                            }%>
                                                                                    </logic:iterate>
                                                                                </tr>
                                                                            </table>
                                                                        </td>

                                                                    </logic:notEmpty>


                                                                    <logic:notEmpty name="deptlist" >
                                                                    <tr  id="hod">
                                                                        <td width="20%" class="formbg1" style="text-align: left">
                                                                            <strong>DEO's<font color="red">*</font>&nbsp;</strong>
                                                                        </td>
                                                                        <td width="80%" class="gridbg3" style="text-align: left">
                                                                            Select All <input type="radio" name="checkall1" onclick="hodcheckAll();"  style="border: none">
                                                                            Deselect All <input type="radio" name="checkall1" onclick="hodremoveAll();"  style="border: none">
                                                                            <br/> <br/>
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="data" >
                                                                                <tr>
                                                                                    <th class="grn-hd-txt" colspan="2" style="text-align: left">DEOs </th>
                                                                                    <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search" title="Search" style="width:auto;" />
                                                                                    </th>
                                                                                </tr>
                                                                                <%i = 2;%>
                                                                                <tr class="tr1" id="one" >
                                                                                    <logic:iterate name="deptlist" id="deff">
                                                                                        <% if (i > 1) {%>
                                                                                        <td class="gridbg0" colspan="2">
                                                                                            <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.department}-${deff.name}                                                    </td>

                                                                                        <%  i--;
                                                                                        } else {%>
                                                                                        <td class="gridbg0" colspan="2">
                                                                                            <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.department}-${deff.name}
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="tr1" id="one">
                                                                                        <% i = 2;
                                                                                            }%>
                                                                                    </logic:iterate>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </logic:notEmpty>


                                                    </tr>
                                                    <%if (request.getAttribute("state1List") != null) {%>
                                                    <tr id="State1" style="display: block">
                                                        <%} else {%>
                                                    <tr id="State1" style="display: none">
                                                        <%}%>
                                                        <logic:notEmpty name="state1List" >
                                                        <tr id="State1" >
                                                            <td width="20%" class="formbg1" style="text-align: left">
                                                                <strong>HOD's<font color="red">*</font>&nbsp;</strong>
                                                            </td>
                                                            <td width="80%" class="gridbg3" style="text-align: left">
                                                                Select All <input type="radio" name="checkall1" onclick="hodcheckAll();"  style="border: none">
                                                                Deselect All <input type="radio" name="checkall1" onclick="hodremoveAll();"  style="border: none">
                                                                <br/> <br/>
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="data" >
                                                                    <tr>
                                                                        <th class="grn-hd-txt" colspan="2" style="text-align: left">Others</th>
                                                                        <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search" title="Search" style="width:auto;" />
                                                                        </th>
                                                                    </tr>
                                                                    <%i = 2;%>
                                                                    <tr class="tr1" id="one" >
                                                                        <logic:iterate name="state1List" id="deff">
                                                                            <% if (i > 1) {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.designation}-${deff.name}
                                                                            </td>

                                                                            <%  i--;
                                                                            } else {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${deff.id}" onclick="validateButton();" />${deff.designation}-${deff.name}
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="tr1" id="one">
                                                                            <% i = 2;
                                                                                }%>
                                                                        </logic:iterate>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                        </logic:notEmpty>
                                                    </tr>
                                                    <%if (request.getAttribute("rdoList") != null) {%>
                                                    <tr id="State1" style="display: block">
                                                        <%} else {%>
                                                    <tr id="State1" style="display: none">
                                                        <%}%>
                                                        <logic:notEmpty name="rdoList">
                                                        <tr id="rdo1">


                                                            <td width="20%" class="formbg1" align="left">
                                                                <strong>RDO's<font color="red">*</font>&nbsp;</strong>
                                                            </td>
                                                            <td width="80%" class="gridbg3" align="left">
                                                                Select All <input type="radio" name="rdocheckall" onclick="rdocheckeAll();" style="border: none">
                                                                Deselect All <input type="radio" name="rdocheckall" onclick="rdoremoveAll();"  style="border: none"><br/>
                                                                <br/> <br/>
                                                                <logic:present name="rdoList">

                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="data1">
                                                                        <tr>
                                                                            <th class="grn-hd-txt" colspan="2" style="text-align: left">RJD Officers</th>
                                                                            <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search1" title="Search" style="width:auto;" />
                                                                            </th>
                                                                        </tr>
                                                                        <%i = 2;%>
                                                                        <tr class="tr1" >
                                                                            <logic:iterate name="rdoList" id="rdos">
                                                                                <% if (i > 1) {%>
                                                                                <td class="gridbg0" colspan="2">
                                                                                    <html:checkbox property="distoff" styleClass="distoff" value="${rdos.id}" onclick="validateButton();"/>${rdos.department}-${rdos.name}
                                                                                </td>

                                                                                <%  i--;
                                                                                } else {%>
                                                                                <td class="gridbg0" colspan="2">
                                                                                    <html:checkbox property="distoff" styleClass="distoff" value="${rdos.id}" onclick="validateButton();"/>${rdos.department}-${rdos.name}
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="tr1" id="one">
                                                                                <% i = 2;
                                                                                    }%>
                                                                            </logic:iterate>
                                                                        </tr>
                                                                    </table>
                                                                </logic:present>
                                                            </td>

                                                        </tr>
                                                    </logic:notEmpty>
                                                    <!--<%if (request.getAttribute("mpdoList") != null) {%>-->
                                                    <tr id="ml1" style="display: block">
                                                        <!-- <%} else {%>-->
                                                    <tr id="ml1" style="display: none">
                                                        <!--<%}%>-->



                                                        <!--                                    Tahsildar<html:radio property="radioButtonProperty" value="Tahasildar" onclick="getTahasildar();"  style="border: none;width:45px"/>
                                                                                            MPDO<html:radio property="radioButtonProperty" value="MPDO" onclick="getMpdo();"  style="border: none;width:110px"/><br/>-->

                                                        <!--                                    Select All 11111111111<input type="radio" name="mlcheck" onclick="mlcheckedAll();"  style="border: none;width: 40px">
                                                                                            Deselect All <input type="radio" name="mlcheck" onclick="mlremoveAll();"  style="border: none;width: 40px"><br/><br/>-->

                                                </table>
                                                <logic:present name="list">

                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="gridtb1" ID="data2">
                                                        <tr>
                                                            <th class="grn-hd-txt" colspan="2" style="text-align: left">Tahsildars</th>
                                                            <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search2" title="Search" style="width:auto;" />
                                                            </th>
                                                        </tr>

                                                        <%i = 2;%>
                                                        <tr class="tr1" id="one">

                                                            <logic:iterate name="list" id="tash">


                                                                <% if (i > 1) {%>
                                                                <td class="gridbg0" colspan="2">
                                                                    <html:checkbox property="distoff" styleClass="distoff" value="${tash.id}" onclick="validateButton();"/>&nbsp;${tash.name}&nbsp(${tash.mandal})
                                                                </td>

                                                                <%  i--;
                                                                } else {%>
                                                                <td class="gridbg0" colspan="2">
                                                                    <html:checkbox property="distoff" styleClass="distoff" value="${tash.id}" onclick="validateButton();"/>&nbsp;${tash.name}&nbsp;(${tash.mandal})
                                                                </td>
                                                            </tr>
                                                            <tr class="tr1" id="one">
                                                                <% i = 2;
                                                                    }%>
                                                            </logic:iterate>
                                                        </tr>
                                                    </table>
                                                    <hr style="color: red">
                                                </logic:present>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <logic:present name="mpdoList">


                                                        <td width="20%" class="formbg1" align="left">
                                                            <strong>School Contacts<font color="red">*</font>&nbsp;</strong>
                                                        </td>
                                                        <td width="80%"  class="gridbg3" style="text-align: center;">
                                                            <!--                                    Tahsildar<html:radio property="radioButtonProperty" value="Tahasildar" onclick="getTahasildar();"  style="border: none;width:45px"/>
                                                                                                MPDO<html:radio property="radioButtonProperty" value="MPDO" onclick="getMpdo();"  style="border: none;width:110px"/><br/>-->

                                                            Select All <input type="radio" name="mlcheck" onclick="mlcheckedAll();"  style="border: none;width: 40px">
                                                            Deselect All <input type="radio" name="mlcheck" onclick="mlremoveAll();"  style="border: none;width: 40px"><br/><br/>
                                                            <style>
                                                                .meodsCont {
                                                                    overflow:scroll;
                                                                    height:800px;
                                                                }
                                                            </style>
                                                            <div class="meodsCont">
                                                                <table cellpadding="0" cellspacing="0" border="1" width="100%"  ID="data3" align="center">
                                                                    <tr>
                                                                        <th class="grn-hd-txt" colspan="2" style="text-align: center;">School Code - School Name</th>
                                                                        <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search3" title="Search" style="width:auto;" />
                                                                        </th>
                                                                    </tr>

                                                                    <br/>
                                                                    <%i = 2;%>
                                                                    <tr class="tr1" id="one" >
                                                                        <logic:iterate name="mpdoList" id="mpd" >
                                                                            <% if (i > 1) {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${mpd.id}" onclick="validateButton();"/>${mpd.designation}-${mpd.name}
                                                                            </td>

                                                                            <%  i--;
                                                                            } else {%>
                                                                            <td class="gridbg0" colspan="2" class="altrowstable1">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${mpd.id}" onclick="validateButton();"/>${mpd.designation}-${mpd.name}
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="tr1" id="one">
                                                                            <% i = 2;
                                                                                }%>
                                                                        </logic:iterate>
                                                                    </tr>
                                                                </table>
                                                            </div>    
                                                        </td>
                                                    </logic:present></tr>

                                                <tr id="vl1" style="display: none">
                                                    <logic:notEmpty name="vlList">

                                                        <td width="20%" class="formbg1" align="left">
                                                            <strong>Village Level Officers<font color="red">*</font>&nbsp;</strong>
                                                        </td>
                                                        <td width="80%" class="gridbg3" align="left" >
                                                            <logic:present name="vlList">
                                                                Select All <input type="radio" name="vlcheck" onclick="vlcheckedAll();"  style="border: none">
                                                                Deselect All <input type="radio" name="vlcheck" onclick="vlremoveAll();"  style="border: none"><br/>
                                                                <br/> <br/>
                                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" id="data4">
                                                                    <tr>
                                                                        <th class="grn-hd-txt" colspan="2" style="text-align: left">Village Revenue Officer</th>
                                                                        <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search4" title="Search" style="width:auto;" />
                                                                        </th>
                                                                    </tr>
                                                                    <%i = 2;%>
                                                                    <tr class="tr1" id="one">
                                                                        <logic:iterate name="vlList" id="vr">
                                                                            <% if (i > 1) {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${vr.id}" onclick="validateButton();"/>&nbsp;${vr.name}&nbsp;(${vr.mandal}/${vr.village})
                                                                            </td>

                                                                            <%  i--;
                                                                            } else {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${vr.id}" onclick="validateButton();"/>&nbsp;${vr.name}&nbsp;(${vr.mandal}/${vr.village})
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="tr1" id="one">
                                                                            <% i = 2;
                                                                                }%>

                                                                        </logic:iterate>
                                                                    </tr>
                                                                </table>
                                                                <hr style="color: red">
                                                            </logic:present>

                                                            <logic:present name="panchayatSecretaryList">

                                                                <br/> <br/>
                                                                <table cellpadding="5" cellspacing="0" border="0" width="100%"  class="gridtb1" ID="data5">
                                                                    <tr>
                                                                        <th class="grn-hd-txt" colspan="2" style="text-align: left">Panchayat Secretary Officers</th>
                                                                        <th class="grn-hd-txt"  align="right">Search  <input type="text" id="search5" title="Search" style="width:auto;" />
                                                                        </th>
                                                                    </tr>

                                                                    <br/>
                                                                    <%i = 2;%>
                                                                    <tr class="tr1" id="one">
                                                                        <logic:iterate name="panchayatSecretaryList" id="panchayat">
                                                                            <% if (i > 1) {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${panchayat.id}" onclick="validateButton();"/>&nbsp;${panchayat.name}&nbsp;(${panchayat.mandal}/${panchayat.village})
                                                                            </td>

                                                                            <%  i--;
                                                                            } else {%>
                                                                            <td class="gridbg0" colspan="2">
                                                                                <html:checkbox property="distoff" styleClass="distoff" value="${panchayat.id}" onclick="validateButton();"/>&nbsp;${panchayat.name}&nbsp;(${panchayat.mandal}/${panchayat.village})
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="tr1" id="one">
                                                                            <% i = 2;
                                                                                }%>
                                                                        </logic:iterate>
                                                                    </tr>
                                                                </table>
                                                            </logic:present>
                                                        </td>

                                                    </logic:notEmpty>
                                                </tr>
                                                <tr >
                                                    <td colspan="6" align="center" valign="middle"  class="gridbg3" style="text-align:center;display: none" id="sendButton">
                                                        <html:button value="Send" property="Send" onclick="validate();" />
                                                    </td>
                                                </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </html:form>
                        </body>
                        </html>    