<%-- 
    Document   : newjspVCHMConfirmation
    Created on : Feb 20, 2018, 2:49:32 PM
    Author     : 1250881
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RMSA</title> <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/></script>
    <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/></script>
<script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/></script>
<script type="text/javascript" class="init">
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 300,
            "scrollX": true
        });
    });
</script>
<style type="text/css">
    table.dataTable.nowrap td {
        border-bottom: 1px #083254 solid;
        border-left: 1px #083254 solid;
        vertical-align: middle;
        padding-left: 3px;
        padding-right: 3px;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
    }
    table.dataTable.nowrap th {
        background-color: #0E94C5;
        text-align: center;
        border-bottom: 1px #000 solid;
        padding-left: 3px;
        padding-right: 3px;
        border-left: 1px #000 solid;
        vertical-align: left;
        font-size: 11px;
        font-family: verdana;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }
    table.dataTable.display tbody tr.odd {
        background: #E2E4FF !important;
    }
    table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
        background: #E2E4FF !important;
    }
    .dataTables_wrapper {
        padding: 10px !important;
    }
    .dataTables_length label {
        font-size: 12px !important;
    }
    .dataTables_filter label {
        font-size: 12px !important;
    }
    .dataTables_info {
        font-size: 12px !important;
    }
    .dataTables_paginate {
        font-size: 12px !important;
    }
    form input {
        margin-bottom: 5px;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: initial !important;
    }
    .well {
        overflow: hidden;
    }
    .well-lg {
        padding: 10px !important;
    }
    form input {
        padding: 0px !important;
        border-radius: 0 !important;
        font-size: 12px;
        padding: 3px 5px !important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
        height: auto !important;
    }
    select {
        display: inline-block !important;
    }
    /* .dataTables_wrapper .dataTables_scroll {
        clear: both;
width: 1000px;
    } */
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid #49A0E3 !important;
    }
    .table {
        border: 0 !important;
        border-radius: 0 !important;
    }
    input[type="button"] {
        width: 60px !important;
    }
</style>
<style type="text/css">
    .districts_main {
        width:1000px;
        margin:0px auto;
        text-align: center;
    }
    .distirct1 {
        width:250px;
        float:left;
        margin:10px 0;
    }
    .distirct2 {
        width:230px;
        float:left;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;
        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;
        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        font-size: 16px;
        text-align: right;
        font-weight: bold;
    }
    @-moz-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @-webkit-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
</style>
<style type="text/css">
    table.altrowstable1 th {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        text-align: center !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 td {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 {
        border-right: 1px #000000 solid !important;
        border-top: 1px #000000 solid !important;
    }
    table.altrowstable1 thead th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    table.altrowstable1 tbody th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    input { 
        padding: 2px 5px;
        margin: 5px 0px;
    }
</style>
<script>
    window.onload = function() {
        document.getElementById("vcHMRemarksTextId${list.vctrainerId}") == "";
    }
//            window.onload = function() {
//               $(document).ready(function() {
//                   alert($('#hmText'));
//                
//            });
//                document.getElementsByClassName('hmText') =="";
//                document.getElementsByClassName("hmTrain") =="";
//                
//
//            };

    function hmApprovalStatus(vctrainerId, schcd) {
        document.forms[0].schoolId.value = schcd;
        document.forms[0].vTrainerId.value = vctrainerId;
        var vcHMRema = document.getElementById("vcHMRemarksTextId" + vctrainerId).value;
        var vcHMRemarFeedba = document.getElementById("vcHMremarksId" + vctrainerId).value;
        document.forms[0].vcHMRemarksValue.value = vcHMRema;
        document.forms[0].vcHMRemarksFeedbackValue.value = vcHMRemarFeedba;
        for (var i = 0; i < document.forms[0].length - 1; i++) {

            if (document.forms[0].vcHMRemarksValue.value == '0' || document.forms[0].vcHMRemarksValue.value == '' || document.forms[0].vcHMRemarksValue.value == null) {
                alert('Please Enter Remarks');
                return false;
            }
            else if (document.forms[0].vcHMRemarksFeedbackValue.value == null || document.forms[0].vcHMRemarksFeedbackValue.value == '0' || document.forms[0].vcHMRemarksFeedbackValue.value == '') {
                alert('Please Select Feedback');
                return false;
            }
        }
        document.forms[0].action = "./vCHMConfirmation.do?mode=vcHMStatusUpdate";
        document.forms[0].submit();
        document.forms[0].vcHMRemarksText.value = ""
        document.forms[0].vcHMremarks.value = ""
    }

    function inputLimiter(e, allow) {
        var AllowableCharacters = '';
        if (allow == 'Letters') {
            AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        }
        if (allow == 'Numbers') {
            AllowableCharacters = '1234567890';
        }
        if (allow == 'landline') {
            AllowableCharacters = '1234567890-';
        }
        if (allow == 'NameCharacters') {
            AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
        }
        if (allow == 'NameCharactersAndNumbers') {
            AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
        }
        if (allow == 'website') {
            AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
        }

        var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
        if (k != 13 && k != 8 && k != 0) {
            if ((e.ctrlKey == false) && (e.altKey == false)) {
                return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    function onlyNumbers(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 46)
            return true;
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function space(evt, thisvalue) {
        var number = thisvalue.value;
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (number.length < 1) {
            if (evt.keyCode == 32) {
                return false;
            }
        }
        return true;
    }


</script>
<!-- <script language="javascript">
            document.onmousedown = disableclick;
            status = "Right Click Disabled";
            function disableclick(event)
            {
                if (event.button == 2)
                {
                    alert(status);
                    return false;
                }
            }
        </script>-->

</head>
<body>
    <html:form action="/vCHMConfirmation" method="post">
        <html:hidden property="mode"/>
        <html:hidden property="vcTrainerId"/>
        <html:hidden property="schoolId"/>
        <html:hidden property="vTrainerId"/>
        <html:hidden property="vcHMRemarksValue"/>
        <html:hidden property="vcHMRemarksFeedbackValue"/>

        <section class="testimonial_sec clear_fix">
            <div class="container">
                <!--<div class="row">-->
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                    <div class="col-xs-12">
                        <h3> Vocational HM Confirmation </h3>
                    </div>
                    <logic:present name="vclist">
                        <div style="overflow-x: scroll; width: 1070px; margin: 0 20px">
                            <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="darkgrey" >
                                        <th  rowspan="2"  style="text-align: center">Sl.No</th>
                                        <!--<th  rowspan="2"  style="text-align: center">School Code</th>-->
                                        <th  rowspan="2"  style="text-align: center">VT Id</th>
                                        <th  rowspan="2"  style="text-align: center">VT Name</th>
                                        <th  rowspan="2"  style="text-align: center">DateOfBirth</th>
                                        <th  rowspan="2"  style="text-align: center">Aadhar</th>
                                        <th  rowspan="2"  style="text-align: center">Mobile</th>
                                        <th  rowspan="2"  style="text-align: center">email Id</th>
                                        <th  rowspan="2"  style="text-align: center">Sector</th>
                                        <th  rowspan="2"  style="text-align: center">Trade</th>
                                        <th  colspan="10" style="text-align: center">QAnswer</th>
                                        <th  rowspan="2"  style="text-align: center">Remarks</th>
                                        <th  rowspan="2"  style="text-align: center">HM Remarks /View/PDF</th>
                                        <!--<th  rowspan="2"  style="text-align: center">HM Feedback/View/PDF</th>-->
                                        <!--<th  rowspan="2"  style="text-align: center">Generate Certificate</th>-->

                                    </tr>
                                    <tr class="darkgrey" >
                                        <th style="text-align: center">Q.1</th> 
                                        <th style="text-align: center">Q.2</th>
                                        <th style="text-align: center">Q.3</th>
                                        <th style="text-align: center">Q.4</th>
                                        <th style="text-align: center">Q.5</th>
                                        <th style="text-align: center">Q.6</th> 
                                        <th style="text-align: center">Q.7</th>
                                        <th style="text-align: center">Q.8</th>
                                        <th style="text-align: center">Q.9</th>
                                        <th style="text-align: center">Q.10</th>
                                    </tr>
                                    <tr class="darkgrey" >
                                        <th style="text-align: center">1</th> 
                                        <th style="text-align: center">2</th>
                                        <th style="text-align: center">3</th>
                                        <th style="text-align: center">4</th>
                                        <th style="text-align: center">5</th>
                                        <th style="text-align: center">6</th> 
                                        <th style="text-align: center">7</th>
                                        <th style="text-align: center">8</th>
                                        <th style="text-align: center">9</th>
                                        <th style="text-align: center">10</th>
                                        <th style="text-align: center">11</th>
                                        <th style="text-align: center">12</th>
                                        <th style="text-align: center">13</th> 
                                        <th style="text-align: center">14</th>
                                        <th style="text-align: center">15</th>
                                        <th style="text-align: center">16</th> 
                                        <th style="text-align: center">17</th>
                                        <th style="text-align: center">18</th> 
                                        <th style="text-align: center">19</th>
                                        <th style="text-align: center">20</th> 
                                        <th style="text-align: center">21</th>
                                        <!--<th style="text-align: center">21</th>-->

                                    </tr>
                                </thead>
                                <logic:iterate name="vclist" id="list">
                                    <tr>
                                        <td style="text-align: center"><%=i++%></td>  
                                        <!--<td style="text-align: center;"> ${list.schcd}</td>-->
                                        <td style="text-align: center;"> ${list.vctrainerId}</td>
                                        <td style="text-align: center;"> ${list.vctrainerName}</td>
                                        <td style="text-align: center;"> ${list.dob}</td>
                                        <td style="text-align: center;"> ${list.aadharno}</td>
                                        <td style="text-align: center;"> ${list.mobileno}</td>
                                        <td style="text-align: center;"> ${list.emailId}</td>
                                        <td style="text-align: center;"> ${list.sectorName}</td>
                                        <td style="text-align: center;"> ${list.tradeName}</td>
                                        <td style="text-align: center;"> ${list.firstQAns}</td>
                                        <td style="text-align: center;"> ${list.secondQAns}</td>
                                        <td style="text-align: center;"> ${list.thirdQAns}</td>
                                        <td style="text-align: center;"> ${list.fourthQAns}</td>
                                        <td style="text-align: center;"> ${list.fifthQAns}</td>
                                        <td style="text-align: center;"> ${list.sixthQAns}</td>
                                        <td style="text-align: center;"> ${list.seventhQAns}</td>
                                        <td style="text-align: center;"> ${list.eightQAns}</td>
                                        <td style="text-align: center;"> ${list.ninethQAns}</td>
                                        <td style="text-align: center;"> ${list.tenthQAns}</td>
                                        <td style="text-align: center;"> ${list.remarks}</td>
                                        <c:set var="abc" value="${list.hmRemark}"/>
                                        <c:choose>
                                            <c:when test="${abc  == '0'}">
                                                <%--                                                     <td style="text-align: left">
                                                                                                            <html:textarea property="vcHMRemarksText" styleId ="vcHMRemarksTextId"  cols="50" rows="9" onkeydown="return space(event,this);" onkeypress="return inputLimiter(event,'Letters');" />      
                                                                                                       </td>--%>
                                                <td style="text-align: left">
                                                    <html:textarea property="vcHMRemarksText" styleId ="vcHMRemarksTextId${list.vctrainerId}"  styleClass="hmText"   cols="50" rows="9" onkeydown="return space(event,this);" onkeypress="return inputLimiter(event,'Letters');" />      

                                                    <html:select property="vcHMremarks"  styleId="vcHMremarksId${list.vctrainerId}" styleClass="hmTrain"  >
                                                        <html:option value="0">--Feedback--</html:option>
                                                        <html:option value="Excellent">Excellent</html:option>
                                                        <html:option value="VeryGood">VeryGood</html:option>
                                                        <html:option value="Good">Good</html:option>
                                                        <html:option value="Poor">Poor</html:option>
                                                    </html:select>
                                                    <html:button property="subprop" value="submit" onclick="hmApprovalStatus(${list.vctrainerId},${list.schcd});"/>
                                                    <%--  <html:button property="subprop" value="submit" onclick="hmApprovalStatus();"/> --%>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
<!--                                                        <td style="text-align: center;"> ${list.hmRemarksText}</td>
                                                -->
                                                <td width="10px" style="text-align: center">
                                                    <input type="button" value="View" onclick="window.open('./vCHMConfirmation.do?mode=vcHMStatusView&schcd=${list.schcd}&vctrainerId=${list.vctrainerId}&hmRemark=${list.hmRemark}', '_blank', 'location=no,menubar=no,scrollbars=no,resizable=no,fullscreen=no,top=0,left=0,width=950,height=650');" />

                                                    <input type="button" value="PDF" onclick="window.open('./vCHMConfirmation.do?mode=vcHMStatusPDF&schcd=${list.schcd}&vctrainerId=${list.vctrainerId}&hmRemark=${list.hmRemark}', '_blank', 'location=no,menubar=no,scrollbars=no,resizable=no,fullscreen=no,top=0,left=0,width=950,height=650');" />
                                                </td>


                                            </c:otherwise>
                                        </c:choose>   
                                          </tr>
                                </logic:iterate>
                            </table>
                        </div>
                    </logic:present>
                    <logic:present  name="msg">
                        <center>  <font color="red"> ${msg}</font></center>
                        </logic:present>
                </div>
                <!--</div>-->
            </div>
        </section>       
    </html:form>
</body>
</html>
