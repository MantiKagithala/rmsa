<%-- 
    Document   : RMSAComponentsType1
    Created on : 06 Apr, 2017, 4:32:50 PM
    Author     : 1250892
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String photo = null;
    String year = "";
    if (request.getAttribute("RMSAChequeDetails") != null) {
        ArrayList list = (ArrayList) request.getAttribute("RMSAChequeDetails");
        for (int i = 0; i < list.size(); i++) {
            photo = "kpiinsert(ucsFileUpload" + i + ")";
        }
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: RMSA ::</title>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <script>

            function roundTo(n, digits) {
                var negative = false;
                if (digits === undefined) {
                    digits = 0;
                }
                if (n < 0) {
                    negative = true;
                    n = n * -1;
                }
                var multiplicator = Math.pow(10, digits);
                n = parseFloat((n * multiplicator).toFixed(11));
                n = (Math.round(n) / multiplicator).toFixed(2);
                if (negative) {
                    n = (n * -1).toFixed(2);
                }
                return n;
            }


            window.onload = function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);

            };

            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                $(document).bind("contextmenu", function(e) {
                    return false;
                });
                $(function() {
                    $(document).keydown(function(e) {
                        return (e.which || e.keyCode) !== 116;
                    });
                });
                if ($("#civilWorksReleased") !== null) {
                    $("#civilWorksSpent").autocomplete = "off";
                    $("#majorRepairsSpent").autocomplete = "off";
                    $("#minorRepairsSpent").autocomplete = "off";
                    $("#annualAndOtherSpent").autocomplete = "off";
                    $("#toiletsSpent").autocomplete = "off";
                    $("#selfDefenseGrantsSpent").autocomplete = "off";
                }
                $("#Search").button().click(function() {
                    var year = $('#year').val();
                    if (year === "0") {
                        alert("Select Year ");
                        $("#year").focus().css({'border': '1px solid red'});
                    }
                    else {
                        document.forms[0].mode.value = "searchSchoolFunding";
                        document.forms[0].submit();
                    }
                });

                var input11 = document.querySelector('#civilWorksSpent');
                input11.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    var civilWorksSpent = input11.value;
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var civilWorksAvailable = parseFloat(civilWorksReleased) - parseFloat(civilWorksSpent);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(civilWorksAvailable)) {
                        $("#civilWorksAvailable").val(roundTo(civilWorksAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilSpent)) {
                        $("#totalCivilSpent").val(roundTo(totalCivilSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });


                var input12 = document.querySelector('#majorRepairsSpent');
                input12.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    var majorRepairsSpent = input12.value;
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var majorRepairsAvailable = parseFloat(majorRepairsReleased) - parseFloat(majorRepairsSpent);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(majorRepairsAvailable)) {
                        $("#majorRepairsAvailable").val(roundTo(majorRepairsAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilSpent)) {
                        $("#totalCivilSpent").val(roundTo(totalCivilSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });

                var input13 = document.querySelector('#minorRepairsSpent');
                input13.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    var minorRepairsSpent = input13.value;
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var minorRepairsAvailable = parseFloat(minorRepairsReleased) - parseFloat(minorRepairsSpent);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(minorRepairsAvailable)) {
                        $("#minorRepairsAvailable").val(roundTo(minorRepairsAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilSpent)) {
                        $("#totalCivilSpent").val(roundTo(totalCivilSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });

                var input14 = document.querySelector('#toiletsSpent');
                input14.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var toiletsReleased = $("#toiletsReleased").val();
                    var toiletsSpent = input14.value;
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var toiletsAvailable = parseFloat(toiletsReleased) - parseFloat(toiletsSpent);
                    var totalCivilSpent = parseFloat(civilWorksSpent) + parseFloat(majorRepairsSpent) + parseFloat(minorRepairsSpent) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(toiletsAvailable)) {
                        $("#toiletsAvailable").val(roundTo(toiletsAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilSpent)) {
                        $("#totalCivilSpent").val(roundTo(totalCivilSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });

                var input21 = document.querySelector('#waterElectricityTelephoneChargesSpent');
                input21.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var waterElectricityTelephoneChargesSpent = input21.value;
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var waterElectricityTelephoneChargesAvailable = parseFloat(waterElectricityTelephoneChargesReleased) - parseFloat(waterElectricityTelephoneChargesSpent);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(waterElectricityTelephoneChargesAvailable)) {
                        $("#waterElectricityTelephoneChargesAvailable").val(roundTo(waterElectricityTelephoneChargesAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualSpent)) {
                        $("#totalAnnualSpent").val(roundTo(totalAnnualSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input22 = document.querySelector('#purchaseofBooksAndPeriodicalsAndNewsPapersSpent');
                input22.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = input22.value;
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) - parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable)) {
                        $("#purchaseofBooksAndPeriodicalsAndNewsPapersAvailable").val(roundTo(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualSpent)) {
                        $("#totalAnnualSpent").val(roundTo(totalAnnualSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input23 = document.querySelector('#minorRepairsAnnualSpent');
                input23.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    var minorRepairsAnnualSpent = input23.value;
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var minorRepairsAnnualAvailable = parseFloat(minorRepairsAnnualReleased) - parseFloat(minorRepairsAnnualSpent);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(minorRepairsAnnualAvailable)) {
                        $("#minorRepairsAnnualAvailable").val(roundTo(minorRepairsAnnualAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualSpent)) {
                        $("#totalAnnualSpent").val(roundTo(totalAnnualSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input24 = document.querySelector('#sanitationAndICTSpent');
                input24.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    var sanitationAndICTSpent = input24.value;
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var sanitationAndICTAvailable = parseFloat(sanitationAndICTReleased) - parseFloat(sanitationAndICTSpent);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(sanitationAndICTAvailable)) {
                        $("#sanitationAndICTAvailable").val(roundTo(sanitationAndICTAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualSpent)) {
                        $("#totalAnnualSpent").val(roundTo(totalAnnualSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });


                var input25 = document.querySelector('#needBasedWorksSpent');
                input25.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    var needBasedWorksSpent = input25.value;
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var needBasedWorksAvailable = parseFloat(needBasedWorksReleased) - parseFloat(needBasedWorksSpent);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(needBasedWorksAvailable)) {
                        $("#needBasedWorksAvailable").val(roundTo(needBasedWorksAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualSpent)) {
                        $("#totalAnnualSpent").val(roundTo(totalAnnualSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input26 = document.querySelector('#provisionalLaboratorySpent');
                input26.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    var provisionalLaboratorySpent = input26.value;
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var provisionalLaboratoryAvailable = parseFloat(provisionalLaboratoryReleased) - parseFloat(provisionalLaboratorySpent);
                    var totalAnnualSpent = parseFloat(waterElectricityTelephoneChargesSpent) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) + parseFloat(minorRepairsAnnualSpent) + parseFloat(sanitationAndICTSpent) + parseFloat(needBasedWorksSpent) + parseFloat(provisionalLaboratorySpent);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(provisionalLaboratoryAvailable)) {
                        $("#provisionalLaboratoryAvailable").val(roundTo(provisionalLaboratoryAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualSpent)) {
                        $("#totalAnnualSpent").val(roundTo(totalAnnualSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });



                var input41 = document.querySelector('#furnitureSpent');
                input41.addEventListener('input', function()
                {
                    var totalfurnitureAndLabReleased = $("#totalfurnitureAndLabReleased").val();
                    if (totalfurnitureAndLabReleased === "")
                        totalfurnitureAndLabReleased = "0.00";
                    var furnitureReleased = $("#furnitureReleased").val();
                    var furnitureSpent = input41.value;
                    if (furnitureSpent === "")
                        furnitureSpent = "0.00";
                    var labEquipmentSpent = $("#labEquipmentSpent").val();
                    if (labEquipmentSpent === "")
                        labEquipmentSpent = "0.00";
                    var furnitureAvailable = parseFloat(furnitureReleased) - parseFloat(furnitureSpent);
                    var totalfurnitureAndLabSpent = parseFloat(furnitureSpent) + parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabAvailable = parseFloat(totalfurnitureAndLabReleased) - parseFloat(totalfurnitureAndLabSpent);
                    if (!isNaN(furnitureAvailable)) {
                        $("#furnitureAvailable").val(roundTo(furnitureAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabSpent)) {
                        $("#totalfurnitureAndLabSpent").val(roundTo(totalfurnitureAndLabSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabAvailable)) {
                        $("#totalfurnitureAndLabAvailable").val(roundTo(totalfurnitureAndLabAvailable, 2));
                    } else {

                    }
                });

                var input42 = document.querySelector('#labEquipmentSpent');
                input42.addEventListener('input', function()
                {
                    var totalfurnitureAndLabReleased = $("#totalfurnitureAndLabReleased").val();
                    if (totalfurnitureAndLabReleased === "")
                        totalfurnitureAndLabReleased = "0.00";
                    var labEquipmentReleased = $("#labEquipmentReleased").val();
                    var labEquipmentSpent = input42.value;
                    if (labEquipmentSpent === "")
                        labEquipmentSpent = "0.00";
                    var furnitureSpent = $("#furnitureSpent").val();
                    if (furnitureSpent === "")
                        furnitureSpent = "0.00";
                    var labEquipmentAvailable = parseFloat(labEquipmentReleased) - parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabSpent = parseFloat(furnitureSpent) + parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabAvailable = parseFloat(totalfurnitureAndLabReleased) - parseFloat(totalfurnitureAndLabSpent);
                    if (!isNaN(labEquipmentAvailable)) {
                        $("#labEquipmentAvailable").val(roundTo(labEquipmentAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabSpent)) {
                        $("#totalfurnitureAndLabSpent").val(roundTo(totalfurnitureAndLabSpent, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabAvailable)) {
                        $("#totalfurnitureAndLabAvailable").val(roundTo(totalfurnitureAndLabAvailable, 2));
                    } else {

                    }
                });

                var input31 = document.querySelector('#recurringExcursionSpent');
                input31.addEventListener('input', function()
                {
                    var recurringTotalRelease = $("#recurringTotalRelease").val();
                    if (recurringTotalRelease === "")
                        recurringTotalRelease = "0.00";
                    var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                    var recurringExcursionSpent = input31.value;
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringExcursionAvailable = parseFloat(recurringExcursionRelease) - parseFloat(recurringExcursionSpent);
                    var recurringTotalSpent = parseFloat(recurringExcursionSpent) + parseFloat(recurringSelfDefenseSpent) + parseFloat(recurringOtherSpent);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringExcursionAvailable)) {
                        $("#recurringExcursionAvailable").val(roundTo(recurringExcursionAvailable, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalSpent)) {
                        $("#recurringTotalSpent").val(roundTo(recurringTotalSpent, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable, 2));
                    } else {

                    }
                });


                var input32 = document.querySelector('#recurringSelfDefenseSpent');
                input32.addEventListener('input', function()
                {
                    var recurringTotalRelease = $("#recurringTotalRelease").val();
                    if (recurringTotalRelease === "")
                        recurringTotalRelease = "0.00";
                    var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                    var recurringSelfDefenseSpent = input32.value;
                    if (recurringSelfDefenseRelease === "")
                        recurringSelfDefenseRelease = "0.00";
                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringSelfDefenseAvailable = parseFloat(recurringSelfDefenseRelease) - parseFloat(recurringSelfDefenseSpent);
                    var recurringTotalSpent = parseFloat(recurringExcursionSpent) + parseFloat(recurringSelfDefenseSpent) + parseFloat(recurringOtherSpent);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringSelfDefenseAvailable)) {
                        $("#recurringSelfDefenseAvailable").val(roundTo(recurringSelfDefenseAvailable, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalSpent)) {
                        $("#recurringTotalSpent").val(roundTo(recurringTotalSpent, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable, 2));
                    } else {

                    }
                });

                var input33 = document.querySelector('#recurringOtherSpent');
                input33.addEventListener('input', function()
                {
                    var recurringTotalRelease = $("#recurringTotalRelease").val();
                    if (recurringTotalRelease === "")
                        recurringTotalRelease = "0.00";
                    var recurringOtherRelease = $("#recurringOtherRelease").val();
                    var recurringOtherSpent = input33.value;
                    if (recurringOtherRelease === "")
                        recurringOtherRelease = "0.00";
                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringOtherAvailable = parseFloat(recurringOtherRelease) - parseFloat(recurringOtherSpent);
                    var recurringTotalSpent = parseFloat(recurringExcursionSpent) + parseFloat(recurringSelfDefenseSpent) + parseFloat(recurringOtherSpent);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringOtherAvailable)) {
                        $("#recurringOtherAvailable").val(roundTo(recurringOtherAvailable, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalSpent)) {
                        $("#recurringTotalSpent").val(roundTo(recurringTotalSpent, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable, 2));
                    } else {

                    }
                });


                var input51 = document.querySelector('#interestEarned');
                input51.addEventListener('input', function()
                {
                    var interestEarned = input51.value;
                    if (interestEarned === "")
                        interestEarned = "0.00";
                    var interestExpenditure = $("#interestExpenditure").val();
                    if (interestExpenditure === "")
                        interestExpenditure = "0.00";
                    var interestBalance = parseFloat(interestEarned) - parseFloat(interestExpenditure);
                    if (!isNaN(interestBalance)) {
                        $("#interestBalance").val(roundTo(interestBalance, 2));
                    } else {

                    }

                });

                var input52 = document.querySelector('#interestExpenditure');
                input52.addEventListener('input', function()
                {
                    var interestExpenditure = input52.value;
                    if (interestExpenditure === "")
                        interestExpenditure = "0.00";
                    var interestEarned = $("#interestEarned").val();
                    if (interestEarned === "")
                        interestEarned = "0.00";
                    var interestBalance = parseFloat(interestEarned) - parseFloat(interestExpenditure);
                    if (!isNaN(interestBalance)) {
                        $("#interestBalance").val(roundTo(interestBalance, 2));
                    } else {

                    }

                });
                //reshma
                var input53 = document.querySelector('#civilWorksReleased');
                input53.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    var civilWorksReleased = input53.value;
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var civilWorksSpent = $("#civilWorksSpent").val();
                    if (civilWorksSpent === "")
                        civilWorksSpent = "0.00";
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var totalCivilSpent = $("#totalCivilSpent").val();
                    if (totalCivilSpent === "")
                        totalCivilSpent = "0.00";
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var civilWorksAvailable = parseFloat(civilWorksReleased) - parseFloat(civilWorksSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(civilWorksAvailable)) {
                        $("#civilWorksAvailable").val(roundTo(civilWorksAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(roundTo(totalCivilReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });
                var input54 = document.querySelector('#majorRepairsReleased');
                input54.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var totalCivilSpent = $("#totalCivilSpent").val();
                    if (totalCivilSpent === "")
                        totalCivilSpent = "0.00";
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var majorRepairsReleased = input54.value;
                    // var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var majorRepairsAvailable = parseFloat(majorRepairsReleased) - parseFloat(majorRepairsSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(majorRepairsAvailable)) {
                        $("#majorRepairsAvailable").val(roundTo(majorRepairsAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(roundTo(totalCivilReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });
                var input55 = document.querySelector('#minorRepairsReleased');
                input55.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var totalCivilSpent = $("#totalCivilSpent").val();
                    if (totalCivilSpent === "")
                        totalCivilSpent = "0.00";
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var minorRepairsReleased = input55.value;
                    if (minorRepairsReleased === "")
                        minorRepairsReleased = "0.00";
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var minorRepairsAvailable = parseFloat(minorRepairsReleased) - parseFloat(minorRepairsSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsSpent);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(minorRepairsAvailable)) {
                        $("#minorRepairsAvailable").val(roundTo(minorRepairsAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(roundTo(totalCivilReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });
                var input56 = document.querySelector('#toiletsReleased');
                input56.addEventListener('input', function()
                {
                    var totalCivilReleased = $("#totalCivilReleased").val();
                    if (totalCivilReleased === "")
                        totalCivilReleased = "0.00";
                    var totalCivilSpent = $("#totalCivilSpent").val();
                    if (totalCivilSpent === "")
                        totalCivilSpent = "0.00";
                    var civilWorksReleased = $("#civilWorksReleased").val();
                    if (civilWorksReleased === "")
                        civilWorksReleased = "0.00";
                    var toiletsReleased = input56.value;
                    if (toiletsReleased === "")
                        toiletsReleased = "0.00";
                    var majorRepairsReleased = $("#majorRepairsReleased").val();
                    if (majorRepairsReleased === "")
                        majorRepairsReleased = "0.00";
                    var majorRepairsSpent = $("#majorRepairsSpent").val();
                    if (majorRepairsSpent === "")
                        majorRepairsSpent = "0.00";
                    var minorRepairsSpent = $("#minorRepairsSpent").val();
                    if (minorRepairsSpent === "")
                        minorRepairsSpent = "0.00";
                    var minorRepairsReleased = $("#minorRepairsReleased").val();
                    var toiletsSpent = $("#toiletsSpent").val();
                    if (toiletsSpent === "")
                        toiletsSpent = "0.00";
                    var toiletsAvailable = parseFloat(toiletsReleased) - parseFloat(toiletsSpent);
                    var totalCivilReleased = parseFloat(civilWorksReleased) + parseFloat(majorRepairsReleased) + parseFloat(minorRepairsReleased) + parseFloat(toiletsReleased);
                    var totalCivilAvailable = parseFloat(totalCivilReleased) - parseFloat(totalCivilSpent);
                    if (!isNaN(toiletsAvailable)) {
                        $("#toiletsAvailable").val(roundTo(toiletsAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilReleased)) {
                        $("#totalCivilReleased").val(roundTo(totalCivilReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalCivilAvailable)) {
                        $("#totalCivilAvailable").val(roundTo(totalCivilAvailable, 2));
                    } else {

                    }
                });

                var input57 = document.querySelector('#furnitureReleased');
                input57.addEventListener('input', function()
                {
                    var totalfurnitureAndLabReleased = $("#totalfurnitureAndLabReleased").val();
                    if (totalfurnitureAndLabReleased === "")
                        totalfurnitureAndLabReleased = "0.00";
                    // var furnitureReleased = $("#furnitureReleased").val();
                    var furnitureReleased = input57.value;
                    var furnitureSpent = $("#furnitureSpent").val();
                    if (furnitureSpent === "")
                        furnitureSpent = "0.00";
                    var labEquipmentReleased = $("#labEquipmentReleased").val();
                    if (labEquipmentReleased === "")
                        labEquipmentReleased = "0.00";
                    var totalfurnitureAndLabSpent = $("#totalfurnitureAndLabSpent").val();
                    if (totalfurnitureAndLabSpent === "")
                        totalfurnitureAndLabSpent = "0.00";
                    var labEquipmentSpent = $("#labEquipmentSpent").val();
                    if (labEquipmentSpent === "")
                        labEquipmentSpent = "0.00";
                    var furnitureAvailable = parseFloat(furnitureReleased) - parseFloat(furnitureSpent);
                    var totalfurnitureAndLabReleased = parseFloat(furnitureReleased) + parseFloat(labEquipmentReleased);
                    var totalfurnitureAndLabAvailable = parseFloat(totalfurnitureAndLabReleased) - parseFloat(totalfurnitureAndLabSpent);
                    if (!isNaN(furnitureAvailable)) {
                        $("#furnitureAvailable").val(roundTo(furnitureAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabReleased)) {
                        $("#totalfurnitureAndLabReleased").val(roundTo(totalfurnitureAndLabReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabAvailable)) {
                        $("#totalfurnitureAndLabAvailable").val(roundTo(totalfurnitureAndLabAvailable, 2));
                    } else {

                    }
                });

                var input58 = document.querySelector('#labEquipmentReleased');
                input58.addEventListener('input', function()
                {
                    var totalfurnitureAndLabReleased = $("#totalfurnitureAndLabReleased").val();
                    if (totalfurnitureAndLabReleased === "")
                        totalfurnitureAndLabReleased = "0.00";
                    // var furnitureReleased = $("#furnitureReleased").val();
                    var labEquipmentReleased = input58.value;
                    var furnitureSpent = $("#furnitureSpent").val();
                    if (furnitureSpent === "")
                        furnitureSpent = "0.00";
                    // var labEquipmentReleased = $("#labEquipmentReleased").val();
                    if (labEquipmentReleased === "")
                        labEquipmentReleased = "0.00";
                    var totalfurnitureAndLabSpent = $("#totalfurnitureAndLabSpent").val();
                    if (totalfurnitureAndLabSpent === "")
                        totalfurnitureAndLabSpent = "0.00";
                    var labEquipmentSpent = $("#labEquipmentSpent").val();
                    if (labEquipmentSpent === "")
                        labEquipmentSpent = "0.00";
                    var labEquipmentAvailable = parseFloat(labEquipmentReleased) - parseFloat(labEquipmentSpent);
                    var totalfurnitureAndLabReleased = parseFloat(labEquipmentReleased) + parseFloat(labEquipmentReleased);
                    var totalfurnitureAndLabAvailable = parseFloat(totalfurnitureAndLabReleased) - parseFloat(totalfurnitureAndLabSpent);
                    if (!isNaN(labEquipmentAvailable)) {
                        $("#labEquipmentAvailable").val(roundTo(labEquipmentAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabReleased)) {
                        $("#totalfurnitureAndLabReleased").val(roundTo(totalfurnitureAndLabReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalfurnitureAndLabAvailable)) {
                        $("#totalfurnitureAndLabAvailable").val(roundTo(totalfurnitureAndLabAvailable, 2));
                    } else {

                    }
                });

                var input59 = document.querySelector('#waterElectricityTelephoneChargesReleased');
                input59.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    //   var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var waterElectricityTelephoneChargesReleased = input59.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var totalAnnualSpent = $("#totalAnnualSpent").val();
                    if (totalAnnualSpent === "")
                        totalAnnualSpent = "0.00";

                    var waterElectricityTelephoneChargesAvailable = parseFloat(waterElectricityTelephoneChargesReleased) - parseFloat(waterElectricityTelephoneChargesSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(waterElectricityTelephoneChargesAvailable)) {
                        $("#waterElectricityTelephoneChargesAvailable").val(roundTo(waterElectricityTelephoneChargesAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(roundTo(totalAnnualReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input60 = document.querySelector('#purchaseofBooksAndPeriodicalsAndNewsPapersReleased');
                input60.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = input60.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersSpent = "0.00";
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var totalAnnualSpent = $("#totalAnnualSpent").val();
                    if (totalAnnualSpent === "")
                        totalAnnualSpent = "0.00";

                    var purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) - parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable)) {
                        $("#purchaseofBooksAndPeriodicalsAndNewsPapersAvailable").val(roundTo(purchaseofBooksAndPeriodicalsAndNewsPapersAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(roundTo(totalAnnualReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input61 = document.querySelector('#minorRepairsAnnualReleased');
                input61.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var minorRepairsAnnualReleased = input61.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var totalAnnualSpent = $("#totalAnnualSpent").val();
                    if (totalAnnualSpent === "")
                        totalAnnualSpent = "0.00";

                    var minorRepairsAnnualAvailable = parseFloat(minorRepairsAnnualReleased) - parseFloat(minorRepairsAnnualSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(minorRepairsAnnualAvailable)) {
                        $("#minorRepairsAnnualAvailable").val(roundTo(minorRepairsAnnualAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(roundTo(totalAnnualReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input62 = document.querySelector('#sanitationAndICTReleased');
                input62.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var sanitationAndICTReleased = input62.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";

                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualSpent = "0.00";
                    var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                    if (minorRepairsAnnualSpent === "")
                        minorRepairsAnnualSpent = "0.00";
                    var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                    if (sanitationAndICTSpent === "")
                        sanitationAndICTSpent = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var totalAnnualSpent = $("#totalAnnualSpent").val();
                    if (totalAnnualSpent === "")
                        totalAnnualSpent = "0.00";

                    var sanitationAndICTAvailable = parseFloat(sanitationAndICTReleased) - parseFloat(sanitationAndICTSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(sanitationAndICTAvailable)) {
                        $("#sanitationAndICTAvailable").val(roundTo(sanitationAndICTAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(roundTo(totalAnnualReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input63 = document.querySelector('#needBasedWorksReleased');
                input63.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var needBasedWorksReleased = input63.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";

                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                    if (needBasedWorksSpent === "")
                        needBasedWorksSpent = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var totalAnnualSpent = $("#totalAnnualSpent").val();
                    if (totalAnnualSpent === "")
                        totalAnnualSpent = "0.00";

                    var needBasedWorksAvailable = parseFloat(needBasedWorksReleased) - parseFloat(needBasedWorksSpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(needBasedWorksAvailable)) {
                        $("#needBasedWorksAvailable").val(roundTo(needBasedWorksAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(roundTo(totalAnnualReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });

                var input64 = document.querySelector('#provisionalLaboratoryReleased');
                input64.addEventListener('input', function()
                {
                    var totalAnnualReleased = $("#totalAnnualReleased").val();
                    if (totalAnnualReleased === "")
                        totalAnnualReleased = "0.00";
                    var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                    var provisionalLaboratoryReleased = input64.value;
                    if (waterElectricityTelephoneChargesReleased === "")
                        waterElectricityTelephoneChargesReleased = "0.00";
                    var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                    if (waterElectricityTelephoneChargesSpent === "")
                        waterElectricityTelephoneChargesSpent = "0.00";
                    var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                    if (purchaseofBooksAndPeriodicalsAndNewsPapersReleased === "")
                        purchaseofBooksAndPeriodicalsAndNewsPapersReleased = "0.00";
                    var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                    if (minorRepairsAnnualReleased === "")
                        minorRepairsAnnualReleased = "0.00";
                    var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                    if (provisionalLaboratorySpent === "")
                        provisionalLaboratorySpent = "0.00";
                    var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                    if (sanitationAndICTReleased === "")
                        sanitationAndICTReleased = "0.00";
                    var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                    if (needBasedWorksReleased === "")
                        needBasedWorksReleased = "0.00";
                    if (provisionalLaboratoryReleased === "")
                        provisionalLaboratoryReleased = "0.00";
                    var totalAnnualSpent = $("#totalAnnualSpent").val();
                    if (totalAnnualSpent === "")
                        totalAnnualSpent = "0.00";

                    var provisionalLaboratoryAvailable = parseFloat(provisionalLaboratoryReleased) - parseFloat(provisionalLaboratorySpent);
                    var totalAnnualReleased = parseFloat(waterElectricityTelephoneChargesReleased) + parseFloat(purchaseofBooksAndPeriodicalsAndNewsPapersReleased) + parseFloat(minorRepairsAnnualReleased) + parseFloat(sanitationAndICTReleased) + parseFloat(needBasedWorksReleased) + parseFloat(provisionalLaboratoryReleased);
                    var totalAnnualAvailable = parseFloat(totalAnnualReleased) - parseFloat(totalAnnualSpent);
                    if (!isNaN(provisionalLaboratoryAvailable)) {
                        $("#provisionalLaboratoryAvailable").val(roundTo(provisionalLaboratoryAvailable, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualReleased)) {
                        $("#totalAnnualReleased").val(roundTo(totalAnnualReleased, 2));
                    } else {

                    }
                    if (!isNaN(totalAnnualAvailable)) {
                        $("#totalAnnualAvailable").val(roundTo(totalAnnualAvailable, 2));
                    } else {

                    }
                });


                var input65 = document.querySelector('#recurringExcursionRelease');
                input65.addEventListener('input', function()
                {

                    var recurringTotalRelease = $("#recurringTotalRelease").val();
                    if (recurringTotalRelease === "")
                        recurringTotalRelease = "0.00";
                    var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                    if (recurringSelfDefenseRelease === "")
                        recurringSelfDefenseRelease = "0.00";
                    var recurringExcursionRelease = input65.value;
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";

                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringTotalSpent = $("#recurringTotalSpent").val();
                    if (recurringTotalSpent === "")
                        recurringTotalSpent = "0.00";
                    var recurringOtherRelease = $("#recurringOtherRelease").val();
                    if (recurringOtherRelease === "")
                        recurringOtherRelease = "0.00";


                    var recurringExcursionAvailable = parseFloat(recurringExcursionRelease) - parseFloat(recurringExcursionSpent);
                    var recurringTotalRelease = parseFloat(recurringExcursionRelease) + parseFloat(recurringSelfDefenseRelease) + parseFloat(recurringOtherRelease);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringExcursionAvailable)) {
                        $("#recurringExcursionAvailable").val(roundTo(recurringExcursionAvailable, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalRelease)) {
                        $("#recurringTotalRelease").val(roundTo(recurringTotalRelease, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable, 2));
                    } else {

                    }
                });

                var input66 = document.querySelector('#recurringSelfDefenseRelease');
                input66.addEventListener('input', function()
                {

                    var recurringTotalRelease = $("#recurringTotalRelease").val();
                    if (recurringTotalRelease === "")
                        recurringTotalRelease = "0.00";
                    // var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
//                   if (recurringSelfDefenseRelease === "")
//                        recurringSelfDefenseRelease = "0.00";
                    var recurringSelfDefenseRelease = input66.value;
                    var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";

                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringTotalSpent = $("#recurringTotalSpent").val();
                    if (recurringTotalSpent === "")
                        recurringTotalSpent = "0.00";
                    var recurringOtherRelease = $("#recurringOtherRelease").val();
                    if (recurringOtherRelease === "")
                        recurringOtherRelease = "0.00";


                    var recurringSelfDefenseAvailable = parseFloat(recurringSelfDefenseRelease) - parseFloat(recurringSelfDefenseSpent);
                    var recurringTotalRelease = parseFloat(recurringExcursionRelease) + parseFloat(recurringSelfDefenseRelease) + parseFloat(recurringOtherRelease);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringSelfDefenseAvailable)) {
                        $("#recurringSelfDefenseAvailable").val(roundTo(recurringSelfDefenseAvailable, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalRelease)) {
                        $("#recurringTotalRelease").val(roundTo(recurringTotalRelease, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable, 2));
                    } else {

                    }
                });

                var input67 = document.querySelector('#recurringOtherRelease');
                input67.addEventListener('input', function()
                {

                    var recurringTotalRelease = $("#recurringTotalRelease").val();
                    if (recurringTotalRelease === "")
                        recurringTotalRelease = "0.00";
                    var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                    if (recurringSelfDefenseRelease === "")
                        recurringSelfDefenseRelease = "0.00";
                    var recurringOtherRelease = input67.value;
                    var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                    if (recurringExcursionRelease === "")
                        recurringExcursionRelease = "0.00";

                    var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                    if (recurringExcursionSpent === "")
                        recurringExcursionSpent = "0.00";
                    var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                    if (recurringSelfDefenseSpent === "")
                        recurringSelfDefenseSpent = "0.00";
                    var recurringOtherSpent = $("#recurringOtherSpent").val();
                    if (recurringOtherSpent === "")
                        recurringOtherSpent = "0.00";
                    var recurringTotalSpent = $("#recurringTotalSpent").val();
                    if (recurringTotalSpent === "")
                        recurringTotalSpent = "0.00";
//                      var recurringOtherRelease = $("#recurringOtherRelease").val();
//                    if (recurringOtherRelease === "")
//                        recurringOtherRelease = "0.00";


                    var recurringOtherAvailable = parseFloat(recurringOtherRelease) - parseFloat(recurringOtherSpent);
                    var recurringTotalRelease = parseFloat(recurringExcursionRelease) + parseFloat(recurringSelfDefenseRelease) + parseFloat(recurringOtherRelease);
                    var recurringTotalAvailable = parseFloat(recurringTotalRelease) - parseFloat(recurringTotalSpent);
                    if (!isNaN(recurringOtherAvailable)) {
                        $("#recurringOtherAvailable").val(roundTo(recurringOtherAvailable, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalRelease)) {
                        $("#recurringTotalRelease").val(roundTo(recurringTotalRelease, 2));
                    } else {

                    }
                    if (!isNaN(recurringTotalAvailable)) {
                        $("#recurringTotalAvailable").val(roundTo(recurringTotalAvailable, 2));
                    } else {

                    }
                });



            });

            function submitForm() {

                var roleId = $("#roleId").val();

                ///Civil Works
                var civilWorksReleased = $("#civilWorksReleased").val();
                var majorRepairsReleased = $("#majorRepairsReleased").val();
                var minorRepairsReleased = $("#minorRepairsReleased").val();
                var toiletsReleased = $("#toiletsReleased").val();

                var civilWorksSpent = $("#civilWorksSpent").val();
                var majorRepairsSpent = $("#majorRepairsSpent").val();
                var minorRepairsSpent = $("#minorRepairsSpent").val();
                var toiletsSpent = $("#toiletsSpent").val();

                var civilWorksAvailable = $("#civilWorksAvailable").val();
                var majorRepairsAvailable = $("#majorRepairsAvailable").val();
                var minorRepairsAvailable = $("#minorRepairsAvailable").val();
                var toiletsAvailable = $("#toiletsAvailable").val();

                var totalCivilReleased = $("#totalCivilReleased").val();
                var totalCivilSpent = $("#totalCivilSpent").val();
                var totalCivilAvailable = $("#totalCivilAvailable").val();

                ///Annual Grants
                var waterElectricityTelephoneChargesReleased = $("#waterElectricityTelephoneChargesReleased").val();
                var purchaseofBooksAndPeriodicalsAndNewsPapersReleased = $("#purchaseofBooksAndPeriodicalsAndNewsPapersReleased").val();
                var minorRepairsAnnualReleased = $("#minorRepairsAnnualReleased").val();
                var sanitationAndICTReleased = $("#sanitationAndICTReleased").val();
                var needBasedWorksReleased = $("#needBasedWorksReleased").val();
                var provisionalLaboratoryReleased = $("#provisionalLaboratoryReleased").val();
                var waterElectricityTelephoneChargesSpent = $("#waterElectricityTelephoneChargesSpent").val();
                var purchaseofBooksAndPeriodicalsAndNewsPapersSpent = $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").val();
                var minorRepairsAnnualSpent = $("#minorRepairsAnnualSpent").val();
                var sanitationAndICTSpent = $("#sanitationAndICTSpent").val();
                var needBasedWorksSpent = $("#needBasedWorksSpent").val();
                var provisionalLaboratorySpent = $("#provisionalLaboratorySpent").val();
                var waterElectricityTelephoneChargesAvailable = $("#waterElectricityTelephoneChargesAvailable").val();
                var purchaseofBooksAndPeriodicalsAndNewsPapersAvailable = $("#purchaseofBooksAndPeriodicalsAndNewsPapersAvailable").val();
                var minorRepairsAnnualAvailable = $("#minorRepairsAnnualAvailable").val();
                var sanitationAndICTAvailable = $("#sanitationAndICTAvailable").val();
                var needBasedWorksAvailable = $("#needBasedWorksAvailable").val();
                var provisionalLaboratoryAvailable = $("#provisionalLaboratoryAvailable").val();
                var totalAnnualReleased = $("#totalAnnualReleased").val();
                var totalAnnualSpent = $("#totalAnnualSpent").val();
                var totalAnnualAvailable = $("#totalAnnualAvailable").val();

                ///Recurring
                var recurringExcursionRelease = $("#recurringExcursionRelease").val();
                var recurringExcursionSpent = $("#recurringExcursionSpent").val();
                var recurringExcursionAvailable = $("#recurringExcursionAvailable").val();
                var recurringSelfDefenseRelease = $("#recurringSelfDefenseRelease").val();
                var recurringSelfDefenseSpent = $("#recurringSelfDefenseSpent").val();
                var recurringSelfDefenseAvailable = $("#recurringSelfDefenseAvailable").val();
                var recurringOtherRelease = $("#recurringOtherRelease").val();
                var recurringOtherSpent = $("#recurringOtherSpent").val();
                var recurringOtherAvailable = $("#recurringOtherAvailable").val();
                var recurringTotalRelease = $("#recurringTotalRelease").val();
                var recurringTotalSpent = $("#recurringTotalSpent").val();
                var recurringTotalAvailable = $("#recurringTotalAvailable").val();
                ///Furniture & Lab Equipment
                var furnitureReleased = $("#furnitureReleased").val();
                var labEquipmentReleased = $("#labEquipmentReleased").val();
                var totalfurnitureAndLabReleased = $("#totalfurnitureAndLabReleased").val();
                var furnitureSpent = $("#furnitureSpent").val();
                var totalfurnitureAndLabSpent = $("#totalfurnitureAndLabSpent").val();
                var labEquipmentSpent = $("#labEquipmentSpent").val();
                var furnitureAvailable = $("#furnitureAvailable").val();
                var labEquipmentAvailable = $("#labEquipmentAvailable").val();
                var totalfurnitureAndLabAvailable = $("#totalfurnitureAndLabAvailable").val();

                var interestEarned = $("#interestEarned").val();
                var interestExpenditure = $("#interestExpenditure").val();
                var interestBalance = $("#interestBalance").val();

                if (civilWorksSpent === "" || civilWorksSpent === "NaN") {
                    alert("Enter civilWorks Expenditure value");
                    $("#civilWorksSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(civilWorksSpent) > parseInt(civilWorksReleased)) {
//                    alert("CivilWorks Expenditure value should be less than CivilWorks Released");
//                    $("#civilWorksSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (majorRepairsSpent === "" || majorRepairsSpent === "NaN") {
                    alert("Enter majorRepairs Expenditure value");
                    $("#majorRepairsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(majorRepairsSpent) > parseInt(majorRepairsReleased)) {
//                    alert("majorRepairs Expenditure value should be less than Major Repairs Released");
//                    $("#majorRepairsSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (minorRepairsSpent === "" || minorRepairsSpent === "NaN") {
                    alert("Enter Minor Repairs Released value");
                    $("#minorRepairsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(minorRepairsSpent) > parseInt(minorRepairsReleased)) {
//                    alert("Minor Repairs Expenditure value should be less than Minor Repairs Released");
//                    $("#minorRepairsSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (toiletsSpent === "" || toiletsSpent === "NaN") {
                    alert("Enter Toilets Spent value");
                    $("#toiletsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (parseInt(toiletsSpent) > parseInt(toiletsReleased)) {
                    alert("Toilets Expenditure value should be less than Toilets Released");
                    $("#toiletsSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (waterElectricityTelephoneChargesSpent === "" || waterElectricityTelephoneChargesSpent === "NaN") {
                    alert("Enter Annual Grants waterElectricityTelephoneCharges Expenditure value");
                    $("#waterElectricityTelephoneChargesSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(waterElectricityTelephoneChargesSpent) > parseInt(waterElectricityTelephoneChargesReleased)) {
//                    alert("Annual Grants WaterElectricityTelephoneCharges Expenditure value should be less than waterElectricityTelephoneCharges Released");
//                    $("#waterElectricityTelephoneChargesSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "" || purchaseofBooksAndPeriodicalsAndNewsPapersSpent === "NaN") {
                    alert("Enter Annual Grants purchaseofBooksAndPeriodicalsAndNewsPapers Expenditure value");
                    $("#waterElectricityTelephoneChargesSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(purchaseofBooksAndPeriodicalsAndNewsPapersSpent) > parseInt(purchaseofBooksAndPeriodicalsAndNewsPapersReleased)) {
//                    alert("Annual Grants PurchaseofBooksAndPeriodicalsAndNewsPapers Expenditure value should be less than purchaseofBooksAndPeriodicalsAndNewsPapers Released");
//                    $("#purchaseofBooksAndPeriodicalsAndNewsPapersSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                } 
                else if (minorRepairsAnnualSpent === "" || minorRepairsAnnualSpent === "NaN") {
                    alert("Enter Annual Grants Minor Repairs Expenditure value");
                    $("#minorRepairsAnnualSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(minorRepairsAnnualSpent) > parseInt(minorRepairsAnnualReleased)) {
//                    alert("Annual Grants Minor Repairs Expenditure value should be less than Minor Repairs Released");
//                    $("#minorRepairsAnnualSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (sanitationAndICTSpent === "" || sanitationAndICTSpent === "NaN") {
                    alert("Enter Annual Grants Sanitation and ICT Expenditure value");
                    $("#sanitationAndICTSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(sanitationAndICTSpent) > parseInt(sanitationAndICTReleased)) {
//                    alert("Annual Grants Sanitation and ICT value should be less than Sanitation and ICT Released");
//                    $("#sanitationAndICTSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (needBasedWorksSpent === "" || needBasedWorksSpent === "NaN") {
                    alert("Enter Annual Grants needBasedWorks Expenditure value");
                    $("#needBasedWorksSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(needBasedWorksSpent) > parseInt(needBasedWorksReleased)) {
//                    alert("Annual Grants needBasedWorks Expenditure value should be less than needBasedWorks Released");
//                    $("#needBasedWorksSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (provisionalLaboratorySpent === "" || provisionalLaboratorySpent === "NaN") {
                    alert("Enter  Annual Grants Provisional’s for Laboratory value");
                    $("#provisionalLaboratorySpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(provisionalLaboratorySpent) > parseInt(provisionalLaboratoryReleased)) {
//                    alert("Annual Grants Provisional’s for Laboratory Expenditure value should be less than Provisional’s for Laboratory Released");
//                    $("#provisionalLaboratorySpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (recurringExcursionSpent === "" || recurringExcursionSpent === "NaN") {
                    alert("Enter Recurring Excursion Trip Expenditure value");
                    $("#recurringExcursionSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(recurringExcursionSpent) > parseInt(recurringExcursionRelease)) {
//                    alert("Recurring Excursion Trip Expenditure value should be less than Excursion Trip Released");
//                    $("#recurringExcursionSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (recurringSelfDefenseSpent === "" || recurringSelfDefenseSpent === "NaN") {
                    alert("Enter Recurring Self Defense Expenditure value");
                    $("#recurringSelfDefenseSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(recurringSelfDefenseSpent) > parseInt(recurringSelfDefenseRelease)) {
//                    alert("Recurring Self Defense Expenditure value should be less than Self Defense Released");
//                    $("#recurringSelfDefenseSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (recurringOtherSpent === "" || recurringOtherSpent === "NaN") {
                    alert("Enter Recurring Other Recurring Grants Expenditure value");
                    $("#recurringOtherSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(recurringOtherSpent) > parseInt(recurringOtherRelease)) {
//                    alert("Recurring Other Recurring Grants Expenditure value should be less than Other Recurring Grants Released");
//                    $("#recurringOtherSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (furnitureSpent === "" || furnitureSpent === "NaN") {
                    alert("Enter furniture Expenditure value");
                    $("#furnitureSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(furnitureSpent) > parseInt(furnitureReleased)) {
//                    alert("Furniture Expenditure value should be less than Furniture Released");
//                    $("#furnitureSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (labEquipmentSpent === "" || labEquipmentSpent === "NaN") {
                    alert("Enter Lab Equipment Expenditure value");
                    $("#labEquipmentSpent").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(labEquipmentSpent) > parseInt(labEquipmentReleased)) {
//                    alert("Lab Equipment Expenditure value should be less than Lab Equipment Released");
//                    $("#labEquipmentSpent").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (interestEarned === "" || interestEarned === "NaN") {
                    alert("Enter Earned Interest");
                    $("#interestEarned").focus().css({'border': '1px solid red'});
                    return false;
                } else if (interestExpenditure === "" || interestExpenditure === "NaN") {
                    alert("Enter Expenditure");
                    $("#interestExpenditure").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if (parseInt(interestExpenditure) > parseInt(interestEarned)) {
//                    alert("Expenditure value should be less than Earned");
//                    $("#interestEarned").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else {

                    document.forms[0].mode.value = "updateRmsaComponentMaster";
                    document.forms[0].submit();
                }
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode === 46)
                    return true;
                else if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <style type="text/css">
            table.altrowstable1 th {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                text-align: center !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 td {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 {
                border-right: 1px #000000 solid !important;
                border-top: 1px #000000 solid !important;
            }
            table.altrowstable1 thead th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            table.altrowstable1 tbody th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            input { 
                padding: 2px 5px;
                margin: 5px 0px;
                border: 1px solid #ccc;
                text-align: center;

            }
            .table tbody td {
                background: transparent !important;
            }
        </style>
    </head>
    <body>
        <html:form action="/rmsaSchoolComponent" method="post" >
            <html:hidden property="mode"/>
            <html:hidden property="sno"/>
            <input type="hidden" name="roleId" id="roleId" value="<%=session.getAttribute("RoleId").toString()%>"/>
            <%  String msg = null;
                if (request.getAttribute("msg") != null) {
                    msg = request.getAttribute("msg").toString();

            %>
        <center><div id="msg"><span style="color:#0095ff !important"><b><%=msg%><b></span></div></center><%}%>
                            <section class="testimonial_sec clear_fix">
                                <div class="container">
                                    <div class="row">
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                                            <div class="innerbodyrow">
                                                <div class="col-xs-12">
                                                    <h3> EXPENDITURE </h3>
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Year :</label>
                                                    <html:select property="year" styleId="year" style="display: block; width: 100%; height: 34px;width 20%; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value='0'>Select Year</html:option>
                                                        <html:optionsCollection property="yearList" label="yearCode" value="yearCode"/>
                                                    </html:select> 
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <input class="btn btn-primary" style="margin-top: 22px;"  type='button' value='Search' id="Search"/>
                                                </div>

                                                <logic:present  name="RMSAMaster"> 
                                                    <div class="col-xs-12 col-sm-12" style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Bank Details</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">School Code</label>
                                                        <html:text property="schoolCode" styleId="schoolCode"  onkeypress='return onlyNumbers(event);'  readonly="true"  style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">Bank Name</label>
                                                        <html:text property="bankName" styleId="bankName"  onkeypress='return onlyNumbers(event);'  readonly="true"  style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">Account Number</label>
                                                        <html:text property="bankAccountNumber" styleId="bankAccountNumber"  onkeypress='return onlyNumbers(event);' readonly="true" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label class="formlabel">IFSC Code</label>
                                                        <html:text property="ifscCode" styleId="ifscCode"  onkeypress='return onlyNumbers(event);'  readonly="true" style="display: block; width: 100%; height: 34px; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;"/>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Non Recurring</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Civil Works (Construction)</th>
                                                                    <th style="width: 80px">Major Repairs</th>
                                                                    <th style="width: 80px">Minor Repairs</th>
                                                                    <th style="width: 80px">Toilets</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 80px">Released</th>
                                                                        <% if (session.getAttribute(year).equals("2016-17")) {%>

                                                                    <td style="text-align: center;"><html:text property="civilWorksReleased" styleId="civilWorksReleased" onkeypress='return onlyNumbers(event);' maxlength="10"  style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsReleased" styleId="majorRepairsReleased" onkeypress='return onlyNumbers(event);' maxlength="10"  style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="minorRepairsReleased" styleId="minorRepairsReleased" onkeypress='return onlyNumbers(event);' maxlength="10"   style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="toiletsReleased" styleId="toiletsReleased" onkeypress='return onlyNumbers(event);' maxlength="5"  style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="totalCivilReleased" styleId="totalCivilReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <% } else {%>  
                                                                    <td style="text-align: center;"><html:text property="civilWorksReleased" styleId="civilWorksReleased" onkeypress='return onlyNumbers(event);' maxlength="10" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsReleased" styleId="majorRepairsReleased" onkeypress='return onlyNumbers(event);' maxlength="10" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="minorRepairsReleased" styleId="minorRepairsReleased" onkeypress='return onlyNumbers(event);' maxlength="10"  readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="toiletsReleased" styleId="toiletsReleased" onkeypress='return onlyNumbers(event);' maxlength="5" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="totalCivilReleased" styleId="totalCivilReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>

                                                                    <%}%>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 80px">Expenditure</th>
                                                                    <td style="text-align: center;"><html:text property="civilWorksSpent" styleId="civilWorksSpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsSpent" styleId="majorRepairsSpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="minorRepairsSpent" styleId="minorRepairsSpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="toiletsSpent" styleId="toiletsSpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="totalCivilSpent" styleId="totalCivilSpent" onkeypress='return onlyNumbers(event);'  readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 80px">Balance</th>
                                                                    <td style="text-align: center;"> <html:text property="civilWorksAvailable" styleId="civilWorksAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="majorRepairsAvailable" styleId="majorRepairsAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAvailable" styleId="minorRepairsAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="toiletsAvailable" styleId="toiletsAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalCivilAvailable" styleId="totalCivilAvailable" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Furniture & Lab Equipment</p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12">        
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Furniture</th>
                                                                    <th style="width: 80px">Lab Equipment</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <%if (session.getAttribute(year).equals("2016-17")) {%>
                                                                    <th style="width: 20px">Released</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureReleased" styleId="furnitureReleased" onkeypress='return onlyNumbers(event);' maxlength="10" style="text-align: center;width:80px;" /></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentReleased" styleId="labEquipmentReleased" onkeypress='return onlyNumbers(event);' maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabReleased" styleId="totalfurnitureAndLabReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <% } else {%>  
                                                                    <th style="width: 20px">Released</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureReleased" styleId="furnitureReleased" onkeypress='return onlyNumbers(event);' maxlength="10" style="text-align: center;width:80px;"  readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentReleased" styleId="labEquipmentReleased" onkeypress='return onlyNumbers(event);' maxlength="10" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabReleased" styleId="totalfurnitureAndLabReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <%}%>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Expenditure</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureSpent" styleId="furnitureSpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentSpent" styleId="labEquipmentSpent"  onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabSpent" styleId="totalfurnitureAndLabSpent" onkeypress='return onlyNumbers(event);'  readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Balance</th>
                                                                    <td style="text-align: center;"> <html:text property="furnitureAvailable" styleId="furnitureAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="labEquipmentAvailable" styleId="labEquipmentAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalfurnitureAndLabAvailable" styleId="totalfurnitureAndLabAvailable" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                            </tbody>
                                                        </table> 
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Annual Grants</p>
                                                    </div>  
                                                    <div class="col-xs-12 col-sm-12">        
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Water, Electricity, Telephone Charges</th>
                                                                    <th style="width: 80px">Purchase of Books, Periodicals, News Papers</th>
                                                                    <th style="width: 80px">Minor Repairs</th>
                                                                    <th style="width: 80px">Sanitation and ICT</th>
                                                                    <th style="width: 80px">Need based Works etc..if any</th>
                                                                    <th style="width: 80px">Provisional’s for Laboratory</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 65px">Released</th>
                                                                        <%if (session.getAttribute(year).equals("2016-17")) {%>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesReleased" styleId="waterElectricityTelephoneChargesReleased" onkeypress='return onlyNumbers(event);' maxlength="10"  style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersReleased" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersReleased" onkeypress='return onlyNumbers(event);'  maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualReleased" styleId="minorRepairsAnnualReleased" onkeypress='return onlyNumbers(event);'  maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTReleased" styleId="sanitationAndICTReleased" onkeypress='return onlyNumbers(event);'  maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksReleased" styleId="needBasedWorksReleased" onkeypress='return onlyNumbers(event);' maxlength="10"  style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratoryReleased" styleId="provisionalLaboratoryReleased" onkeypress='return onlyNumbers(event);' maxlength="10"  style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualReleased" styleId="totalAnnualReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <% } else {%>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesReleased" styleId="waterElectricityTelephoneChargesReleased" onkeypress='return onlyNumbers(event);' maxlength="10" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersReleased" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersReleased" onkeypress='return onlyNumbers(event);' readonly="true" maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualReleased" styleId="minorRepairsAnnualReleased" onkeypress='return onlyNumbers(event);' readonly="true" maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTReleased" styleId="sanitationAndICTReleased" onkeypress='return onlyNumbers(event);' readonly="true" maxlength="10" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksReleased" styleId="needBasedWorksReleased" onkeypress='return onlyNumbers(event);' maxlength="10" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratoryReleased" styleId="provisionalLaboratoryReleased" onkeypress='return onlyNumbers(event);' maxlength="10" readonly="true" style="text-align: center;width:80px;"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualReleased" styleId="totalAnnualReleased" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>

                                                                    <%}%>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 65px">Expenditure</th>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesSpent" styleId="waterElectricityTelephoneChargesSpent"  onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;"/></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersSpent" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersSpent"  onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualSpent" styleId="minorRepairsAnnualSpent"  onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTSpent" styleId="sanitationAndICTSpent"  onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksSpent" styleId="needBasedWorksSpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratorySpent" styleId="provisionalLaboratorySpent" onkeypress='return onlyNumbers(event);' maxlength="10" style="width:80px;" /></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualSpent" styleId="totalAnnualSpent" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 65px">Balance</th>
                                                                    <td style="text-align: center;"> <html:text property="waterElectricityTelephoneChargesAvailable" styleId="waterElectricityTelephoneChargesAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="purchaseofBooksAndPeriodicalsAndNewsPapersAvailable" styleId="purchaseofBooksAndPeriodicalsAndNewsPapersAvailable" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="minorRepairsAnnualAvailable" styleId="minorRepairsAnnualAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"><html:text property="sanitationAndICTAvailable" styleId="sanitationAndICTAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="needBasedWorksAvailable" styleId="needBasedWorksAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="provisionalLaboratoryAvailable" styleId="provisionalLaboratoryAvailable" onkeypress='return onlyNumbers(event);' style="text-align: center;width:80px;" readonly="true"/></td>
                                                                    <td style="text-align: center;"> <html:text property="totalAnnualAvailable" styleId="totalAnnualAvailable" onkeypress='return onlyNumbers(event);' readonly="true" style="text-align: center;width:80px;"/></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                        <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Recurring</p>
                                                    </div>  
                                                    <div class="col-xs-12 col-sm-12">
                                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr colspan="4">
                                                                    <th></th>
                                                                    <th style="width: 80px">Excursion Trip</th>
                                                                    <th style="width: 80px">Self Defense</th>
                                                                    <th style="width: 80px">Other Recurring Grants</th>
                                                                    <th style="width: 80px">Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Released</th>
                                                                        <%if (session.getAttribute(year).equals("2016-17")) {%>
                                                                    <td style="text-align: center;"> <html:text property="recurringExcursionRelease" styleId="recurringExcursionRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringSelfDefenseRelease" styleId="recurringSelfDefenseRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherRelease" styleId="recurringOtherRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalRelease" styleId="recurringTotalRelease" onkeypress='return onlyNumbers(event);' readonly="true" maxlength="5" style="text-align: center;width:80px;"></html:text></td>
                                                                    <% } else {%>  
                                                                    <td style="text-align: center;"> <html:text property="recurringExcursionRelease" styleId="recurringExcursionRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;"  readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringSelfDefenseRelease" styleId="recurringSelfDefenseRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherRelease" styleId="recurringOtherRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalRelease" styleId="recurringTotalRelease" onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"  ></html:text></td>
                                                                    <%}%>
                                                                </tr>
                                                                <tr colspan="4">
                                                                    <th style="width: 20px">Expenditure</th>
                                                                    <td style="text-align: center;"><html:text property="recurringExcursionSpent" styleId="recurringExcursionSpent"  onkeypress='return onlyNumbers(event);' maxlength="5" style="width:80px;"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringSelfDefenseSpent" styleId="recurringSelfDefenseSpent"  onkeypress='return onlyNumbers(event);' maxlength="5"   style="width:80px;" ></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherSpent" styleId="recurringOtherSpent" onkeypress='return onlyNumbers(event);' maxlength="5"   style="width:80px;" ></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalSpent" styleId="recurringTotalSpent" onkeypress='return onlyNumbers(event);' maxlength="5"  readonly="true"   style="text-align: center;width:80px;"></html:text></td>
                                                                    </tr>
                                                                    <tr colspan="4">
                                                                        <th style="width: 20px">Balance</th>
                                                                        <td style="text-align: center;"><html:text property="recurringExcursionAvailable" styleId="recurringExcursionAvailable"   onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringSelfDefenseAvailable" styleId="recurringSelfDefenseAvailable"   onkeypress='return onlyNumbers(event);' maxlength="5" style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"><html:text property="recurringOtherAvailable" styleId="recurringOtherAvailable"   onkeypress='return onlyNumbers(event);' maxlength="5"   style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    <td style="text-align: center;"> <html:text property="recurringTotalAvailable" styleId="recurringTotalAvailable"  onkeypress='return onlyNumbers(event);' maxlength="5"  style="text-align: center;width:80px;" readonly="true"></html:text></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table> 
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                            <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Interest</p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12">
                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                                <thead>
                                                                    <tr colspan="3">
                                                                        <th style="width: 80px">Earned</th>
                                                                        <th style="width: 80px">Expenditure</th>
                                                                        <th style="width: 80px">Balance</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr colspan="3">
                                                                        <td style="text-align: center;"> <html:text property="interestEarned" styleId="interestEarned" onkeypress='return onlyNumbers(event);' maxlength="9"   /></td>
                                                                    <td style="text-align: center;"><html:text property="interestExpenditure" styleId="interestExpenditure" onkeypress='return onlyNumbers(event);' maxlength="9"/></td>
                                                                    <td style="text-align: center;"> <html:text property="interestBalance" styleId="interestBalance" onkeypress='return onlyNumbers(event);' readonly="true" /></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>           
                                                    </div>
                                                    <div class="col-xs-12" style="text-align:center">
                                                        <center> <input class="btn btn-primary"  type="button" value="Update" name="Update" onclick="submitForm();" /></center>
                                                    </div>
                                                </logic:present>    
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </div>
                            </section>
                        </html:form>
                        </body>
                        </html>