<%-- 
    Document   : VCOrganization
    Created on : Feb 21, 2018, 3:30:29 PM
    Author     : 1250881
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
    ArrayList vclist = (ArrayList) request.getAttribute("vclist");
    ArrayList list1 = new ArrayList();
    if (request.getAttribute("vclist") != null) {
        list1 = (ArrayList) request.getAttribute("vclist");
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RMSA</title> <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/></script>
    <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/></script>
<script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/></script>
<script type="text/javascript" class="init">
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 300,
            "scrollX": true
        });
    });</script>
<style type="text/css">
    table.dataTable.nowrap td {
        border-bottom: 1px #083254 solid;
        border-left: 1px #083254 solid;
        vertical-align: middle;
        padding-left: 3px;
        padding-right: 3px;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
    }
    table.dataTable.nowrap th {
        background-color: #0E94C5;
        text-align: center;
        border-bottom: 1px #000 solid;
        padding-left: 3px;
        padding-right: 3px;
        border-left: 1px #000 solid;
        vertical-align: left;
        font-size: 11px;
        font-family: verdana;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }
    table.dataTable.display tbody tr.odd {
        background: #E2E4FF !important;
    }
    table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
        background: #E2E4FF !important;
    }
    .dataTables_wrapper {
        padding: 10px !important;
    }
    .dataTables_length label {
        font-size: 12px !important;
    }
    .dataTables_filter label {
        font-size: 12px !important;
    }
    .dataTables_info {
        font-size: 12px !important;
    }
    .dataTables_paginate {
        font-size: 12px !important;
    }
    form input {
        margin-bottom: 5px;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: initial !important;
    }
    .well {
        overflow: hidden;
    }
    .well-lg {
        padding: 10px !important;
    }
    form input {
        padding: 0px !important;
        border-radius: 0 !important;
        font-size: 12px;
        padding: 3px 5px !important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
        height: auto !important;
    }
    select {
        display: inline-block !important;
    }
    /* .dataTables_wrapper .dataTables_scroll {
        clear: both;
width: 1000px;
    } */
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid #49A0E3 !important;
    }
    .table {
        border: 0 !important;
        border-radius: 0 !important;
    }
    input[type="button"] {
        width: 60px !important;
    }
</style>
<style type="text/css">
    .districts_main {
        width:1000px;
        margin:0px auto;
        text-align: center;
    }
    .distirct1 {
        width:250px;
        float:left;
        margin:10px 0;
    }
    .distirct2 {
        width:230px;
        float:left;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;
        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;
        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        font-size: 16px;
        text-align: right;
        font-weight: bold;
    }
    @-moz-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @-webkit-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
</style>
<style type="text/css">
    table.altrowstable1 th {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        text-align: center !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 td {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 {
        border-right: 1px #000000 solid !important;
        border-top: 1px #000000 solid !important;
    }
    table.altrowstable1 thead th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    table.altrowstable1 tbody th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    input { 
        padding: 2px 5px;
        margin: 5px 0px;
    }
</style>
<script>

    function hmApprovalStatus(reqType, nextTypeName, vcHMremarksId) {
      
        var paraData = "vctrainerId=" + reqType + "&schcd=" + nextTypeName + "&vcHMremarksId=" + $('#' + vcHMremarksId).val();
        $.ajax({
            type: "POST",
            url: "<%=basePath%>vCHMConfirmation.do?mode=vcHMStatusUpdate",
            data: paraData,
            success: function(response) {

            },
            error: function(e) {
                alert('Error: ' + e);
            }
        });
    }

</script>
<script language="javascript">
    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(event)
    {
        if (event.button == 2)
        {
            alert(status);
            return false;
        }
    }
</script>

</head>
<body>
    <html:form action="/vcOrganization" method="post">
        <html:hidden property="mode"/>
        <html:hidden property="statusFlag"/>
        <html:hidden property="vcTrainerIdValue"/>

        <section class="testimonial_sec clear_fix">
            <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                        <div class="col-xs-12">
                            <h3> Vocational Organization </h3>
                        </div>
                        <logic:present name="vclist">
                            <div style="overflow-x: scroll; width: 1070px; margin: 0 20px">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <tr class="darkgrey" >
                                        <th    style="text-align: center">Sl.No  </th>
                                            <logic:present name="checkList">
                                            <th> Select All <INPUT type="checkbox" onchange="checkAll(this)" name="chk[]" /></th>
                                            </logic:present>
                                            <logic:notPresent name="checkList">
                                            <th    style="text-align: center">select</th>
                                            </logic:notPresent>

                                        <th    style="text-align: center">VC Trainer</th>
                                        <th    style="text-align: center">DateOfBirth</th>
                                        <th    style="text-align: center">Aadhar</th>
                                        <th    style="text-align: center">Mobile</th>
                                        <th    style="text-align: center">email Id</th>
                                        <th    style="text-align: center">Sector</th>
                                        <th    style="text-align: center">Trade</th>
                                        <th    style="text-align: center">VC Remarks</th>
                                        <th    style="text-align: center">HM Remarks</th>
                                        <th    style="text-align: center">Organization Type</th>
                                    </tr>
                                    <tr class="darkgrey" >
                                        <th style="text-align: center">1</th> 
                                        <th style="text-align: center">2</th>
                                        <th style="text-align: center">3</th>
                                        <th style="text-align: center">4</th>
                                        <th style="text-align: center">5</th>
                                        <th style="text-align: center">6</th> 
                                        <th style="text-align: center">7</th>
                                        <th style="text-align: center">8</th>
                                        <th style="text-align: center">9</th>
                                        <th style="text-align: center">10</th>
                                        <th style="text-align: center">11</th>
                                        <th style="text-align: center">12</th>
                                    </tr>
                                    <logic:iterate name="vclist" id="list">
                                        <tr>
                                            <td style="text-align: center"><%=i++%></td> 
                                            <logic:present name="checkList">                                                
                                                 <c:set var="abc" value="${list.statusFlag}"/>
                                            <c:choose>
                                                <c:when test="${abc  == 'N'}">
                                                <td style="text-align: center;">
                                                    <html:checkbox property="vcCheckBox" styleId="vcId" styleClass="vcCheckBox" value="${list.vctrainerId},${list.statusFlag}" onclick="validateButton();"/>
                                                </td>                                                
                                                </c:when>
                                                <c:otherwise>
                                                   <td style="text-align: center;">
                                                     <input type="checkbox" styleId="vcId" disabled="true"  styleClass="vcCheckBox" checked>
                                                     </td>
                                                </c:otherwise>
                                            </c:choose>   
                                            </logic:present>
                                            <logic:notPresent name="checkList">
                                                <td style="text-align: center;">
                                                    <input type="checkbox" styleId="vcId" disabled="true"  styleClass="vcCheckBox" checked>
                                               </td>
                                            </logic:notPresent>
                                            <td style="text-align: center;">${list.vctrainerName}</td>
                                            <td style="text-align: center;"> ${list.dob}</td>
                                            <td style="text-align: center;"> ${list.aadhar}</td>
                                            <td style="text-align: center;"> ${list.mobileno}</td>
                                            <td style="text-align: center;"> ${list.emailId}</td>
                                            <td style="text-align: center;"> ${list.sectorName}</td>
                                            <td style="text-align: center;"> ${list.tradeName}</td>
                                            <td style="text-align: center;"> ${list.appRemarks}</td>
                                            <td style="text-align: center;"> ${list.hmRemarks}</td>
                                            <td style="text-align: center;"> ${list.orgType}</td>
                                        </tr> 
                                    </logic:iterate>
                                    <logic:present name="checkList">
                                        <tr>
                                            <td colspan="30" style="text-align: center"> 
                                                <input type="button" name="clickto" value="Submit" style="cursor: pointer;" onclick="updateRecords();"/>
                                            </td>
                                        </tr>
                                    </logic:present>
                                </table>
                            </div>
                        </logic:present>
                        <logic:present  name="msg">
                            <center>  <font color="red"> ${msg}</font></center>
                            </logic:present>
                    </div>
                </div>
            </div>
        </section>       
    </html:form>
    <script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
   
    function validateButton() {
        var k = false;
        $('.vcCheckBox').each(function() {
            if (this.checked) {
                k = true;
            } else {
                k = false;
            }

        });
    }
    
//     function seletAllBox() {
//                var chkbox = document.getElementsByName('teacherIdsDeo');
//                var typSel = document.getElementById("checkAll").checked;
//                for (var i = 0, n = chkbox.length; i < n; i++) {
//                    if (typSel) {
//                        chkbox [i].checked = true;
//                    } else {
//                        chkbox [i].checked = false;
//                    }
//
//                }
//            }
    function updateRecords() {
        var isChk = false; 
        var vcTrainerIdVal = null;
        var vcTrainerIdValue = [];
        var statusFlag = [];
        var inputElements = document.getElementsByClassName('vcCheckBox');
        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                 isChk = true;
                vcTrainerIdVal = inputElements[i].value;
                var myarray = vcTrainerIdVal.split(",");
                vcTrainerIdValue = vcTrainerIdValue + "~" + myarray[0];
                statusFlag = statusFlag + "~" + myarray[1];
            }
        } if (isChk == false) {
                    alert("Please check atleast one check box");
                    return false;
                }
        document.forms[0].vcTrainerIdValue.value = vcTrainerIdValue;
        document.forms[0].statusFlag.value = statusFlag;
        document.forms[0].mode.value = "updateRecords";
        document.forms[0].submit();
    }

    </script>
</body>
</html>

