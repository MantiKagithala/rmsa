

<%-- 
    Document   : rmsaComponentReport
    Created on : Jan 23, 2018, 5:20:00 PM
    Author     : 1259084
--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%
    int i = 1, checkId = 0;
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
System.out.println(".......CHANGE PASSWORD TARGET");
    String district = null;
    if (request.getAttribute("district") != null) {
        district = (String) request.getAttribute("district");
    }
    String mandal = null;
    if (request.getAttribute("mandal") != null) {
        mandal = (String) request.getAttribute("mandal");
    }
%>
<html>
    <head>
        <title>:RMSA:</title>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/>
    </script>
    <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/>
</script>
<script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/>
</script>
<script src="js/md5.js" type="text/javascript"></script>

<style type="text/css">
    table.dataTable.nowrap td {
        border-bottom: 1px #083254 solid;
        border-left: 1px #083254 solid;
        vertical-align: middle;
        padding-left: 3px;
        padding-right: 3px;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
    }
    table.dataTable.nowrap th {
        background-color: #0E94C5;
        text-align: center;
        border-bottom: 1px #000 solid;
        padding-left: 3px;
        padding-right: 3px;
        border-left: 1px #000 solid;
        vertical-align: left;
        font-size: 11px;
        font-family: verdana;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }
    table.dataTable.display tbody tr.odd {
        background: #E2E4FF !important;
    }
    table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
        background: #E2E4FF !important;
    }
    .dataTables_wrapper {
        padding: 10px !important;
    }
    .dataTables_length label {
        font-size: 12px !important;
    }
    .dataTables_filter label {
        font-size: 12px !important;
    }
    .dataTables_info {
        font-size: 12px !important;
    }
    .dataTables_paginate {
        font-size: 12px !important;
    }
    form input {
        margin-bottom: 5px;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: initial !important;
    }
    .well {
        overflow: hidden;
    }
    .well-lg {
        padding: 10px !important;
    }
    form input {
        padding: 0px !important;
        border-radius: 0 !important;
        font-size: 12px;
        padding: 3px 5px !important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
        height: auto !important;
    }
    select {
        display: inline-block !important;
    }
    /* .dataTables_wrapper .dataTables_scroll {
        clear: both;
width: 1000px;
    } */
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid #49A0E3 !important;
    }
    .table {
        border: 0 !important;
        border-radius: 0 !important;
    }
    input[type="button"] {
        width: 60px !important;
    }
</style>
<style type="text/css">
    .districts_main {
        width:1000px;
        margin:0px auto;
        text-align: center;
    }
    .distirct1 {
        width:250px;
        float:left;
        margin:10px 0;
    }
    .distirct2 {
        width:230px;
        float:left;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;
        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;
        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        font-size: 16px;
        text-align: right;
        font-weight: bold;
    }
    @-moz-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @-webkit-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
</style>
<style type="text/css">
    table.altrowstable1 th {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        text-align: center !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 td {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 {
        border-right: 1px #000000 solid !important;
        border-top: 1px #000000 solid !important;
    }
    table.altrowstable1 thead th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    table.altrowstable1 tbody th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    input { 
        padding: 2px 5px;
        margin: 5px 0px;
    }
</style>
<script type="text/javascript" class="init">
    $(document).ready(function() {
        $('input, :input').attr('autocomplete', 'off');
        $('#submit').hide();
        $(".checkbox").on("click", function() {
            var anyChecked = false;
            var chkbox = document.getElementsByName('checkid1');
            for (var b = 0, mn = chkbox.length; b < mn; b++) {
                if (chkbox[b].checked) {
                    anyChecked = true;
                }
            }
            if (anyChecked === true) {
                $('#submit').show();
            } else {
                $('#submit').hide();
            }

        });
    });

    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 300,
            "scrollX": true
        });
    });
    function mysearch() {
        var district = $('#district').val();
        var mandal = $('#mandal').val();
        var phaseNo = $('#phaseNo').val();

        if (phaseNo === "0") {
            alert("Select PhaseNo ");
            $("#phaseNo").focus().css({'border': '1px solid red'});
        } else if (district === "0") {
            alert("Select District Name");
            $("#district").focus().css({'border': '1px solid red'});
        }
        else if (mandal === "0") {
            alert("Select Mandal Name");
            $("#mandal").focus().css({'border': '1px solid red'});
        }
        else {
            document.forms[0].mode.value = "searchSchoolFunding";
            document.forms[0].submit();
        }
    }
    function mySubmit() {
        var SanctionedForCivilCost = "";
        var SanctionedForFurniture = "";
        var SanctionofLabEquipment = "";
        var TotalSanctioned = "";
        var TotalRelease = "";
        var checkedBoxes = [];
        var anyChecked = false;
        var chkbox = document.getElementsByName('checkid1');
        for (var j = 0, n = chkbox.length; j < n; j++) {
            if (chkbox[j].checked) {
                checkedBoxes.push(chkbox[j].value);
                if ($('#sc' + j).val() === null || $('#sc' + j).val() === "") {
                    alert("Please Enter Sanctioned For Civil Cost ");
                    $('#sc' + j).focus();
                    return false;
                } else {
                    SanctionedForCivilCost = SanctionedForCivilCost + $('#sc' + j).val() + "~";
                }
                if ($('#sf' + j).val() === null || $('#sf' + j).val() === "") {
                    alert("Please Enter Sanctioned For Furniture ");
                    $('#sf' + j).focus();
                    return false;
                } else {
                    SanctionedForFurniture = SanctionedForFurniture + $('#sf' + j).val() + "~";
                }
                if ($('#sl' + j).val() === null || $('#sl' + j).val() === "") {
                    alert("Please Enter Sanction of Lab Equipment ");
                    $('#sl' + j).focus();
                    return false;
                } else {
                    SanctionofLabEquipment = SanctionofLabEquipment + $('#sl' + j).val() + "~";
                }
                if ($('#ts' + j).val() === null || $('#ts' + j).val() === "") {
                    alert("Please Enter Total Sanctioned ");
                    $('#ts' + j).focus();
                    return false;
                } else {
                    TotalSanctioned = TotalSanctioned + $('#ts' + j).val() + "~";
                }
                if ($('#tr' + j).val() === null || $('#tr' + j).val() === "") {
                    alert("Please Enter Total Release ");
                    $('#tr' + j).focus();
                    return false;
                } else {
                    TotalRelease = TotalRelease + $('#tr' + j).val() + "~";
                }
                anyChecked = true;
            }
        }
        if (anyChecked === false) {
            alert("Please select at least once cehck box");
            return false;
        } else {
            document.forms[0].checkedBoxesdp.value = checkedBoxes;
            document.forms[0].sanctionedForCivilCost.value = SanctionedForCivilCost;
            document.forms[0].sanctionedForFurniture.value = SanctionedForFurniture;
            document.forms[0].sanctionofLabEquipment.value = SanctionofLabEquipment;
            document.forms[0].totalSanctioned1.value = TotalSanctioned;
            document.forms[0].totalRelease.value = TotalRelease;
            document.forms[0].checkedIndex.value = j;
            document.forms[0].mode.value = "rmsaMasterUpdate";
            document.forms[0].submit();
            return true;
        }
    }
    function onlyNumbers(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode === 46)
            return true;
        else if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function space(evt, thisvalue) {
        var number = thisvalue.value;
        if (number.length < 1) {
            if (evt.keyCode === 32) {
                return false;
            }
        }
        return true;
    }
    function GetXmlHttpObject() {
        var objXmlHttp = null;
        if (window.XMLHttpRequest)
        {
            objXmlHttp = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            objXmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        return objXmlHttp;
    }
    function  getDitsricts() {
        x = GetXmlHttpObject();
        x.onreadystatechange = getDistrictList;
        var phaseNo = document.forms[0].phaseNo.value;
        var url = "rmsaCivils.do?mode=getDistrictList&phaseNo=" + phaseNo + "";
        x.open("GET", url, true);
        x.send();
    }
    function getDistrictList() {
        var rs1, rs2;

        removeall("district");
        removeall("mandal");
        document.forms[0].district.value = "0";
        if (x.readyState === 4 || x.readyState === "complete")
        {
            var m = x.responseXML.documentElement;
            var z = 0;
            var counts = m.getElementsByTagName("name")[z].childNodes[0].nodeValue;
            m.getElementsByTagName("id")[z].childNodes[0].nodeValues;
            z = 1;
            while (z <= counts)
            {
                rs1 = m.getElementsByTagName("name")[z].childNodes[0].nodeValue;
                rs2 = m.getElementsByTagName("id")[z].childNodes[0].nodeValue;
                addoption(rs1, rs2, "district");
                z++;
            }
        }
    }
    function  getMandals() {
        x = GetXmlHttpObject();
        x.onreadystatechange = getMandalList;
        var phaseNo = document.forms[0].phaseNo.value;
        var district = document.forms[0].district.value;
        var url = "rmsaCivils.do?mode=getMandalList&phaseNo=" + phaseNo + "&districtId=" + district + "";
        x.open("GET", url, true);
        x.send();
    }
    function getMandalList() {
        var rs1, rs2;

        removeall("mandal");
        document.forms[0].mandal.value = "0";
        if (x.readyState === 4 || x.readyState === "complete")
        {
            var m = x.responseXML.documentElement;
            var z = 0;
            var counts = m.getElementsByTagName("name")[z].childNodes[0].nodeValue;
            m.getElementsByTagName("id")[z].childNodes[0].nodeValues;
            z = 1;
            while (z <= counts)
            {
                rs1 = m.getElementsByTagName("name")[z].childNodes[0].nodeValue;
                rs2 = m.getElementsByTagName("id")[z].childNodes[0].nodeValue;
                addoption(rs1, rs2, "mandal");
                z++;
            }
        }
    }
    function addoption(result1, result2, name) {
        var opt = document.createElement("OPTION");
        opt.text = result1;
        opt.value = result2;
        try {
            document.getElementById(name).add(opt);
        } catch (ex)
        {
            if (name === "district") {
                document.forms[0].district.appendChild(opt, null);
            }
            if (name === "mandal") {
                document.forms[0].mandal.appendChild(opt, null);
            }
        }

    }
    function removeall(name) {
        if (name === "district") {
            var x1 = document.forms[0].district.options.length;
        }
        if (name === "mandal") {
            var x1 = document.forms[0].mandal.options.length;
        }

        for (i = x1; i > 0; i--) {
            if (name === "district") {
                document.forms[0].district.options[i] = null;
            }
            if (name === "mandal") {
                document.forms[0].mandal.options[i] = null;
            }
        }
    }
    window.onload = function() {
        var seconds = 5;
        setTimeout(function() {
            if (document.getElementById("msg") !== null) {
                document.getElementById("msg").style.display = "none";
            }
        }, seconds * 1000);
    };

    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right

    function onlyNumbersWithchar(e) {
        var keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
        var ret = ((keyCode !== 32) || (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
        if (ret === false)
            alert("This key not allowed");
        return ret;
    }

    function submitForm() {
        var currentPassword = $('#currentPassword').val();
        var newPassword = $('#newPassword').val();
        var confirmPassword = $('#confirmPassword').val();
       
        if (currentPassword === "" || currentPassword === 'undefined' || currentPassword === null) {
            $('#currentPassword').focus();
            alert("Enter Current Password");
            return false;
        } else if (newPassword === "" || newPassword === 'undefined' || newPassword === null) {
            $('#newPassword').focus();
            alert("Enter New Password");
            return false;
        }
        else if (newPassword.length<4) {
            $('#newPassword').focus();
            alert("New Password length should be greater than 3");
            return false;
        } 
        else if (confirmPassword === "" || confirmPassword === 'undefined' || confirmPassword === null) {
            $('#confirmPassword').focus();
            alert("Enter Confirm Password");
            return false;
        } 
        else if(confirmPassword.length<4) {
            $('#confirmPassword').focus();
            alert("Confirm Password length should be greater than 3");
            return false;
        } 
         else if (newPassword !== confirmPassword) {
            $("#newPassword").focus();
            alert("New password and confirm password did't match");
            return false;
        }
        else if (currentPassword === confirmPassword) {
            $("#newPassword").focus();
            alert("Current Password and New Password should not be same");
            return false;
        }
        else {
            document.forms[0].mode.value = "updatePassword";
            document.forms[0].currentPasswordEncreption.value = calcMD5($("#currentPassword").val());
            document.forms[0].updatePassword.value = calcMD5($("#newPassword").val());
            document.forms[0].submit();
        }
    }

</script>
</head>
<body>
    <html:form action="/changepassword" method="post" enctype="multipart/form-data">
        <html:hidden property="mode"/>
        <html:hidden property="updatePassword"/>
        <html:hidden property="currentPasswordEncreption"/>


        <section class="testimonial_sec clear_fix">
            <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                        <div class="innerbodyrow">
                            <div class="col-xs-12">
                                <h3>Change Password </h3>
                            </div>

                            <%  String msg = null, color = null;
                                if (request.getAttribute("SuccessMsg") != null) {
                                    msg = request.getAttribute("SuccessMsg").toString();
                                    color = request.getAttribute("colour").toString();
                            %>
                            <center><span id="msg" style="color:<%=color%> !important"><b><%=msg%><b></span></center><%}%>

                                            <div class="row"  style="margin:0px">
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Current Password</label><span style="color:#f00 !important">*</span>
                                                    <input type="password" name="currentPassword" id="currentPassword"  maxlength="8" value="" class="form-control textbox" placeholder="Current Password" autocomplete="off">
                                                </div>

                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">New Password</label><span style="color:#f00 !important">*</span>
                                                    <input type="password" name="newPassword" id="newPassword" maxlength="8" value="" class="form-control textbox" placeholder="New Password" onkeypress="return onlyNumbersWithchar(event);" autocomplete="off">
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Confirm Password</label><span style="color:#f00 !important">*</span>
                                                    <input type="password" name="confirmPassword" id="confirmPassword" maxlength="8" value="" class="form-control textbox" placeholder="Confirm Password" onkeypress="return onlyNumbersWithchar(event);" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="row"  style="margin:0px">
                                                <div class="col-xs-12 col-sm-1" style="text-align:right">
                                                    <input type="button" class="btn btn-primary" name="but" value="Submit" onclick="return submitForm();"/>
                                                </div>

                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </section>                    
                                        </html:form>
                                        </body>
                                        </html>