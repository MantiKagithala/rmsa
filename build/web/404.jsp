<%-- 
    Document   : Welcome
    Created on : Jan 9, 2018, 11:52:22 AM
    Author     : 1250892
--%>
<%@page isErrorPage="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>::RMSA::</title>
    </head>
    <body>
        <h1 class="blink_me">
           404 Error : Invalid Request <br>
           Go to home Page : <a href="https://rmsa.ap.gov.in/RMSA/">RMSA</a>
        </h1>
    </body>
</html>
