<%-- 
    Document   : rmsaComponentReport
    Created on : Jan 23, 2018, 5:20:00 PM
    Author     : 1259084
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
    String district = null;
    if (request.getAttribute("district") != null) {
        district = (String) request.getAttribute("district");
    }
    String mandal = null;
    if (request.getAttribute("mandal") != null) {
        mandal = (String) request.getAttribute("mandal");
    }

%>
<html>
    <head>

        <title>JSP Page</title>
        
        <script type="text/javascript" class="init">

                $("#Search").click(function() {
                    var year = $('#year').val();
                    if (year === "0") {
                        alert("Select Year");
                        $("#year").focus().css({'border': '1px solid red'});
                    }
                    else {
                        document.forms[0].mode.value = "getDistList";
                        document.forms[0].submit();
                    }
                });
              </script>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/>
        </script>
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/>
        </script>
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/>
        </script>

        <script type="text/javascript" class="init">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollY": 300,
                    "scrollX": true
                });
                $("#Search").click(function() {
                    var year = $('#year').val();
                    if (year === "0") {
                        alert("Select Year");
                        $("#year").focus().css({'border': '1px solid red'});
                    }
                    else {
                        document.forms[0].mode.value = "getDistList";
                        document.forms[0].submit();
                    }
                });
            });</script>

        <style type="text/css">
            table.dataTable.nowrap td {
                border-bottom: 1px #083254 solid;
                border-left: 1px #083254 solid;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;

                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
            }

            table.dataTable.nowrap th {
                background-color: #0E94C5;
                text-align: center;
                border-bottom: 1px #000 solid;
                padding-left: 3px;
                padding-right: 3px;
                border-left: 1px #000 solid;
                vertical-align: left;
                font-size: 11px;
                font-family: verdana;

                font-weight: bold;
                color: #fff;
                padding: 5px;
            }

            table.dataTable.display tbody tr.odd {
                background: #E2E4FF !important;
            }
            table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
                background: #E2E4FF !important;
            }
            .dataTables_wrapper {

                padding: 10px !important;
            }
            .dataTables_length label {
                font-size: 12px !important;
            }
            .dataTables_filter label {
                font-size: 12px !important;
            }
            .dataTables_info {
                font-size: 12px !important;
            }
            .dataTables_paginate {
                font-size: 12px !important;
            }
            form input {
                margin-bottom: 5px;
            }
            table.dataTable.nowrap th, table.dataTable.nowrap td {
                white-space: initial !important;
            }
            .well {
                overflow: hidden;
            }
            .well-lg {
                padding: 10px !important;
            }
            form input {
                padding: 0px !important;
                border-radius: 0 !important;
                font-size: 12px;
                padding: 3px 5px !important;
            }
            .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
                height: auto !important;
            }
            select {
                display: inline-block !important;
            }
            /* .dataTables_wrapper .dataTables_scroll {
                clear: both;
    width: 1000px;
            } */

            .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
                border-top: 1px solid #49A0E3 !important;
            }
            .table {
                border: 0 !important;
                border-radius: 0 !important;
            }
            input[type="button"] {
                width: 60px !important;
            }

        </style>

        <style type="text/css">
            .districts_main {
                width:1000px;
                margin:0px auto;
                text-align: center;
            }
            .distirct1 {
                width:250px;
                float:left;
                margin:10px 0;
            }
            .distirct2 {
                width:230px;
                float:left;
            }
            .blink_me {
                -webkit-animation-name: blinker;
                -webkit-animation-duration: 2s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;

                -moz-animation-name: blinker;
                -moz-animation-duration: 2s;
                -moz-animation-timing-function: linear;
                -moz-animation-iteration-count: infinite;

                animation-name: blinker;
                animation-duration: 2s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;

                font-size: 16px;
                text-align: right;
                font-weight: bold;

            }
            @-moz-keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
            @-webkit-keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
            @keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
        </style>
        <style type="text/css">
            table.altrowstable1 th {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                text-align: center !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 td {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 {
                border-right: 1px #000000 solid !important;
                border-top: 1px #000000 solid !important;
            }
            table.altrowstable1 thead th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            table.altrowstable1 tbody th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            input { 
                padding: 2px 5px;
                margin: 5px 0px;

            }
        </style>
    </head>
    <body>
        <html:form action="/rmsaComponentReport">
            <html:hidden property="mode"/>
           
              <section class="testimonial_sec clear_fix">
                                <div class="container">
                                    <div class="row">
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">

                                            <div class="innerbodyrow">
                                                <div class="col-xs-12">
                                                    <h3> RMSA Components Report </h3>
                                                </div>
                                               
            <logic:present name="yList">
            <table align="center" cellpadding="0" cellspacing="0" border="0" width="40%" class="altrowstable1">
                <tr>
                    <th>Year </th>
                    <td>
                        <html:select property="year" styleId="year" style="width:220px">
                            <html:option value='0'>Select Year</html:option>
                            <html:optionsCollection property="yearList" label="yearCode" value="yearCode"/>
                        </html:select> 
                    </td>
                    <td>
                        <input type='button' value='Search' id="Search"/>
                    </td>

                </tr>

            </table>
            </logic:present><br>
            <logic:present name="distlist">
                <table align="center"  border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr><td align="right">
                            <a href="./rmsaComponentReport.xls?mode=getDistrictExcel&year=${year}" target="_blank"><img src="<%=basePath%>imgs/excel.jpg"  height="20px" width="25px"/></a>
                        </td> </tr>
                </table>

                <div style="overflow-x: scroll; width: 1070px; margin: 0 20px">

                    <!--<table align="center"  border="0" cellpadding="0" cellspacing="1" width="97%"  id="demo_datatables" class="altrowstable table_style">-->
                    <table id="example" class="display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr class="darkgrey" >
                                <th  rowspan="3"  style="text-align: center">Sl.No</th>
                                <th  rowspan="3"  style="text-align: center">District</th>
                                <th  rowspan="2"  colspan="8" style="text-align: center">Opening Balance</th>
                                <th  rowspan="2"  colspan="8" style="text-align: center">Expenditure on Opening Balances</th>
                                <th  rowspan="2"  colspan="9" style="text-align: center">Unspent Balances on Opening Balances</th>
                                <th  colspan="15" style="text-align: center">Non Recurring</th>
                                <th  colspan="21" style="text-align: center">Annual Grants</th>
                                <th  colspan="12" style="text-align: center">Recurring</th>
                                <th  colspan="9"  style="text-align: center">Furniture and Lab Equipment</th>
                                <th  rowspan="2"  colspan="3" style="text-align: center">Interest</th>
                                <th  colspan="13" style="text-align: center">Grand Total</th>
                            </tr>
                            <tr>

                                <th colspan="3" style="text-align: center">CivilWorks Construction </th>
                                <th colspan="3" style="text-align: center">Major Repairs</th>
                                <th colspan="3" style="text-align: center">Minor Repairs</th>
                                <th colspan="3" style="text-align: center">Toilets </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Water Electricity</br> Telephone charges </th>
                                <th colspan="3" style="text-align: center">Purchase of Books,</br>Periodical,News Papers</th>
                                <th colspan="3" style="text-align: center">Minor Repairs</th>
                                <th colspan="3" style="text-align: center">sanitation and ICT</th>
                                <th colspan="3" style="text-align: center">Need Based works etc. </th>
                                <th colspan="3" style="text-align: center">Provisional for Laboratory </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Excursion Trip</th>
                                <th colspan="3" style="text-align: center">Self Defense</th>
                                <th colspan="3" style="text-align: center">Other Recurring grants </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Furniture</th>
                                <th colspan="3" style="text-align: center">Lab equipment </th>
                                <th colspan="3" style="text-align: center">Total </th>

                                <th colspan="4" style="text-align: center">Total Releases</th>
                                <th colspan="5" style="text-align: center">Expenditure Incurred</th>
                                <th colspan="4" style="text-align: center">Closing Balance on Financial Year</th>

                            </tr>
                            <tr>
                                <!--                                 <th  style="text-align: center">CivilWorks Construction </th>
                                                                <th  style="text-align: center">Major Repairs</th>
                                                                <th  style="text-align: center">Minor Repairs</th>
                                                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                                                <th  style="text-align: center">Toilets </th>
                                -->
                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>
                                <th  style="text-align: center">Released </th>

                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>

                                <th  style="text-align: center">Earned </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>

                                <th  style="text-align: center">Total Opening Balance</th>
                                <th  style="text-align: center">Relases</th>
                                <th  style="text-align: center">Interest</th>
                                <th  style="text-align: center">Total</th>

                                <th  style="text-align: center">Expenditure incurred on opening balances</th>
                                <th  style="text-align: center">Expenditure Incurred on Releases</th>
                                <th  style="text-align: center">Expenditure Incurred on Interest</th>
                                <th  style="text-align: center">Remmitance Amount</th>
                                <th  style="text-align: center">Total Expenditure</th>

                                <th  style="text-align: center">Unspent Balance on opening balances</th>
                                <th  style="text-align: center">Unspent Balance on Releases Made</th>
                                <th  style="text-align: center">Unspent Balance on Interest</th>
                                <th  style="text-align: center">Total Unspent Balance</th>


                            </tr>
                            <tr class="darkgrey num_height">    
                                <th style="text-align: center"><b>(1)</b></th> 
                                <th style="text-align: center"><b>(2)</b> </th>
                                <th style="text-align: center"><b>(3)</b> </th>
                                <th style="text-align: center"><b>(4)</b> </th>
                                <th style="text-align: center"><b>(5)</b> </th>
                                <th style="text-align: center"><b>(6)</b></th> 
                                <th style="text-align: center"><b>(7)</b> </th>
                                <th style="text-align: center"><b>(8)</b> </th>
                                <th style="text-align: center"><b>(9)</b> </th>
                                <th style="text-align: center"><b>(10)</b> </th>
                                <th style="text-align: center"><b>(11)</b></th> 
                                <th style="text-align: center"><b>(12)</b> </th>
                                <th style="text-align: center"><b>(13)</b> </th>
                                <th style="text-align: center"><b>(14)</b> </th>
                                <th style="text-align: center"><b>(15)</b> </th>
                                <th style="text-align: center"><b>(16)</b></th> 
                                <th style="text-align: center"><b>(17)</b> </th>
                                <th style="text-align: center"><b>(18)</b></th> 
                                <th style="text-align: center"><b>(19)</b> </th>
                                <th style="text-align: center"><b>(20)</b> </th>
                                <th style="text-align: center"><b>(21)</b> </th>
                                <th style="text-align: center"><b>(22)</b> </th>
                                <th style="text-align: center"><b>(23)</b></th> 
                                <th style="text-align: center"><b>(24)</b> </th>
                                <th style="text-align: center"><b>(25)</b> </th>
                                <th style="text-align: center"><b>(26)</b> </th>
                                <th style="text-align: center"><b>(27)</b> </th>
                                <th style="text-align: center"><b>(28)</b></th> 
                                <th style="text-align: center"><b>(29)</b> </th>
                                <th style="text-align: center"><b>(30)</b> </th>
                                <th style="text-align: center"><b>(31)</b> </th>
                                <th style="text-align: center"><b>(32)</b> </th>
                                <th style="text-align: center"><b>(33)</b></th> 
                                <th style="text-align: center"><b>(34)</b> </th>
                                <th style="text-align: center"><b>(35)</b> </th>
                                <th style="text-align: center"><b>(36)</b> </th>
                                <th style="text-align: center"><b>(37)</b> </th>
                                <th style="text-align: center"><b>(38)</b> </th>
                                <th style="text-align: center"><b>(39)</b> </th>
                                <th style="text-align: center"><b>(40)</b> </th>
                                <th style="text-align: center"><b>(41)</b> </th>
                                <th style="text-align: center"><b>(42)</b> </th>
                                <th style="text-align: center"><b>(43)</b> </th>
                                <th style="text-align: center"><b>(44)</b> </th>
                                <th style="text-align: center"><b>(45)</b> </th>
                                <th style="text-align: center"><b>(46)</b> </th>
                                <th style="text-align: center"><b>(47)</b> </th>
                                <th style="text-align: center"><b>(48)</b> </th>
                                <th style="text-align: center"><b>(49)</b> </th>
                                <th style="text-align: center"><b>(50)</b> </th>
                                <th style="text-align: center"><b>(51)</b> </th>
                                <th style="text-align: center"><b>(52)</b> </th>
                                <th style="text-align: center"><b>(53)</b> </th>
                                <th style="text-align: center"><b>(54)</b> </th>
                                <th style="text-align: center"><b>(55)</b> </th>
                                <th style="text-align: center"><b>(56)</b> </th>
                                <th style="text-align: center"><b>(57)</b> </th>
                                <th style="text-align: center"><b>(58)</b> </th>
                                <th style="text-align: center"><b>(59)</b> </th>

                                <th style="text-align: center"><b>(60)</b> </th>
                                <th style="text-align: center"><b>(61)</b> </th>
                                <th style="text-align: center"><b>(62)</b> </th>
                                <th style="text-align: center"><b>(63)</b> </th>
                                <th style="text-align: center"><b>(64)</b> </th>
                                <th style="text-align: center"><b>(65)</b> </th>
                                <th style="text-align: center"><b>(66)</b> </th>
                                <th style="text-align: center"><b>(67)</b> </th>
                                <th style="text-align: center"><b>(68)</b> </th>
                                <th style="text-align: center"><b>(69)</b> </th>
                                <th style="text-align: center"><b>(70)</b> </th>
                                <th style="text-align: center"><b>(71)</b> </th>
                                <th style="text-align: center"><b>(72)</b> </th>
                                <th style="text-align: center"><b>(73)</b> </th>
                                <th style="text-align: center"><b>(74)</b> </th>
                                <th style="text-align: center"><b>(75)</b> </th>

                                <th style="text-align: center"><b>(76)</b> </th>
                                <th style="text-align: center"><b>(77)</b> </th>
                                <th style="text-align: center"><b>(78)</b> </th>
                                <th style="text-align: center"><b>(79)</b> </th>
                                <th style="text-align: center"><b>(80)</b> </th>
                                <th style="text-align: center"><b>(81)</b> </th>
                                <th style="text-align: center"><b>(82)</b> </th>
                                <th style="text-align: center"><b>(83)</b> </th>
                                <th style="text-align: center"><b>(84)</b> </th>
                                <th style="text-align: center"><b>(85)</b> </th>
                                <th style="text-align: center"><b>(86)</b> </th>
                                <th style="text-align: center"><b>(87)</b> </th>
                                <th style="text-align: center"><b>(88)</b> </th>


                                <th style="text-align: center"><b>(89)</b> </th>
                                <th style="text-align: center"><b>(90)</b> </th>
                                <th style="text-align: center"><b>(91)</b> </th>
                                <th style="text-align: center"><b>(92)</b> </th>
                                <th style="text-align: center"><b>(93)</b> </th>
                                <th style="text-align: center"><b>(94)</b> </th>
                                <th style="text-align: center"><b>(95)</b> </th>
                                <th style="text-align: center"><b>(96)</b> </th>
                                <th style="text-align: center"><b>(97)</b> </th>
                                <th style="text-align: center"><b>(98)</b> </th>
                                <th style="text-align: center"><b>(99)</b> </th>
                                <th style="text-align: center"><b>(100)</b> </th>


                            </tr>
                        </thead>
                        <tbody>
                            <logic:iterate name="distlist" id="list">
                                <tr>
                                    <td style="text-align: center"  class=" normalRow"><%=i++%></td>
                                    <td style="text-align: left"    class=" normalRow"><a href="./rmsaComponentReport.do?mode=getMandalList&district=${list.distcode}&year=${year}">${list.distname}</a></td>
                                    <td>${list.CivilWorks_OB}</td><td>${list.MajorRepairs_OB}</td><td>${list.MinorRepairs_OB}</td><td>${list.AnnualOtherRecurringGrants_OB}</td><td>${list.Toilets_OB}</td><td>${list.SelfDefenseTrainingGrants_OB}</td><td>${list.Interest_OB}</td><td>${list.total_OB}</td>
                                    <td>${list.CivilWorks_EOB}</td><td>${list.MajorRepairs_EOB}</td><td>${list.MinorRepairs_EOB}</td><td>${list.AnnualOtherRecurringGrants_EOB}</td><td>${list.Toilets_EOB}</td><td>${list.SelfDefenseETrainingGrants_EOB}</td><td>${list.Interest_EOB}</td><td>${list.total_EOB}</td>
                                    <td>${list.CivilWorks_UBOB}</td><td>${list.MajorRepairs_UBOB}</td><td>${list.MinorRepairs_UBOB}</td><td>${list.AnnualOtherRecurringGrants_UBOB}</td><td>${list.Toilets_UBOB}</td><td>${list.SelfDefenseETrainingGrants_UBOB}</td><td>${list.Interest_UBOB}</td><td>${list.total_UBOB}</td>
                                    <td>${list.Released}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.civilReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.civilExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.civilBal}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.majorReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurBal}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.waterReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.waterExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.waterBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorReleasedAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorExpAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorBalAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipBal}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.Earned_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Expenditure_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Balance_Interest}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.total_OB}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRealse_Realses}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Earned_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRealse_total}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.total_EOB}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Grand_Total_Expenditure_Incurred_on_Releases}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Expenditure_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.RemmitanceAmount_ExpenditureIncurred}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Grand_Total_Total_Expenditure}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_ob}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_rb}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_ineterest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_total}</td>




                                </tr>

                            </logic:iterate>
                        </tbody>
                        <tfoot>
                            <tr>    
                                <th colspan="2" style="text-align: center"><b>Total</b></th>

                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_OB}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_EOB}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Released}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.civilNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expcivilNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totcivilNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.majorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expmajorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totmajorNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.minorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expminorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totminorNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.toiletNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptoiletNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tottoiletNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptotNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tottotNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.waterAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expwaterAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.twaterAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.booksAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expbooksAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.twaterAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.minorAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expminorAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tminorAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.sanitationAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expsanitationAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tsanitationAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.needAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expneedAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tneedAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.provisiAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expprovisiAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tprovisiAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptotAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.ttotAnnualGrantsTotal}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.excurTriprecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excexcurTriprecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.texcurTriprecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.selfdefencerecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excselfdefencerecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tselfdefencerecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.otherrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excotherrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totherrecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exctotrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.ttotrecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.relfurFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exfurFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tfurFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.rellabFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exlabFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tlabFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.reltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tltotFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_Earned_Interest}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Expenditure_Interest}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Balance_Interest}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_total_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_totalRealse_Realses}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Earned_Interest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totalRealse_total_total}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Expenditure_Incurred_on_Opening_balances}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Expenditure_Incurred_on_Releases_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Expenditure_Interest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_RemmitanceAmount_ExpenditureIncurred_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Total_Expenditure_total}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_ob_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_rb_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_ineterest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_total_total}</b> </th>


                            </tr>
                        </tfoot>
                    </table>
                </div>

            </logic:present>
            <logic:present name="mandallist">
                <!--                     <table align="center"  border="0" cellpadding="0" cellspacing="0" width="95%">
                                    <tr><td align="right">
                                            <a href="./rmsaComponentReport.xls?mode=getDistrictExcel" target="_blank"><img src="<%=basePath%>images/excel.jpg"  height="20px" width="25px"/></a>
                                        </td> </tr>
                                </table>-->
                <div style="float:right;margin-bottom: 10px; margin-right: 24px;">
                    <a href="./rmsaComponentReport.xls?mode=getMandalExcel&district=<%=district%>&year=${year}" target="_blank"><img src="<%=basePath%>imgs/excel.jpg"  height="20px" width="25px"/></a>

                    <a href="./rmsaComponentReport.do?mode=getDistList&year=${year}">Back</a>
                </div>
                <div style="overflow-x: scroll; width: 1000px; margin: 0 20px">

                    <!--<table align="center"  border="0" cellpadding="0" cellspacing="1" width="97%"  id="demo_datatables" class="altrowstable table_style">-->
                    <table id="example" class="display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr class="darkgrey" >
                                <th  rowspan="3"style="text-align: center">Sl.No</th>
                                <th  rowspan="3"style="text-align: center">Mandal</th>
                                <th  rowspan="2"  colspan="8" style="text-align: center">Opening Balance</th>
                                <th  rowspan="2"  colspan="8" style="text-align: center">Expenditure on Opening Balances</th>
                                <th  rowspan="2"  colspan="9" style="text-align: center">Unspent Balances on Opening Balances</th>
                                <th  colspan="15" style="text-align: center">Non Recurring</th>
                                <th  colspan="21" style="text-align: center">Annual Grants</th>
                                <th  colspan="12" style="text-align: center">Recurring</th>
                                <th  colspan="9" style="text-align: center">Furniture and Lab Equipment</th>
                                <th  rowspan="2"  colspan="3" style="text-align: center">Interest</th>
                                <th  colspan="13" style="text-align: center">Grand Total</th>
                            </tr>
                            <tr>
                                <th colspan="3" style="text-align: center">CivilWorks Construction </th>
                                <th colspan="3" style="text-align: center">Major Repairs</th>
                                <th colspan="3" style="text-align: center">Minor Repairs</th>
                                <th colspan="3" style="text-align: center">Toilets </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Water Electricity</br> Telephone charges </th>
                                <th colspan="3" style="text-align: center">Purchase of Books,</br>Periodical,News Papers</th>
                                <th colspan="3" style="text-align: center">Minor Repairs</th>
                                <th colspan="3" style="text-align: center">sanitation and ICT</th>
                                <th colspan="3" style="text-align: center">Need Based works etc. </th>
                                <th colspan="3" style="text-align: center">Provisional for Laboratory </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Excursion Trip</th>
                                <th colspan="3" style="text-align: center">Self Defense</th>
                                <th colspan="3" style="text-align: center">Other Recurring grants </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Furniture</th>
                                <th colspan="3" style="text-align: center">Lab equipment </th>
                                <th colspan="3" style="text-align: center">Total </th>

                                <th colspan="4" style="text-align: center">Total Releases</th>
                                <th colspan="5" style="text-align: center">Expenditure Incurred</th>
                                <th colspan="4" style="text-align: center">Closing Balance on Financial Year</th>


                            </tr>
                            <tr>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>
                                <th  style="text-align: center">Released </th>

                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>

                                <th  style="text-align: center">Earned </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>

                                <th  style="text-align: center">Total Opening Balance</th>
                                <th  style="text-align: center">Relases</th>
                                <th  style="text-align: center">Interest</th>
                                <th  style="text-align: center">Total</th>

                                <th  style="text-align: center">Expenditure incurred on opening balances</th>
                                <th  style="text-align: center">Expenditure Incurred on Releases</th>
                                <th  style="text-align: center">Expenditure Incurred on Interest</th>
                                <th  style="text-align: center">Remmitance Amount</th>
                                <th  style="text-align: center">Total Expenditure</th>

                                <th  style="text-align: center">Unspent Balance on opening balances</th>
                                <th  style="text-align: center">Unspent Balance on Releases Made</th>
                                <th  style="text-align: center">Unspent Balance on Interest</th>
                                <th  style="text-align: center">Total Unspent Balance</th>

                            </tr>
                            <tr class="darkgrey num_height">    
                                <th style="text-align: center"><b>(1)</b></th> 
                                <th style="text-align: center"><b>(2)</b> </th>
                                <th style="text-align: center"><b>(3)</b> </th>
                                <th style="text-align: center"><b>(4)</b> </th>
                                <th style="text-align: center"><b>(5)</b> </th>
                                <th style="text-align: center"><b>(6)</b></th> 
                                <th style="text-align: center"><b>(7)</b> </th>
                                <th style="text-align: center"><b>(8)</b> </th>
                                <th style="text-align: center"><b>(9)</b> </th>
                                <th style="text-align: center"><b>(10)</b> </th>
                                <th style="text-align: center"><b>(11)</b></th> 
                                <th style="text-align: center"><b>(12)</b> </th>
                                <th style="text-align: center"><b>(13)</b> </th>
                                <th style="text-align: center"><b>(14)</b> </th>
                                <th style="text-align: center"><b>(15)</b> </th>
                                <th style="text-align: center"><b>(16)</b></th> 
                                <th style="text-align: center"><b>(17)</b> </th>
                                <th style="text-align: center"><b>(18)</b></th> 
                                <th style="text-align: center"><b>(19)</b> </th>
                                <th style="text-align: center"><b>(20)</b> </th>
                                <th style="text-align: center"><b>(21)</b> </th>
                                <th style="text-align: center"><b>(22)</b> </th>
                                <th style="text-align: center"><b>(23)</b></th> 
                                <th style="text-align: center"><b>(24)</b> </th>
                                <th style="text-align: center"><b>(25)</b> </th>
                                <th style="text-align: center"><b>(26)</b> </th>
                                <th style="text-align: center"><b>(27)</b> </th>
                                <th style="text-align: center"><b>(28)</b></th> 
                                <th style="text-align: center"><b>(29)</b> </th>
                                <th style="text-align: center"><b>(30)</b> </th>
                                <th style="text-align: center"><b>(31)</b> </th>
                                <th style="text-align: center"><b>(32)</b> </th>
                                <th style="text-align: center"><b>(33)</b></th> 
                                <th style="text-align: center"><b>(34)</b> </th>
                                <th style="text-align: center"><b>(35)</b> </th>
                                <th style="text-align: center"><b>(36)</b> </th>
                                <th style="text-align: center"><b>(37)</b> </th>
                                <th style="text-align: center"><b>(38)</b> </th>
                                <th style="text-align: center"><b>(39)</b> </th>
                                <th style="text-align: center"><b>(40)</b> </th>
                                <th style="text-align: center"><b>(41)</b> </th>
                                <th style="text-align: center"><b>(42)</b> </th>
                                <th style="text-align: center"><b>(43)</b> </th>
                                <th style="text-align: center"><b>(44)</b> </th>
                                <th style="text-align: center"><b>(45)</b> </th>
                                <th style="text-align: center"><b>(46)</b> </th>
                                <th style="text-align: center"><b>(47)</b> </th>
                                <th style="text-align: center"><b>(48)</b> </th>
                                <th style="text-align: center"><b>(49)</b> </th>
                                <th style="text-align: center"><b>(50)</b> </th>
                                <th style="text-align: center"><b>(51)</b> </th>
                                <th style="text-align: center"><b>(52)</b> </th>
                                <th style="text-align: center"><b>(53)</b> </th>
                                <th style="text-align: center"><b>(54)</b> </th>
                                <th style="text-align: center"><b>(55)</b> </th>
                                <th style="text-align: center"><b>(56)</b> </th>
                                <th style="text-align: center"><b>(57)</b> </th>
                                <th style="text-align: center"><b>(58)</b> </th>
                                <th style="text-align: center"><b>(59)</b> </th>
                                <th style="text-align: center"><b>(60)</b> </th>
                                <th style="text-align: center"><b>(61)</b> </th>
                                <th style="text-align: center"><b>(62)</b> </th>
                                <th style="text-align: center"><b>(63)</b> </th>
                                <th style="text-align: center"><b>(64)</b> </th>
                                <th style="text-align: center"><b>(65)</b> </th>
                                <th style="text-align: center"><b>(66)</b> </th>
                                <th style="text-align: center"><b>(67)</b> </th>
                                <th style="text-align: center"><b>(68)</b> </th>
                                <th style="text-align: center"><b>(69)</b> </th>
                                <th style="text-align: center"><b>(70)</b> </th>
                                <th style="text-align: center"><b>(71)</b> </th>
                                <th style="text-align: center"><b>(72)</b> </th>
                                <th style="text-align: center"><b>(73)</b> </th>
                                <th style="text-align: center"><b>(74)</b> </th>
                                <th style="text-align: center"><b>(75)</b> </th>
                                <th style="text-align: center"><b>(76)</b> </th>
                                <th style="text-align: center"><b>(77)</b> </th>
                                <th style="text-align: center"><b>(78)</b> </th>
                                <th style="text-align: center"><b>(79)</b> </th>
                                <th style="text-align: center"><b>(80)</b> </th>
                                <th style="text-align: center"><b>(81)</b> </th>
                                <th style="text-align: center"><b>(82)</b> </th>
                                <th style="text-align: center"><b>(83)</b> </th>
                                <th style="text-align: center"><b>(84)</b> </th>
                                <th style="text-align: center"><b>(85)</b> </th>
                                <th style="text-align: center"><b>(86)</b> </th>
                                <th style="text-align: center"><b>(87)</b> </th>
                                <th style="text-align: center"><b>(88)</b> </th>
                                <th style="text-align: center"><b>(89)</b> </th>
                                <th style="text-align: center"><b>(90)</b> </th>
                                <th style="text-align: center"><b>(91)</b> </th>
                                <th style="text-align: center"><b>(92)</b> </th>
                                <th style="text-align: center"><b>(93)</b> </th>
                                <th style="text-align: center"><b>(94)</b> </th>
                                <th style="text-align: center"><b>(95)</b> </th>
                                <th style="text-align: center"><b>(96)</b> </th>
                                <th style="text-align: center"><b>(97)</b> </th>
                                <th style="text-align: center"><b>(98)</b> </th>
                                <th style="text-align: center"><b>(99)</b> </th>
                                <th style="text-align: center"><b>(100)</b> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <logic:iterate name="mandallist" id="list">
                                <tr>
                                    <td style="text-align: center"  class=" normalRow"><%=i++%></td>
                                    <td style="text-align: left"    class=" normalRow"><a href="./rmsaComponentReport.do?mode=getSchoolList&mandal=${list.mandalcode}&year=${year}">${list.mandalname}</a></td>


                                    <td>${list.CivilWorks_OB}</td><td>${list.MajorRepairs_OB}</td><td>${list.MinorRepairs_OB}</td><td>${list.AnnualOtherRecurringGrants_OB}</td><td>${list.Toilets_OB}</td><td>${list.SelfDefenseTrainingGrants_OB}</td><td>${list.Interest_OB}</td><td>${list.total_OB}</td>
                                    <td>${list.CivilWorks_EOB}</td><td>${list.MajorRepairs_EOB}</td><td>${list.MinorRepairs_EOB}</td><td>${list.AnnualOtherRecurringGrants_EOB}</td><td>${list.Toilets_EOB}</td><td>${list.SelfDefenseETrainingGrants_EOB}</td><td>${list.Interest_EOB}</td><td>${list.total_EOB}</td>
                                    <td>${list.CivilWorks_UBOB}</td><td>${list.MajorRepairs_UBOB}</td><td>${list.MinorRepairs_UBOB}</td><td>${list.AnnualOtherRecurringGrants_UBOB}</td><td>${list.Toilets_UBOB}</td><td>${list.SelfDefenseETrainingGrants_UBOB}</td><td>${list.Interest_UBOB}</td><td>${list.total_UBOB}</td>
                                    <td>${list.Released}</td>



                                    <td style="text-align: center"  class=" normalRow">${list.civilReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.civilExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.civilBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurBal}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.waterReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.waterExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.waterBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorReleasedAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorExpAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorBalAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantBal}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.excurtionReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurBal}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.furnitureReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipBal}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.Earned_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Expenditure_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Balance_Interest}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.total_OB}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRealse_Realses}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Earned_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRealse_total}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.total_EOB}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Grand_Total_Expenditure_Incurred_on_Releases}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Expenditure_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.RemmitanceAmount_ExpenditureIncurred}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Grand_Total_Total_Expenditure}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_ob}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_rb}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_ineterest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_total}</td>





                                </tr>

                            </logic:iterate>
                        </tbody>
                        <tfoot>
                            <tr>    
                                <th colspan="2" style="text-align: center"><b>Total</b></th>


                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_OB}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_EOB}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totalRealse_total}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.civilNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expcivilNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totcivilNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.majorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expmajorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totmajorNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.minorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expminorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totminorNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.toiletNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptoiletNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tottoiletNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptotNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tottotNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.waterAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expwaterAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.twaterAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.booksAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expbooksAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.twaterAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.minorAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expminorAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tminorAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.sanitationAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expsanitationAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tsanitationAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.needAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expneedAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tneedAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.provisiAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expprovisiAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tprovisiAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptotAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.ttotAnnualGrantsTotal}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.excurTriprecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excexcurTriprecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.texcurTriprecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.selfdefencerecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excselfdefencerecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tselfdefencerecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.otherrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excotherrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totherrecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exctotrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.ttotrecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.relfurFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exfurFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tfurFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.rellabFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exlabFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tlabFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.reltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tltotFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_Earned_Interest}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Expenditure_Interest}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Balance_Interest}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_total_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_totalRealse_Realses}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Earned_Interest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totalRealse_total_total}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Expenditure_Incurred_on_Opening_balances}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Expenditure_Incurred_on_Releases_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Expenditure_Interest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_RemmitanceAmount_ExpenditureIncurred_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Total_Expenditure_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_ob_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_rb_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_ineterest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_total_total}</b> </th>

                            </tr>
                        </tfoot>
                    </table>
                </div>

            </logic:present>


            <logic:present name="schoollist">
                <div style="float:right;margin-bottom: 10px; margin-right: 24px;">
                    <a href="./rmsaComponentReport.xls?mode=getSchoolExcel&mandal=<%=mandal%>&year=${year}" target="_blank"><img src="<%=basePath%>imgs/excel.jpg"  height="20px" width="25px"/></a>

                    <a href="./rmsaComponentReport.do?mode=getMandalList&district=<%=district%>&year=${year}">Back</a>
                </div>
                <div style="overflow-x: scroll; width: 1000px; margin: 0 20px">

                    <!--<table align="center"  border="0" cellpadding="0" cellspacing="1" width="97%"  id="demo_datatables" class="altrowstable table_style">-->
                    <table id="example" class="display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr class="darkgrey" >
                                <th  rowspan="3"style="text-align: center">Sl.No</th>

                                <th  rowspan="3"style="text-align: center">District</th>
                                <th  rowspan="3"style="text-align: center">Mandal</th>
                                <th  rowspan="3"style="text-align: center">Udise Code</th>
                                <th  rowspan="3"style="text-align: center">School</th>
                                <th  rowspan="3"style="text-align: center">Bank Account</th>
                                <th  rowspan="3"style="text-align: center">IFSC</th>
                                <th  rowspan="3"style="text-align: center">Bank Name</th>

                                <th  rowspan="2"  colspan="8" style="text-align: center">Opening Balance</th>
                                <th  rowspan="2"  colspan="8" style="text-align: center">Expenditure on Opening Balances</th>
                                <th  rowspan="2"  colspan="9" style="text-align: center">Unspent Balances on Opening Balances</th>
                                <th  colspan="15" style="text-align: center">Non Recurring</th>
                                <th  colspan="21" style="text-align: center">Annual Grants</th>
                                <th  colspan="12" style="text-align: center">Recurring</th>
                                <th  colspan="9" style="text-align: center">Furniture and Lab Equipment</th>

                                <th  rowspan="2"  colspan="3" style="text-align: center">Interest</th>
                                <th  colspan="13" style="text-align: center">Grand Total</th>

                            </tr>
                            <tr>
                                <th colspan="3" style="text-align: center">CivilWorks Construction </th>
                                <th colspan="3" style="text-align: center">Major Repairs</th>
                                <th colspan="3" style="text-align: center">Minor Repairs</th>
                                <th colspan="3" style="text-align: center">Toilets </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Water Electricity</br> Telephone charges </th>
                                <th colspan="3" style="text-align: center">Purchase of Books,</br>Periodical,News Papers</th>
                                <th colspan="3" style="text-align: center">Minor Repairs</th>
                                <th colspan="3" style="text-align: center">sanitation and ICT</th>
                                <th colspan="3" style="text-align: center">Need Based works etc. </th>
                                <th colspan="3" style="text-align: center">Provisional for Laboratory </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Excursion Trip</th>
                                <th colspan="3" style="text-align: center">Self Defense</th>
                                <th colspan="3" style="text-align: center">Other Recurring grants </th>
                                <th colspan="3" style="text-align: center">Total </th>
                                <th colspan="3" style="text-align: center">Furniture</th>
                                <th colspan="3" style="text-align: center">Lab equipment </th>
                                <th colspan="3" style="text-align: center">Total </th>

                                <th colspan="4" style="text-align: center">Total Releases</th>
                                <th colspan="5" style="text-align: center">Expenditure Incurred</th>
                                <th colspan="4" style="text-align: center">Closing Balance on Financial Year</th>
                            </tr>
                            <tr>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>

                                <th  style="text-align: center">CivilWorks Construction </th>
                                <th  style="text-align: center">Major Repairs</th>
                                <th  style="text-align: center">Minor Repairs</th>
                                <th  style="text-align: center">Annual & Other Recurring Grants</th>
                                <th  style="text-align: center">Toilets</th>
                                <th  style="text-align: center">Self Defense Training Grants if Any</th>
                                <th  style="text-align: center">Interest </th>
                                <th  style="text-align: center">Total </th>
                                <th  style="text-align: center">Released </th>

                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>
                                <th  style="text-align: center">Rleases </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>

                                <th  style="text-align: center">Earned </th>
                                <th  style="text-align: center">Expenditure</th>
                                <th  style="text-align: center">Balance</th>

                                <th  style="text-align: center">Total Opening Balance</th>
                                <th  style="text-align: center">Relases</th>
                                <th  style="text-align: center">Interest</th>
                                <th  style="text-align: center">Total</th>

                                <th  style="text-align: center">Expenditure incurred on opening balances</th>
                                <th  style="text-align: center">Expenditure Incurred on Releases</th>
                                <th  style="text-align: center">Expenditure Incurred on Interest</th>
                                <th  style="text-align: center">Remmitance Amount</th>
                                <th  style="text-align: center">Total Expenditure</th>

                                <th  style="text-align: center">Unspent Balance on opening balances</th>
                                <th  style="text-align: center">Unspent Balance on Releases Made</th>
                                <th  style="text-align: center">Unspent Balance on Interest</th>
                                <th  style="text-align: center">Total Unspent Balance</th>



                            </tr>
                            <tr class="darkgrey num_height">    
                                <th style="text-align: center"><b>(1)</b></th> 
                                <th style="text-align: center"><b>(2)</b> </th>
                                <th style="text-align: center"><b>(3)</b> </th>
                                <th style="text-align: center"><b>(4)</b> </th>
                                <th style="text-align: center"><b>(5)</b> </th>
                                <th style="text-align: center"><b>(6)</b></th> 
                                <th style="text-align: center"><b>(7)</b> </th>
                                <th style="text-align: center"><b>(8)</b> </th>
                                <th style="text-align: center"><b>(9)</b> </th>
                                <th style="text-align: center"><b>(10)</b> </th>
                                <th style="text-align: center"><b>(11)</b></th> 
                                <th style="text-align: center"><b>(12)</b> </th>
                                <th style="text-align: center"><b>(13)</b> </th>
                                <th style="text-align: center"><b>(14)</b> </th>
                                <th style="text-align: center"><b>(15)</b> </th>
                                <th style="text-align: center"><b>(16)</b></th> 
                                <th style="text-align: center"><b>(17)</b> </th>
                                <th style="text-align: center"><b>(18)</b></th> 
                                <th style="text-align: center"><b>(19)</b> </th>
                                <th style="text-align: center"><b>(20)</b> </th>
                                <th style="text-align: center"><b>(21)</b> </th>
                                <th style="text-align: center"><b>(22)</b> </th>
                                <th style="text-align: center"><b>(23)</b></th> 
                                <th style="text-align: center"><b>(24)</b> </th>
                                <th style="text-align: center"><b>(25)</b> </th>
                                <th style="text-align: center"><b>(26)</b> </th>
                                <th style="text-align: center"><b>(27)</b> </th>
                                <th style="text-align: center"><b>(28)</b></th> 
                                <th style="text-align: center"><b>(29)</b> </th>
                                <th style="text-align: center"><b>(30)</b> </th>
                                <th style="text-align: center"><b>(31)</b> </th>
                                <th style="text-align: center"><b>(32)</b> </th>
                                <th style="text-align: center"><b>(33)</b></th> 
                                <th style="text-align: center"><b>(34)</b> </th>
                                <th style="text-align: center"><b>(35)</b> </th>
                                <th style="text-align: center"><b>(36)</b> </th>
                                <th style="text-align: center"><b>(37)</b> </th>
                                <th style="text-align: center"><b>(38)</b> </th>
                                <th style="text-align: center"><b>(39)</b> </th>
                                <th style="text-align: center"><b>(40)</b> </th>
                                <th style="text-align: center"><b>(41)</b> </th>
                                <th style="text-align: center"><b>(42)</b> </th>
                                <th style="text-align: center"><b>(43)</b> </th>
                                <th style="text-align: center"><b>(44)</b> </th>
                                <th style="text-align: center"><b>(45)</b> </th>
                                <th style="text-align: center"><b>(46)</b> </th>
                                <th style="text-align: center"><b>(47)</b> </th>
                                <th style="text-align: center"><b>(48)</b> </th>
                                <th style="text-align: center"><b>(49)</b> </th>
                                <th style="text-align: center"><b>(50)</b> </th>
                                <th style="text-align: center"><b>(51)</b> </th>
                                <th style="text-align: center"><b>(52)</b> </th>
                                <th style="text-align: center"><b>(53)</b> </th>
                                <th style="text-align: center"><b>(54)</b> </th>
                                <th style="text-align: center"><b>(55)</b> </th>
                                <th style="text-align: center"><b>(56)</b> </th>
                                <th style="text-align: center"><b>(57)</b> </th>
                                <th style="text-align: center"><b>(58)</b> </th>
                                <th style="text-align: center"><b>(59)</b> </th>
                                <th style="text-align: center"><b>(60)</b> </th>
                                <th style="text-align: center"><b>(61)</b> </th>
                                <th style="text-align: center"><b>(62)</b> </th>
                                <th style="text-align: center"><b>(63)</b> </th>
                                <th style="text-align: center"><b>(64)</b> </th>
                                <th style="text-align: center"><b>(65)</b> </th>

                                <th style="text-align: center"><b>(66)</b> </th>
                                <th style="text-align: center"><b>(67)</b> </th>
                                <th style="text-align: center"><b>(68)</b> </th>
                                <th style="text-align: center"><b>(69)</b> </th>
                                <th style="text-align: center"><b>(70)</b> </th>
                                <th style="text-align: center"><b>(71)</b> </th>
                                <th style="text-align: center"><b>(72)</b> </th>
                                <th style="text-align: center"><b>(73)</b> </th>

                                <th style="text-align: center"><b>(74)</b> </th>
                                <th style="text-align: center"><b>(75)</b> </th>
                                <th style="text-align: center"><b>(76)</b> </th>
                                <th style="text-align: center"><b>(77)</b> </th>
                                <th style="text-align: center"><b>(78)</b> </th>
                                <th style="text-align: center"><b>(79)</b> </th>
                                <th style="text-align: center"><b>(80)</b> </th>
                                <th style="text-align: center"><b>(81)</b> </th>

                                <th style="text-align: center"><b>(82)</b> </th>
                                <th style="text-align: center"><b>(83)</b> </th>
                                <th style="text-align: center"><b>(84)</b> </th>
                                <th style="text-align: center"><b>(85)</b> </th>
                                <th style="text-align: center"><b>(86)</b> </th>
                                <th style="text-align: center"><b>(87)</b> </th>
                                <th style="text-align: center"><b>(88)</b> </th>
                                <th style="text-align: center"><b>(89)</b> </th>
                                <th style="text-align: center"><b>(90)</b> </th>

                                <th style="text-align: center"><b>(91)</b> </th>
                                <th style="text-align: center"><b>(92)</b> </th>
                                <th style="text-align: center"><b>(93)</b> </th>

                                <th style="text-align: center"><b>(94)</b> </th>
                                <th style="text-align: center"><b>(95)</b> </th>
                                <th style="text-align: center"><b>(96)</b> </th>
                                <th style="text-align: center"><b>(97)</b> </th>

                                <th style="text-align: center"><b>(98)</b> </th>
                                <th style="text-align: center"><b>(99)</b> </th>
                                <th style="text-align: center"><b>(100)</b> </th>
                                <th style="text-align: center"><b>(101)</b> </th>
                                <th style="text-align: center"><b>(102)</b> </th>

                                <th style="text-align: center"><b>(103)</b> </th>
                                <th style="text-align: center"><b>(104)</b> </th>
                                <th style="text-align: center"><b>(105)</b> </th>
                                <th style="text-align: center"><b>(106)</b> </th>



                            </tr>
                        </thead>
                        <tbody>
                            <logic:iterate name="schoollist" id="list">
                                <tr>
                                    <td style="text-align: center"  class=" normalRow"><%=i++%></td>
                                    <td style="text-align: left"    class=" normalRow">${list.distName}</td>
                                    <td style="text-align: left"    class=" normalRow">${list.MandalName}</td>
                                    <td style="text-align: left"    class=" normalRow">${list.schoolCode}</td>
                                    <td style="text-align: left"    class=" normalRow">${list.schoolname}</td>
                                    <td style="text-align: left"    class=" normalRow">${list.bankAccNum}</td>
                                    <td style="text-align: left"    class=" normalRow">${list.ifsc}</td>
                                    <td style="text-align: left"    class=" normalRow">${list.bankName}</td>

                                    <td>${list.CivilWorks_OB}</td><td>${list.MajorRepairs_OB}</td><td>${list.MinorRepairs_OB}</td><td>${list.AnnualOtherRecurringGrants_OB}</td><td>${list.Toilets_OB}</td><td>${list.SelfDefenseTrainingGrants_OB}</td><td>${list.Interest_OB}</td><td>${list.total_OB}</td>
                                    <td>${list.CivilWorks_EOB}</td><td>${list.MajorRepairs_EOB}</td><td>${list.MinorRepairs_EOB}</td><td>${list.AnnualOtherRecurringGrants_EOB}</td><td>${list.Toilets_EOB}</td><td>${list.SelfDefenseETrainingGrants_EOB}</td><td>${list.Interest_EOB}</td><td>${list.total_EOB}</td>
                                    <td>${list.CivilWorks_UBOB}</td><td>${list.MajorRepairs_UBOB}</td><td>${list.MinorRepairs_UBOB}</td><td>${list.AnnualOtherRecurringGrants_UBOB}</td><td>${list.Toilets_UBOB}</td><td>${list.SelfDefenseETrainingGrants_UBOB}</td><td>${list.Interest_UBOB}</td><td>${list.total_UBOB}</td>
                                    <td>${list.Released}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.civilReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.civilExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.civilBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.majorBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.toiletsBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalNonRecurBal}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.waterReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.waterExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.waterBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.booksBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorReleasedAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorExpAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.minorBalAnnual}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.sanitationBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.needBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.provisionalBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalGrantBal}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.excurtionReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.excurtionBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.selfdefBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.otherRecuGrantBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRecurBal}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.furnitureReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.furnitureBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.labEquipBal}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipReleased}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipExp}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totallabEquipBal}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.Earned_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Expenditure_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Balance_Interest}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.total_OB}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRealse_Realses}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Earned_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.totalRealse_total}</td>


                                    <td style="text-align: center"  class=" normalRow">${list.total_EOB}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Grand_Total_Expenditure_Incurred_on_Releases}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Expenditure_Interest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.RemmitanceAmount_ExpenditureIncurred}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.Grand_Total_Total_Expenditure}</td>

                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_ob}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_rb}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_ineterest}</td>
                                    <td style="text-align: center"  class=" normalRow">${list.close_balnce_ub_total}</td>



                                </tr>

                            </logic:iterate>
                        </tbody>
                        <tfoot>
                            <tr>    
                                <th colspan="7" style="text-align: center"><b>Total</b></th>




                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_OB}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_EOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_EOB}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_CivilWorks_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MajorRepairs_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_MinorRepairs_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_AnnualOtherRecurringGrants_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Toilets_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_SelfDefenseTrainingGrants_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Interest_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_total_total_UBOB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totalRealse_total}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.civilNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expcivilNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totcivilNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.majorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expmajorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totmajorNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.minorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expminorNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totminorNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.toiletNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptoiletNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tottoiletNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptotNonRecTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tottotNonRecTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.waterAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expwaterAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.twaterAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.booksAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expbooksAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.twaterAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.minorAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expminorAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tminorAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.sanitationAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expsanitationAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tsanitationAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.needAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expneedAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tneedAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.provisiAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.expprovisiAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tprovisiAnnualGrantsTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exptotAnnualGrantsTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.ttotAnnualGrantsTotal}</b> </th>


                                <th style="text-align: center">
                                    <b>${list.excurTriprecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excexcurTriprecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.texcurTriprecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.selfdefencerecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excselfdefencerecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tselfdefencerecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.otherrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.excotherrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totherrecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.totrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exctotrecurringTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.ttotrecurringTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.relfurFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exfurFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tfurFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.rellabFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exlabFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tlabFurniturLabEquipmentTotal}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.reltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.exltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.tltotFurniturLabEquipmentTotal}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Earned_Interest}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Expenditure_Interest}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Balance_Interest}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.total_total_OB}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_totalRealse_Realses}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Earned_Interest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.totalRealse_total_total}</b> </th>

                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Expenditure_Incurred_on_Opening_balances}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Expenditure_Incurred_on_Releases_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_Expenditure_Interest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.total_RemmitanceAmount_ExpenditureIncurred_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.Grand_Total_Total_Expenditure_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_ob_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_rb_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_ineterest_total}</b> </th>
                                <th style="text-align: center">
                                    <b>${list.close_balnce_ub_total_total}</b> </th>


                            </tr>
                        </tfoot>
                    </table>
                </div>

            </logic:present>

 </div>
                                        </div>    
                                    </div>
                                    <!-- End row --> 
                                </div>
                                <!-- End Container --> 
                            </section>



                        </html:form>
                        </body>
                        </html>
