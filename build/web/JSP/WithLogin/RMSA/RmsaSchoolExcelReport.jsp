<%-- 
    Document   : RmsaMasterReport
    Created on : June 06, 2017, 11:31:47 AM
    Author     : 1250892
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
%>
<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">   
        <link rel="stylesheet" type="text/css" href="<%= basePath%>/js/css/css.css">
        <title>JSP Page</title>
        <style type="text/css">
            table .altrowstable tr:nth-child(odd)
            {
                background:#E2E4FF;
            }
            table .altrowstable tr:nth-child(even)
            {
                background:#FFF;
            }
            .num_height th {
                height: 20px !important;
                font-size: 8px !important;
            }
        </style>
        <link rel="stylesheet" href="<%= basePath%>table_bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="<%= basePath%>table_bootstrap/bootstrap-theme.min.css">
        <script src="https://code.jquery.com/jquery.js"></script>
        <script src="<%= basePath%>table_bootstrap/bootstrap.min.js"></script>
        <script src="<%= basePath%>table_bootstrap/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="<%= basePath%>table_bootstrap/jquery.dataTables.min.css" />
        <script type="text/javascript">
            $(document).ready(function() {
                var table = $('#demo_datatables').DataTable();

                $("#Back").button().click(function() {
                   // window.history.back();
                    document.forms[0].mode.value = "getRmsaDistWiseReport";
                    document.forms[0].submit();
                });

            });</script>
        <style type="text/css">
            #demo_datatables_info {
                padding: 10px 15px;
                font-size: 12px;
                font-weight: bold;

            }
            #demo_datatables_paginate{
                padding: 10px 15px;
                font-size: 15px;
                font-weight: bold;

            }
            #demo_datatables_length {
                padding: 10px 15px;
                font-size: 12px;
                font-weight: bold;

            }
            #demo_datatables_filter {
                padding: 10px 15px;
                font-size: 12px;
                font-weight: bold;

            }
            .table {
                width: 0 !important;
            }
            .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
                border-top: 1px solid #000 !important;
            }
        </style>
        <style type="text/css">
            table.altrowstable1 th {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                text-align: center !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 td {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 {
                border-right: 1px #000000 solid !important;
                border-top: 1px #000000 solid !important;
            }
            table.altrowstable1 thead th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            table.altrowstable1 tbody th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            input { 
                padding: 2px 5px;
                margin: 5px 0px;

            }
        </style>

    </head>
    <script>
        function goBack() {
            window.history.back();
        }

    </script>


    <body>
        <html:form action="/rmsaMaster">
            <html:hidden property="mode" name="status" value="displayDistricts"/>
            <br/>
            <!--<h3> GIS Coordinates</h3>--> 
            <h5 style="font-weight: bold;text-align: center;color: #ffffff;background: #fd8760;padding: 7px;margin: 10px;box-shadow: 10px 7px 10px #D4D4D4;text-transform: uppercase; margin-bottom: 30px;">Rashtriya Madhyamik Shiksha Abhiyan (RMSA) </h5>

            <logic:present name="msg">
            <center> <font color="red" align="center">No Records Available</font></center>
            </logic:present> 

        </br>
        <logic:present name="masterList">
            <% String phaseNo1=request.getAttribute("phaseNo").toString(); 
             request.setAttribute("phaseNo", phaseNo1);
            %>
            <table align="center"  border="0" cellpadding="0" cellspacing="0" width="95%">
                <tr><td align="right">
                        <input type="button" name="Back" id="Back" value="Back" style="width: 100px;height: 30px"/>  <a href="./rmsaMaster.xls?mode=getRmsaDistWiseReport&type=report&phaseNo=<%=request.getAttribute("phaseNo")%>" target="_blank"><img src="<%=basePath%>images/excel.jpg"  height="20px" width="25px"/></a>
                        <input type="hidden" name="phaseNo" id="phaseNo" value="<%=phaseNo1%>" >
                    </td> </tr>
            </table>

            <table align="center"  border="0" cellpadding="0" cellspacing="1" width="98%"  id="demo_datatables" class="altrowstable">                   
                <thead>
                    <tr>
                        <th>Sl.No </th>
                        <th>School Code </th>
                        <th>School Name </th>
                        <th>Total Sanctioned (GOI)</th>
                        <th>Amount Estimated</th>
                        <th>Amount Released</th>
                        <th>Amount yet to be Released</th>
                        <th>Expenditure Incurred</th>
                        <th>Ucs Submitted</th>
                           <th>Opening Balance</th>
                        <th>Closing  Balance</th>

                    </tr>
                </thead> 
                <tbody>

                    <logic:iterate id="list" name="masterList" >
                        <tr>
                            <td width="20px" style="text-align: center"><%=i++%></td>
                            <td style="text-align: center">
                                ${list.schoolId}
                            </td> 
                            <td style="text-align: left">
                                ${list.schoolName}
                            </td>
                            <td style="text-align: center">
                                ${list.totalSanction}
                            </td>
                            <td style="text-align: center">
                                ${list.amountEstimated}
                            </td>
                            <td style="text-align: center">
                                ${list.amountReleased}
                            </td>
                            <td style="text-align: center">
                                ${list.amountYetToBeRelease}
                            </td>
                            <td style="text-align: center">
                                ${list.expenditureIncurred}
                            </td>
                            <td style="text-align: center">
                                ${list.ucsSubmitted}
                            </td>
                              <td style="text-align: center">
                                ${list.openingBal}
                            </td>
                            <td style="text-align: center">
                                ${list.closingBal}
					</td>
                            
                        </tr>
                    </logic:iterate>
                </tbody>
            
                    
                    <tr>
                        <th colspan="3" style="text-align: center">
                            Total
                        </th>
                        <th style="text-align: center">
                            <b> ${totalDistSanction} </b>
                        </th>
                        <th style="text-align: center">
                            <b> ${totalDistamountEstimated} </b>
                        </th>
                        <th style="text-align: center">
                            <b> ${totalDistamountReleased} </b>
                        </th>
                        <th style="text-align: center">
                            <b> ${totalDistamountYetToBeRelease} </b>
                        </th>
                        <th style="text-align: center">
                            <b> ${totalDistexpenditureIncurred} </b>
                        </th>
                        <th style="text-align: center">
                            <b> ${totalDistucsSubmitted} </b>
                        </th>
                         <th style="text-align: center">
                            <b> ${totalOpeningBal} </b>
                        </th>
                        <th style="text-align: center">
                            <b> ${totalClosingBal} </b>
                        </th>
                        
                    </tr>
                
            </table>
                     
        </logic:present>
    </html:form>
</body>
</html>
