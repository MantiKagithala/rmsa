<%-- 
    Document   : RmsaMasterReport
    Created on : June 06, 2017, 11:31:47 AM
    Author     : 1250892
--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
%>
<html>
    <head>

        <title>JSP Page</title>


        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap.min.css">
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap1/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery-1.12.4.js"/>
    </script>
    <script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/jquery.dataTables.min.js"/>
</script>
<script type="text/javascript" language="javascript" src="<%=basePath%>table_bootstrap1/demo.js"/>
</script>


<style type="text/css">
    table.dataTable.nowrap td {
        border-bottom: 1px #083254 solid;
        border-left: 1px #083254 solid;
        vertical-align: middle;
        padding-left: 3px;
        padding-right: 3px;

        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
    }

    table.dataTable.nowrap th {
        background-color: #0E94C5;
        text-align: center;
        border-bottom: 1px #000 solid;
        padding-left: 3px;
        padding-right: 3px;
        border-left: 1px #000 solid;
        vertical-align: left;
        font-size: 11px;
        font-family: verdana;

        font-weight: bold;
        color: #fff;
        padding: 5px;
    }

    table.dataTable.display tbody tr.odd {
        background: #E2E4FF !important;
    }
    table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
        background: #E2E4FF !important;
    }
    .dataTables_wrapper {

        padding: 10px !important;
    }
    .dataTables_length label {
        font-size: 12px !important;
    }
    .dataTables_filter label {
        font-size: 12px !important;
    }
    .dataTables_info {
        font-size: 12px !important;
    }
    .dataTables_paginate {
        font-size: 12px !important;
    }
    form input {
        margin-bottom: 5px;
    }
    table.dataTable.nowrap th, table.dataTable.nowrap td {
        white-space: initial !important;
    }
    .well {
        overflow: hidden;
    }
    .well-lg {
        padding: 10px !important;
    }
    form input {
        padding: 0px !important;
        border-radius: 0 !important;
        font-size: 12px;
        padding: 3px 5px !important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
        height: auto !important;
    }
    select {
        display: inline-block !important;
    }
    /* .dataTables_wrapper .dataTables_scroll {
        clear: both;
width: 1000px;
    } */

    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid #49A0E3 !important;
    }
    .table {
        border: 0 !important;
        border-radius: 0 !important;
    }
    input[type="button"] {
        width: 60px !important;
    }

</style>

<style type="text/css">
    .districts_main {
        width:1000px;
        margin:0px auto;
        text-align: center;
    }
    .distirct1 {
        width:250px;
        float:left;
        margin:10px 0;
    }
    .distirct2 {
        width:230px;
        float:left;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;

        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;

        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;

        font-size: 16px;
        text-align: right;
        font-weight: bold;

    }
    @-moz-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @-webkit-keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
    @keyframes blinker {  
        0% { color: red; }
    50% { color: green; }
    100% { color: blue; }
    }
</style>
<style type="text/css">
    table.altrowstable1 th {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        text-align: center !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 td {
        border-bottom: 1px #000000 solid !important;
        border-left: 1px #000000 solid !important;
        font-size: 11px;
        font-family: verdana;
        font-weight: normal;
        height: 20px;
        padding: 5px;
    }
    table.altrowstable1 {
        border-right: 1px #000000 solid !important;
        border-top: 1px #000000 solid !important;
    }
    table.altrowstable1 thead th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    table.altrowstable1 tbody th {
        background-color: #b9dbff !important;
        color: #000 !important;
    }
    input { 
        padding: 2px 5px;
        margin: 5px 0px;

    }
</style>
</head>

<script>
    function goBack() {
        window.history.back();
    }

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 300,
            "scrollX": true
        });
        var table = $('#demo_datatables').DataTable();

        $("#Back").button().click(function() {
            // window.history.back();
            document.forms[0].mode.value = "getRmsaDistWiseReport";
            document.forms[0].submit();
        });

    });</script>


<body>
    <html:form action="/rmsaMaster">
        <html:hidden property="mode" name="status" value="displayDistricts"/>
        <br/>
        <!--<h3> GIS Coordinates</h3>--> 
        <!--<h5 style="font-weight: bold;text-align: center;color: #ffffff;background: #fd8760;padding: 7px;margin: 10px;box-shadow: 10px 7px 10px #D4D4D4;text-transform: uppercase; margin-bottom: 30px;">Rashtriya Madhyamik Shiksha Abhiyan (RMSA) </h5>-->
        <section class="testimonial_sec clear_fix">
            <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">

                        <div class="innerbodyrow">
                            <div class="col-xs-12">
                                <h3> Rashtriya Madhyamik Shiksha Abhiyan (RMSA) </h3>
                            </div>
                            <logic:present name="msg">
                                <center> <font color="red" align="center">No Records Available</font></center>
                                </logic:present> 

                            </br>
                            <logic:present name="masterList">
                                <% String phaseNo1 = request.getAttribute("phaseNo").toString();
                                    request.setAttribute("phaseNo", phaseNo1);
                                %>
                                <table align="center"  border="0" cellpadding="0" cellspacing="0" width="95%">
                                    <tr><td align="right">
                                          <%--  <input type="button" name="Back" id="Back" value="Back" style="width: 100px;height: 30px"/> --%>
                                            <a href="./rmsaMaster.do?mode=getRmsaDistWiseReport&phaseNo=<%=request.getAttribute("phaseNo")%>" >Back</a>
                                            <a href="./rmsaMaster.xls?mode=getRmsaDistSchoolReport&type=report&distId=<%=request.getAttribute("distId")%>&phaseNo=<%=request.getAttribute("phaseNo")%>" ><img src="<%=basePath%>images/excel.jpg"  height="20px" width="25px"/></a>
                                            <input type="hidden" name="phaseNo" id="phaseNo" value="<%=phaseNo1%>" >
                                        </td> </tr>
                                </table>
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <!--<table align="center"  border="0" cellpadding="0" cellspacing="1" width="98%"  id="demo_datatables" class="altrowstable">-->                   
                                    <thead>
                                        <tr>
                                            <th>Sl.No </th>
                                            <th>School Code </th>
                                            <th>School Name </th>
                                            <th>Total Sanctioned (GOI)</th>
                                            <th>Amount Estimated</th>
                                            <th>Amount Released</th>
                                            <th>Amount yet to be Released</th>
                                            <th>Expenditure Incurred</th>
                                            <th>Ucs Submitted</th>
                                            <th>Opening Balance</th>
                                            <th>Closing  Balance</th>

                                        </tr>
                                    </thead> 
                                    <tbody>

                                        <logic:iterate id="list" name="masterList" >
                                            <tr>
                                                <td width="20px" style="text-align: center"><%=i++%></td>
                                                <td style="text-align: center">
                                                    ${list.schoolId}
                                                </td> 
                                                <td style="text-align: left">
                                                    ${list.schoolName}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.totalSanction}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountEstimated}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountReleased}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.amountYetToBeRelease}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.expenditureIncurred}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.ucsSubmitted}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.openingBal}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.closingBal}
                                                </td>

                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                    <tfoot>

                                        <tr>
                                            <th colspan="3" style="text-align: center">
                                                Total
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalDistSanction} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalDistamountEstimated} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalDistamountReleased} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalDistamountYetToBeRelease} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalDistexpenditureIncurred} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalDistucsSubmitted} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalOpeningBal} </b>
                                            </th>
                                            <th style="text-align: center">
                                                <b> ${totalClosingBal} </b>
                                            </th>

                                        </tr>
                                    </tfoot>
                                </table>

                            </logic:present>

                        </div>
                    </div>    
                </div>
                <!-- End row --> 
            </div>
            <!-- End Container --> 
        </section>

    </html:form>
</body>
</html>
