<%-- 
    Document   : newjspVCHMConfirmation
    Created on : Feb 20, 2018, 2:49:32 PM
    Author     : 1250881
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RMSA</title> <link rel="stylesheet" type="text/css" href="<%=basePath%>table_bootstrap1/site-examples.css">
     <link rel="stylesheet"  media="print,handheld" href="<%=basePath%>Styles/CSS/printReport.css"/>
  <script>
            function printPage()
            {
                window.print()
            }

        </script>
    </head>
    <body onload="printPage()" style="background:#fff !important;">
  <div>
            <table class="h3_text" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr class="vendorListHeading">

                    <th style="text-align: left;">
                        <img src="<%=basePath%>Styles/images/aplogo.png"  height="80px" width="70px" style="float: right; "/>
                    </th>

                    <th style="text-align: center;">
                <h3>Vocational HM Confirmation 
                </h3>
                </th>

                <th style="text-align: right;">
                    <img src="<%=basePath%>Styles/images/scrt.png"  height="80px" width="70px" style="float: right;"/>


                </th>
                </tr>
            </table>
        </div>
    
        <section class="testimonial_sec clear_fix">
            <div class="container">
                <div class="row">
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                        <div class="col-xs-12">
                            <h3> Vocational HM Confirmation </h3>
                        </div>
                       <logic:present name="vclist">
                            <div style="overflow-x: scroll; width: 1070px; margin: 0 20px">
                                <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="darkgrey" >
                                            <th  rowspan="2"  style="text-align: center">Sl.No</th>
                                            <th  rowspan="2"  style="text-align: center">VC Trainer</th>
                                            <th  rowspan="2"  style="text-align: center">DateOfBirth</th>
                                            <th  rowspan="2"  style="text-align: center">Aadhar</th>
                                            <th  rowspan="2"  style="text-align: center">Mobile</th>
                                            <th  rowspan="2"  style="text-align: center">email Id</th>
                                            <th  rowspan="2"  style="text-align: center">Sector</th>
                                            <th  rowspan="2"  style="text-align: center">Trade</th>
                                            <th  colspan="10" style="text-align: center">QAnswer</th>
                                            <th  rowspan="2"  style="text-align: center">Remarks</th>
                                            <th  rowspan="2"  style="text-align: center">HMRemarks</th>
                                            <!--<th  rowspan="2"  style="text-align: center">Generate Certificate</th>-->

                                        </tr>
                                        <tr class="darkgrey" >
                                            <th style="text-align: center">Q.1</th> 
                                            <th style="text-align: center">Q.2</th>
                                            <th style="text-align: center">Q.3</th>
                                            <th style="text-align: center">Q.4</th>
                                            <th style="text-align: center">Q.5</th>
                                            <th style="text-align: center">Q.6</th> 
                                            <th style="text-align: center">Q.7</th>
                                            <th style="text-align: center">Q.8</th>
                                            <th style="text-align: center">Q.9</th>
                                            <th style="text-align: center">Q.10</th>
                                        </tr>
                                        <tr class="darkgrey" >
                                            <th style="text-align: center">1</th> 
                                            <th style="text-align: center">2</th>
                                            <th style="text-align: center">3</th>
                                            <th style="text-align: center">4</th>
                                            <th style="text-align: center">5</th>
                                            <th style="text-align: center">6</th> 
                                            <th style="text-align: center">7</th>
                                            <th style="text-align: center">8</th>
                                            <th style="text-align: center">9</th>
                                            <th style="text-align: center">10</th>
                                            <th style="text-align: center">11</th>
                                            <th style="text-align: center">12</th>
                                            <th style="text-align: center">13</th> 
                                            <th style="text-align: center">14</th>
                                            <th style="text-align: center">15</th>
                                            <th style="text-align: center">16</th> 
                                            <th style="text-align: center">17</th>
                                            <th style="text-align: center">18</th> 
                                            <th style="text-align: center">19</th>
                                            <th style="text-align: center">20</th> 
                                            <!--<th style="text-align: center">21</th>-->

                                        </tr>
                                    </thead>
                                    <logic:iterate name="vclist" id="list">
                                        <tr>
                                            <td style="text-align: center"><%=i++%></td>  
                                            <td style="text-align: center;"> ${list.vctrainerName}</td>
                                            <td style="text-align: center;"> ${list.dob}</td>
                                            <td style="text-align: center;"> ${list.aadharno}</td>
                                            <td style="text-align: center;"> ${list.mobileno}</td>
                                            <td style="text-align: center;"> ${list.emailId}</td>
                                            <td style="text-align: center;"> ${list.sectorName}</td>
                                            <td style="text-align: center;"> ${list.tradeName}</td>
                                            <td style="text-align: center;"> ${list.firstQAns}</td>
                                            <td style="text-align: center;"> ${list.secondQAns}</td>
                                            <td style="text-align: center;"> ${list.thirdQAns}</td>
                                            <td style="text-align: center;"> ${list.fourthQAns}</td>
                                            <td style="text-align: center;"> ${list.fifthQAns}</td>
                                            <td style="text-align: center;"> ${list.sixthQAns}</td>
                                            <td style="text-align: center;"> ${list.seventhQAns}</td>
                                            <td style="text-align: center;"> ${list.eightQAns}</td>
                                            <td style="text-align: center;"> ${list.ninethQAns}</td>
                                            <td style="text-align: center;"> ${list.tenthQAns}</td>
                                            <td style="text-align: center;"> ${list.remarks}</td>
                                            <td style="text-align: center;"> ${list.hmRemark}</td>
                                        </tr>
                                    </logic:iterate>


                                </table>
                            </div>
                        </logic:present>

 </div>
                </div>
            </div>
        </section>  
</body>
</html>

