
<%@page import="com.aponline.rmsa.WithLogin.Form.RMSAForm"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<%
    int j = 1;
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String photo = null;
    if (request.getAttribute("RMSAChequeDetails") != null) {
        ArrayList list = (ArrayList) request.getAttribute("RMSAChequeDetails");
        for (int i = 0; i < list.size(); i++) {
            photo = "kpiinsert(ucsFileUpload" + i + ")";
        }
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RMSA</title>
        <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet" type="text/css" />
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script type="text/javascript" src="table_bootstrap/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<%=basePath%>table_bootstrap/bootstrap.min.css" type="text/css" />
        <script>

            $(document).ready(function() {
                $('#myModal').hide();
                $("#myBtn").click(function() {
                    $('#myModal').show();
                    var form = document.forms[0];
                    dataString = $(form).serialize();
                });
            });

            window.onload = function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null) {
                        document.getElementById("msg").style.display = "none";
                    }
                    if (document.getElementById("result") !== null) {
                        document.getElementById("result").style.display = "none";
                    }
                }, seconds * 1000);
                if ($("#artCraftSanction").val() === '0') {

                    document.getElementById("artCraftStageOfProgress").style.display = "none";
                    document.getElementById("showArt").style.display = "none";
                    document.getElementById("showNa").style.display = '';
                    // $('#artCraftStageOfProgress').attr('disabled', true);
                } else if ($("#artCraftSanction").val() > 0) {
                    document.getElementById("showArt").style.display = '';
                    document.getElementById("showNa").style.display = "none";
                }
                if ($("#computerRoomSanction").val() === '0') {
                    document.getElementById("computerRoomStageOfProgress").style.display = "none";
                    document.getElementById("showC").style.display = "none";
                    document.getElementById("showNaC").style.display = '';
//                    $('#computerRoomStageOfProgress').attr('disabled', true);
                } else if ($("#computerRoomSanction").val() > 0) {
                    document.getElementById("showC").style.display = '';
                    document.getElementById("showNaC").style.display = "none";
                }
                if ($("#scienceLabSanction").val() === '0') {
                    document.getElementById("scienceLabStageOfProgress").style.display = "none";
                    document.getElementById("showSci").style.display = "none";
                    document.getElementById("showNaSci").style.display = '';
//                    $('#scienceLabStageOfProgress').attr('disabled', true);
                } else if ($("#scienceLabSanction").val() > 0) {
                    document.getElementById("showSci").style.display = '';
                    document.getElementById("showNaSci").style.display = "none";
                }
                if ($("#libraryRoomSanction").val() === '0') {
                    document.getElementById("libraryRoomStageOfProgress").style.display = "none";
                    document.getElementById("showLib").style.display = "none";
                    document.getElementById("showNaLib").style.display = '';

//                    $('#libraryRoomStageOfProgress').attr('disabled', true);
                } else if ($("#libraryRoomSanction").val() > 0) {
                    document.getElementById("showLib").style.display = '';
                    document.getElementById("showNaLib").style.display = "none";
                }
                if ($("#additionalClassRoomSanction").val() === '0') {
                    document.getElementById("additionalClassRoomStageOfProgress").style.display = "none";
                    document.getElementById("showAD").style.display = "none";
                    document.getElementById("showNaAD").style.display = '';

//                    $('#additionalClassRoomStageOfProgress').attr('disabled', true);
                } else if ($("#additionalClassRoomSanction").val() > 0) {
                    document.getElementById("showAD").style.display = '';
                    document.getElementById("showNaAD").style.display = "none";
                }
                if ($("#toiletBlockSanction").val() === '0') {
                    document.getElementById("toiletBlockStageOfProgress").style.display = "none";
                    document.getElementById("showToi").style.display = "none";
                    document.getElementById("showNaToi").style.display = '';

//                    $('#toiletBlockStageOfProgress').attr('disabled', true);
                } else if ($("#toiletBlockSanction").val() > 0) {
                    document.getElementById("showToi").style.display = '';
                    document.getElementById("showNaToi").style.display = "none";
                }
                if ($("#drinkingWaterSanction").val() === '0') {
                    document.getElementById("drinkingWaterStageOfProgress").style.display = "none";
                    document.getElementById("showDri").style.display = "none";
                    document.getElementById("showNaDri").style.display = '';

//                    $('#drinkingWaterStageOfProgress').attr('disabled', true);
                } else if ($("#drinkingWaterSanction").val() > 0) {
                    document.getElementById("showDri").style.display = '';
                    document.getElementById("showNaDri").style.display = "none";
                }
            };

            $(function() {
                $(document).bind("contextmenu", function(e) {
                    return false;
                });
                $(document).keydown(function(e) {
                    return (e.which || e.keyCode) !== 116;
                });
                //F12 block
                $(document).keydown(function(e) {
                    return (e.which || e.keyCode) !== 123;
                });

                $("#Search").button().click(function() {
                    var phaseNo = $('#phaseNumber').val();
                    if (phaseNo === "0") {
                        alert("Select Phase");
                        $("#phaseNumber").focus().css({'border': '1px solid red'});
                    }
                    else {
                        document.forms[0].mode.value = "getData";
                        document.forms[0].submit();
                    }
                });
                $('#phaseNumber').on('change', function() {
                    $("#mainDiv").hide();
                });
            });

            function submitForm() {
                if ($("#artCraftSanction").val() === undefined || $("#artCraftSanction").val() === "") {
                    alert("Art/Craft room sanction should have value");
                    $("#artCraftSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#artCraftSanction").val() !== '0') {
                    if ($("#artCraftStageOfProgress").val() === 'Select') {
                        alert("Please Select Art/Craft room");
                        $("#artCraftStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }

                if ($("#computerRoomSanction").val() === undefined || $("#computerRoomSanction").val() === "") {
                    alert("Computer Room sanction should have value");
                    $("#computerRoomSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#computerRoomSanction").val() !== '0') {
                    if ($("#computerRoomStageOfProgress").val() === 'Select') {
                        alert("Please Select Computer Room");
                        $("#computerRoomStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
                if ($("#scienceLabSanction").val() === undefined || $("#scienceLabSanction").val() === "") {
                    alert("Science Lab sanction should have value");
                    $("#scienceLabSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#scienceLabSanction").val() !== '0') {
                    if ($("#scienceLabStageOfProgress").val() === 'Select') {
                        alert("Please Select Science Lab");
                        $("#scienceLabStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
                if ($("#libraryRoomSanction").val() === undefined || $("#libraryRoomSanction").val() === "") {
                    alert("library Room sanction should have value");
                    $("#libraryRoomSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#libraryRoomSanction").val() !== '0') {
                    if ($("#libraryRoomStageOfProgress").val() === 'Select') {
                        alert("Please Select Library Room");
                        $("#libraryRoomStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
                if ($("#additionalClassRoomSanction").val() === undefined || $("#additionalClassRoomSanction").val() === "") {
                    alert("Additional class room sanction should have value");
                    $("#additionalClassRoomSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#additionalClassRoomSanction").val() !== '0') {
                    if ($("#additionalClassRoomStageOfProgress").val() === 'Select') {
                        alert("Please Select Additional Class Room");
                        $("#additionalClassRoomStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
                if ($("#toiletBlockSanction").val() === undefined || $("#toiletBlockSanction").val() === "") {
                    alert("Toilet block sanction should have value");
                    $("#toiletBlockSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#toiletBlockSanction").val() !== '0') {
                    if ($("#toiletBlockStageOfProgress").val() === 'Select') {
                        alert("Please Select Toilet Block");
                        $("#toiletBlockStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
                if ($("#drinkingWaterSanction").val() === undefined || $("#drinkingWaterSanction").val() === "") {
                    alert("Drinking water Sanction should have value");
                    $("#drinkingWaterSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#drinkingWaterSanction").val() !== '0') {
                    if ($("#drinkingWaterStageOfProgress").val() === 'Select') {
                        alert("Please Select Drinking Water");
                        $("#drinkingWaterStageOfProgress").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
                if ($("#administrativeSanction").val() === undefined || $("#administrativeSanction").val() === "") {
                    alert("Administrative sanction should have value");
                    $("#administrativeSanction").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#totalFurniture").val() === undefined | $("#totalFurniture").val() === "") {
                    alert("Sanctioned For Furniture should have value");
                    $("#totalFurniture").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#furnitureAndLabEquipment").val() === undefined || $("#furnitureAndLabEquipment").val() === "") {
                    alert("Total Furniture for LabEquipment have value");
                    $("#furnitureAndLabEquipment").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#totalSanctioned").val() === undefined || $("#totalSanctioned").val() === "") {
                    alert("Total Sanction should have value");
                    $("#totalSanctioned").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#estimatedCost").val() === undefined || $("#estimatedCost").val() === "") {
                    alert("Estimated Cost should have value");
                    $("#estimatedCost").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#tenderCostValue").val() === undefined || $("#tenderCostValue").val() === "") {
                    alert("Tender should have value");
                    $("#tenderCostValue").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#vat").val() === undefined || $("#vat").val() === "") {
                    alert("Vat should have value");
                    $("#vat").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#departemntCharges").val() === undefined || $("#departemntCharges").val() === "") {
                    alert("Dept.Charges should have value");
                    $("#departemntCharges").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#totalEligibleCost").val() === undefined || $("#totalEligibleCost").val() === "") {
                    alert("Total Eligible Cost should have value");
                    $("#totalEligibleCost").focus().css({'border': '1px solid red'});
                    return false;
                }

                if ($("#releases").val() === undefined || $("#releases").val() === "") {
                    alert("Total Released should have value");
                    $("#releases").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#expenditureIncurred").val() === undefined || $("#expenditureIncurred").val() === "" || $("#expenditureIncurred").val() === "0") {
                    alert("Expenditure Incurred should have value");
                    $("#expenditureIncurred").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#balanceToBeReleased").val() === undefined || $("#balanceToBeReleased").val() === "") {
                    alert("balance To Be Released should have value");
                    $("#balanceToBeReleased").focus().css({'border': '1px solid red'});
                    return false;
                }

                if ($("#ucsUpload").val() === '') {
                    alert("Please Choose File");
                    $("#ucsUpload").focus().css({'border': '1px solid red'});
                    return false;
                }
                var totalcheckAmt = 0;
                //  alert(doValidateCheque());
                if (doValidateCheque()) {
                    alert("Please Remove Duplicate Cheque Files");
                    return false;
                }
                if (!doUploadFilesValidation()) {
                    return false;
                }
                else if (!doBillsValidation()) {
                    return false;
                } else {
                    var releasesAmount = document.getElementById("releases").value;
//                    alert(releasesAmount);
                    var table = document.getElementById("tableID");
                    var rowCount = table.rows.length;
                    for (var i = 0; i < rowCount; i++) {
                        var rowId = "ChequeAmount" + i;
                        var element = document.getElementById(rowId);
                        if (typeof element === "undefined") {
                        }
                        else if (element === null) {
                        }
                        else {
                            var checkAmt = element.value;
                            totalcheckAmt += parseFloat(checkAmt);
                        }

                    }
                    document.forms[0].ucsFilesCount.value = '<%=photo%>';
                    if (releasesAmount >= totalcheckAmt) {
                        document.forms[0].phaseNo.value = document.getElementById("phaseNo").value;
                        document.forms[0].artCraftStageOfProgress1.value = document.forms[0].elements['artCraftStageOfProgress'].value;
                        //alert("artCraftStageOfProgress" + document.forms[0].elements['artCraftStageOfProgress'].value);
                        document.forms[0].computerRoomStageOfProgress1.value = document.forms[0].elements['computerRoomStageOfProgress'].value;
                        //alert("computerRoomStageOfProgress" + document.forms[0].elements['artCraftStageOfProgress1'].value);
                        document.forms[0].scienceLabStageOfProgress1.value = document.forms[0].elements['scienceLabStageOfProgress'].value;
                        //alert("scienceLabSanction" + document.forms[0].elements['scienceLabStageOfProgress1'].value);
                        document.forms[0].libraryRoomStageOfProgress1.value = document.forms[0].elements['libraryRoomStageOfProgress'].value;
                        //alert("libraryRoomStageOfProgress " + document.forms[0].elements['libraryRoomStageOfProgress1'].value);
                        document.forms[0].additionalClassRoomStageOfProgress1.value = document.forms[0].elements['additionalClassRoomStageOfProgress'].value;
                        //alert("additionalClassRoomSanction " + document.forms[0].elements['additionalClassRoomStageOfProgress1'].value);
                        document.forms[0].toiletBlockStageOfProgress1.value = document.forms[0].elements['toiletBlockStageOfProgress'].value;
                        //alert("toiletBlockSanction " + document.forms[0].elements['toiletBlockStageOfProgress1'].value);
                        document.forms[0].drinkingWaterStageOfProgress1.value = document.forms[0].elements['drinkingWaterStageOfProgress'].value;
                        //alert("drinkingWaterSanction " + document.forms[0].elements['drinkingWaterStageOfProgress1'].value);
                        document.forms[0].mode.value = "RmsaSubmit";
                        document.forms[0].submit();
                    } else {
                        alert("Total Cheques Amount Should Be Less Than Released Amount");
                        return false;
                    }
                }
            }
            function validateCheckAmount() {
                var totalcheckAmt = 0;
                var releasesAmount = document.getElementById("releases").value;
                var table = document.getElementById("tableID");
                var rowCount = table.rows.length;
                for (var i = 0; i < rowCount; i++) {
                    var rowId = "ChequeAmount" + i;
                    var element = document.getElementById(rowId);
                    if (typeof element === "undefined") {
                    }
                    else if (element === null) {
                    }
                    else {
                        var checkAmt = element.value;
                        totalcheckAmt += parseFloat(checkAmt);
                    }

                }
                if (releasesAmount < totalcheckAmt) {
                    alert("Total Cheques Amount Should Be Less Than Released Amount");
                    document.getElementById("ChequeNumber").focus();
                }
                return false;
            }
            function downloadFile(ucsFileName, schCode) {
                document.forms[0].phaseNo.value = document.getElementById("phaseNo").value;
                document.forms[0].mode.value = "downloadFile";
                document.forms[0].userName.value = schCode;
                document.forms[0].ucsSubmitted.value = ucsFileName;
                document.forms[0].submit();
            }
            function doUploadFilesValidation() {

                var table = document.getElementById("tableID4");
                var isValid = true;
                var rowCount = table.rows.length;
                if (rowCount > 1) {
                    for (var i = 1; i < rowCount; i++) {

                        if (document.getElementById("UCSAmount" + i) && (document.getElementById("UCSAmount" + i).value === null || document.getElementById("UCSAmount" + i).value === '')) {
                            alert("Please Enter UCS Amount");
                            document.getElementById("UCSAmount" + i).focus();
                            isValid = false;
                        } else if (document.getElementById("ucsFileUpload" + i) && (document.getElementById("ucsFileUpload" + i).value === null || document.getElementById("ucsFileUpload" + i).value === '')) {
                            alert("Please Select UCS file to Upload");
                            document.getElementById("ucsFileUpload" + i).focus();
                            isValid = false;
                        }
                        if (!isValid)
                            break;
                    }
                }
                if (isValid) {
                    return true;
                }
                else {
                    return false;
                }

            }
            function doValidateCheque() {
                var cheque = new Array();
                var table = document.getElementById("tableID");
                var rowCount = table.rows.length;
                if (rowCount > 1) {
                    for (var i = 0; i < rowCount; i++) {
                        if (document.getElementById("ChequeNumber" + i) !== null) {
                            var chequeNo = document.getElementById("ChequeNumber" + i).value;
//                                   alert(chequeNo);
                            cheque.push(chequeNo);
                        }
                    }

                }
                var val = hasDuplicates(cheque);
                cheque = null;
                return val;
            }
            function hasDuplicates(array) {
                // alert(array);
                for (var i = 0; i < array.length; i++)
                {
                    for (var j = 0; j < array.length; j++)
                    {
                        if (i !== j)
                        {
                            if (array[i] === array[j])
                            {
                                return true; // means there are duplicate values
                            }
                        }
                    }
                }
                return false; // means there are no duplicate values.
            }
            function doBillsValidation() {

                var table = document.getElementById("tableID");
                var isValid = true;
                var rowCount = table.rows.length;
                if (rowCount > 1) {
                    for (var i = 1; i < rowCount; i++) {
                        if (document.getElementById("ChequeNumber" + i) && (document.getElementById("ChequeNumber" + i).value == null || document.getElementById("ChequeNumber" + i).value == '')) {
                            alert("Please Enter Cheque Number");
                            document.getElementById("ChequeNumber" + i).focus();
                            isValid = false;
                        } else if (document.getElementById("ChequeDate" + i) && (document.getElementById("ChequeDate" + i).value == null || document.getElementById("ChequeDate" + i).value == '')) {
                            alert("Please Enter Cheque Date");
                            document.getElementById("ChequeDate" + i).focus();
                            isValid = false;
                        } else if (document.getElementById("ChequeAmount" + i) && (document.getElementById("ChequeAmount" + i).value == null || document.getElementById("ChequeAmount" + i).value == '')) {
                            alert("Please Enter Cheque Amount");
                            document.getElementById("ChequeAmount" + i).focus();
                            isValid = false;
                        }
                        if (!isValid)
                            break;
                    }
                }
            <%--  else {
                alert("Add atleast one Cheque details");
                isValid = false;
            } --%>

                    if (isValid) {
                        return true;
                    }
                    else {
                        return false;
                    }

                }
                function Checkfiles(tableId, val, myval) {
                    var fup = document.getElementById(myval);
                    var fileName = fup.value;
                    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
                    if (ext === "zip" || ext === "ZIP"){
                        var file = fup.files[0];
                        var size = file.size / 1024;
                        if (size > 3 && size < 10240) {
                            return true;
                        } else {
                            alert("File size should be greater than 3KB and less than 10240KB only");
                            fup.focus();
                            $("#" + myval).val('').clone(true);
                            return false;
                        }
                    }
                    else{
                        alert("Upload ZIP only");
                        fup.focus();
                        $("#" + myval).val('').clone(true);
                        return false;
                    }
                }
                function addUCSRow(tableID) {
                    var table = document.getElementById(tableID);
                    var rowCount = table.rows.length;
                    document.forms[0].noofRec.value = rowCount;
                    var row = table.insertRow(rowCount);
                    for (var i = 0; i < 3; i++) {
                        var newcell = row.insertCell(i);
                        newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                        if (i == 0) {
                            var na = 'UCSAmount' + rowCount;
                            newcell.innerHTML = "<input type='text' name='UCSAmount' id='" + na + "'  placeholder='UCS Amount In Lakhs.' onkeydown='return space(event,this);' onkeypress='return onlyNumbers(event);' autocomplete='off'/>";
                        }
                        else if (i == 1) {
                            var na = 'ucsFileUpload' + rowCount;
                            var tt = 'return Checkfiles("tableID4", this,id)';
                            newcell.innerHTML = "<input type='file'  name='kpiinsert(ucsFileUpload" + i + ")' id='" + na + "' onchange='" + tt + "'/>";
                        }
                        else if (i == 2) {
                            var t = 'deleteRow("tableID4", this,id)';
                            newcell.innerHTML = "<input type='button' value='Delete' onclick='" + t + "'/>";
                        }
                    }
                }
                function addRow(tableID) {
                    document.getElementById("cheque").style.display = "";
                    var table = document.getElementById(tableID);
                    var rowCount = table.rows.length;
                    var row = table.insertRow(rowCount);
                    for (var i = 0; i < 4; i++) {
                        var newcell = row.insertCell(i);
                        newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                        if (i == 0) {
                            var na = 'ChequeNumber' + rowCount;
                            newcell.innerHTML = "<input type='text' name='ChequeNumber' id='" + na + "'  placeholder='Cheque Number' onkeydown='return space(event,this);' onkeypress='return onlyNumbers(event);' autocomplete='off' />";
                        }
                        else if (i == 1) {
                            //var t = 'testT(this);';
                            var na = 'ChequeDate' + rowCount;
                            newcell.innerHTML = "<input type='date' name='ChequeDate' id='" + na + "' onkeydown='return space(event,this);' onkeypress='return inputLimiter(event,'Numbers');'/>";
                        }
                        else if (i == 2) {
                            var ca = 'return validateCheckAmount()';
                            var na = 'ChequeAmount' + rowCount;
                            newcell.innerHTML = "<input type='text'   name='ChequeAmount' id='" + na + "' onchange='" + ca + "'  placeholder='Cheque Amount In Lakhs.' onkeydown='return space(event,this);'  onkeypress='return onlyNumbers(event);' autocomplete='off' />";
                        }
                        else if (i == 3) {
                            var t = 'deleteRow("tableID", this,id)';
                            newcell.innerHTML = "<input type='button' value='Delete' onclick='" + t + "'/>";
                        }

                    }
                    document.forms[0].noofRec.value = rowCount;
                }
                function deleteUcsFile(tableID, currentObj, schoolCode, sno, ucsAmount) {
                    var result = window.confirm('Are you sure you want to delete UCS file which contains amount ' + ucsAmount + " Lakh ?");
                    if (result === false) {
                        return false;
                    } else {
                        var ucstotalAmount = document.getElementById("ucstotalAmount").value;
                        var totalcheckAmt = parseFloat(ucstotalAmount) - parseFloat(ucsAmount);
                        deleteRow(tableID, currentObj);
                        deleteFromDB(schoolCode, sno);
                        var amount = totalcheckAmt.toFixed(2);
                        document.getElementById("ucstotalAmount").value = amount;

                    }
                }
                function deleteChequeFile(tableID, currentObj, schoolCode, sno, chequeNumber, chequeAmount) {
                    var result = window.confirm('Are you sure you want to delete cheque number ' + chequeNumber + ' which contains amount ' + chequeAmount + " Lakh ?");
                    if (result === false) {
                        return false;
                    } else {
                        var ctotalAmount = document.getElementById("totalchequeAmount").value;
                        var totalcheckAmt = parseFloat(ctotalAmount) - parseFloat(chequeAmount);
                        deleteRow(tableID, currentObj);
                        deleteFromCheque(schoolCode, sno);
                        var amount = totalcheckAmt.toFixed(2);
                        document.getElementById("totalchequeAmount").value = amount;

                    }
                }
                function deleteRow(tableID, currentRow) {
                    try {
                        var table = document.getElementById(tableID);
                        var rowCount = table.rows.length;
                        for (var i = 0; i < rowCount; i++) {
                            var row = table.rows[i];
                            if (row == currentRow.parentNode.parentNode) {
                                //  if (rowCount <= 2) {
                                //    alert("Cannot delete all the rows.");
                                //      break;
                                //   }
                                table.deleteRow(i);
                                rowCount--;
                                i--;
                            }
                            document.forms[0].noofRec.value = rowCount;
                        }

                    } catch (e) {
                        alert(e);
                    }

                }
                function deleteFromDB(schoolName, sno) {
                    var paraData = "schoolName=" + $.trim(schoolName) + "&sno=" + $.trim(sno);
                    $.ajax({
                        type: "POST",
                        url: "<%=basePath%>rmsaSchool.do?mode=deleteUcsFile",
                        data: paraData,
                        success: function(response) {
                            //  $("#component").empty().append(response.trim());

                        },
                        error: function(e) {
                            alert('Error: ' + e);
                        }
                    });
                }
                function deleteFromCheque(schoolName, sno) {
                    var paraData = "schoolName=" + $.trim(schoolName) + "&sno=" + $.trim(sno);
                    $.ajax({
                        type: "POST",
                        url: "<%=basePath%>rmsaSchool.do?mode=deleteCheque",
                        data: paraData,
                        success: function(response) {
                            //  $("#component").empty().append(response.trim());

                        },
                        error: function(e) {
                            alert('Error: ' + e);
                        }
                    });
                }
                function inputLimiter(e, allow) {
                    var AllowableCharacters = '';
                    if (allow == 'Letters') {
                        AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                    }
                    if (allow == 'Numbers') {
                        AllowableCharacters = '1234567890';
                    }
                    if (allow == 'landline') {
                        AllowableCharacters = '1234567890-';
                    }
                    if (allow == 'NameCharacters') {
                        AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                    }
                    if (allow == 'NameCharactersAndNumbers') {
                        AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                    }
                    if (allow == 'website') {
                        AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                    }

                    var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                    if (k != 13 && k != 8 && k != 0) {
                        if ((e.ctrlKey == false) && (e.altKey == false)) {
                            return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
                function onlyNumbers(evt) {
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    if (charCode === 46)
                        return true;
                    else if (charCode > 31 && (charCode < 48 || charCode > 57))
                        return false;
                    return true;
                }
                function space(evt, thisvalue) {
                    var number = thisvalue.value;
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    if (number.length < 1) {
                        if (evt.keyCode === 32) {
                            return false;
                        }
                    }
                    return true;
                }
        </script>
        <style type="text/css">
            table.altrowstable1 th {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                text-align: center !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 td {
                border-bottom: 1px #000000 solid !important;
                border-left: 1px #000000 solid !important;
                font-size: 11px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 5px;
            }
            table.altrowstable1 {
                border-right: 1px #000000 solid !important;
                border-top: 1px #000000 solid !important;
            }
            table.altrowstable1 thead th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            table.altrowstable1 tbody th {
                background-color: #b9dbff !important;
                color: #000 !important;
            }
            input { 
                padding: 2px 5px;
                margin: 5px 0px;
                border: 1px solid #ccc;


            }
            .table tbody td {
                background: transparent !important;
            }
        </style>
    </head>
    <body>
        <html:form action="/rmsaSchool" method="post" enctype="multipart/form-data" styleId="myform">
            <html:hidden property="mode"/>
            <html:hidden property="noofRec"/>
            <html:hidden property="userName"/>
            <html:hidden property="ucsSubmitted"/>
            <html:hidden property="phaseNo"/>
            <html:hidden property="ucsFilesCount" styleId="<%=photo%>"/>
            <html:hidden property="artCraftStageOfProgress1"/>
            <html:hidden property="scienceLabStageOfProgress1"/>
            <html:hidden property="additionalClassRoomStageOfProgress1"/>
            <html:hidden property="drinkingWaterStageOfProgress1"/>
            <html:hidden property="computerRoomStageOfProgress1" />
            <html:hidden property="libraryRoomStageOfProgress1"/>
            <html:hidden property="toiletBlockStageOfProgress1"/>
            <%  String msg = null;
                if (request.getAttribute("msg") != null) {
                    msg = request.getAttribute("msg").toString();%>
        <center><div id="msg"><span style="color:#0095ff !important"><b><%=msg%><b></span></div></center>
                            <%}%>
                            <section class="testimonial_sec clear_fix">
                                <div class="container">
                                    <div class="row">
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonial_container" style="background: #fff; border: 2px solid #f6ba18;">
                                            <div class="innerbodyrow">
                                                <div class="col-xs-12">
                                                    <h3> RMSA Expenditure</h3>
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <label class="formlabel">Phase :</label>
                                                    <html:select property="phaseNumber" styleId="phaseNumber" style="display: block; width: 100%; height: 34px;width 20%; padding: 6px 12px;  font-size: 14px;  line-height: 1.42857143;  color: #555;  background-color: #fff;  background-image: none;  border: 1px solid #ccc;  border-radius: 4px;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;">
                                                        <html:option value='0'>--Select--</html:option>
                                                        <html:optionsCollection property="phaseList" label="phaseName" value="phaseNumber"/>
                                                    </html:select> 
                                                </div>
                                                <div class="col-xs-12 col-sm-3">
                                                    <input class="btn btn-primary" style="margin-top: 22px;"  type='button' value='Search' id="Search"/>
                                                </div>
                                                <logic:present  name="RMSAMaster">
                                                    <div id="mainDiv">       
                                                        <%  String result = null;
                                                            if (request.getAttribute("result") != null) {
                                                                result = request.getAttribute("result").toString();%>
                                                        <center><div id="result"><span style="color:#0095ff !important"><b><%=result%><b></span></div></center><%}%>
                                                                            <%if (request.getAttribute("phaseNo") != null) {%>
                                                                            <input type="hidden" name="phaseNo" id="phaseNo" value="<%=request.getAttribute("phaseNo")%>" />
                                                                            <%}%>
                                                                            <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">School Details</p>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12">
                                                                                <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>District Name </th>
                                                                                            <td style="text-align: center;"><%=request.getAttribute("distname")%></td>
                                                                                            <th>Mandal Name </th>
                                                                                            <td style="text-align: center;"><%=request.getAttribute("mandalname")%></td>
                                                                                            <th>school Id </th>
                                                                                            <td style="text-align: center;" id="phno"><%=request.getAttribute("schoolCode")%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th>school Name </th>
                                                                                            <td style="text-align: center;"><%=request.getAttribute("schoolName")%></td>
                                                                                            <th>Phase Number </th>
                                                                                            <td style="text-align: center;"><%=request.getAttribute("phaseNo")%></td>
                                                                                            <th>Year </th>
                                                                                            <td style="text-align: center;"><%=request.getAttribute("year")%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th>Management</th>
                                                                                            <td style="text-align: center;border-right: 1px #000000 solid !important;"><%=request.getAttribute("mgtname")%></td>
                                                                                        </tr>
                                                                                    </thead>
                                                                                </table>
                                                                                <!--<p style="text-align: right; font-size: 15px; font-weight: bold;">School Code : <span style=" color: #ff4000; margin-right: 20px;"><%=request.getAttribute("schoolCode")%></span> School Name : <span style=" color: #ff4000; margin-right: 20px;"><%=request.getAttribute("schoolName")%></span></p>-->

                                                                            </div>

                                                                            <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Physical</p>
                                                                            </div>
                                                                            <table align="center" cellpadding="0" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                                                <thead>
                                                                                    <tr colspan="4">
                                                                                        <th></th>
                                                                                        <th>Sanction</th>
                                                                                        <th>Stage of Progress</th>
                                                                                        <th></th>
                                                                                        <th>Sanction</th>
                                                                                        <th>Stage of Progress</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr colspan="4">
                                                                                        <th>Art/Craft room</th>
                                                                                        <td><html:text property="artCraftSanction" style="width:80px;  text-area:center;" readonly="true" styleId="artCraftSanction"/></td>
                                                                                        <td style="display: none" id="showArt">  <html:select property="artCraftStageOfProgress"  styleId="artCraftStageOfProgress"  >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select>  </td>
                                                                                        <td style="display: none" id="showNa">NA</td>

                                                                                        <th style="width: 10px;text-decoration: nowrap" >Computer Room</th>
                                                                                        <td> <html:text property="computerRoomSanction" style="width:80px;  text-area:center;" readonly="true" styleId="computerRoomSanction"/></td>
                                                                                        <td style="display: none" id="showC">  <html:select property="computerRoomStageOfProgress"  styleId="computerRoomStageOfProgress" >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select>  </td>
                                                                                        <td style="display: none" id="showNaC">NA</td>

                                                                                    </tr>
                                                                                    <tr colspan="4">
                                                                                        <th>Science Lab</th>
                                                                                        <td> <html:text property="scienceLabSanction" style="width:80px;  text-area:center;" readonly="true" styleId="scienceLabSanction"/></td>
                                                                                        <td style="display: none" id="showSci"> <html:select property="scienceLabStageOfProgress"  styleId="scienceLabStageOfProgress" >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select>  </td>
                                                                                        <td style="display: none" id="showNaSci">NA</td>


                                                                                        <th>Library Room</th>
                                                                                        <td> <html:text property="libraryRoomSanction" style="width:80px;  text-area:center;" readonly="true" styleId="libraryRoomSanction"/></td>
                                                                                        <td style="display: none" id="showLib">  <html:select property="libraryRoomStageOfProgress"  styleId="libraryRoomStageOfProgress" >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select>  </td>
                                                                                        <td style="display: none" id="showNaLib">NA</td>

                                                                                    </tr>
                                                                                    <tr colspan="4">
                                                                                        <th style="width: 10px;text-">Additional Class Room</th>
                                                                                        <td> <html:text property="additionalClassRoomSanction" style="width:80px;  text-area:center;"  readonly="true" styleId="additionalClassRoomSanction"/></td>
                                                                                        <td style="display: none" id="showAD">  <html:select property="additionalClassRoomStageOfProgress"  styleId="additionalClassRoomStageOfProgress" >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select>  </td>
                                                                                        <td style="display: none" id="showNaAD">NA</td>


                                                                                        <th>Toilet Block</th>
                                                                                        <td> <html:text property="toiletBlockSanction" style="width:80px;  text-area:center;" readonly="true" styleId="toiletBlockSanction"/></td>
                                                                                        <td style="display: none" id="showToi">  <html:select property="toiletBlockStageOfProgress"  styleId="toiletBlockStageOfProgress" >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select>  </td>
                                                                                        <td style="display: none" id="showNaToi">NA</td>

                                                                                    </tr>
                                                                                    <tr colspan="4">
                                                                                        <th>Drinking Water</th>
                                                                                        <td> <html:text property="drinkingWaterSanction" style="width:80px;  text-area:center;" readonly="true" styleId="drinkingWaterSanction"/></td>
                                                                                        <td style="display: none" id="showDri"> <html:select property="drinkingWaterStageOfProgress"  styleId="drinkingWaterStageOfProgress" >
                                                                                                <html:option value="Select">--Select--</html:option>
                                                                                                <html:option value="Not Sanction">Not Sanction</html:option>
                                                                                                <html:option value="Site Problem">Site Problem</html:option>
                                                                                                <html:option value="Tender Stage" >Tender Stage</html:option>
                                                                                                <html:option value="Below Basement Level">Below Basement Level</html:option>
                                                                                                <html:option value="Basement Level">Basement Level</html:option>
                                                                                                <html:option value="Roof Level">Roof Level</html:option>
                                                                                                <html:option value="Roof Laid">Roof Laid</html:option>
                                                                                                <html:option value="Finishings">Finishings</html:option>
                                                                                                <html:option value="Completed">Completed</html:option>
                                                                                                <html:option value="Hand over">Hand Over</html:option>
                                                                                            </html:select></td>
                                                                                        <td style="display: none" id="showNaDri">NA</td>

                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Sanctioned Amount</p>
                                                                            </div>
                                                                            <table align="center" cellpadding="0" id="tableID1" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">

                                                                                <tr colspan="4">
                                                                                    <th style="text-align: center;">Sanctioned For Civil Cost</th>
                                                                                    <th style="text-align: center;">Sanctioned For Furniture</th>
                                                                                    <th style="text-align: center;">Sanctioned For LabEquipment</th>
                                                                                    <th style="text-align: center;">Total Sanctioned</th>
                                                                                </tr>
                                                                                <tr colspan="3">
                                                                                    <td style="text-align: center;"><html:text property="administrativeSanction" styleId="administrativeSanction" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="totalFurniture" styleId="totalFurniture" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="furnitureAndLabEquipment" styleId="furnitureAndLabEquipment" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="totalSanctioned" styleId="totalSanctioned" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                </tr>
                                                                            </table>
                                                                            <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Eligible Cost</p>
                                                                            </div>
                                                                            <table align="center" cellpadding="0" id="tableID2" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">

                                                                                <tr colspan="5">
                                                                                    <th style="text-align: center;">Estimated Cost</th>
                                                                                    <th style="text-align: center;">Tender</th>
                                                                                    <th style="text-align: center;">Vat</th>
                                                                                    <th style="text-align: center;">Dept.Charges</th>
                                                                                    <th style="text-align: center;">Total Eligible Cost</th>
                                                                                </tr>
                                                                                <tr colspan="5">
                                                                                    <td style="text-align: center;"><html:text property="estimatedCost" styleId="estimatedCost" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="tenderCostValue" styleId="tenderCostValue" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="vat" styleId="vat" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="departemntCharges" styleId="departemntCharges" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                    <td style="text-align: center;"><html:text property="totalEligibleCost" styleId="totalEligibleCost" style="width:170px;  text-area:center; font-weight: bold;" readonly="true"/></td>
                                                                                </tr>
                                                                            </table>
                                                                            <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">Releases And Expenditure</p>
                                                                            </div> 
                                                                            <table align="center" cellpadding="0" id="tableID3" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">

                                                                                <tr colspan="3">
                                                                                    <th style="text-align: center;">Total Released</th>
                                                                                    <th style="text-align: center;">Expenditure Incurred</th>
                                                                                    <th style="text-align: center;">Balance to be Released</th>

                                                                                </tr>
                                                                                <tr colspan="3">
                                                                                    <td style="text-align: center;" id="myBtn"  data-toggle="modal" data-target="#myModal"><u><html:text property="releases" styleId="releases" style="width:170px; text-area:center; font-weight: bold;text-decoration: underline;" readonly="true"/></u></td>
                                                                                <td style="text-align: center;"><html:text property="expenditureIncurred" styleId="expenditureIncurred" style="width:170px;" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Numbers');"/></td>
                                                                                <td style="text-align: center;"><html:text property="balanceToBeReleased" styleId="balanceToBeReleased" style="width:170px;  text-area:center;" readonly="true"/></td>
                                                                                </tr>
                                                                            </table>
                                                                            <div class="col-xs-12 col-sm-12"  style="padding: 0px;">
                                                                                <p style=" font-size: 16px; font-weight: bold; color: #ff4000; border-bottom: 3px dotted #ff4000; padding-bottom: 5px; margin: 15px 15px;">File Uploads</p>
                                                                            </div> 
                                                                            <table align="center" cellpadding="0" id="tableID4" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                                                <tr><th colspan="2" style="text-align: center"> UCS File Upload</th>
                                                                                    <th style="text-align: right"><input type="button" value="Add UCS Files To Upload" name="Add Row" style="width:200px;" onClick="addUCSRow('tableID4')" /></th>
                                                                                </tr>
                                                                                <%  double ucstotalAmount = 0;
                                                                                    if (request.getAttribute("filesList") != null) {
                                                                                        ArrayList filesList = (ArrayList) request.getAttribute("filesList");
                                                                                        if (filesList.size() > 0) {

                                                                                            for (int i = 0; i < filesList.size(); i++) {
                                                                                                HashMap filesMap = (HashMap) filesList.get(i);
                                                                                                String sno = filesMap.get("sno").toString();
                                                                                                String ucsAmount = filesMap.get("ucsAmount").toString();
                                                                                                ucstotalAmount = ucstotalAmount + Double.parseDouble(ucsAmount);
                                                                                                String filePath = filesMap.get("filePath").toString();
                                                                                                String schoolCode = filesMap.get("schoolCode").toString();
                                                                                                String ucsFileName = filesMap.get("ucsFileName").toString();
                                                                                                String fileName = "kpiinsert(ucsFileUpload" + i + ")";

                                                                                %>
                                                                                <tr><td><input type='text' name='UCSAmount' id=""  value="<%=ucsAmount%>" readonly style="width:170px;  text-area:center;"/></td>
                                                                                    <td><%=ucsFileName%> :<input type="button"  value="Download" onclick="downloadFile('<%=ucsFileName%>', '<%=schoolCode%>');"/></td>
                                                                                        <%--   <td><input type='Submit' value='Delete' onclick='deleteRow("tableID4", this), deleteFromDB(<%=schoolCode%>,<%=sno%>);'/></td> --%>
                                                                                    <td>
                                                                                        <input type='button' value='Delete' onclick='deleteUcsFile("tableID4", this,<%=schoolCode%>,<%=sno%>,<%=ucsAmount%>);'/>
                                                                                    </td> 
                                                                                </tr>
                                                                                <%}
                                                                                %>

                                                                                <%}
                                                                                    }%>
                                                                            </table><table align="center" cellpadding="0" id="tableID10" cellspacing="0"  width="97%" class="table table-bordered table-hover table-striped">
                                                                                <tr><td>Total UCS Amount : <input type="text"  name="ucstotalAmount" id="ucstotalAmount" value="<%=ucstotalAmount%>" style="border: none" readonly="true"/></td></tr> </table>                            

                                                                            <br>  
                                                                            <table align="center" cellpadding="0" id="tableID" cellspacing="0" border="0" width="97%" class="table table-bordered table-hover table-striped">
                                                                                <tr><th colspan="3" style="text-align: center"> Cheque File Upload</th>
                                                                                    <th  style="text-align: right">
                                                                                        <input type="button" value="Add Cheque Details To Upload" name="Add Row" style="width:200px;" onClick="addRow('tableID')" />
                                                                                    </th></tr> 
                                                                                <tr id="cheque" ><th  style="text-align: center">Cheque Number</th>
                                                                                    <th  style="text-align: center">Date(mm/dd/yyyy)</th>
                                                                                    <th  style="text-align: center">Cheque Amount in Lakhs </th>
                                                                                    <th  style="text-align: center"></th>
                                                                                </tr> 

                                                                                <% double totalchequeAmount = 0;
                                                                                    if (request.getAttribute("RMSAChequeDetails") != null) {
                                                                                        ArrayList list = (ArrayList) request.getAttribute("RMSAChequeDetails");
                                                                                        if (list.size() > 0) {
                                                                                            for (int i = 0; i < list.size(); i++) {
                                                                                                String ChequeNumber1 = null, chequeDate1 = null, chequeAmount1 = null;
                                                                                                HashMap map = (HashMap) list.get(i);
                                                                                                String sno = map.get("sno").toString();
                                                                                                String ChequeNumber = map.get("chequeNo").toString();
                                                                                                String schoolCode = map.get("schoolCode").toString();
                                                                                                String chequeDate = map.get("chequeDate").toString();
                                                                                                String chequeAmount = map.get("chequeAmount").toString();
                                                                                                totalchequeAmount = totalchequeAmount + Double.parseDouble(chequeAmount);
                                                                                                ChequeNumber1 = "ChequeNumber" + i;
                                                                                                chequeDate1 = "ChequeDate" + i;
                                                                                                chequeAmount1 = "ChequeAmount" + i;
                                                                                %>

                                                                                <tr id="cheque11">
                                                                                    <td><input type='text' name='ChequeNumber' id="<%=ChequeNumber1%>"  value="<%=ChequeNumber%>" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Numbers');" readonly style="width:170px;  text-area:center;"/></td>
                                                                                    <td><input type='date' name='ChequeDate' id="<%=chequeDate1%>" value="<%=chequeDate%>" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Numbers');" readonly style="width:170px;  text-area:center;"/></td>
                                                                                    <td><input type='text' name='ChequeAmount' id="<%=chequeAmount1%>"  value="<%=chequeAmount%>" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Numbers');" readonly style="width:170px;  text-area:center;"/></td>
                                                                                        <%--  <td><input type='Submit' value='Delete' onclick='deleteRow("tableID", this), deleteFromCheque(<%=schoolCode%>,<%=sno%>);'/></td> --%>
                                                                                    <td>
                                                                         <!--//            <input type='button' value='Delete' onclick='deleteChequeFile("tableID", this,<%=schoolCode%>,<%=sno%>,<%=ChequeNumber%>,<%=chequeAmount%>);'/>-->
                                                                                    </td> 
                                                                                </tr>
                                                                                <%}
                                                                                    }%>
                                                                                <%}%>
                                                                            </table><table align="center" cellpadding="0" id="tableID10" cellspacing="0"  width="97%" class="table table-bordered table-hover table-striped">
                                                                                <tr><td>Total Cheque Amount : <input type="text"  name="totalchequeAmount" id="totalchequeAmount" value="<%=totalchequeAmount%>" style="border: none" readonly="true"/></td></tr> </table>                            


                                                                            <br><br>
                                                                            <div class="col-xs-12" style="text-align:center">
                                                                                <%if (request.getAttribute("RMSAMaster") != null) {%>
                                                                                <center> <input type="button" value="Update" name="Update" onclick="submitForm();" /></center>
                                                                                    <%} else {%>  
                                                                                <center> <input type="button" value="Submit" name="Submit" onclick="submitForm();" /></center>
                                                                                    <%}%> 
                                                                            </div>

                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                                <div class="modal-dialog">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel">Total Release</h4>
                                                                                            <table align="center"  border="0" cellpadding="0" cellspacing="1" width="98%"  id="demo_datatables" class="table table-bordered table-hover table-striped">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>Sl.No</th>
                                                                                                        <th>School Code</th>
                                                                                                        <th>Release Amount</th>
                                                                                                        <th>Release By</th>
                                                                                                        <th>Release Date</th>

                                                                                                    </tr>
                                                                                                </thead> 
                                                                                                <logic:present name="releaseMasterList">
                                                                                                    <tbody>
                                                                                                        <logic:iterate id="list" name="releaseMasterList" >
                                                                                                            <tr>
                                                                                                                <td width="20px" style="text-align: left"><%=j++%></td>
                                                                                                                <td style="text-align: left">
                                                                                                                    ${list.schoolCode}
                                                                                                                </td> 
                                                                                                                <td style="text-align: left">
                                                                                                                    ${list.releaseAmount}
                                                                                                                </td>
                                                                                                                <td style="text-align: right">
                                                                                                                    ${list.createdBy}
                                                                                                                </td>
                                                                                                                <td style="text-align: right">
                                                                                                                    ${list.createdDate}
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </logic:iterate>
                                                                                                    </tbody>
                                                                                                    <tfoot>

                                                                                                        <tr>
                                                                                                            <th colspan="2" style="text-align: center">
                                                                                                                Total
                                                                                                            </th>
                                                                                                            <th style="text-align: left">
                                                                                                                <b> ${list.totalRelease} </b>
                                                                                                            </th>
                                                                                                            <th colspan="2" style="text-align: right"></th>
                                                                                                        </tr>
                                                                                                    </tfoot>
                                                                                                </logic:present>
                                                                                                <logic:notPresent name="releaseMasterList">
                                                                                                    <tbody>
                                                                                                    <th colspan="5" style="text-align: center;background-color: white;color: black">No data available</th></tbody>
                                                                                                    </logic:notPresent>
                                                                                            </table>

                                                                                        </div>
                                                                                        <!--            <div class="modal-footer">
                                                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                    </div>-->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        </logic:present>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </section>
                                                                    </html:form>                     
                                                                    </body>
                                                                    </html>