<%-- 
    Document   : mainLayout
    Created on : Jun 21, 2017, 6:00:50 PM
    Author     : 1259084
--%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld"  prefix="tiles"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: RMSA ::</title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <!-- Global CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Plugins CSS -->
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/flexslider.css">
        <link rel="stylesheet" href="css/prettyPhoto.css">

        <!-- Theme CSS -->
        <link  rel="stylesheet" href="css/styles.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
        <style type="text/css">
            table td a {
                margin-bottom: 10px;
            }
        </style>
        <script>
            function  getInsatnce(){
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
        </script>
    </head>
    <body class="home-page" >
    <html:form action="/Welcome" method="post" focus="loginRoles">
        <html:hidden property="mode"/>          
        <div style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; padding: 0px;" class="content container">
            <div style="float: left; width: 100%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" class="header-main">
    <h1 class="logo col-md-12 col-sm-12"> <a href="#"><img id="logo" src="images/logo.jpg" alt="Logo"></a> </h1>
    <!--//logo--> 
  </div>
            <div class="wrapper"> 
    <!-- ******HEADER****** -->
    
    <nav class="main-nav" role="navigation">
      <div class="">
        <div class="navbar-header">
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <!--//nav-toggle--> 
        </div>
        <!--//navbar-header-->
         <div class="navbar-collapse collapse" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active nav-item"><a href="<%=basePath%>">Home</a></li>
            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">SGFAP PROFILE <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="./about.do">VISION</a></li>
                <li><a href="./objective.do">OBJECTIVE</a></li>
                <li><a href="./affiliated_districts.do">AFFILIATED DISTRICTS OR UNITS</a></li>
                <li><a href="./decipline.do">RECOGNISED DISCIPLINES</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">RULES &amp; REGULATIONS <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="./sgfap.do">SGFAP RULES</a></li>
                <li><a href="./games.do">GAMES</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">RESULTS <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
<!--                <li><a href="#">STATE MEET</a></li>-->
                <li><a href="#">MERIT CERTIFICATES</a></li>
              </ul>
            </li>
            
<!--            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">CALENDER <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#">STATE MEET</a></li>
              </ul>
            </li>-->
            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">CALENDER <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#">63rd SGF A.P INTER DISTRICT </a></li>
              </ul>
            </li>
            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">STATISTICS <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="./medal_tally.do">MEDAL TALLY </a></li>
              </ul>
            </li>
            <li class="nav-item dropdown"> <a href="./contacts.do">Contact Us</a></li>
            <li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">LOGIN <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                  <li><a href="#" data-toggle="modal" data-target="#myModal" onclick="getInsatnce();">ONLINE ENTRY</a></li>
              </ul>
            </li>
            <!--<li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">About Organization <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#">Organization</a></li>
                <li><a href="#">Functionaries</a></li>
                <li><a href="#">Organization Chart</a></li>
                <li><a href="#">Cadre Strength</a></li>
              </ul>
            </li>--> 
            <!--<li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Administration <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#">AP Secretariat</a></li>
                <li><a href="#">Head Office</a></li>
                <li><a href="#">Circle Offices</a></li>
              </ul>
            </li>--> 
            <!--<li class="nav-item dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Budgets <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#">Programmes</a></li>
              </ul>
            </li>-->
            
          </ul>
          <!--//nav--> 
        </div>
        <!--//navabr-collapse--> 
      </div>
      <!--//container--> 
    </nav>
    <!--//main-nav-->
    <div class="border_header2"></div>
    
    <div style="min-height: 500px;">     <tiles:insert attribute="content" name="content"/></div>
</div>
  <!--//wrapper--> 
  
  
  <!-- ******FOOTER****** -->
  <footer class="footer"> 
    <!--//footer-content-->
    <div class="bottom-bar">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6" style="text-align:left"> <small class="copyright col-md-12 col-sm-12 col-xs-12" style="font-size:14px;">© 2017 School Game Federation</small></div>
          <div class="col-xs-12 col-sm-6" style="text-align:right">Design & Developed by <img src="images/aponline-logo.png"  /></div>
        </div>
        <!--//row--> 
      </div>
      <!--//container--> 
    </div>
    <!--//bottom-bar--> 
  </footer>
  <!--//footer--> 
        </div>
        <!--//content--> 
        <!-- *****CONFIGURE STYLE****** --> 
        <div class="modal fade" id="myModal" role="dialog" onblur="">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Login</h4>
                    </div>
                    <logic:present name="login">
                    <div>
                        <h5 style="color: red">${msg}</h5>
                    </div>
                    </logic:present>
                    <div class="modal-body">
                        <div class="col-xs-12 col-sm-12">
                            <label class="formlabel">UserName :</label>
                            <!--<input type="text" class="form-control textbox" placeholder="UserName" />-->
                            <input type="text" name="userName" class="form-control textbox" id="userName" onkeydown="return space(event, this);" autocomplete="off" onfocus="disableText();" style="border:1px solid #c7c7c7; padding:3px">
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label class="formlabel">Password</label>
                            <!--<input type="password" class="form-control textbox" placeholder="Password" />-->
                             <input type="password" name="password" class="form-control textbox" id="password" onkeydown="space(event, this);" autocomplete="off" style="border:1px solid #c7c7c7; padding:3px">
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <label class="formlabel">Enter Captcha :</label>
                            <logic:present name="captchaCode">
                            <font style="color: #f00; font-weight: bold;"><bean:write name="captchaCode"/></font>
                            </logic:present>
                            <input type="text" name="txtInput" class="form-control textbox" id="txtInput" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" autocomplete="off" onfocus="clearText();" style="border:1px solid #c7c7c7; padding:3px">
                            <!--<input type="text" class="form-control textbox" placeholder="UserName" />-->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="return validateUser();" class="btn btn-primary">Submit</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Javascript --> 
         
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script> 
        <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script> 
        <script type="text/javascript" src="js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="js/bootstrap-hover-dropdown.min.js"></script> 
        <script type="text/javascript" src="js/back-to-top.js"></script> 
        <script type="text/javascript" src="js/jquery.placeholder.js"></script> 
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script> 
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script> 
        <script type="text/javascript" src="js/jflickrfeed.min.js"></script> 
        <script type="text/javascript" src="js/main.js"></script> 
         <script src="js/md5.js" type="text/javascript"></script>
         <script type="text/javascript">
            function validateUser() {
                alert("000=="+document.getElementById('userName').value);
                if (document.getElementById('userName').value == "" || document.getElementById('userName').value == "") {
                    alert("Please Enter Username");
                    document.getElementById('userName').focus();
                    return false;
                } else if (document.getElementById('password').value == "" || document.getElementById('password').value == "P a s s w o r d. .") {
                    alert("Please Enter Password");
                    document.getElementById('password').focus();
                    return false;
                } else if (document.getElementById('txtInput').value == ""
                        || document.getElementById('txtInput').value == "Enter A b o v e  C o d e. .") {
                    alert("Please Enter Captcha Code");
                    document.getElementById('txtInput').focus();
                    return false;
                } else {
                    with (document.forms[0]) {
                        
                        password.value = calcMD5(document.getElementById('password').value);
                        password.value = document.getElementById('password').value;
                        document.forms[0].mode.value = "checkDetails";
//                        document.forms[0].action = "Welcome.do?mode=checkDetails";
                        document.forms[0].submit();
                    }
                }
            }

            // Validate the Entered input aganist the generated security code function
            function ValidCaptcha() {
                var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
                var str2 = removeSpaces(document.getElementById('txtInput').value);
                if (str1 == str2) {
                    return true;
                } else {
                    return false;
                }
            }
            // Remove the spaces from the entered and generated code
            function removeSpaces(string) {
                return string.split(' ').join('');
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }

            var xmlHTTP = false;
            var panchayatRequest;
            function ajaxFunction() {
                try {
                    panchayatRequest = new XMLHttpRequest();

                } catch (e) {
                    // Internet Explorer Browsers
                    try {
                        panchayatRequest = new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (e) {
                        try {
                            panchayatRequest = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {
                            alert("Your browser broke!");
                            return false;
                        }
                    }
                }
            }

            function getHash(pwd)
            {
                ajaxFunction();
                logFlag(pwd);
                panchayatRequest.onreadystatechange = parseMessage;
                var url = "officialLogin.do?mode=getEncryptPwd&encString=" + pwd + "&user=" + document.forms[0].elements["userName"].value + "";
                panchayatRequest.open("GET", url, true)
                panchayatRequest.send(null);
            }

            function parseMessage() {
                if (!(panchayatRequest.readyState == 4 || panchayatRequest.readyState == "complete")) {
                    var encString = panchayatRequest.responseText;
                    if (encString != null) {
                        document.forms[0].password.value = encString;
                    }
                }
            }
        </script>
        <script>
            $('#carousel - 900202').carousel({
                interval: 2000
            });
            $('.carousel').carousel({
                interval: 3000
            });
        </script>
       </html:form>
    </body>
</html>
